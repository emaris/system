--
-- PostgreSQL database dump
--

-- Dumped from database version 14.4
-- Dumped by pg_dump version 15.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: fusionauth; Type: DATABASE; Schema: -; Owner: apprise
--

CREATE DATABASE fusionauth WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.UTF-8';


ALTER DATABASE fusionauth OWNER TO apprise;

\connect fusionauth

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: apprise
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO apprise;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: application_daily_active_users; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.application_daily_active_users (
    applications_id uuid NOT NULL,
    count integer NOT NULL,
    day integer NOT NULL
);


ALTER TABLE public.application_daily_active_users OWNER TO apprise;

--
-- Name: application_monthly_active_users; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.application_monthly_active_users (
    applications_id uuid NOT NULL,
    count integer NOT NULL,
    month integer NOT NULL
);


ALTER TABLE public.application_monthly_active_users OWNER TO apprise;

--
-- Name: application_registration_counts; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.application_registration_counts (
    applications_id uuid NOT NULL,
    count integer NOT NULL,
    decremented_count integer NOT NULL,
    hour integer NOT NULL
);


ALTER TABLE public.application_registration_counts OWNER TO apprise;

--
-- Name: application_roles; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.application_roles (
    id uuid NOT NULL,
    applications_id uuid NOT NULL,
    description character varying(255),
    insert_instant bigint NOT NULL,
    is_default boolean NOT NULL,
    is_super_role boolean NOT NULL,
    last_update_instant bigint NOT NULL,
    name character varying(191) NOT NULL
);


ALTER TABLE public.application_roles OWNER TO apprise;

--
-- Name: applications; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.applications (
    id uuid NOT NULL,
    access_token_populate_lambdas_id uuid,
    access_token_signing_keys_id uuid,
    active boolean NOT NULL,
    admin_registration_forms_id uuid NOT NULL,
    data text NOT NULL,
    email_update_email_templates_id uuid,
    email_verification_email_templates_id uuid,
    email_verified_email_templates_id uuid,
    forgot_password_email_templates_id uuid,
    forms_id uuid,
    id_token_populate_lambdas_id uuid,
    id_token_signing_keys_id uuid,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    login_id_in_use_on_create_email_templates_id uuid,
    login_id_in_use_on_update_email_templates_id uuid,
    login_new_device_email_templates_id uuid,
    login_suspicious_email_templates_id uuid,
    multi_factor_email_message_templates_id uuid,
    multi_factor_sms_message_templates_id uuid,
    name character varying(191) NOT NULL,
    passwordless_email_templates_id uuid,
    password_update_email_templates_id uuid,
    password_reset_success_email_templates_id uuid,
    samlv2_default_verification_keys_id uuid,
    samlv2_issuer character varying(191),
    samlv2_keys_id uuid,
    samlv2_logout_keys_id uuid,
    samlv2_logout_default_verification_keys_id uuid,
    samlv2_populate_lambdas_id uuid,
    samlv2_single_logout_keys_id uuid,
    self_service_user_forms_id uuid,
    set_password_email_templates_id uuid,
    tenants_id uuid NOT NULL,
    themes_id uuid,
    two_factor_method_add_email_templates_id uuid,
    two_factor_method_remove_email_templates_id uuid,
    ui_ip_access_control_lists_id uuid,
    verification_email_templates_id uuid
);


ALTER TABLE public.applications OWNER TO apprise;

--
-- Name: asynchronous_tasks; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.asynchronous_tasks (
    id uuid NOT NULL,
    data text,
    entity_id uuid,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    nodes_id uuid,
    status smallint NOT NULL,
    type smallint NOT NULL
);


ALTER TABLE public.asynchronous_tasks OWNER TO apprise;

--
-- Name: audit_logs; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.audit_logs (
    id bigint NOT NULL,
    insert_instant bigint NOT NULL,
    insert_user character varying(255) NOT NULL,
    message text NOT NULL,
    data text
);


ALTER TABLE public.audit_logs OWNER TO apprise;

--
-- Name: audit_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: apprise
--

CREATE SEQUENCE public.audit_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.audit_logs_id_seq OWNER TO apprise;

--
-- Name: audit_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: apprise
--

ALTER SEQUENCE public.audit_logs_id_seq OWNED BY public.audit_logs.id;


--
-- Name: authentication_keys; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.authentication_keys (
    id uuid NOT NULL,
    insert_instant bigint NOT NULL,
    ip_access_control_lists_id uuid,
    last_update_instant bigint NOT NULL,
    key_manager boolean NOT NULL,
    key_value character varying(191) NOT NULL,
    permissions text,
    meta_data text,
    tenants_id uuid
);


ALTER TABLE public.authentication_keys OWNER TO apprise;

--
-- Name: breached_password_metrics; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.breached_password_metrics (
    tenants_id uuid NOT NULL,
    matched_exact_count integer NOT NULL,
    matched_sub_address_count integer NOT NULL,
    matched_common_password_count integer NOT NULL,
    matched_password_count integer NOT NULL,
    passwords_checked_count integer NOT NULL
);


ALTER TABLE public.breached_password_metrics OWNER TO apprise;

--
-- Name: clean_speak_applications; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.clean_speak_applications (
    applications_id uuid NOT NULL,
    clean_speak_application_id uuid NOT NULL
);


ALTER TABLE public.clean_speak_applications OWNER TO apprise;

--
-- Name: common_breached_passwords; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.common_breached_passwords (
    password character varying(191) NOT NULL
);


ALTER TABLE public.common_breached_passwords OWNER TO apprise;

--
-- Name: connectors; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.connectors (
    id uuid NOT NULL,
    data text NOT NULL,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    name character varying(191) NOT NULL,
    reconcile_lambdas_id uuid,
    ssl_certificate_keys_id uuid,
    type smallint NOT NULL
);


ALTER TABLE public.connectors OWNER TO apprise;

--
-- Name: connectors_tenants; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.connectors_tenants (
    connectors_id uuid NOT NULL,
    data text NOT NULL,
    sequence smallint NOT NULL,
    tenants_id uuid NOT NULL
);


ALTER TABLE public.connectors_tenants OWNER TO apprise;

--
-- Name: consents; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.consents (
    id uuid NOT NULL,
    consent_email_templates_id uuid,
    data text,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    name character varying(191) NOT NULL,
    email_plus_email_templates_id uuid
);


ALTER TABLE public.consents OWNER TO apprise;

--
-- Name: data_sets; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.data_sets (
    name character varying(191) NOT NULL,
    last_update_instant bigint NOT NULL
);


ALTER TABLE public.data_sets OWNER TO apprise;

--
-- Name: email_templates; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.email_templates (
    id uuid NOT NULL,
    default_from_name character varying(255),
    default_html_template text NOT NULL,
    default_subject character varying(255) NOT NULL,
    default_text_template text NOT NULL,
    from_email character varying(255),
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    localized_from_names text,
    localized_html_templates text NOT NULL,
    localized_subjects text NOT NULL,
    localized_text_templates text NOT NULL,
    name character varying(191) NOT NULL
);


ALTER TABLE public.email_templates OWNER TO apprise;

--
-- Name: entities; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.entities (
    id uuid NOT NULL,
    client_id character varying(191) NOT NULL,
    client_secret character varying(255) NOT NULL,
    data text,
    entity_types_id uuid NOT NULL,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    name character varying(255) NOT NULL,
    parent_id uuid,
    tenants_id uuid NOT NULL
);


ALTER TABLE public.entities OWNER TO apprise;

--
-- Name: entity_entity_grant_permissions; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.entity_entity_grant_permissions (
    entity_entity_grants_id uuid NOT NULL,
    entity_type_permissions_id uuid NOT NULL
);


ALTER TABLE public.entity_entity_grant_permissions OWNER TO apprise;

--
-- Name: entity_entity_grants; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.entity_entity_grants (
    id uuid NOT NULL,
    data text,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    recipient_id uuid NOT NULL,
    target_id uuid NOT NULL
);


ALTER TABLE public.entity_entity_grants OWNER TO apprise;

--
-- Name: entity_type_permissions; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.entity_type_permissions (
    id uuid NOT NULL,
    data text,
    description text,
    entity_types_id uuid NOT NULL,
    insert_instant bigint NOT NULL,
    is_default boolean NOT NULL,
    last_update_instant bigint NOT NULL,
    name character varying(191) NOT NULL
);


ALTER TABLE public.entity_type_permissions OWNER TO apprise;

--
-- Name: entity_types; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.entity_types (
    id uuid NOT NULL,
    access_token_signing_keys_id uuid,
    data text,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    name character varying(191) NOT NULL
);


ALTER TABLE public.entity_types OWNER TO apprise;

--
-- Name: entity_user_grant_permissions; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.entity_user_grant_permissions (
    entity_user_grants_id uuid NOT NULL,
    entity_type_permissions_id uuid NOT NULL
);


ALTER TABLE public.entity_user_grant_permissions OWNER TO apprise;

--
-- Name: entity_user_grants; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.entity_user_grants (
    id uuid NOT NULL,
    data text,
    entities_id uuid NOT NULL,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    users_id uuid NOT NULL
);


ALTER TABLE public.entity_user_grants OWNER TO apprise;

--
-- Name: event_logs; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.event_logs (
    id bigint NOT NULL,
    insert_instant bigint NOT NULL,
    message text NOT NULL,
    type smallint NOT NULL
);


ALTER TABLE public.event_logs OWNER TO apprise;

--
-- Name: event_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: apprise
--

CREATE SEQUENCE public.event_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_logs_id_seq OWNER TO apprise;

--
-- Name: event_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: apprise
--

ALTER SEQUENCE public.event_logs_id_seq OWNED BY public.event_logs.id;


--
-- Name: external_identifiers; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.external_identifiers (
    id character varying(191) NOT NULL,
    applications_id uuid,
    data text,
    expiration_instant bigint,
    insert_instant bigint NOT NULL,
    tenants_id uuid NOT NULL,
    type smallint NOT NULL,
    users_id uuid
);


ALTER TABLE public.external_identifiers OWNER TO apprise;

--
-- Name: families; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.families (
    data text,
    family_id uuid NOT NULL,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    owner boolean NOT NULL,
    role smallint NOT NULL,
    users_id uuid NOT NULL
);


ALTER TABLE public.families OWNER TO apprise;

--
-- Name: federated_domains; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.federated_domains (
    identity_providers_id uuid NOT NULL,
    domain character varying(191) NOT NULL
);


ALTER TABLE public.federated_domains OWNER TO apprise;

--
-- Name: form_fields; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.form_fields (
    id uuid NOT NULL,
    consents_id uuid,
    data text,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    name character varying(191) NOT NULL
);


ALTER TABLE public.form_fields OWNER TO apprise;

--
-- Name: form_steps; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.form_steps (
    form_fields_id uuid NOT NULL,
    forms_id uuid NOT NULL,
    sequence smallint NOT NULL,
    step smallint NOT NULL
);


ALTER TABLE public.form_steps OWNER TO apprise;

--
-- Name: forms; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.forms (
    id uuid NOT NULL,
    data text,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    name character varying(191) NOT NULL,
    type smallint NOT NULL
);


ALTER TABLE public.forms OWNER TO apprise;

--
-- Name: global_daily_active_users; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.global_daily_active_users (
    count integer NOT NULL,
    day integer NOT NULL
);


ALTER TABLE public.global_daily_active_users OWNER TO apprise;

--
-- Name: global_monthly_active_users; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.global_monthly_active_users (
    count integer NOT NULL,
    month integer NOT NULL
);


ALTER TABLE public.global_monthly_active_users OWNER TO apprise;

--
-- Name: global_registration_counts; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.global_registration_counts (
    count integer NOT NULL,
    decremented_count integer NOT NULL,
    hour integer NOT NULL
);


ALTER TABLE public.global_registration_counts OWNER TO apprise;

--
-- Name: group_application_roles; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.group_application_roles (
    application_roles_id uuid NOT NULL,
    groups_id uuid NOT NULL
);


ALTER TABLE public.group_application_roles OWNER TO apprise;

--
-- Name: group_members; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.group_members (
    id uuid NOT NULL,
    groups_id uuid NOT NULL,
    data text,
    insert_instant bigint NOT NULL,
    users_id uuid NOT NULL
);


ALTER TABLE public.group_members OWNER TO apprise;

--
-- Name: groups; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.groups (
    id uuid NOT NULL,
    data text,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    name character varying(191) NOT NULL,
    tenants_id uuid NOT NULL
);


ALTER TABLE public.groups OWNER TO apprise;

--
-- Name: hourly_logins; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.hourly_logins (
    applications_id uuid NOT NULL,
    count integer NOT NULL,
    data text,
    hour integer NOT NULL
);


ALTER TABLE public.hourly_logins OWNER TO apprise;

--
-- Name: identities; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.identities (
    id bigint NOT NULL,
    breached_password_last_checked_instant bigint,
    breached_password_status smallint,
    connectors_id uuid NOT NULL,
    email character varying(191),
    encryption_scheme character varying(255) NOT NULL,
    factor integer NOT NULL,
    insert_instant bigint NOT NULL,
    last_login_instant bigint,
    last_update_instant bigint NOT NULL,
    password character varying(255) NOT NULL,
    password_change_reason smallint,
    password_change_required boolean NOT NULL,
    password_last_update_instant bigint NOT NULL,
    salt character varying(255) NOT NULL,
    status smallint NOT NULL,
    tenants_id uuid NOT NULL,
    username character varying(191),
    username_index character varying(191),
    username_status smallint NOT NULL,
    users_id uuid NOT NULL,
    verified boolean NOT NULL
);


ALTER TABLE public.identities OWNER TO apprise;

--
-- Name: identities_id_seq; Type: SEQUENCE; Schema: public; Owner: apprise
--

CREATE SEQUENCE public.identities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.identities_id_seq OWNER TO apprise;

--
-- Name: identities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: apprise
--

ALTER SEQUENCE public.identities_id_seq OWNED BY public.identities.id;


--
-- Name: identity_provider_links; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.identity_provider_links (
    data text NOT NULL,
    identity_providers_id uuid NOT NULL,
    identity_providers_user_id character varying(191) NOT NULL,
    insert_instant bigint NOT NULL,
    last_login_instant bigint NOT NULL,
    tenants_id uuid NOT NULL,
    users_id uuid NOT NULL
);


ALTER TABLE public.identity_provider_links OWNER TO apprise;

--
-- Name: identity_providers; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.identity_providers (
    id uuid NOT NULL,
    data text NOT NULL,
    enabled boolean NOT NULL,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    name character varying(191) NOT NULL,
    type character varying(255) NOT NULL,
    keys_id uuid,
    request_signing_keys_id uuid,
    reconcile_lambdas_id uuid
);


ALTER TABLE public.identity_providers OWNER TO apprise;

--
-- Name: identity_providers_applications; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.identity_providers_applications (
    applications_id uuid NOT NULL,
    data text NOT NULL,
    enabled boolean NOT NULL,
    identity_providers_id uuid NOT NULL,
    keys_id uuid
);


ALTER TABLE public.identity_providers_applications OWNER TO apprise;

--
-- Name: identity_providers_tenants; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.identity_providers_tenants (
    tenants_id uuid NOT NULL,
    data text NOT NULL,
    identity_providers_id uuid NOT NULL
);


ALTER TABLE public.identity_providers_tenants OWNER TO apprise;

--
-- Name: instance; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.instance (
    id uuid NOT NULL,
    activate_instant bigint,
    license text,
    license_id character varying(255),
    setup_complete boolean NOT NULL
);


ALTER TABLE public.instance OWNER TO apprise;

--
-- Name: integrations; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.integrations (
    data text NOT NULL
);


ALTER TABLE public.integrations OWNER TO apprise;

--
-- Name: ip_access_control_lists; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.ip_access_control_lists (
    id uuid NOT NULL,
    data text NOT NULL,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    name character varying(191) NOT NULL
);


ALTER TABLE public.ip_access_control_lists OWNER TO apprise;

--
-- Name: ip_location_database; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.ip_location_database (
    data bytea,
    last_modified bigint NOT NULL
);


ALTER TABLE public.ip_location_database OWNER TO apprise;

--
-- Name: keys; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.keys (
    id uuid NOT NULL,
    algorithm character varying(10),
    certificate text,
    expiration_instant bigint,
    insert_instant bigint NOT NULL,
    issuer character varying(255),
    kid character varying(191) NOT NULL,
    last_update_instant bigint NOT NULL,
    name character varying(191) NOT NULL,
    private_key text,
    public_key text,
    secret text,
    type character varying(10) NOT NULL
);


ALTER TABLE public.keys OWNER TO apprise;

--
-- Name: kickstart_files; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.kickstart_files (
    id uuid NOT NULL,
    kickstart bytea NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.kickstart_files OWNER TO apprise;

--
-- Name: lambdas; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.lambdas (
    id uuid NOT NULL,
    body text NOT NULL,
    debug boolean NOT NULL,
    enabled boolean NOT NULL,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    name character varying(255) NOT NULL,
    type smallint NOT NULL
);


ALTER TABLE public.lambdas OWNER TO apprise;

--
-- Name: last_login_instants; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.last_login_instants (
    applications_id uuid,
    registration_last_login_instant bigint,
    users_id uuid,
    user_last_login_instant bigint
);


ALTER TABLE public.last_login_instants OWNER TO apprise;

--
-- Name: locks; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.locks (
    type character varying(191) NOT NULL,
    update_instant bigint
);


ALTER TABLE public.locks OWNER TO apprise;

--
-- Name: master_record; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.master_record (
    id uuid NOT NULL,
    instant bigint NOT NULL
);


ALTER TABLE public.master_record OWNER TO apprise;

--
-- Name: message_templates; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.message_templates (
    id uuid NOT NULL,
    data text NOT NULL,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    name character varying(191) NOT NULL,
    type smallint NOT NULL
);


ALTER TABLE public.message_templates OWNER TO apprise;

--
-- Name: messengers; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.messengers (
    id uuid NOT NULL,
    data text NOT NULL,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    name character varying(191) NOT NULL,
    type smallint NOT NULL
);


ALTER TABLE public.messengers OWNER TO apprise;

--
-- Name: migrations; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.migrations (
    name character varying(191) NOT NULL,
    run_instant bigint NOT NULL
);


ALTER TABLE public.migrations OWNER TO apprise;

--
-- Name: nodes; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.nodes (
    id uuid NOT NULL,
    data text NOT NULL,
    insert_instant bigint NOT NULL,
    last_checkin_instant bigint NOT NULL,
    runtime_mode character varying(255) NOT NULL,
    url character varying(255) NOT NULL
);


ALTER TABLE public.nodes OWNER TO apprise;

--
-- Name: previous_passwords; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.previous_passwords (
    insert_instant bigint NOT NULL,
    encryption_scheme character varying(255) NOT NULL,
    factor integer NOT NULL,
    password character varying(255) NOT NULL,
    salt character varying(255) NOT NULL,
    users_id uuid NOT NULL
);


ALTER TABLE public.previous_passwords OWNER TO apprise;

--
-- Name: raw_application_daily_active_users; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.raw_application_daily_active_users (
    applications_id uuid NOT NULL,
    day integer NOT NULL,
    users_id uuid NOT NULL
);


ALTER TABLE public.raw_application_daily_active_users OWNER TO apprise;

--
-- Name: raw_application_monthly_active_users; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.raw_application_monthly_active_users (
    applications_id uuid NOT NULL,
    month integer NOT NULL,
    users_id uuid NOT NULL
);


ALTER TABLE public.raw_application_monthly_active_users OWNER TO apprise;

--
-- Name: raw_application_registration_counts; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.raw_application_registration_counts (
    id bigint NOT NULL,
    applications_id uuid NOT NULL,
    count integer NOT NULL,
    decremented_count integer NOT NULL,
    hour integer NOT NULL
);


ALTER TABLE public.raw_application_registration_counts OWNER TO apprise;

--
-- Name: raw_application_registration_counts_id_seq; Type: SEQUENCE; Schema: public; Owner: apprise
--

CREATE SEQUENCE public.raw_application_registration_counts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.raw_application_registration_counts_id_seq OWNER TO apprise;

--
-- Name: raw_application_registration_counts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: apprise
--

ALTER SEQUENCE public.raw_application_registration_counts_id_seq OWNED BY public.raw_application_registration_counts.id;


--
-- Name: raw_global_daily_active_users; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.raw_global_daily_active_users (
    day integer NOT NULL,
    users_id uuid NOT NULL
);


ALTER TABLE public.raw_global_daily_active_users OWNER TO apprise;

--
-- Name: raw_global_monthly_active_users; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.raw_global_monthly_active_users (
    month integer NOT NULL,
    users_id uuid NOT NULL
);


ALTER TABLE public.raw_global_monthly_active_users OWNER TO apprise;

--
-- Name: raw_global_registration_counts; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.raw_global_registration_counts (
    id bigint NOT NULL,
    count integer NOT NULL,
    decremented_count integer NOT NULL,
    hour integer NOT NULL
);


ALTER TABLE public.raw_global_registration_counts OWNER TO apprise;

--
-- Name: raw_global_registration_counts_id_seq; Type: SEQUENCE; Schema: public; Owner: apprise
--

CREATE SEQUENCE public.raw_global_registration_counts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.raw_global_registration_counts_id_seq OWNER TO apprise;

--
-- Name: raw_global_registration_counts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: apprise
--

ALTER SEQUENCE public.raw_global_registration_counts_id_seq OWNED BY public.raw_global_registration_counts.id;


--
-- Name: raw_logins; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.raw_logins (
    applications_id uuid,
    instant bigint NOT NULL,
    ip_address character varying(255),
    users_id uuid NOT NULL
);


ALTER TABLE public.raw_logins OWNER TO apprise;

--
-- Name: refresh_tokens; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.refresh_tokens (
    id uuid NOT NULL,
    applications_id uuid,
    data text NOT NULL,
    insert_instant bigint NOT NULL,
    start_instant bigint NOT NULL,
    tenants_id uuid,
    token character varying(191) NOT NULL,
    users_id uuid NOT NULL
);


ALTER TABLE public.refresh_tokens OWNER TO apprise;

--
-- Name: request_frequencies; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.request_frequencies (
    count integer NOT NULL,
    last_update_instant bigint NOT NULL,
    request_id character varying(191) NOT NULL,
    tenants_id uuid NOT NULL,
    type smallint NOT NULL
);


ALTER TABLE public.request_frequencies OWNER TO apprise;

--
-- Name: system_configuration; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.system_configuration (
    data text NOT NULL,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    report_timezone character varying(255) NOT NULL
);


ALTER TABLE public.system_configuration OWNER TO apprise;

--
-- Name: tenants; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.tenants (
    id uuid NOT NULL,
    access_token_signing_keys_id uuid NOT NULL,
    admin_user_forms_id uuid NOT NULL,
    client_credentials_access_token_populate_lambdas_id uuid,
    confirm_child_email_templates_id uuid,
    data text,
    email_update_email_templates_id uuid,
    email_verified_email_templates_id uuid,
    failed_authentication_user_actions_id uuid,
    family_request_email_templates_id uuid,
    forgot_password_email_templates_id uuid,
    id_token_signing_keys_id uuid NOT NULL,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    login_id_in_use_on_create_email_templates_id uuid,
    login_id_in_use_on_update_email_templates_id uuid,
    login_new_device_email_templates_id uuid,
    login_suspicious_email_templates_id uuid,
    multi_factor_email_message_templates_id uuid,
    multi_factor_sms_message_templates_id uuid,
    multi_factor_sms_messengers_id uuid,
    name character varying(191) NOT NULL,
    password_reset_success_email_templates_id uuid,
    password_update_email_templates_id uuid,
    parent_registration_email_templates_id uuid,
    passwordless_email_templates_id uuid,
    set_password_email_templates_id uuid,
    themes_id uuid NOT NULL,
    two_factor_method_add_email_templates_id uuid,
    two_factor_method_remove_email_templates_id uuid,
    ui_ip_access_control_lists_id uuid,
    verification_email_templates_id uuid
);


ALTER TABLE public.tenants OWNER TO apprise;

--
-- Name: themes; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.themes (
    id uuid NOT NULL,
    data text NOT NULL,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    name character varying(191) NOT NULL
);


ALTER TABLE public.themes OWNER TO apprise;

--
-- Name: user_action_logs; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.user_action_logs (
    id uuid NOT NULL,
    actioner_users_id uuid,
    actionee_users_id uuid NOT NULL,
    comment text,
    email_user_on_end boolean NOT NULL,
    end_event_sent boolean,
    expiry bigint,
    history text,
    insert_instant bigint NOT NULL,
    localized_name character varying(191),
    localized_option character varying(255),
    localized_reason character varying(255),
    name character varying(191),
    notify_user_on_end boolean NOT NULL,
    option_name character varying(255),
    reason character varying(255),
    reason_code character varying(255),
    user_actions_id uuid NOT NULL
);


ALTER TABLE public.user_action_logs OWNER TO apprise;

--
-- Name: user_action_logs_applications; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.user_action_logs_applications (
    applications_id uuid NOT NULL,
    user_action_logs_id uuid NOT NULL
);


ALTER TABLE public.user_action_logs_applications OWNER TO apprise;

--
-- Name: user_action_reasons; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.user_action_reasons (
    id uuid NOT NULL,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    localized_texts text,
    text character varying(191) NOT NULL,
    code character varying(191) NOT NULL
);


ALTER TABLE public.user_action_reasons OWNER TO apprise;

--
-- Name: user_actions; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.user_actions (
    id uuid NOT NULL,
    active boolean NOT NULL,
    cancel_email_templates_id uuid,
    end_email_templates_id uuid,
    include_email_in_event_json boolean NOT NULL,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    localized_names text,
    modify_email_templates_id uuid,
    name character varying(191) NOT NULL,
    options text,
    prevent_login boolean NOT NULL,
    send_end_event boolean NOT NULL,
    start_email_templates_id uuid,
    temporal boolean NOT NULL,
    transaction_type smallint NOT NULL,
    user_notifications_enabled boolean NOT NULL,
    user_emailing_enabled boolean NOT NULL
);


ALTER TABLE public.user_actions OWNER TO apprise;

--
-- Name: user_comments; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.user_comments (
    id uuid NOT NULL,
    comment text,
    commenter_id uuid NOT NULL,
    insert_instant bigint NOT NULL,
    users_id uuid NOT NULL
);


ALTER TABLE public.user_comments OWNER TO apprise;

--
-- Name: user_consents; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.user_consents (
    id uuid NOT NULL,
    consents_id uuid NOT NULL,
    data text,
    giver_users_id uuid NOT NULL,
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    users_id uuid NOT NULL
);


ALTER TABLE public.user_consents OWNER TO apprise;

--
-- Name: user_consents_email_plus; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.user_consents_email_plus (
    id bigint NOT NULL,
    next_email_instant bigint NOT NULL,
    user_consents_id uuid NOT NULL
);


ALTER TABLE public.user_consents_email_plus OWNER TO apprise;

--
-- Name: user_consents_email_plus_id_seq; Type: SEQUENCE; Schema: public; Owner: apprise
--

CREATE SEQUENCE public.user_consents_email_plus_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_consents_email_plus_id_seq OWNER TO apprise;

--
-- Name: user_consents_email_plus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: apprise
--

ALTER SEQUENCE public.user_consents_email_plus_id_seq OWNED BY public.user_consents_email_plus.id;


--
-- Name: user_registrations; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.user_registrations (
    id uuid NOT NULL,
    applications_id uuid NOT NULL,
    authentication_token character varying(255),
    clean_speak_id uuid,
    data text,
    insert_instant bigint NOT NULL,
    last_login_instant bigint,
    last_update_instant bigint NOT NULL,
    timezone character varying(255),
    username character varying(191),
    username_status smallint NOT NULL,
    users_id uuid NOT NULL,
    verified boolean NOT NULL
);


ALTER TABLE public.user_registrations OWNER TO apprise;

--
-- Name: user_registrations_application_roles; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.user_registrations_application_roles (
    application_roles_id uuid NOT NULL,
    user_registrations_id uuid NOT NULL
);


ALTER TABLE public.user_registrations_application_roles OWNER TO apprise;

--
-- Name: users; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.users (
    id uuid NOT NULL,
    active boolean NOT NULL,
    birth_date character(10),
    clean_speak_id uuid,
    data text,
    expiry bigint,
    first_name character varying(255),
    full_name character varying(255),
    image_url text,
    insert_instant bigint NOT NULL,
    last_name character varying(255),
    last_update_instant bigint NOT NULL,
    middle_name character varying(255),
    mobile_phone character varying(255),
    parent_email character varying(255),
    tenants_id uuid NOT NULL,
    timezone character varying(255)
);


ALTER TABLE public.users OWNER TO apprise;

--
-- Name: version; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.version (
    version character varying(255) NOT NULL
);


ALTER TABLE public.version OWNER TO apprise;

--
-- Name: webhooks; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.webhooks (
    id uuid NOT NULL,
    connect_timeout integer NOT NULL,
    description character varying(255),
    data text,
    global boolean NOT NULL,
    headers text,
    http_authentication_username character varying(255),
    http_authentication_password character varying(255),
    insert_instant bigint NOT NULL,
    last_update_instant bigint NOT NULL,
    read_timeout integer NOT NULL,
    ssl_certificate text,
    url text NOT NULL
);


ALTER TABLE public.webhooks OWNER TO apprise;

--
-- Name: webhooks_applications; Type: TABLE; Schema: public; Owner: apprise
--

CREATE TABLE public.webhooks_applications (
    webhooks_id uuid NOT NULL,
    applications_id uuid NOT NULL
);


ALTER TABLE public.webhooks_applications OWNER TO apprise;

--
-- Name: audit_logs id; Type: DEFAULT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.audit_logs ALTER COLUMN id SET DEFAULT nextval('public.audit_logs_id_seq'::regclass);


--
-- Name: event_logs id; Type: DEFAULT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.event_logs ALTER COLUMN id SET DEFAULT nextval('public.event_logs_id_seq'::regclass);


--
-- Name: identities id; Type: DEFAULT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identities ALTER COLUMN id SET DEFAULT nextval('public.identities_id_seq'::regclass);


--
-- Name: raw_application_registration_counts id; Type: DEFAULT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.raw_application_registration_counts ALTER COLUMN id SET DEFAULT nextval('public.raw_application_registration_counts_id_seq'::regclass);


--
-- Name: raw_global_registration_counts id; Type: DEFAULT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.raw_global_registration_counts ALTER COLUMN id SET DEFAULT nextval('public.raw_global_registration_counts_id_seq'::regclass);


--
-- Name: user_consents_email_plus id; Type: DEFAULT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_consents_email_plus ALTER COLUMN id SET DEFAULT nextval('public.user_consents_email_plus_id_seq'::regclass);


--
-- Data for Name: application_daily_active_users; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.application_daily_active_users (applications_id, count, day) FROM stdin;
3c219e58-ed0e-4b18-ad48-f4f92793ae32	1	19605
0979b36e-8a48-4c4b-a0ba-1eb846cd50ff	1	19605
\.


--
-- Data for Name: application_monthly_active_users; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.application_monthly_active_users (applications_id, count, month) FROM stdin;
3c219e58-ed0e-4b18-ad48-f4f92793ae32	1	644
0979b36e-8a48-4c4b-a0ba-1eb846cd50ff	1	644
\.


--
-- Data for Name: application_registration_counts; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.application_registration_counts (applications_id, count, decremented_count, hour) FROM stdin;
\.


--
-- Data for Name: application_roles; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.application_roles (id, applications_id, description, insert_instant, is_default, is_super_role, last_update_instant, name) FROM stdin;
631ecd9d-8d40-4c13-8277-80cedb8236e2	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Global admin	1666539852785	f	t	1666539852785	admin
631ecd9d-8d40-4c13-8277-80cedb8236e3	3c219e58-ed0e-4b18-ad48-f4f92793ae32	API key manager	1666539852785	f	f	1666539852785	api_key_manager
631ecd9d-8d40-4c13-8277-80cedb8236e4	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Application deleter	1666539852785	f	f	1666539852785	application_deleter
631ecd9d-8d40-4c13-8277-80cedb8236e5	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Application manager	1666539852785	f	f	1666539852785	application_manager
631ecd9d-8d40-4c13-8277-80cedb8236e6	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Audit log viewer	1666539852785	f	f	1666539852785	audit_log_viewer
631ecd9d-8d40-4c13-8277-80cedb8236e7	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Email template manager	1666539852785	f	f	1666539852785	email_template_manager
631ecd9d-8d40-4c13-8277-80cedb8236e8	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Report viewer	1666539852785	f	f	1666539852785	report_viewer
631ecd9d-8d40-4c13-8277-80cedb8236e9	3c219e58-ed0e-4b18-ad48-f4f92793ae32	System configuration manager	1666539852785	f	f	1666539852785	system_manager
631ecd9d-8d40-4c13-8277-80cedb8236f0	3c219e58-ed0e-4b18-ad48-f4f92793ae32	User action deleter	1666539852785	f	f	1666539852785	user_action_deleter
631ecd9d-8d40-4c13-8277-80cedb8236f1	3c219e58-ed0e-4b18-ad48-f4f92793ae32	User action manager	1666539852785	f	f	1666539852785	user_action_manager
631ecd9d-8d40-4c13-8277-80cedb8236f2	3c219e58-ed0e-4b18-ad48-f4f92793ae32	User deleter	1666539852785	f	f	1666539852785	user_deleter
631ecd9d-8d40-4c13-8277-80cedb8236f3	3c219e58-ed0e-4b18-ad48-f4f92793ae32	User manager	1666539852785	f	f	1666539852785	user_manager
631ecd9d-8d40-4c13-8277-80cedb8236f4	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Webhook manager	1666539852785	f	f	1666539852785	webhook_manager
631ecd9d-8d40-4c13-8277-80cedb8236f5	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Group manager	1666539852785	f	f	1666539852785	group_manager
631ecd9d-8d40-4c13-8277-80cedb8236f6	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Group deleter	1666539852785	f	f	1666539852785	group_deleter
631ecd9d-8d40-4c13-8277-80cedb8236f7	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Tenant manager	1666539852785	f	f	1666539852785	tenant_manager
631ecd9d-8d40-4c13-8277-80cedb8236f8	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Tenant deleter	1666539852785	f	f	1666539852785	tenant_deleter
631ecd9d-8d40-4c13-8277-80cedb8236f9	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Lambda manager	1666539852785	f	f	1666539852785	lambda_manager
631ecd9d-8d40-4c13-8277-80cedb8236fa	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Event log viewer	1666539852785	f	f	1666539852785	event_log_viewer
631ecd9d-8d40-4c13-8277-80cedb8236fb	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Key manager	1666539852785	f	f	1666539852785	key_manager
631ecd9d-8d40-4c13-8277-80cedb8236fc	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Consent deleter	1666539852785	f	f	1666539852785	consent_deleter
631ecd9d-8d40-4c13-8277-80cedb8236fd	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Consent manager	1666539852785	f	f	1666539852785	consent_manager
631ecd9d-8d40-4c13-8277-80cedb8236fe	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Theme manager	1666539852785	f	f	1666539852785	theme_manager
631ecd9d-8d40-4c13-8277-80cedb8236ff	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Reactor manager	1666539852785	f	f	1666539852785	reactor_manager
631ecd9d-8d40-4c13-8277-80cedb823700	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Connector deleter	1666539852785	f	f	1666539852785	connector_deleter
631ecd9d-8d40-4c13-8277-80cedb823701	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Connector manager	1666539852785	f	f	1666539852785	connector_manager
631ecd9d-8d40-4c13-8277-80cedb823702	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Form deleter	1666539852785	f	f	1666539852785	form_deleter
631ecd9d-8d40-4c13-8277-80cedb823703	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Form manager	1666539852785	f	f	1666539852785	form_manager
631ecd9d-8d40-4c13-8277-80cedb823704	3c219e58-ed0e-4b18-ad48-f4f92793ae32	User support manager	1666539852785	f	f	1666539852785	user_support_manager
631ecd9d-8d40-4c13-8277-80cedb823705	3c219e58-ed0e-4b18-ad48-f4f92793ae32	User support viewer	1666539852785	f	f	1666539852785	user_support_viewer
631ecd9d-8d40-4c13-8277-80cedb823706	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Entity manager	1666539852785	f	f	1666539852785	entity_manager
631ecd9d-8d40-4c13-8277-80cedb823707	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Messenger deleter	1666539852785	f	f	1666539852785	messenger_deleter
631ecd9d-8d40-4c13-8277-80cedb823708	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Messenger manager	1666539852785	f	f	1666539852785	messenger_manager
631ecd9d-8d40-4c13-8277-80cedb823709	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Message template deleter	1666539852785	f	f	1666539852785	message_template_deleter
631ecd9d-8d40-4c13-8277-80cedb823710	3c219e58-ed0e-4b18-ad48-f4f92793ae32	Message template manager	1666539852785	f	f	1666539852785	message_template_manager
631ecd9d-8d40-4c13-8277-80cedb823711	3c219e58-ed0e-4b18-ad48-f4f92793ae32	ACL deleter	1666539852785	f	f	1666539852785	acl_deleter
631ecd9d-8d40-4c13-8277-80cedb823712	3c219e58-ed0e-4b18-ad48-f4f92793ae32	ACL manager	1666539852785	f	f	1666539852785	acl_manager
\.


--
-- Data for Name: applications; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.applications (id, access_token_populate_lambdas_id, access_token_signing_keys_id, active, admin_registration_forms_id, data, email_update_email_templates_id, email_verification_email_templates_id, email_verified_email_templates_id, forgot_password_email_templates_id, forms_id, id_token_populate_lambdas_id, id_token_signing_keys_id, insert_instant, last_update_instant, login_id_in_use_on_create_email_templates_id, login_id_in_use_on_update_email_templates_id, login_new_device_email_templates_id, login_suspicious_email_templates_id, multi_factor_email_message_templates_id, multi_factor_sms_message_templates_id, name, passwordless_email_templates_id, password_update_email_templates_id, password_reset_success_email_templates_id, samlv2_default_verification_keys_id, samlv2_issuer, samlv2_keys_id, samlv2_logout_keys_id, samlv2_logout_default_verification_keys_id, samlv2_populate_lambdas_id, samlv2_single_logout_keys_id, self_service_user_forms_id, set_password_email_templates_id, tenants_id, themes_id, two_factor_method_add_email_templates_id, two_factor_method_remove_email_templates_id, ui_ip_access_control_lists_id, verification_email_templates_id) FROM stdin;
3c219e58-ed0e-4b18-ad48-f4f92793ae32	\N	42ac0869-83a6-af28-8dd2-e9f5f84383e4	t	368c226b-f1a4-d32e-346e-bbde23a0aba7	{"jwtConfiguration": {"enabled": true, "timeToLiveInSeconds": 60, "refreshTokenExpirationPolicy": "SlidingWindow", "refreshTokenTimeToLiveInMinutes": 60, "refreshTokenUsagePolicy": "Reusable"},"registrationConfiguration": {"type":"basic"}, "oauthConfiguration": {"authorizedRedirectURLs": ["/admin/login"], "clientId": "3c219e58-ed0e-4b18-ad48-f4f92793ae32", "clientSecret": "N2VjZjZkMzc3Yzk2MzBhMDk2Nzc2N2NjOWIyZTM5NTYwMDAyMjg5OGMxOWZmNTRlYzYxNTQ4OTY3NjE1ZDQwYw==", "enabledGrants": ["authorization_code"], "logoutURL": "/admin/", "generateRefreshTokens": true, "clientAuthenticationPolicy": "Required", "proofKeyForCodeExchangePolicy": "NotRequired" },"loginConfiguration": {"allowTokenRefresh": false, "generateRefreshTokens": false, "requireAuthentication": true},"unverified":{ "behavior": "Allow" },"verificationStrategy":"ClickableLink","state": "Active"}	\N	\N	\N	\N	\N	\N	092dbedc-30af-4149-9c61-b578f2c72f59	1666539852785	1666539852785	\N	\N	\N	\N	\N	\N	FusionAuth	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	e4ed14d6-3706-3908-2179-9e3b74617677	\N	\N	\N	\N	\N
0979b36e-8a48-4c4b-a0ba-1eb846cd50ff	\N	42ac0869-83a6-af28-8dd2-e9f5f84383e4	t	368c226b-f1a4-d32e-346e-bbde23a0aba7	{"accessControlConfiguration":{},"authenticationTokenConfiguration":{"enabled":false},"data":{},"formConfiguration":{"adminRegistrationFormId":"368c226b-f1a4-d32e-346e-bbde23a0aba7"},"jwtConfiguration":{"enabled":true,"refreshTokenExpirationPolicy":"SlidingWindow","refreshTokenTimeToLiveInMinutes":43200,"refreshTokenUsagePolicy":"Reusable","timeToLiveInSeconds":3600},"loginConfiguration":{"allowTokenRefresh":false,"generateRefreshTokens":false,"requireAuthentication":true},"multiFactorConfiguration":{"email":{},"sms":{}},"oauthConfiguration":{"authorizedRedirectURLs":["https://localhost:8443/oauth2/cblogin"],"clientAuthenticationPolicy":"Required","clientId":"0979b36e-8a48-4c4b-a0ba-1eb846cd50ff","clientSecret":"zYJK2r934aMDen6GPkNXfGi4C_2c6gXhREoPzqVNrAY","debug":false,"enabledGrants":["refresh_token","authorization_code"],"generateRefreshTokens":true,"logoutBehavior":"RedirectOnly","logoutURL":"https://localhost:8443/oauth2/cblogout","proofKeyForCodeExchangePolicy":"NotRequired","requireClientAuthentication":true,"requireRegistration":false},"passwordlessConfiguration":{"enabled":false},"registrationConfiguration":{"birthDate":{"enabled":false,"required":false},"confirmPassword":false,"enabled":false,"firstName":{"enabled":false,"required":false},"fullName":{"enabled":false,"required":false},"lastName":{"enabled":false,"required":false},"loginIdType":"email","middleName":{"enabled":false,"required":false},"mobilePhone":{"enabled":false,"required":false},"type":"basic"},"registrationDeletePolicy":{"unverified":{"enabled":false,"numberOfDaysToRetain":120}},"samlv2Configuration":{"debug":false,"enabled":false,"logout":{"behavior":"AllParticipants","requireSignedRequests":false,"singleLogout":{"enabled":false,"xmlSignatureC14nMethod":"exclusive_with_comments"},"xmlSignatureC14nMethod":"exclusive_with_comments"},"requireSignedRequests":false,"xmlSignatureC14nMethod":"exclusive_with_comments","xmlSignatureLocation":"Assertion"},"state":"Active","unverified":{"behavior":"Allow"},"verificationStrategy":"ClickableLink","verifyRegistration":false}	\N	\N	\N	\N	\N	\N	42ac0869-83a6-af28-8dd2-e9f5f84383e4	1666540057424	1693901337753	\N	\N	\N	\N	\N	\N	emaris	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	e4ed14d6-3706-3908-2179-9e3b74617677	\N	\N	\N	\N	\N
\.


--
-- Data for Name: asynchronous_tasks; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.asynchronous_tasks (id, data, entity_id, insert_instant, last_update_instant, nodes_id, status, type) FROM stdin;
\.


--
-- Data for Name: audit_logs; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.audit_logs (id, insert_instant, insert_user, message, data) FROM stdin;
\.


--
-- Data for Name: authentication_keys; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.authentication_keys (id, insert_instant, ip_access_control_lists_id, last_update_instant, key_manager, key_value, permissions, meta_data, tenants_id) FROM stdin;
0d1f22fb-9bf1-9d90-9e8c-1bc972461dd8	1666539852785	\N	1666539852785	f	__internal_OGFkYjg1ODBhZjVlOTJhZTA1M2VmOTQ5ZTFlZjRlZWJmYmQxZjk0OGMxOWJiYTdlYTljZDQ1YWFlZjAwN2NiYQ==	{"endpoints": {"/api/cache/reload": ["POST"], "/api/system/log/export": ["POST"]}}	{"attributes": {"description": "Internal Use Only. [DistributedCacheNotifier][DistributedLogDownloader]", "internalCacheReloader": "true", "internalLogDownloader": "true"}}	\N
f3900952-39e5-4a28-89c8-4883f3bb053b	1666540694879	\N	1693924627183	f	oMAtFIwuucv0SthSR4sFYpAF6Ws8HVTncA7nWd_tZDJh-94SW1nLnaal	\N	{"attributes":{"description":"app-key"}}	\N
\.


--
-- Data for Name: breached_password_metrics; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.breached_password_metrics (tenants_id, matched_exact_count, matched_sub_address_count, matched_common_password_count, matched_password_count, passwords_checked_count) FROM stdin;
\.


--
-- Data for Name: clean_speak_applications; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.clean_speak_applications (applications_id, clean_speak_application_id) FROM stdin;
\.


--
-- Data for Name: common_breached_passwords; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.common_breached_passwords (password) FROM stdin;
\.


--
-- Data for Name: connectors; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.connectors (id, data, insert_instant, last_update_instant, name, reconcile_lambdas_id, ssl_certificate_keys_id, type) FROM stdin;
e3306678-a53a-4964-9040-1c96f36dda72	{}	1666539852785	1666539852785	Default	\N	\N	0
\.


--
-- Data for Name: connectors_tenants; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.connectors_tenants (connectors_id, data, sequence, tenants_id) FROM stdin;
e3306678-a53a-4964-9040-1c96f36dda72	{"data":{},"domains":["*"],"migrate":false}	0	e4ed14d6-3706-3908-2179-9e3b74617677
\.


--
-- Data for Name: consents; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.consents (id, consent_email_templates_id, data, insert_instant, last_update_instant, name, email_plus_email_templates_id) FROM stdin;
d34dcb91-ac7b-4406-83e4-63a6fc0f992f	\N	{"countryMinimumAgeForSelfConsent":{},"data":{},"defaultMinimumAgeForSelfConsent":13,"emailPlus":{"enabled":true,"maximumTimeToSendEmailInHours":48,"minimumTimeToSendEmailInHours":24},"multipleValuesAllowed":false,"values":[]}	1666539967308	1666539967308	COPPA Email+	\N
c6aaad40-ba01-4376-b092-90c91935af3f	\N	{"countryMinimumAgeForSelfConsent":{},"data":{},"defaultMinimumAgeForSelfConsent":13,"emailPlus":{"enabled":false,"maximumTimeToSendEmailInHours":48,"minimumTimeToSendEmailInHours":24},"multipleValuesAllowed":false,"values":[]}	1666539967310	1666539967310	COPPA VPC	\N
\.


--
-- Data for Name: data_sets; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.data_sets (name, last_update_instant) FROM stdin;
BreachPasswords	1581476456155
\.


--
-- Data for Name: email_templates; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.email_templates (id, default_from_name, default_html_template, default_subject, default_text_template, from_email, insert_instant, last_update_instant, localized_from_names, localized_html_templates, localized_subjects, localized_text_templates, name) FROM stdin;
ce3402e2-8597-4045-ae39-77a7c744be1b	\N	[#-- @ftlvariable name="event" type="io.fusionauth.domain.event.UserLoginSuspiciousEvent" --]\n[#setting url_escaping_charset="UTF-8"]\n[#if event.type == "UserLoginSuspicious"]\n  <p>A suspicious login was made on your account. If this was you, you can safely ignore this email. If this wasn't you, we recommend that you change your password as soon as possible.</p>\n[#elseif event.type == "UserLoginNewDevice"]\n  <p>A login from a new device was detected on your account. If this was you, you can safely ignore this email. If this wasn't you, we recommend that you change your password as soon as possible.</p>\n[#else]\n  <p>Suspicious activity has been observed on your account. In order to secure your account, it is recommended to change your password at your earliest convenience.</p>\n[/#if]\n\n<p>Device details</p>\n<ul>\n  <li><strong>Device name:</strong> ${(event.info.deviceName)!'&mdash;'}</li>\n  <li><strong>Device description:</strong> ${(event.info.deviceDescription)!'&mdash;'}</li>\n  <li><strong>Device type:</strong> ${(event.info.deviceType)!'&mdash;'}</li>\n  <li><strong>User agent:</strong> ${(event.info.userAgent)!'&mdash;'}</li>\n</ul>\n\n<p>Event details</p>\n<ul>\n  <li><strong>IP address:</strong> ${(event.info.ipAddress)!'&mdash;'}</li>\n  <li><strong>City:</strong> ${(event.info.location.city)!'&mdash;'}</li>\n  <li><strong>Country:</strong> ${(event.info.location.country)!'&mdash;'}</li>\n  <li><strong>Zipcode:</strong> ${(event.info.location.zipcode)!'&mdash;'}</li>\n  <li><strong>Lat/long:</strong> ${(event.info.location.latitude)!'&mdash;'}/${(event.info.location.longitude)!'&mdash;'}</li>\n</ul>\n\n- FusionAuth Admin\n	Threat Detected	[#setting url_escaping_charset="UTF-8"]\n[#if event.type == "UserLoginSuspicious"]\nA suspicious login was made on your account. If this was you, you can safely ignore this email. If this wasn't you, we recommend that you change your password as soon as possible.\n[#elseif event.type == "UserLoginNewDevice"]\nA login from a new device was detected on your account. If this was you, you can safely ignore this email. If this wasn't you, we recommend that you change your password as soon as possible.\n[#else]\nSuspicious activity has been observed on your account. In order to secure your account, it is recommended to change your password at your earliest convenience.\n[/#if]\n\nDevice details\n\n* Device name: ${(event.info.deviceName)!'&mdash;'}\n* Device description: ${(event.info.deviceDescription)!'&mdash;'}\n* Device type: ${(event.info.deviceType)!'&mdash;'}\n* User agent: ${(event.info.userAgent)!'&mdash;'}\n\nEvent details\n\n* IP address: ${(event.info.ipAddress)!'-'}\n* City: ${(event.info.location.city)!'-'}\n* Country: ${(event.info.location.country)!'-'}\n* Zipcode: ${(event.info.location.zipcode)!'-'}\n* Lat/long: ${(event.info.location.latitude)!'-'}/${(event.info.location.longitude)!'-'}\n\n- FusionAuth Admin\n	\N	1666539967254	1666539967254	{}	{}	{}	{}	[FusionAuth Default] Threat Detected
99aa0ce2-c543-42eb-ae91-ccf797a72d2b	\N	Your child has created an account with us and you need to confirm them before they are added to your family. Click the link below to confirm your child's account.\n<p>\n  <a href="http://example.com/family/confirm-child">http://example.com/family/confirm-child</a>\n</p>\n- FusionAuth Admin	Confirm your child's account	Your child has created an account with us and you need to confirm them before they are added to your family. Click the link below to confirm your child's account.\n\nhttp://example.com/family/confirm-child\n\n- FusionAuth Admin	\N	1666539967259	1666539967259	{}	{}	{}	{}	[FusionAuth Default] Confirm Child Account
e71b9d88-59be-4b6f-9b67-31aabe7dc9f6	\N	A while ago, you granted your child consent in our system. This email is a second notice of this consent as required by law and also to remind to that you can revoke this consent at anytime on our website or by clicking the link below:\n<p>\n  <a href="http://example.com/consent/manage">http://example.com/consent/manage</a>\n</p>\n- FusionAuth Admin	Reminder: Notice of your consent	A while ago, you granted your child consent in our system. This email is a second notice of this consent as required by law and also to remind to that you can revoke this consent at anytime on our website or by clicking the link below:\n\nhttp://example.com/consent/manage\n\n- FusionAuth Admin	\N	1666539967259	1666539967259	{}	{}	{}	{}	[FusionAuth Default] COPPA Notice Reminder
73f34ac9-f272-4d3d-89e1-f297368d5e9d	\N	You recently granted your child consent in our system. This email is to notify you of this consent. If you did not grant this consent or wish to revoke this consent, click the link below:\n<p>\n  <a href="http://example.com/consent/manage">http://example.com/consent/manage</a>\n</p>\n- FusionAuth Admin	Notice of your consent	You recently granted your child consent in our system. This email is to notify you of this consent. If you did not grant this consent or wish to revoke this consent, click the link below:\n\nhttp://example.com/consent/manage\n\n- FusionAuth Admin	\N	1666539967261	1666539967261	{}	{}	{}	{}	[FusionAuth Default] COPPA Notice
6358834c-b2c8-4b00-b227-6ca05982661c	\N	[#if user.verified]\nPro tip, your email has already been verified, but feel free to complete the verification process to verify your verification of your email address.\n[/#if]\n\n[#-- When a one-time code is provided, you will want the user to enter this value interactively using a form. In this workflow the verificationId\n     is not shown to the user and instead the one-time code must be paired with the verificationId which is usually in a hidden form field. When the two\n     values are presented together, the email address can be verified --]\n[#if verificationOneTimeCode??]\n<p>To complete your email verification enter this code into the email verification form.</p>\n<p> ${verificationOneTimeCode} </p>\n[#else]\nTo complete your email verification click on the following link.\n<p>\n  <a href="http://localhost:9011/email/verify/${verificationId}?client_id=${(application.oauthConfiguration.clientId)!''}&postMethod=true&tenantId=${user.tenantId}">\n    http://localhost:9011/email/verify/${verificationId}?client_id=${(application.oauthConfiguration.clientId)!''}&postMethod=true&tenantId=${user.tenantId}\n  </a>\n</p>\n[/#if]\n\n- FusionAuth Admin	Verify your FusionAuth email address	[#if user.verified]\nPro tip, your email has already been verified, but feel free to complete the verification process to verify your verification of your email address.\n[/#if]\n\n[#-- When a one-time code is provided, you will want the user to enter this value interactively using a form. In this workflow the verificationId\n     is not shown to the user and instead the one-time code must be paired with the verificationId which is usually in a hidden form field. When the two\n     values are presented together, the email address can be verified --]\n[#if verificationOneTimeCode??]\nTo complete your email verification enter this code into the email verification form.\n\n${verificationOneTimeCode}\n[#else]\nTo complete your email verification click on the following link.\n\nhttp://localhost:9011/email/verify/${verificationId}?client_id=${(application.oauthConfiguration.clientId)!''}&postMethod=true&tenantId=${user.tenantId}\n[/#if]\n\n- FusionAuth Admin	\N	1666539967262	1666539967262	{}	{}	{}	{}	[FusionAuth Default] Email Verification
af16e542-8dd9-4853-831c-56008f3f51ed	\N	Your child has created an account with us and needs you to create an account and verify them. You can sign up using the link below:\n<p>\n  <a href="http://example.com/family/confirm-child">http://example.com/family/confirm-child</a>\n</p>\n- FusionAuth Admin	Create your parental account	Your child has created an account with us and needs you to create an account and verify them. You can sign up using the link below:\n\nhttp://example.com/family/confirm-child\n\n- FusionAuth Admin	\N	1666539967263	1666539967263	{}	{}	{}	{}	[FusionAuth Default] Parent Registration
64076463-c170-43f2-95e9-11524faa4ebd	\N	[#setting url_escaping_charset="UTF-8"]\nYou have requested to log into FusionAuth using this email address. If you do not recognize this request please ignore this email.\n<p>\n  [#-- The optional 'state' map provided on the Start Passwordless API call is exposed in the template as 'state' --]\n  [#assign url = "http://localhost:9011/oauth2/passwordless/${code}?postMethod=true&tenantId=${user.tenantId}" /]\n  [#list state!{} as key, value][#if key != "tenantId" && value??][#assign url = url + "&" + key?url + "=" + value?url/][/#if][/#list]\n  <a href="${url}">${url}</a>\n</p>\n- FusionAuth Admin\n	Log into FusionAuth	[#setting url_escaping_charset="UTF-8"]\nYou have requested to log into FusionAuth using this email address. If you do not recognize this request please ignore this email.\n\n[#-- The optional 'state' map provided on the Start Passwordless API call is exposed in the template as 'state' --]\n[#assign url = "http://localhost:9011/oauth2/passwordless/${code}?postMethod=true&tenantId=${user.tenantId}" /]\n[#list state!{} as key, value][#if key != "tenantId" && value??][#assign url = url + "&" + key?url + "=" + value?url/][/#if][/#list]\n\n${url}\n\n- FusionAuth Admin\n	\N	1666539967263	1666539967263	{}	{}	{}	{}	[FusionAuth Default] Passwordless Login
62da841b-f249-475a-87b4-b0bae8c7bfe7	\N	[#if registration.verified]\nPro tip, your registration has already been verified, but feel free to complete the verification process to verify your verification of your registration.\n[/#if]\n\n[#-- When a one-time code is provided, you will want the user to enter this value interactively using a form. In this workflow the verificationId\n     is not shown to the user and instead the one-time code must be paired with the verificationId which is usually in a hidden form field. When the two\n     values are presented together, the registration can be verified --]\n[#if verificationOneTimeCode??]\n<p>To complete your registration verification enter this code into the registration verification form.</p>\n<p> ${verificationOneTimeCode} </p>\n[#else]\nTo complete your registration verification click on the following link.\n<p>\n  <a href="http://localhost:9011/registration/verify/${verificationId}?client_id=${(application.oauthConfiguration.clientId)!''}&postMethod=true&tenantId=${user.tenantId}">\n    http://localhost:9011/registration/verify/${verificationId}?client_id=${(application.oauthConfiguration.clientId)!''}&postMethod=true&tenantId=${user.tenantId}\n  </a>\n</p>\n[/#if]\n\n- FusionAuth Admin	Verify your registration	[#if registration.verified]\nPro tip, your registration has already been verified, but feel free to complete the verification process to verify your verification of your registration.\n[/#if]\n\n[#-- When a one-time code is provided, you will want the user to enter this value interactively using a form. In this workflow the verificationId\n     is not shown to the user and instead the one-time code must be paired with the verificationId which is usually in a hidden form field. When the two\n     values are presented together, the registration can be verified --]\n[#if verificationOneTimeCode??]\nTo complete your registration verification enter this code into the registration verification form.\n\n${verificationOneTimeCode}\n[#else]\nTo complete your registration verification click on the following link.\n\nhttp://localhost:9011/registration/verify/${verificationId}?client_id=${(application.oauthConfiguration.clientId)!''}&postMethod=true&tenantId=${user.tenantId}\n[/#if]\n\n- FusionAuth Admin	\N	1666539967264	1666539967264	{}	{}	{}	{}	[FusionAuth Default] Registration Verification
d858ba9e-9148-4a9e-ad80-5a64bdccf99d	\N	<p>\n  To complete your login request, enter this one-time code code on the login form when prompted.\n</p>\n<p>\n  <strong>${code}</strong>\n</p>\n\n- FusionAuth Admin	Your second factor code	To complete your login request, enter this one-time code code on the login form when prompted.\n\n${code}\n\n- FusionAuth Admin	\N	1666539967264	1666539967264	{}	{}	{}	{}	[FusionAuth Default] Two Factor Authentication
0d2d87e8-d865-425c-af44-64fbaaa4339e	\N	<html>\r\n  <head>\r\n    <meta name="color-scheme" content="light">\r\n    <style type="text/css">\r\n        * {\r\n            text-align: center;\r\n            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'PingFang SC', 'Ubuntu', 'Hiragino Sans GB', 'Microsoft YaHei', 'Helvetica Neue', Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol' !important;\r\n        }\r\n        body {\r\n            height: 100% !important;\r\n            margin: 0px;\r\n            box-sizing: border-box;\r\n            color: darkslategray !important;\r\n            background: rgb(46, 69, 82) !important;\r\n        }\r\n        .logo {\r\n            margin: 10px;\r\n        }\r\n        .logo img {\r\n            margin: 0 auto;\r\n            display: block;\r\n        }\r\n        .logo-text {\r\n            font-size: smaller !important;\r\n            color: lightblue !important;\r\n        }\r\n        .hf {\r\n            padding: 15px;\r\n            background: rgb(46, 69, 82) !important;\r\n        }\r\n        .hf .footer {\r\n            padding: 12px 0;\r\n            white-space: nowrap !important;\r\n        }\r\n        .hf .footer div {\r\n            margin: 3px;\r\n        }\r\n        .asset {\r\n            font-weight: 200 !important;\r\n        }\r\n        .hf-content {\r\n            font-size: 12px !important;\r\n            white-space: pre-line;\r\n            color: lightslategray !important;\r\n        }\r\n        .main {\r\n            text-align: center;\r\n            font-size: 32px !important;\r\n            margin-bottom: 0px;\r\n            font-weight: 200 !important;\r\n            white-space:pre-wrap;\r\n            color: darkslategray !important;\r\n        }\r\n        .explainer {\r\n            text-align: center;\r\n            font-size: 20px !important;\r\n            font-weight: 200 !important;\r\n            margin-top: 8px;\r\n            color: lightslategrey !important;\r\n            white-space:pre-wrap;\r\n        }\r\n        .context {\r\n            margin-bottom: -10px;\r\n        }\r\n        .context-campaign {\r\n            font-size: 16px !important;\r\n            color: lightseagreen !important;\r\n        }\r\n        .main {\r\n            margin-top: 40px;\r\n        }\r\n        .header {\r\n            height: 42px;\r\n        }\r\n        .context-pill {\r\n            display: inline-block;\r\n            font-size: 16px !important;\r\n            text-transform: lowercase !important;\r\n            font-variant: small-caps !important;\r\n            margin-top:10px;\r\n            padding: 3px 8px;\r\n            background: rgb(234, 238, 237);\r\n            border-radius: 6px;\r\n            color: darkslategray !important;\r\n        }\r\n        .context-campaign .context-tenant {\r\n            margin-top:8px;\r\n        }\r\n        .blurb {\r\n            margin-top:20px;\r\n            padding: 50px 100px;\r\n            font-weight: 300 !important;\r\n            text-align: center;\r\n            white-space: pre-wrap;\r\n            color: darkslategray !important;\r\n        }\r\n        a {\r\n            text-decoration: none;\r\n            color: inherit;\r\n            color: darkcyan !important;\r\n        }\r\n        .emphasis {\r\n            font-weight: 300 !important;\r\n        }\r\n        .action {\r\n            margin-top: 50px;\r\n            padding: 10px 15px;\r\n            border-radius: 4px;\r\n            color: white !important;\r\n            background: lightseagreen !important;\r\n            display: inline-block;\r\n        }\r\n        .action:hover {\r\n            background: #1ca8a1 !important;\r\n            cursor: pointer;\r\n        }\r\n    </style>\r\n  </head>\r\n  <body>\r\n      \r\n  [#assign domain = "localhost" /]\r\n  [#assign auth_base_url = "https://${domain}:9000" /]\r\n  [#assign app_base_url = "http://${domain}:3000" /]\r\n  [#assign url = "${auth_base_url}/password/change/${changePasswordId}?tenantId=${user.tenantId}" /]\r\n    \r\n    <div class="hf">\r\n      <div class="logo">\r\n        <img width="120" src="${app_base_url}/mc/images/mclogo.png">\r\n      </div>\r\n    </div>\r\n    <div style="padding: 40px 20px !important;  background: white !important; min-height: 500px !important; ">\r\n        <div class="main">Hi ${user.firstName}, your account requires a new password.</div>\r\n        <div class="explainer">Please reset it to access the system again.</div>\r\n        <a class="action" href="${url}">\r\n            Reset Password Now\r\n        </a>\r\n    </div>    \r\n    <div class="hf" style="padding:30px 0px">\r\n      <div class="hf-content footer">\r\n            <div>This is an automated email, please do not reply directly to it.</div>\r\n      </div>\r\n    </div>\r\n  </body>\r\n</html>	Reset your password	[#setting url_escaping_charset="UTF-8"]\r\nHi ${user.firstName}, your account requires a new password.\r\n\r\nClick on the following link to reset it and access the system again:\r\n\r\n  [#assign domain = "localhost" /]\r\n  [#assign auth_base_url = "https://${domain}:9000" /]\r\n  [#assign app_base_url = "http://${domain}:3000" /]\r\n  [#assign url = "${auth_base_url}/password/change/${changePasswordId}?tenantId=${user.tenantId}" /]\r\n\r\n  [#assign url = "${auth_base_url}/password/change/${changePasswordId}?tenantId=${user.tenantId}" /]\r\n  [#list state!{} as key, value][#if key != "tenantId" && value??][#assign url = url + "&" + key?url + "=" + value?url/][/#if][/#list]  \r\n${url}\r\n\r\nThis is an automated email, please do not reply directly to it.	\N	1666539967258	1693921468239	{}	{"fr":"<html>\\n  <head>\\n    <meta name=\\"color-scheme\\" content=\\"light\\">\\n    <style type=\\"text/css\\">\\n        * {\\n            text-align: center;\\n            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'PingFang SC', 'Ubuntu', 'Hiragino Sans GB', 'Microsoft YaHei', 'Helvetica Neue', Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol' !important;\\n        }\\n        body {\\n            height: 100% !important;\\n            margin: 0px;\\n            box-sizing: border-box;\\n            color: darkslategray !important;\\n            background: rgb(46, 69, 82) !important;\\n        }\\n        .logo {\\n            margin: 10px;\\n        }\\n        .logo img {\\n            margin: 0 auto;\\n            display: block;\\n        }\\n        .logo-text {\\n            font-size: smaller !important;\\n            color: lightblue !important;\\n        }\\n        .hf {\\n            padding: 15px;\\n            background: rgb(46, 69, 82) !important;\\n        }\\n        .hf .footer {\\n            padding: 12px 0;\\n            white-space: nowrap !important;\\n        }\\n        .hf .footer div {\\n            margin: 3px;\\n        }\\n        .asset {\\n            font-weight: 200 !important;\\n        }\\n        .hf-content {\\n            font-size: 12px !important;\\n            white-space: pre-line;\\n            color: lightslategray !important;\\n        }\\n        .main {\\n            text-align: center;\\n            font-size: 32px !important;\\n            margin-bottom: 0px;\\n            font-weight: 200 !important;\\n            white-space:pre-wrap;\\n            color: darkslategray !important;\\n        }\\n        .explainer {\\n            text-align: center;\\n            font-size: 20px !important;\\n            font-weight: 200 !important;\\n            margin-top: 8px;\\n            color: lightslategrey !important;\\n            white-space:pre-wrap;\\n        }\\n        .context {\\n            margin-bottom: -10px;\\n        }\\n        .context-campaign {\\n            font-size: 16px !important;\\n            color: lightseagreen !important;\\n        }\\n        .main {\\n            margin-top: 40px;\\n        }\\n        .header {\\n            height: 42px;\\n        }\\n        .context-pill {\\n            display: inline-block;\\n            font-size: 16px !important;\\n            text-transform: lowercase !important;\\n            font-variant: small-caps !important;\\n            margin-top:10px;\\n            padding: 3px 8px;\\n            background: rgb(234, 238, 237);\\n            border-radius: 6px;\\n            color: darkslategray !important;\\n        }\\n        .context-campaign .context-tenant {\\n            margin-top:8px;\\n        }\\n        .blurb {\\n            margin-top:20px;\\n            padding: 50px 100px;\\n            font-weight: 300 !important;\\n            text-align: center;\\n            white-space: pre-wrap;\\n            color: darkslategray !important;\\n        }\\n        a {\\n            text-decoration: none;\\n            color: inherit;\\n            color: darkcyan !important;\\n        }\\n        .emphasis {\\n            font-weight: 300 !important;\\n        }\\n        .action {\\n            margin-top: 50px;\\n            padding: 10px 15px;\\n            border-radius: 4px;\\n            color: white !important;\\n            background: lightseagreen !important;\\n            display: inline-block;\\n        }\\n        .action:hover {\\n            background: #1ca8a1 !important;\\n            cursor: pointer;\\n        }\\n    </style>\\n  </head>\\n  <body>\\n      \\n  [#assign domain = \\"localhost\\" /]\\n  [#assign auth_base_url = \\"https://${domain}:9000\\" /]\\n  [#assign app_base_url = \\"http://${domain}:3000\\" /]\\n  [#assign url = \\"${auth_base_url}/password/change/${changePasswordId}?tenantId=${user.tenantId}\\" /]\\n    \\n    <div class=\\"hf\\">\\n      <div class=\\"logo\\">\\n        <img width=\\"120\\" src=\\"${app_base_url}/mc/images/mclogo.png\\">\\n      </div>\\n    </div>\\n    <div style=\\"padding: 40px 20px !important;  background: white !important; min-height: 500px !important; \\">\\n        <div class=\\"main\\">${user.firstName}, votre compte nécessite un nouveau mot de passe.</div>\\n        <div class=\\"explainer\\">euillez le changer pour accéder à l'application.</div>\\n        <a class=\\"action\\" href=\\"${url}\\">\\n            Changer le mot de passe\\n        </a>\\n    </div>    \\n    <div class=\\"hf\\" style=\\"padding:30px 0px\\">\\n      <div class=\\"hf-content footer\\">\\n            <div>Il s'agit d'un courrier électronique automatisé, veuillez ne pas y répondre directement.</div>\\n      </div>\\n    </div>\\n  </body>\\n</html>"}	{"fr":"Changez votre mot de passe"}	{"fr":"[#setting url_escaping_charset=\\"UTF-8\\"]\\n${user.firstName}, votre compte nécessite un nouveau mot de passe.\\n\\nCliquez sur le lien suivant pour le changer et accéder à nouveau à l'application:\\n\\n  [#assign domain = \\"localhost\\" /]\\n  [#assign auth_base_url = \\"https://${domain}:9000\\" /]\\n  [#assign app_base_url = \\"http://${domain}:3000\\" /]\\n  [#assign url = \\"${auth_base_url}/password/change/${changePasswordId}?tenantId=${user.tenantId}\\" /]\\n\\n  [#assign url = \\"${auth_base_url}/password/change/${changePasswordId}?tenantId=${user.tenantId}\\" /]\\n  [#list state!{} as key, value][#if key != \\"tenantId\\" && value??][#assign url = url + \\"&\\" + key?url + \\"=\\" + value?url/][/#if][/#list]  \\n${url}\\n\\nIl s'agit d'un courrier électronique automatisé, veuillez ne pas y répondre directement."}	Reset Password
76927a87-9056-48db-8f95-8014d45ddd76	\N	<html>\r\n  <head>\r\n    <meta name="color-scheme" content="light">\r\n    <style type="text/css">\r\n        * {\r\n            text-align: center;\r\n            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'PingFang SC', 'Ubuntu', 'Hiragino Sans GB', 'Microsoft YaHei', 'Helvetica Neue', Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol' !important;\r\n        }\r\n        body {\r\n            height: 100% !important;\r\n            margin: 0px;\r\n            box-sizing: border-box;\r\n            color: darkslategray !important;\r\n            background: rgb(46, 69, 82) !important;\r\n        }\r\n        .logo {\r\n            margin: 10px;\r\n        }\r\n        .logo img {\r\n            margin: 0 auto;\r\n            display: block;\r\n        }\r\n        .logo-text {\r\n            font-size: smaller !important;\r\n            color: lightblue !important;\r\n        }\r\n        .hf {\r\n            padding: 15px;\r\n            background: rgb(46, 69, 82) !important;\r\n        }\r\n        .hf .footer {\r\n            padding: 12px 0;\r\n            white-space: nowrap !important;\r\n        }\r\n        .hf .footer div {\r\n            margin: 3px;\r\n        }\r\n        .asset {\r\n            font-weight: 200 !important;\r\n        }\r\n        .hf-content {\r\n            font-size: 12px !important;\r\n            white-space: pre-line;\r\n            color: lightslategray !important;\r\n        }\r\n        .main {\r\n            text-align: center;\r\n            font-size: 32px !important;\r\n            margin-bottom: 0px;\r\n            font-weight: 200 !important;\r\n            white-space:pre-wrap;\r\n            color: darkslategray !important;\r\n        }\r\n        .explainer {\r\n            text-align: center;\r\n            font-size: 20px !important;\r\n            font-weight: 200 !important;\r\n            margin-top: 8px;\r\n            color: lightslategrey !important;\r\n            white-space:pre-wrap;\r\n        }\r\n        .context {\r\n            margin-bottom: -10px;\r\n        }\r\n        .context-campaign {\r\n            font-size: 16px !important;\r\n            color: lightseagreen !important;\r\n        }\r\n        .main {\r\n            margin-top: 40px;\r\n        }\r\n        .header {\r\n            height: 42px;\r\n        }\r\n        .context-pill {\r\n            display: inline-block;\r\n            font-size: 16px !important;\r\n            text-transform: lowercase !important;\r\n            font-variant: small-caps !important;\r\n            margin-top:10px;\r\n            padding: 3px 8px;\r\n            background: rgb(234, 238, 237);\r\n            border-radius: 6px;\r\n            color: darkslategray !important;\r\n        }\r\n        .context-campaign .context-tenant {\r\n            margin-top:8px;\r\n        }\r\n        .blurb {\r\n            margin-top:20px;\r\n            padding: 50px 100px;\r\n            font-weight: 300 !important;\r\n            text-align: center;\r\n            white-space: pre-wrap;\r\n            color: darkslategray !important;\r\n        }\r\n        a {\r\n            text-decoration: none;\r\n            color: inherit;\r\n            color: darkcyan !important;\r\n        }\r\n        .emphasis {\r\n            font-weight: 300 !important;\r\n        }\r\n        .action {\r\n            margin-top: 50px;\r\n            padding: 10px 15px;\r\n            border-radius: 4px;\r\n            color: white !important;\r\n            background: lightseagreen !important;\r\n            display: inline-block;\r\n        }\r\n        .action:hover {\r\n            background: #1ca8a1 !important;\r\n            cursor: pointer;\r\n        }\r\n    </style>\r\n  </head>\r\n  <body>\r\n      \r\n  [#assign domain = "localhost" /]\r\n  [#assign auth_base_url = "https://${domain}:9000" /]\r\n  [#assign app_base_url = "http://${domain}:3000" /]\r\n  [#assign url = "${auth_base_url}/password/change/${changePasswordId}?tenantId=${user.tenantId}" /]\r\n    \r\n    <div class="hf">\r\n      <div class="logo">\r\n        <img width="120" src="${app_base_url}/mc/images/mclogo.png">\r\n      </div>\r\n    </div>\r\n    <div style="padding: 40px 20px !important;  background: white !important; min-height: 500px !important; ">\r\n        <div class="main">Hi ${user.firstName}, you have a brand new account.</div>\r\n        <div class="explainer">To access the system, setup a password for it.</div>\r\n        <a class="action" href="${url}">\r\n            Setup Password Now\r\n        </a>\r\n    </div>    \r\n    <div class="hf" style="padding:30px 0px">\r\n      <div class="hf-content footer">\r\n            <div>This is an automated email, please do not reply directly to it.</div>\r\n      </div>\r\n    </div>\r\n  </body>\r\n</html>	Setup your password	[#setting url_escaping_charset="UTF-8"]\r\nHi ${user.firstName}, you have a brand new account.\r\n\r\nTo access the system, click on the following link and setup a password:\r\n  [#assign domain = "localhost" /]\r\n  [#assign auth_base_url = "https://${domain}:9000" /]\r\n  [#assign app_base_url = "http://${domain}:3000" /]\r\n  \r\n${auth_base_url}/password/change/${changePasswordId}?tenantId=${user.tenantId}\r\n\r\nThis is an automated email, please do not reply directly to it.	\N	1666539967264	1693921487558	{}	{"fr":"<html>\\n  <head>\\n    <meta name=\\"color-scheme\\" content=\\"light\\">\\n    <style type=\\"text/css\\">\\n        * {\\n            text-align: center;\\n            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'PingFang SC', 'Ubuntu', 'Hiragino Sans GB', 'Microsoft YaHei', 'Helvetica Neue', Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol' !important;\\n        }\\n        body {\\n            height: 100% !important;\\n            margin: 0px;\\n            box-sizing: border-box;\\n            color: darkslategray !important;\\n            background: rgb(46, 69, 82) !important;\\n        }\\n        .logo {\\n            margin: 10px;\\n        }\\n        .logo img {\\n            margin: 0 auto;\\n            display: block;\\n        }\\n        .logo-text {\\n            font-size: smaller !important;\\n            color: lightblue !important;\\n        }\\n        .hf {\\n            padding: 15px;\\n            background: rgb(46, 69, 82) !important;\\n        }\\n        .hf .footer {\\n            padding: 12px 0;\\n            white-space: nowrap !important;\\n        }\\n        .hf .footer div {\\n            margin: 3px;\\n        }\\n        .asset {\\n            font-weight: 200 !important;\\n        }\\n        .hf-content {\\n            font-size: 12px !important;\\n            white-space: pre-line;\\n            color: lightslategray !important;\\n        }\\n        .main {\\n            text-align: center;\\n            font-size: 32px !important;\\n            margin-bottom: 0px;\\n            font-weight: 200 !important;\\n            white-space:pre-wrap;\\n            color: darkslategray !important;\\n        }\\n        .explainer {\\n            text-align: center;\\n            font-size: 20px !important;\\n            font-weight: 200 !important;\\n            margin-top: 8px;\\n            color: lightslategrey !important;\\n            white-space:pre-wrap;\\n        }\\n        .context {\\n            margin-bottom: -10px;\\n        }\\n        .context-campaign {\\n            font-size: 16px !important;\\n            color: lightseagreen !important;\\n        }\\n        .main {\\n            margin-top: 40px;\\n        }\\n        .header {\\n            height: 42px;\\n        }\\n        .context-pill {\\n            display: inline-block;\\n            font-size: 16px !important;\\n            text-transform: lowercase !important;\\n            font-variant: small-caps !important;\\n            margin-top:10px;\\n            padding: 3px 8px;\\n            background: rgb(234, 238, 237);\\n            border-radius: 6px;\\n            color: darkslategray !important;\\n        }\\n        .context-campaign .context-tenant {\\n            margin-top:8px;\\n        }\\n        .blurb {\\n            margin-top:20px;\\n            padding: 50px 100px;\\n            font-weight: 300 !important;\\n            text-align: center;\\n            white-space: pre-wrap;\\n            color: darkslategray !important;\\n        }\\n        a {\\n            text-decoration: none;\\n            color: inherit;\\n            color: darkcyan !important;\\n        }\\n        .emphasis {\\n            font-weight: 300 !important;\\n        }\\n        .action {\\n            margin-top: 50px;\\n            padding: 10px 15px;\\n            border-radius: 4px;\\n            color: white !important;\\n            background: lightseagreen !important;\\n            display: inline-block;\\n        }\\n        .action:hover {\\n            background: #1ca8a1 !important;\\n            cursor: pointer;\\n        }\\n    </style>\\n  </head>\\n  <body>\\n      \\n  [#assign domain = \\"localhost\\" /]\\n  [#assign auth_base_url = \\"https://${domain}:9000\\" /]\\n  [#assign app_base_url = \\"http://${domain}\\" /]\\n    \\n    <div class=\\"hf\\">\\n      <div class=\\"logo\\">\\n        <img width=\\"120\\" src=\\"${app_base_url}/mc/images/mclogo.png\\">\\n      </div>\\n    </div>\\n    <div style=\\"padding: 40px 20px !important;  background: white !important; min-height: 500px !important; \\">\\n        <div class=\\"main\\">${user.firstName}, votre compte a été créé.</div>\\n        <div class=\\"explainer\\">Pour accéder à l'application, choisissez un mot de passe.</div>\\n        <a class=\\"action\\" href=\\"${auth_base_url}/password/change/${changePasswordId}?tenantId=${user.tenantId}\\">\\n            Définir le mot de passer\\n        </a>\\n    </div>    \\n    <div class=\\"hf\\" style=\\"padding:30px 0px\\">\\n      <div class=\\"hf-content footer\\">\\n            <div>Il s'agit d'un courrier électronique automatisé, veuillez ne pas y répondre directement.</div>\\n      </div>\\n    </div>\\n  </body>\\n</html>"}	{"fr":"Choisissez votre mot de passe"}	{"fr":"[#setting url_escaping_charset=\\"UTF-8\\"]\\n${user.firstName}, votre compte a été créé.\\n\\nPour accéder à l'application, cliquez sur le lien suivant et créez un mot de passe:\\n  [#assign domain = \\"localhost\\" /]\\n  [#assign auth_base_url = \\"https://${domain}:9000\\" /]\\n  [#assign app_base_url = \\"http://${domain}:3000\\" /]\\n  \\n${auth_base_url}/password/change/${changePasswordId}?tenantId=${user.tenantId}\\n\\nIl s'agit d'un courrier électronique automatisé, veuillez ne pas y répondre directement."}	Setup Password
b7aa516b-825c-402e-bb88-169f85360fa6	\N	[#setting url_escaping_charset="UTF-8"]\r\n<p>This password was found in the list of vulnerable passwords, and is no longer secure.</p>\r\n\r\n<p>In order to secure your account, it is recommended to change your password at your earliest convenience.</p>\r\n\r\n<p>Follow this link to change your password.</p>\r\n\r\n<a href="http://localhost:9000/password/forgot?client_id=${(application.oauthConfiguration.clientId)!''}&email=${user.email?url}&tenantId=${user.tenantId}">\r\n  http://localhost:9011/password/forgot?client_id=${(application.oauthConfiguration.clientId)!''}&email=${user.email?url}&tenantId=${user.tenantId}\r\n</a>\r\n\r\n- FusionAuth Admin	Your password is not secure	[#setting url_escaping_charset="UTF-8"]\r\nThis password was found in the list of vulnerable passwords, and is no longer secure.\r\n\r\nIn order to secure your account, it is recommended to change your password at your earliest convenience.\r\n\r\nFollow this link to change your password.\r\n\r\nhttp://localhost:9011/password/forgot?client_id=${(application.oauthConfiguration.clientId)!''}&email=${user.email?url}&tenantId=${user.tenantId}\r\n\r\n- FusionAuth Admin	\N	1666539967257	1693923536706	{}	{}	{}	{}	[FusionAuth Default] Breached Password Notification
\.


--
-- Data for Name: entities; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.entities (id, client_id, client_secret, data, entity_types_id, insert_instant, last_update_instant, name, parent_id, tenants_id) FROM stdin;
\.


--
-- Data for Name: entity_entity_grant_permissions; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.entity_entity_grant_permissions (entity_entity_grants_id, entity_type_permissions_id) FROM stdin;
\.


--
-- Data for Name: entity_entity_grants; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.entity_entity_grants (id, data, insert_instant, last_update_instant, recipient_id, target_id) FROM stdin;
\.


--
-- Data for Name: entity_type_permissions; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.entity_type_permissions (id, data, description, entity_types_id, insert_instant, is_default, last_update_instant, name) FROM stdin;
\.


--
-- Data for Name: entity_types; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.entity_types (id, access_token_signing_keys_id, data, insert_instant, last_update_instant, name) FROM stdin;
\.


--
-- Data for Name: entity_user_grant_permissions; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.entity_user_grant_permissions (entity_user_grants_id, entity_type_permissions_id) FROM stdin;
\.


--
-- Data for Name: entity_user_grants; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.entity_user_grants (id, data, entities_id, insert_instant, last_update_instant, users_id) FROM stdin;
\.


--
-- Data for Name: event_logs; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.event_logs (id, insert_instant, message, type) FROM stdin;
\.


--
-- Data for Name: external_identifiers; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.external_identifiers (id, applications_id, data, expiration_instant, insert_instant, tenants_id, type, users_id) FROM stdin;
\.


--
-- Data for Name: families; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.families (data, family_id, insert_instant, last_update_instant, owner, role, users_id) FROM stdin;
\.


--
-- Data for Name: federated_domains; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.federated_domains (identity_providers_id, domain) FROM stdin;
\.


--
-- Data for Name: form_fields; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.form_fields (id, consents_id, data, insert_instant, last_update_instant, name) FROM stdin;
2d9d8baf-6a5b-9f05-ed88-8b8fe5ab93ee	\N	{"key": "user.email", "control": "text", "required": true, "type": "email", "data": {"leftAddon": "user"}}	1666539852785	1666539852785	Email
3c8160f9-bfe8-945b-faf9-d654324832a2	\N	{"key": "user.password", "control": "password", "required": true, "type": "string", "data": {"leftAddon": "lock"}}	1666539852785	1666539852785	Password
b21ce810-f702-b485-711b-a093edab0f88	\N	{"key": "user.firstName", "control": "text", "required": false, "type": "string", "data": {"leftAddon": "info"}}	1666539852785	1666539852785	First name
d328f8b9-944c-9478-5774-bd16d885740b	\N	{"key": "user.middleName", "control": "text", "required": false, "type": "string", "data": {"leftAddon": "info"}}	1666539852785	1666539852785	Middle name
7232aebe-5f3b-492d-a7ff-e0905eb67113	\N	{"key": "user.lastName", "control": "text", "required": false, "type": "string", "data": {"leftAddon": "info"}}	1666539852785	1666539852785	Last name
7d4f4b54-f347-6089-3b22-b64dddff8798	\N	{"key": "user.fullName", "control": "text", "required": false, "type": "string", "data": {"leftAddon": "info"}}	1666539852785	1666539852785	Full name
0376285a-1b23-c0c9-73af-f5362d011f1a	\N	{"key": "user.birthDate", "control": "text", "required": false, "type": "date", "data": {"leftAddon": "calendar"}}	1666539852785	1666539852785	Birthdate
8f88f4e0-d5c3-efbd-a445-dec94a5c9889	\N	{"key": "user.mobilePhone", "control": "text", "required": false, "type": "string", "data": {"leftAddon": "mobile"}}	1666539852785	1666539852785	Mobile phone
7b7c1813-6f1e-ad7e-8fd7-0baaf4894e2b	\N	{"key": "user.username", "control": "text", "required": true, "type": "string", "data": {"leftAddon": "user"}}	1666539852785	1666539852785	Username
96fed866-6839-aca0-ae7e-85247927458c	\N	{"key": "registration.preferredLanguages", "control": "select", "required": false, "type": "string", "data": {"leftAddon": "info"}}	1666539852785	1666539852785	[Admin Registration] preferred languages
c32eb2b4-6c61-fafc-9e14-29ccc843cf9b	\N	{"key": "registration.roles", "control": "checkbox", "required": false, "type": "string", "data": {"leftAddon": "info"}}	1666539852785	1666539852785	[Admin Registration] roles
93583070-43bf-712a-6d93-423ba7a1a227	\N	{"key": "registration.timezone", "control": "select", "required": false, "type": "string", "data": {"leftAddon": "info"}}	1666539852785	1666539852785	[Admin Registration] timezone
3e8e7f0e-4010-2ddb-90ad-ae50a1ab0988	\N	{"key": "registration.username", "control": "text", "required": false, "type": "string", "data": {"leftAddon": "user"}}	1666539852785	1666539852785	[Admin Registration] username
97d845e0-2f7f-4e62-98f7-5b033358df63	\N	{"key": "user.birthDate", "control": "text", "required": false, "type": "date", "data": {"leftAddon": "calendar"}}	1666539852785	1666539852785	[Admin User] birthdate
e6607e53-83c2-c7ec-fe01-20779f1a6801	\N	{"key": "user.email", "control": "text", "required": false, "type": "email", "data": {"leftAddon": "user"}}	1666539852785	1666539852785	[Admin User] email
6a51544a-18a8-3256-4b3c-b428e2c34db9	\N	{"key": "user.firstName", "control": "text", "required": false, "type": "string", "data": {"leftAddon": "info"}}	1666539852785	1666539852785	[Admin User] first name
b903f25b-952a-a533-e17a-81e2197c9ab3	\N	{"key": "user.fullName", "control": "text", "required": false, "type": "string", "data": {"leftAddon": "info"}}	1666539852785	1666539852785	[Admin User] full name
1b9389fa-3d94-08c2-affd-e7bfaeed003d	\N	{"key": "user.imageUrl", "control": "text", "required": false, "type": "string", "data": {"leftAddon": "info"}}	1666539852785	1666539852785	[Admin User] image URL
2f8f29a1-02ac-8b76-ef72-420ae951f3f0	\N	{"key": "user.lastName", "control": "text", "required": false, "type": "string", "data": {"leftAddon": "info"}}	1666539852785	1666539852785	[Admin User] last name
7aadf885-2d2e-b4ff-31f4-61996a65f048	\N	{"key": "user.middleName", "control": "text", "required": false, "type": "string", "data": {"leftAddon": "info"}}	1666539852785	1666539852785	[Admin User] middle name
12485b77-d9f7-757b-726b-701badf15eaf	\N	{"key": "user.mobilePhone", "control": "text", "required": false, "type": "string", "data": {"leftAddon": "mobile"}}	1666539852785	1666539852785	[Admin User] mobile phone
a6f1e809-f1a6-439a-ed65-0964560c66bc	\N	{"key": "user.password", "control": "password", "required": true, "confirm": true, "type": "string", "data": {"leftAddon": "lock"}}	1666539852785	1666539852785	[Admin User] password
e8494f74-2f7a-79d3-a86f-69b6128dc600	\N	{"key": "user.preferredLanguages", "control": "select", "required": false, "type": "string", "data": {"leftAddon": "info"}}	1666539852785	1666539852785	[Admin User] preferred languages
354f7745-e660-22c3-67af-c8c3c45992f6	\N	{"key": "user.timezone", "control": "select", "required": false, "type": "string", "data": {"leftAddon": "info"}}	1666539852785	1666539852785	[Admin User] timezone
284e5b75-a0aa-0d1b-2bb4-80cd02d71596	\N	{"key": "user.username", "control": "text", "required": false, "type": "string", "data": {"leftAddon": "user"}}	1666539852785	1666539852785	[Admin User] username
3af79e4a-531b-c3c7-a01d-64ec7ec3ee98	\N	{"key": "user.email", "control": "text", "required": false, "type": "email", "data": {"leftAddon": "user"}}	1666539852785	1666539852785	[Self Service User] email
4139d40c-fa5f-7154-4ec8-1a003d7decda	\N	{"key": "user.firstName", "control": "text", "required": false, "type": "string", "data": {"leftAddon": "info"}}	1666539852785	1666539852785	[Self Service User] first name
59786456-4a38-7fb5-b5e2-74a7a99cfdf0	\N	{"key": "user.lastName", "control": "text", "required": false, "type": "string", "data": {"leftAddon": "info"}}	1666539852785	1666539852785	[Self Service User] last name
762cb3a9-e4cb-6ba4-48db-fc422d2e0811	\N	{"key": "user.password", "control": "password", "required": true, "confirm": true, "type": "string", "data": {"leftAddon": "lock"}}	1666539852785	1666539852785	[Self Service User] password
\.


--
-- Data for Name: form_steps; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.form_steps (form_fields_id, forms_id, sequence, step) FROM stdin;
3e8e7f0e-4010-2ddb-90ad-ae50a1ab0988	368c226b-f1a4-d32e-346e-bbde23a0aba7	0	0
96fed866-6839-aca0-ae7e-85247927458c	368c226b-f1a4-d32e-346e-bbde23a0aba7	1	0
93583070-43bf-712a-6d93-423ba7a1a227	368c226b-f1a4-d32e-346e-bbde23a0aba7	2	0
c32eb2b4-6c61-fafc-9e14-29ccc843cf9b	368c226b-f1a4-d32e-346e-bbde23a0aba7	3	0
e6607e53-83c2-c7ec-fe01-20779f1a6801	784b756d-7216-da67-2245-8d4c0608c789	0	0
284e5b75-a0aa-0d1b-2bb4-80cd02d71596	784b756d-7216-da67-2245-8d4c0608c789	1	0
12485b77-d9f7-757b-726b-701badf15eaf	784b756d-7216-da67-2245-8d4c0608c789	2	0
a6f1e809-f1a6-439a-ed65-0964560c66bc	784b756d-7216-da67-2245-8d4c0608c789	3	0
97d845e0-2f7f-4e62-98f7-5b033358df63	784b756d-7216-da67-2245-8d4c0608c789	0	1
6a51544a-18a8-3256-4b3c-b428e2c34db9	784b756d-7216-da67-2245-8d4c0608c789	1	1
7aadf885-2d2e-b4ff-31f4-61996a65f048	784b756d-7216-da67-2245-8d4c0608c789	2	1
2f8f29a1-02ac-8b76-ef72-420ae951f3f0	784b756d-7216-da67-2245-8d4c0608c789	3	1
b903f25b-952a-a533-e17a-81e2197c9ab3	784b756d-7216-da67-2245-8d4c0608c789	4	1
e8494f74-2f7a-79d3-a86f-69b6128dc600	784b756d-7216-da67-2245-8d4c0608c789	5	1
354f7745-e660-22c3-67af-c8c3c45992f6	784b756d-7216-da67-2245-8d4c0608c789	6	1
1b9389fa-3d94-08c2-affd-e7bfaeed003d	784b756d-7216-da67-2245-8d4c0608c789	7	1
3af79e4a-531b-c3c7-a01d-64ec7ec3ee98	d14cd4d3-a124-a22e-01ad-6bdcf8a299b8	0	0
4139d40c-fa5f-7154-4ec8-1a003d7decda	d14cd4d3-a124-a22e-01ad-6bdcf8a299b8	1	0
59786456-4a38-7fb5-b5e2-74a7a99cfdf0	d14cd4d3-a124-a22e-01ad-6bdcf8a299b8	2	0
762cb3a9-e4cb-6ba4-48db-fc422d2e0811	d14cd4d3-a124-a22e-01ad-6bdcf8a299b8	3	0
\.


--
-- Data for Name: forms; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.forms (id, data, insert_instant, last_update_instant, name, type) FROM stdin;
368c226b-f1a4-d32e-346e-bbde23a0aba7	\N	1666539849785	1666539849785	Default Admin Registration provided by FusionAuth	1
784b756d-7216-da67-2245-8d4c0608c789	\N	1666539850785	1666539850785	Default Admin User provided by FusionAuth	2
d14cd4d3-a124-a22e-01ad-6bdcf8a299b8	\N	1666539851785	1666539851785	Default User Self Service provided by FusionAuth	3
\.


--
-- Data for Name: global_daily_active_users; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.global_daily_active_users (count, day) FROM stdin;
1	19288
1	19290
1	19292
1	19291
2	19306
1	19315
1	19605
\.


--
-- Data for Name: global_monthly_active_users; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.global_monthly_active_users (count, month) FROM stdin;
1	633
2	634
1	644
\.


--
-- Data for Name: global_registration_counts; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.global_registration_counts (count, decremented_count, hour) FROM stdin;
1	0	462927
1	0	463364
0	1	470531
\.


--
-- Data for Name: group_application_roles; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.group_application_roles (application_roles_id, groups_id) FROM stdin;
\.


--
-- Data for Name: group_members; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.group_members (id, groups_id, data, insert_instant, users_id) FROM stdin;
\.


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.groups (id, data, insert_instant, last_update_instant, name, tenants_id) FROM stdin;
\.


--
-- Data for Name: hourly_logins; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.hourly_logins (applications_id, count, data, hour) FROM stdin;
3c219e58-ed0e-4b18-ad48-f4f92793ae32	1	\N	462974
3c219e58-ed0e-4b18-ad48-f4f92793ae32	1	\N	462995
3c219e58-ed0e-4b18-ad48-f4f92793ae32	1	\N	463003
3c219e58-ed0e-4b18-ad48-f4f92793ae32	1	\N	463016
3c219e58-ed0e-4b18-ad48-f4f92793ae32	2	\N	463017
3c219e58-ed0e-4b18-ad48-f4f92793ae32	5	\N	463355
3c219e58-ed0e-4b18-ad48-f4f92793ae32	1	\N	463359
0979b36e-8a48-4c4b-a0ba-1eb846cd50ff	2	\N	463364
3c219e58-ed0e-4b18-ad48-f4f92793ae32	1	\N	463364
3c219e58-ed0e-4b18-ad48-f4f92793ae32	2	\N	463568
3c219e58-ed0e-4b18-ad48-f4f92793ae32	1	\N	470528
3c219e58-ed0e-4b18-ad48-f4f92793ae32	1	\N	470532
3c219e58-ed0e-4b18-ad48-f4f92793ae32	1	\N	470533
\.


--
-- Data for Name: identities; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.identities (id, breached_password_last_checked_instant, breached_password_status, connectors_id, email, encryption_scheme, factor, insert_instant, last_login_instant, last_update_instant, password, password_change_reason, password_change_required, password_last_update_instant, salt, status, tenants_id, username, username_index, username_status, users_id, verified) FROM stdin;
1	\N	\N	e3306678-a53a-4964-9040-1c96f36dda72	apprise.craft@gmail.com	salted-pbkdf2-hmac-sha256	24000	1666539967180	1693920176111	1668079764259	/jq3n0lhjo2+FoKUN5pWjIJl9Xp0nHtTEnKS73K0t20=	\N	f	1668080932420	iwaW2LkgVRlwwUjvH1FCCpT1sI2qbZ9sLQRDiUmlSBk=	0	e4ed14d6-3706-3908-2179-9e3b74617677	apprise-support	APPRISE-SUPPORT	0	60b1ad57-9b8f-48c5-be33-0cbb01c35bdb	t
\.


--
-- Data for Name: identity_provider_links; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.identity_provider_links (data, identity_providers_id, identity_providers_user_id, insert_instant, last_login_instant, tenants_id, users_id) FROM stdin;
\.


--
-- Data for Name: identity_providers; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.identity_providers (id, data, enabled, insert_instant, last_update_instant, name, type, keys_id, request_signing_keys_id, reconcile_lambdas_id) FROM stdin;
\.


--
-- Data for Name: identity_providers_applications; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.identity_providers_applications (applications_id, data, enabled, identity_providers_id, keys_id) FROM stdin;
\.


--
-- Data for Name: identity_providers_tenants; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.identity_providers_tenants (tenants_id, data, identity_providers_id) FROM stdin;
\.


--
-- Data for Name: instance; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.instance (id, activate_instant, license, license_id, setup_complete) FROM stdin;
e061677a-d1c5-9060-306b-070792f22234	\N	\N	\N	t
\.


--
-- Data for Name: integrations; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.integrations (data) FROM stdin;
{}
\.


--
-- Data for Name: ip_access_control_lists; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.ip_access_control_lists (id, data, insert_instant, last_update_instant, name) FROM stdin;
\.


--
-- Data for Name: ip_location_database; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.ip_location_database (data, last_modified) FROM stdin;
\N	0
\.


--
-- Data for Name: keys; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.keys (id, algorithm, certificate, expiration_instant, insert_instant, issuer, kid, last_update_instant, name, private_key, public_key, secret, type) FROM stdin;
42ac0869-83a6-af28-8dd2-e9f5f84383e4	HS256	\N	\N	1666539848785	\N	287528f26	1666539848785	Default signing key	\N	\N	LPezGVgbtmnA5LibgiK9FOqLS77CSpzZWsmGX9ManBI=	HMAC
092dbedc-30af-4149-9c61-b578f2c72f59	HS256	\N	\N	1666539849785	\N	8758f548c	1666539849785	OpenID Connect compliant HMAC using SHA-256	\N	\N	\N	HMAC
4b8f1c06-518e-45bd-9ac5-d549686ae02a	HS384	\N	\N	1666539850785	\N	fd6e1369f	1666539850785	OpenID Connect compliant HMAC using SHA-384	\N	\N	\N	HMAC
c753a44d-7f2e-48d3-bc4e-c2c16488a23b	HS512	\N	\N	1666539851785	\N	198a55815	1666539851785	OpenID Connect compliant HMAC using SHA-512	\N	\N	\N	HMAC
\.


--
-- Data for Name: kickstart_files; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.kickstart_files (id, kickstart, name) FROM stdin;
\.


--
-- Data for Name: lambdas; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.lambdas (id, body, debug, enabled, insert_instant, last_update_instant, name, type) FROM stdin;
ecaeb3d4-058d-4d9d-afd0-275c8f3ac1d4	/*\n * Copyright (c) 2021, FusionAuth, All Rights Reserved\n */\n\n// This is the default Apple reconcile, modify this to your liking.\nfunction reconcile(user, registration, idToken) {\n\n  // Un-comment this line to see the idToken object printed to the event log\n  // console.info(JSON.stringify(idToken, null, 2));\n\n  // During the first login attempt, the user object will be available which may contain first and last name.\n  if (idToken.user && idToken.user.name) {\n    user.firstName = idToken.user.name.firstName || user.firstName;\n    user.lastName = idToken.user.name.lastName || user.lastName;\n  }\n}	f	t	1666539967265	1666539967265	[FusionAuth Default] Apple Reconcile	4
a3d8d094-8fd8-41cf-9a51-28f7e0b8c25e	/*\n * Copyright (c) 2021, FusionAuth, All Rights Reserved\n */\n\n// This is the default Facebook reconcile, modify this to your liking.\nfunction reconcile(user, registration, facebookUser) {\n\n  // Un-comment this line to see the facebookUser object printed to the event log\n  // console.info(JSON.stringify(facebookUser, null, 2));\n\n  user.firstName = facebookUser.first_name;\n  user.middleName = facebookUser.middle_name;\n  user.lastName = facebookUser.last_name;\n  user.fullName = facebookUser.name;\n\n  if (facebookUser.picture && !facebookUser.picture.data.is_silhouette) {\n    user.imageUrl = facebookUser.picture.data.url;\n  }\n\n  if (facebookUser.birthday) {\n    // Convert MM/dd/yyyy -> YYYY-MM-DD\n    var parts = facebookUser.birthday.split('/');\n    user.birthDate = parts[2] + '-' +  parts[0] + '-' +  parts[1];\n  }\n}	f	t	1666539967271	1666539967271	[FusionAuth Default] Facebook Reconcile	6
e74879bb-66aa-4d04-a879-8f360066f0ac	/*\n * Copyright (c) 2021, FusionAuth, All Rights Reserved\n */\n\n// This is the default Google reconcile, modify this to your liking.\nfunction reconcile(user, registration, idToken) {\n\n  // Un-comment this line to see the idToken object printed to the event log\n  // console.info(JSON.stringify(idToken, null, 2));\n\n  // The idToken is the response from the tokeninfo endpoint\n  // https://developers.google.com/identity/sign-in/web/backend-auth#calling-the-tokeninfo-endpoint\n  user.firstName = idToken.given_name;\n  user.lastName = idToken.family_name;\n  user.fullName = idToken.name;\n  user.imageUrl = idToken.picture;\n}	f	t	1666539967274	1666539967274	[FusionAuth Default] Google Reconcile	7
8111e0f7-26a6-474c-805c-91dc7252fee1	/*\n * Copyright (c) 2021, FusionAuth, All Rights Reserved\n */\n\n// This is the default LinkedIn reconcile, modify this to your liking.\nfunction reconcile(user, registration, linkedInUser) {\n\n  // Un-comment this line to see the linkedInUser object printed to the event log\n  // console.info(JSON.stringify(linkedInUser, null, ' '));\n\n  user.firstName = linkedInUser.localizedFirstName || user.firstName;\n  user.lastName = linkedInUser.localizedLastName || user.lastName;\n\n  // LinkedIn returns several images sizes.\n  // See https://docs.microsoft.com/en-us/linkedin/shared/references/v2/profile/profile-picture\n  var images = linkedInUser.profilePicture['displayImage~'].elements || [];\n  var image100 = images.length >= 1 ? images[0].identifiers[0].identifier : null;\n  var image200 = images.length >= 2 ? images[1].identifiers[0].identifier : null;\n  var image400 = images.length >= 3 ? images[2].identifiers[0].identifier : null;\n  var image800 = images.length >= 4 ? images[3].identifiers[0].identifier : null;\n\n  // Use the largest image.\n  user.imageUrl = image800;\n\n  // Record the LinkedIn Id\n  registration.data.linkedIn = registration.data.linkedIn || {};\n  registration.data.linkedIn.id = linkedInUser.id;\n}	f	t	1666539967276	1666539967276	[FusionAuth Default] LinkedIn Reconcile	11
74176207-3119-43c6-a326-acab165e95f8	/*\n * Copyright (c) 2021, FusionAuth, All Rights Reserved\n */\n\n// This is the default OpenID Connect reconcile, modify this to your liking.\nfunction reconcile(user, registration, jwt, idToken) {\n  // When the openid scope was requested, and the IdP returns an id_token, this value will optionally\n  // be available to this lambda if signed using the client secret using HS256, HS384, or the HS512 algorithm.\n\n  // Un-comment this line to see the jwt object printed to the event log\n  // console.info(JSON.stringify(jwt, null, 2));\n\n  user.firstName = jwt.given_name;\n  user.middleName = jwt.middle_name;\n  user.lastName = jwt.family_name;\n  user.fullName = jwt.name;\n  user.imageUrl = jwt.picture;\n  user.mobilePhone = jwt.phone_number;\n\n  // https://openid.net/specs/openid-connect-core-1_0.html#StandardClaims\n  if (jwt.birthdate && jwt.birthdate != '0000') {\n    if (jwt.birthdate.length == 4) {\n      // Only a year was provided, set to January 1.\n      user.birthDate = jwt.birthdate + '-01-01';\n    } else {\n      user.birthDate = jwt.birthdate;\n    }\n  }\n\n  // https://openid.net/specs/openid-connect-core-1_0.html#StandardClaims\n  if (jwt.locale) {\n    user.preferredLanguages = user.preferredLanguages || [];\n    // Replace the dash with an under score.\n    user.preferredLanguages.push(jwt.locale.replace('-', '_'));\n  }\n\n  // Set preferred_username in registration.\n  // - This is just for display purposes, this value cannot be used to uniquely identify\n  //   the user in FusionAuth.\n  registration.username = jwt.preferred_username;\n}	f	t	1666539967278	1666539967278	[FusionAuth Default] OpenID Connect Reconcile	1
66f231b9-d065-4155-91b1-e79c8c536860	/*\n * Copyright (c) 2021, FusionAuth, All Rights Reserved\n */\n\n// This is the default SAML v2 reconcile, modify this to your liking.\nfunction reconcile(user, registration, samlResponse) {\n\n  // Un-comment this line to see the samlResponse object printed to the event log\n  // console.info(JSON.stringify(samlResponse, null, 2));\n\n  var getAttribute = function(samlResponse, attribute) {\n    var values = samlResponse.assertion.attributes[attribute];\n    if (values && values.length > 0) {\n      return values[0];\n    }\n\n    return null;\n  };\n\n  // Retrieve an attribute from the samlResponse\n  // - Arguments [2 .. ] provide a preferred order of attribute names to lookup the value in the response.\n  var defaultIfNull = function(samlResponse) {\n    for (var i=1; i < arguments.length; i++) {\n      var value = getAttribute(samlResponse, arguments[i]);\n      if (value !== null) {\n        return value;\n      }\n    }\n  };\n\n  user.birthDate = defaultIfNull(samlResponse, 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/dateofbirth', 'birthdate', 'date_of_birth');\n  user.firstName = defaultIfNull(samlResponse, 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname', 'first_name');\n  user.lastName = defaultIfNull(samlResponse, 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname', 'last_name');\n  user.fullName = defaultIfNull(samlResponse, 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name', 'name', 'full_name');\n  user.mobilePhone = defaultIfNull(samlResponse, 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/mobilephone', 'mobile_phone');\n}	f	t	1666539967282	1666539967282	[FusionAuth Default] SAML v2 Reconcile	2
54034f26-983a-449e-bc2d-452ec2adec23	/*\n * Copyright (c) 2021, FusionAuth, All Rights Reserved\n */\n\n// This is the default Twitter reconcile, modify this to your liking.\nfunction reconcile(user, registration, twitterUser) {\n\n  // Un-comment this line to see the twitterUser object printed to the event log\n  // console.info(JSON.stringify(twitterUser, null, 2));\n\n  // Set name if available in the response\n  if (twitterUser.name) {\n    user.fullName = twitterUser.name;\n  }\n\n  // https://developer.twitter.com/en/docs/accounts-and-users/user-profile-images-and-banners.html\n  if (twitterUser.profile_image_url_https) {\n    // Remove the _normal suffix to get the original size.\n    user.imageUrl = twitterUser.profile_image_url_https.replace('_normal.png', '.png');\n  }\n\n  // Set twitter screen_name in registration.\n  // - This is just for display purposes, this value cannot be used to uniquely identify\n  //   the user in FusionAuth.\n  registration.username = twitterUser.screen_name;\n}	f	t	1666539967283	1666539967283	[FusionAuth Default] Twitter Reconcile	9
\.


--
-- Data for Name: last_login_instants; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.last_login_instants (applications_id, registration_last_login_instant, users_id, user_last_login_instant) FROM stdin;
\.


--
-- Data for Name: locks; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.locks (type, update_instant) FROM stdin;
UserActionEndEvent	\N
EmailPlus	\N
Family	\N
com.inversoft.migration.Migrator	\N
Reindex	\N
Reset	\N
\.


--
-- Data for Name: master_record; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.master_record (id, instant) FROM stdin;
054fa16c-aa57-4c6e-8473-e8fe2503abaa	1693924690874
\.


--
-- Data for Name: message_templates; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.message_templates (id, data, insert_instant, last_update_instant, name, type) FROM stdin;
764ed1a6-35d0-a082-6736-a97916968ebd	{ "defaultTemplate": "Two Factor Code: ${code}" }	1666539852785	1666539852785	Default Two Factor Request	0
\.


--
-- Data for Name: messengers; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.messengers (id, data, insert_instant, last_update_instant, name, type) FROM stdin;
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.migrations (name, run_instant) FROM stdin;
io.fusionauth.api.migration.guice.Migration_1_8_0	0
io.fusionauth.api.migration.guice.Migration_1_9_2	0
io.fusionauth.api.migration.guice.Migration_1_10_0	0
io.fusionauth.api.migration.guice.Migration_1_13_0	0
io.fusionauth.api.migration.guice.Migration_1_15_3	0
io.fusionauth.api.migration.guice.Migration_1_30_0	0
\.


--
-- Data for Name: nodes; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.nodes (id, data, insert_instant, last_checkin_instant, runtime_mode, url) FROM stdin;
054fa16c-aa57-4c6e-8473-e8fe2503abaa	{"data":{},"ipAddresses":{"eth0":["10.244.0.24"]}}	1693922948730	1693924726876	production	http://10.244.0.24:9011
\.


--
-- Data for Name: previous_passwords; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.previous_passwords (insert_instant, encryption_scheme, factor, password, salt, users_id) FROM stdin;
\.


--
-- Data for Name: raw_application_daily_active_users; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.raw_application_daily_active_users (applications_id, day, users_id) FROM stdin;
\.


--
-- Data for Name: raw_application_monthly_active_users; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.raw_application_monthly_active_users (applications_id, month, users_id) FROM stdin;
\.


--
-- Data for Name: raw_application_registration_counts; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.raw_application_registration_counts (id, applications_id, count, decremented_count, hour) FROM stdin;
\.


--
-- Data for Name: raw_global_daily_active_users; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.raw_global_daily_active_users (day, users_id) FROM stdin;
\.


--
-- Data for Name: raw_global_monthly_active_users; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.raw_global_monthly_active_users (month, users_id) FROM stdin;
\.


--
-- Data for Name: raw_global_registration_counts; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.raw_global_registration_counts (id, count, decremented_count, hour) FROM stdin;
\.


--
-- Data for Name: raw_logins; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.raw_logins (applications_id, instant, ip_address, users_id) FROM stdin;
\.


--
-- Data for Name: refresh_tokens; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.refresh_tokens (id, applications_id, data, insert_instant, start_instant, tenants_id, token, users_id) FROM stdin;
dfc142ac-5ebf-4044-a771-295b938e7bb9	\N	{"data":{},"metaData":{"device":{"lastAccessedAddress":"10.244.0.117","lastAccessedInstant":1693920176116,"name":"macOS Chrome","type":"BROWSER"}}}	1693920176116	1693920176116	e4ed14d6-3706-3908-2179-9e3b74617677	Ixn_YJHhLprUITJTgyXepJPFce1xWnJyX3j4F2cUH_a-eR3DxW7uDw	60b1ad57-9b8f-48c5-be33-0cbb01c35bdb
619b708f-3283-4f19-b711-293bbf615c0a	0979b36e-8a48-4c4b-a0ba-1eb846cd50ff	{"data":{},"metaData":{"device":{"lastAccessedAddress":"10.244.0.117","lastAccessedInstant":1693901354956,"type":"UNKNOWN"},"scopes":["openid","offline_access"]}}	1693901354956	1693901354956	\N	nB53QOTvfgHxHwS7858dJk05yqmHa_0teL6D6KaemYEDq572Ycm76A	60b1ad57-9b8f-48c5-be33-0cbb01c35bdb
c4894a2f-bc23-4442-8f45-a0b93678f708	0979b36e-8a48-4c4b-a0ba-1eb846cd50ff	{"data":{},"metaData":{"device":{"lastAccessedAddress":"10.244.0.117","lastAccessedInstant":1693901382381,"name":"macOS Chrome","type":"BROWSER"},"scopes":["openid","offline_access"]}}	1693901382381	1693901382381	\N	eu5iVWHwx2_4nqgB3MhCTqKI4wWH8B4ZGExqNOn2OSPrQiEEmqBzMg	60b1ad57-9b8f-48c5-be33-0cbb01c35bdb
7d119d79-9c23-42fa-b205-0d51aa340a0f	0979b36e-8a48-4c4b-a0ba-1eb846cd50ff	{"data":{},"metaData":{"device":{"lastAccessedAddress":"10.244.0.117","lastAccessedInstant":1693905605748,"name":"macOS Chrome","type":"BROWSER"},"scopes":["openid","offline_access"]}}	1693905605748	1693905605748	\N	DvUfwSbCWNHAP_5czx1a2ivTkvjKt39GABafK9llF_ZQkm2GQ7nAFw	60b1ad57-9b8f-48c5-be33-0cbb01c35bdb
405d728a-b536-4677-bf23-556846b09c9c	3c219e58-ed0e-4b18-ad48-f4f92793ae32	{"data":{},"metaData":{"device":{"lastAccessedAddress":"10.244.0.12","lastAccessedInstant":1693924740982,"name":"macOS Chrome","type":"BROWSER"},"scopes":["offline_access"]}}	1693920176180	1693924740982	\N	KF6JPhhvrxRM_L06MtlEbvSnzqkKz4h301SA-SgEqxQebMhcWo-oNA	60b1ad57-9b8f-48c5-be33-0cbb01c35bdb
b13d8ada-b153-4643-98cb-683c131125ff	0979b36e-8a48-4c4b-a0ba-1eb846cd50ff	{"data":{},"metaData":{"device":{"lastAccessedAddress":"10.244.0.117","lastAccessedInstant":1693906490843,"name":"macOS Chrome","type":"BROWSER"},"scopes":["openid","offline_access"]}}	1693906490843	1693906490843	\N	10h_vLe840asR3NPeYFwOtK5U_b42g1f2AXJlQt8MA9XrBinEbJyyw	60b1ad57-9b8f-48c5-be33-0cbb01c35bdb
8c77412b-bf73-40fc-945b-3f38d91b385f	0979b36e-8a48-4c4b-a0ba-1eb846cd50ff	{"data":{},"metaData":{"device":{"lastAccessedAddress":"10.244.0.117","lastAccessedInstant":1693906508926,"name":"macOS Chrome","type":"BROWSER"},"scopes":["openid","offline_access"]}}	1693906508926	1693906508926	\N	vUWqTZV3Q3L298zHz3eQNm1RtQ-WH3k43ZvOuc4_jvQj6ODjvCIR9Q	60b1ad57-9b8f-48c5-be33-0cbb01c35bdb
2d092a64-5f9f-469d-bf44-3ee39082b135	0979b36e-8a48-4c4b-a0ba-1eb846cd50ff	{"data":{},"metaData":{"device":{"lastAccessedAddress":"10.244.0.117","lastAccessedInstant":1693906585341,"name":"macOS Chrome","type":"BROWSER"},"scopes":["openid","offline_access"]}}	1693906585341	1693906585341	\N	PPML_DgI6ibASjF8TSoqf4XBTTZNFClFD9M4hhbT2jFs7IyV_xMcEA	60b1ad57-9b8f-48c5-be33-0cbb01c35bdb
13cfcbdf-2614-4249-89cd-3986c55a17cf	0979b36e-8a48-4c4b-a0ba-1eb846cd50ff	{"data":{},"metaData":{"device":{"lastAccessedAddress":"10.244.0.117","lastAccessedInstant":1693909973773,"name":"macOS Chrome","type":"BROWSER"},"scopes":["openid","offline_access"]}}	1693909973773	1693909973773	\N	TDv6KS9Q83V6MqaCGTbaZxzdNJcbzOMUinZP77CYSp_-uPkNoK4XhA	60b1ad57-9b8f-48c5-be33-0cbb01c35bdb
b7fee06f-7d0b-4808-8089-845b6fb059aa	0979b36e-8a48-4c4b-a0ba-1eb846cd50ff	{"data":{},"metaData":{"device":{"lastAccessedAddress":"10.244.0.117","lastAccessedInstant":1693909996252,"name":"macOS Chrome","type":"BROWSER"},"scopes":["openid","offline_access"]}}	1693909996252	1693909996252	\N	cRkMl8VuQrGWhmyFSC2mps39qWgYopElx1U_ajXzqmT7WdDTLPVyXg	60b1ad57-9b8f-48c5-be33-0cbb01c35bdb
aec9d857-5eb7-4958-8bf7-56a781eb3b65	0979b36e-8a48-4c4b-a0ba-1eb846cd50ff	{"data":{},"metaData":{"device":{"lastAccessedAddress":"10.244.0.117","lastAccessedInstant":1693919277557,"name":"macOS Chrome","type":"BROWSER"},"scopes":["openid","offline_access"]}}	1693912111087	1693919277557	\N	B58Gr3QdmYoJk19w7gnS1mL7sJl-NE5nDa3PX78atoV03tPOlDVcRA	60b1ad57-9b8f-48c5-be33-0cbb01c35bdb
\.


--
-- Data for Name: request_frequencies; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.request_frequencies (count, last_update_instant, request_id, tenants_id, type) FROM stdin;
\.


--
-- Data for Name: system_configuration; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.system_configuration (data, insert_instant, last_update_instant, report_timezone) FROM stdin;
{"auditLogConfiguration":{"delete":{"enabled":false,"numberOfDaysToRetain":365}},"cookieEncryptionKey":"aspcvbq35Qk9M6p0kW4/Eg==","corsConfiguration":{"allowCredentials":false,"debug":false,"enabled":false,"preflightMaxAgeInSeconds":0},"data":{},"eventLogConfiguration":{"numberToRetain":10000},"loginRecordConfiguration":{"delete":{"enabled":false,"numberOfDaysToRetain":365}},"uiConfiguration":{}}	1666539852785	1666539852785	America/Denver
\.


--
-- Data for Name: tenants; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.tenants (id, access_token_signing_keys_id, admin_user_forms_id, client_credentials_access_token_populate_lambdas_id, confirm_child_email_templates_id, data, email_update_email_templates_id, email_verified_email_templates_id, failed_authentication_user_actions_id, family_request_email_templates_id, forgot_password_email_templates_id, id_token_signing_keys_id, insert_instant, last_update_instant, login_id_in_use_on_create_email_templates_id, login_id_in_use_on_update_email_templates_id, login_new_device_email_templates_id, login_suspicious_email_templates_id, multi_factor_email_message_templates_id, multi_factor_sms_message_templates_id, multi_factor_sms_messengers_id, name, password_reset_success_email_templates_id, password_update_email_templates_id, parent_registration_email_templates_id, passwordless_email_templates_id, set_password_email_templates_id, themes_id, two_factor_method_add_email_templates_id, two_factor_method_remove_email_templates_id, ui_ip_access_control_lists_id, verification_email_templates_id) FROM stdin;
e4ed14d6-3706-3908-2179-9e3b74617677	42ac0869-83a6-af28-8dd2-e9f5f84383e4	784b756d-7216-da67-2245-8d4c0608c789	\N	\N	{"accessControlConfiguration":{},"captchaConfiguration":{"captchaMethod":"GoogleRecaptchaV3","enabled":false,"threshold":0.5},"configured":true,"data":{},"emailConfiguration":{"defaultFromEmail":"emaris@iotc.org","defaultFromName":"e-MARIS","host":"mail","implicitEmailVerificationAllowed":true,"port":1025,"security":"NONE","unverified":{"allowEmailChangeWhenGated":false,"behavior":"Allow"},"verificationStrategy":"ClickableLink","verifyEmail":true,"verifyEmailWhenChanged":false},"eventConfiguration":{"events":{"jwt.public-key.update":{"enabled":false,"transactionType":"None"},"jwt.refresh-token.revoke":{"enabled":false,"transactionType":"None"},"jwt.refresh":{"enabled":false,"transactionType":"None"},"audit-log.create":{"enabled":false,"transactionType":"None"},"event-log.create":{"enabled":false,"transactionType":"None"},"kickstart.success":{"enabled":false,"transactionType":"None"},"user.action":{"enabled":false},"user.bulk.create":{"enabled":false,"transactionType":"None"},"user.create":{"enabled":false,"transactionType":"None"},"user.create.complete":{"enabled":false,"transactionType":"None"},"user.deactivate":{"enabled":false,"transactionType":"None"},"user.delete":{"enabled":false,"transactionType":"None"},"user.delete.complete":{"enabled":false,"transactionType":"None"},"user.loginId.duplicate.create":{"enabled":false,"transactionType":"None"},"user.loginId.duplicate.update":{"enabled":false,"transactionType":"None"},"user.email.update":{"enabled":false,"transactionType":"None"},"user.email.verified":{"enabled":false,"transactionType":"None"},"user.login.failed":{"enabled":false,"transactionType":"None"},"user.login.new-device":{"enabled":false,"transactionType":"None"},"user.login.success":{"enabled":false,"transactionType":"None"},"user.login.suspicious":{"enabled":false,"transactionType":"None"},"user.password.breach":{"enabled":false,"transactionType":"None"},"user.password.reset.send":{"enabled":false,"transactionType":"None"},"user.password.reset.start":{"enabled":false,"transactionType":"None"},"user.password.reset.success":{"enabled":false,"transactionType":"None"},"user.password.update":{"enabled":false,"transactionType":"None"},"user.reactivate":{"enabled":false,"transactionType":"None"},"user.registration.create":{"enabled":false,"transactionType":"None"},"user.registration.create.complete":{"enabled":false,"transactionType":"None"},"user.registration.delete":{"enabled":false,"transactionType":"None"},"user.registration.delete.complete":{"enabled":false,"transactionType":"None"},"user.registration.update":{"enabled":false,"transactionType":"None"},"user.registration.update.complete":{"enabled":false,"transactionType":"None"},"user.registration.verified":{"enabled":false,"transactionType":"None"},"user.two-factor.method.add":{"enabled":false,"transactionType":"None"},"user.two-factor.method.remove":{"enabled":false,"transactionType":"None"},"user.update":{"enabled":false,"transactionType":"None"},"user.update.complete":{"enabled":false,"transactionType":"None"}}},"externalIdentifierConfiguration":{"authorizationGrantIdTimeToLiveInSeconds":30,"changePasswordIdGenerator":{"length":32,"type":"randomBytes"},"changePasswordIdTimeToLiveInSeconds":600,"deviceCodeTimeToLiveInSeconds":300,"deviceUserCodeIdGenerator":{"length":6,"type":"randomAlphaNumeric"},"emailVerificationIdGenerator":{"length":32,"type":"randomBytes"},"emailVerificationIdTimeToLiveInSeconds":86400,"emailVerificationOneTimeCodeGenerator":{"length":6,"type":"randomAlphaNumeric"},"externalAuthenticationIdTimeToLiveInSeconds":300,"oneTimePasswordTimeToLiveInSeconds":60,"passwordlessLoginGenerator":{"length":32,"type":"randomBytes"},"passwordlessLoginTimeToLiveInSeconds":180,"pendingAccountLinkTimeToLiveInSeconds":3600,"registrationVerificationIdGenerator":{"length":32,"type":"randomBytes"},"registrationVerificationIdTimeToLiveInSeconds":86400,"registrationVerificationOneTimeCodeGenerator":{"length":6,"type":"randomAlphaNumeric"},"samlv2AuthNRequestIdTimeToLiveInSeconds":300,"setupPasswordIdGenerator":{"length":32,"type":"randomBytes"},"setupPasswordIdTimeToLiveInSeconds":86400,"twoFactorIdTimeToLiveInSeconds":300,"twoFactorOneTimeCodeIdGenerator":{"length":6,"type":"randomDigits"},"twoFactorOneTimeCodeIdTimeToLiveInSeconds":60,"twoFactorTrustIdTimeToLiveInSeconds":2592000},"failedAuthenticationConfiguration":{"actionDuration":3,"actionDurationUnit":"MINUTES","resetCountInSeconds":60,"tooManyAttempts":5},"familyConfiguration":{"allowChildRegistrations":true,"deleteOrphanedAccounts":false,"deleteOrphanedAccountsDays":30,"enabled":false,"maximumChildAge":12,"minimumOwnerAge":21,"parentEmailRequired":false},"formConfiguration":{"adminUserFormId":"784b756d-7216-da67-2245-8d4c0608c789"},"httpSessionMaxInactiveInterval":3600,"issuer":"emaris.iotc.org","jwtConfiguration":{"enabled":false,"refreshTokenExpirationPolicy":"Fixed","refreshTokenRevocationPolicy":{"onLoginPrevented":true,"onPasswordChanged":true},"refreshTokenTimeToLiveInMinutes":43200,"refreshTokenUsagePolicy":"Reusable","timeToLiveInSeconds":3600},"loginConfiguration":{"requireAuthentication":true},"maximumPasswordAge":{"days":180,"enabled":false},"minimumPasswordAge":{"enabled":false,"seconds":30},"multiFactorConfiguration":{"authenticator":{"algorithm":"HmacSHA1","codeLength":6,"enabled":true,"timeStep":30},"email":{"enabled":false,"templateId":"d858ba9e-9148-4a9e-ad80-5a64bdccf99d"},"sms":{"enabled":false}},"passwordEncryptionConfiguration":{"encryptionScheme":"salted-pbkdf2-hmac-sha256","encryptionSchemeFactor":24000,"modifyEncryptionSchemeOnLogin":false},"passwordValidationRules":{"breachDetection":{"enabled":false,"matchMode":"High","notifyUserEmailTemplateId":"b7aa516b-825c-402e-bb88-169f85360fa6","onLogin":"Off"},"maxLength":256,"minLength":8,"rememberPreviousPasswords":{"count":1,"enabled":false},"requireMixedCase":false,"requireNonAlpha":false,"requireNumber":false,"validateOnLogin":false},"rateLimitConfiguration":{"failedLogin":{"enabled":false,"limit":5,"timePeriodInSeconds":60},"forgotPassword":{"enabled":false,"limit":5,"timePeriodInSeconds":60},"sendEmailVerification":{"enabled":false,"limit":5,"timePeriodInSeconds":60},"sendPasswordless":{"enabled":false,"limit":5,"timePeriodInSeconds":60},"sendRegistrationVerification":{"enabled":false,"limit":5,"timePeriodInSeconds":60},"sendTwoFactor":{"enabled":false,"limit":5,"timePeriodInSeconds":60}},"registrationConfiguration":{},"ssoConfiguration":{"deviceTrustTimeToLiveInSeconds":31536000},"state":"Active","userDeletePolicy":{"unverified":{"enabled":true,"numberOfDaysToRetain":14}},"usernameConfiguration":{"unique":{"enabled":false,"numberOfDigits":5,"separator":"#","strategy":"OnCollision"}}}	\N	\N	\N	\N	0d2d87e8-d865-425c-af44-64fbaaa4339e	092dbedc-30af-4149-9c61-b578f2c72f59	1666539851785	1693912086647	\N	\N	\N	\N	d858ba9e-9148-4a9e-ad80-5a64bdccf99d	\N	\N	Default	\N	\N	\N	\N	76927a87-9056-48db-8f95-8014d45ddd76	97b018c4-ce66-486f-98e7-c73457586b7f	\N	\N	\N	76927a87-9056-48db-8f95-8014d45ddd76
\.


--
-- Data for Name: themes; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.themes (id, data, insert_instant, last_update_instant, name) FROM stdin;
75a068fd-e94b-451a-9aeb-3ddb9a3b5987	{}	1666539852785	1666539852785	FusionAuth
97b018c4-ce66-486f-98e7-c73457586b7f	{"data":{},"defaultMessages":"#\\n# Copyright (c) 2019-2021, FusionAuth, All Rights Reserved\\n#\\n# Licensed under the Apache License, Version 2.0 (the \\"License\\");\\n# you may not use this file except in compliance with the License.\\n# You may obtain a copy of the License at\\n#\\n#   http://www.apache.org/licenses/LICENSE-2.0\\n#\\n# Unless required by applicable law or agreed to in writing,\\n# software distributed under the License is distributed on an\\n# \\"AS IS\\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,\\n# either express or implied. See the License for the specific\\n# language governing permissions and limitations under the License.\\n#\\n\\n#\\n# Text used on the page (inside the HTML). You can create new key-value pairs here and use them in the templates.\\n#\\nreturn-to-emaris=Return to e-Maris\\naccess-denied=Access denied\\naccount=Account\\nadd-two-factor=Add two-factor\\ncancel=Cancel\\ncaptcha-google-branding=This site is protected by reCAPTCHA and the Google <a href=\\"https://policies.google.com/privacy\\">Privacy Policy</a> and <a href=\\"https://policies.google.com/terms\\">Terms of Service</a> apply.\\nauthorized-not-registered=Registration is required to access this application and your account has not been registered for this application.  Please complete your registration and try again.\\nauthorized-not-registered-title=Registration Required\\nback-to-login=Click here to go back to the login form\\ncancel-link=Cancel link request\\nchild-registration-not-allowed=We cannot create an account for you. Your parent or guardian can create an account for you. Enter their email address and we will ask them to create your account.\\ncomplete=Complete\\ncomplete-registration=Complete registration\\nconfigure=Configure\\nconfigured=Configured\\ncreate-an-account=Create an account\\ncomplete-external-login=Complete login on your external device\\\\u2026\\ncompleted-link=You have successfully linked your %s account.\\ncompleted-links=You have successfully linked your %s and %s account.\\nconfirm=Confirm\\ndevice-form-title=Device login\\ndevice-login-complete=Successfully connected device\\ndevice-title=Connect Your Device\\ndisable=Disable\\ndone=Done\\ndont-have-an-account=Don't have an account?\\nedit=Edit\\nemail-verification-complete=Thank you. Your email has been verified.\\nemail-verification-complete-title=Email verification complete\\nemail-verification-form=Complete the form to request a new verification email.\\nemail-verification-form-title=Email verification\\nemail-verification-sent=We have sent an email to %s with your verification code. Follow the instructions in the email to verify your email address.\\nemail-verification-sent-title=Verification sent\\nemail-verification-required-title=Verification required\\nemail-verification-required-send-another=Send me another email\\nenabled=Enabled\\nforgot-password=Forgot your password? Type in your email address in the form below to reset your password.\\nforgot-password-email-sent=We've sent you an email containing a link that will allow you to reset your password. Once you receive the email follow the instructions to change your password.\\nforgot-password-email-sent-title=Email sent\\nforgot-password-title=Forgot password\\nforgot-your-password=Forgot your password?\\nhelp=Help\\ninstructions=Instructions\\nip-address=IP address\\nlink-to-existing-user=Link to an existing user\\nlink-to-new-user=Create a new user\\nlogin=Welcome to e-Maris.\\nlogin-button=Login\\nlogin-cancel-link=Or, cancel the link request.\\nlogout=Logout\\nlogging-out=Logging out\\\\u2026\\nlogout-title=Logging out\\nmulti-factor-configuration=Multi-Factor configuration\\nnext=Next\\nnot-configured=Not configured\\nnote=Note:\\nor=Or\\nparent-notified=We've sent an email to your parent. They can setup an account for you once they receive it.\\nparent-notified-title=Parent notified\\npassword-alpha-constraint=Must contain at least one non-alphanumeric character\\npassword-case-constraint=Must contain both upper and lower case characters\\npassword-change-title=Update your password\\npassword-changed=Your password has been updated successfully.\\npassword-changed-title=Password updated\\npassword-constraints-intro=Password must meet the following constraints:\\npassword-length-constraint=Must be between %s and %s characters in length\\npassword-number-constraint=Must contain at least one number\\npassword-previous-constraint=Must not match the previous %s passwords\\npasswordless-login=Passwordless login\\npasswordless-button-text=Login with a magic link\\npending-link-info=You have successfully authenticated using %s.\\npending-link-next-step=To complete this request you may link to an existing user or create a new user.\\npending-link-next-step-no-registration=To complete this request you must link to an existing user.\\npending-link-login-to-complete=Login to complete your link to %s.\\npending-links-login-to-complete=Login to complete your link to %s and %s.\\npending-device-link=Continue to complete your link to %s.\\npending-device-links=Continue to complete your link to %s and %s.\\npending-link-register-to-complete=Register to complete your link to %s.\\npending-links-register-to-complete=Register to complete your link to %s and %s.\\nprofile=User Profile\\nprovide-parent-email=Provide parent email\\nregister-cancel-link=Or, cancel the link request.\\nregistration-verification-complete=Thank you. Your registration has been verified.\\nregistration-verification-complete-title=Registration verification complete\\nregistration-verification-form=Complete the form to request a new verification email.\\nregistration-verification-form-title=Registration verification\\nregistration-verification-sent=We have sent an email to %s with your verification code. Follow the instructions in the email to verify your registration address.\\nregistration-verification-sent-title=Verification sent\\nregistration-verification-required-title=Verification required\\nregistration-verification-required-send-another=Send me another email\\nreturn-to-login=Return to login\\nsend-another-code=Send another code\\nsend-code-to-phone=Send a code to your mobile phone\\nset-up=Set up\\nsms=SMS\\nsign-in-as-different-user=Sign in as a different user\\nstart-idp-link-title=Link your account\\ntwo-factor-challenge=Authentication challenge\\ntwo-factor-challenge-options=Authentication challenge\\ntwo-factor-recovery-code=Recovery code\\ntwo-factor-select-method=Didn't receive a code? Try another option\\ntwo-factor-use-one-of-n-recover-codes=Use one of your %d recovery codes\\ntrust-computer=Trust this computer for %s days\\nunauthorized=Unauthorized\\nunauthorized-message=You are not authorized to make this request.\\nunauthorized-message-blocked-ip=The owner of this website (%s) has blocked your IP address.\\nvalue=Value\\nwait-title=Complete login on your external device\\nwaiting=Waiting\\n\\n# Locale Specific separators, etc\\n#  - list separator - comma and a space\\nlistSeparator=,\\\\u0020\\npropertySeparator=:\\n\\n#\\n# Success messages displayed at the top of the page. These are hard-coded in the FusionAuth code and the keys cannot be changed. You can\\n# still change the values though.\\n#\\nsent-code=Code successfully sent\\n\\n\\n#\\n# Labels for form fields. You can change the key names to anything you like but ensure that you don't change the name of the form fields.\\n#\\nbirthDate=Birth date\\ncode=Enter your verification code\\nemail=Email\\nfirstName=First name\\nfullName=Full name\\nlastName=Last name\\nloginId=Username or Email address\\nmiddleName=Middle name\\nmobilePhone=Mobile phone\\npassword=Password\\npasswordConfirm=Confirm password\\nparentEmail=Parent's email\\nregister=Register\\nregister-step=Step %d of %d\\nremember-device=Keep me signed in\\nsend=Send\\nsubmit=Submit\\nupdate=Update\\nusername=Username\\nuserCode=Enter your user code\\nverify=Verify\\n\\n#\\n# Custom Registration forms. These must match the domain names.\\n#\\nregistration.preferredLanguages=Languages\\nregistration.timezone=Timezone\\nregistration.username=Username\\nuser.birthDate=Birthdate\\nuser.email=Email\\nuser.firstName=First name\\nuser.fullName=Full name\\nuser.imageUrl=Image URL\\nuser.lastName=Last name\\nuser.mobilePhone=Mobile phone\\nuser.middleName=Middle name\\nuser.password=Password\\nconfirm.user.password=Confirm password\\nuser.preferredLanguages=Languages\\nuser.timezone=Timezone\\nuser.username=Username\\n\\n#\\n# Self service account management\\n#\\ncancel-go-back=Cancel and go back\\nchange-password=Change password\\ndisable-instructions=Disable Two-factor\\ndisable-two-factor=Disable two factor\\nedit-profile=Edit profile\\nenable-instructions=Enable Two-factor\\nenable-two-factor=Enable two factor\\ngo-back=Go back\\nsend-one-time-code=Send a one-time code\\n\\n#\\n# Self service two-factor configuration\\n#\\nno-two-factor-methods-configured=No methods have been configured\\nselect-two-factor-method=Select a method\\ntwo-factor-authentication=Two-factor authentication\\ntwo-factor-method=Method\\n\\n# Form input place holders\\n{placeholder}two-factor-code=Enter the one-time code\\n\\n#\\n# Multi-factor configuration text\\n#\\nauthenticator=Authenticator app\\n\\n# Authenticator Enable / Disable\\nauthenticator-disable-step-1=Enter the code from your authenticator app in the verification code field below to disable this two-factor method.\\nauthenticator-enable-step-1=Open your authentication app and add your account by scanning the QR code to the right or by manually entering the Base32 encoded secret <strong>%s</strong>.\\nauthenticator-enable-step-2=Once you have completed the first step, enter the code from your authenticator app in the verification code field below.\\n\\n# Email Enable / Disable\\nemail-disable-step-1=To disable two-factor using email, click the button to send a one-time use code to %s. Once you receive the code, enter it in the form below.\\nemail-enable-step-1=To enable two-factor using email, enter an email address and click the button to send a one-time use code. Once you receive the code, enter it in the form below.\\n\\n# SMS Enable / Disable\\nsms-disable-step-1=To disable two-factor using SMS, click the button to send a one-time use code to %s. Once you receive the code, enter it in the form below.\\nsms-enable-step-1=Two enable two-factor using SMS, enter a mobile phone and click the button to send a one-time use code. Once you receive the code, enter it in the form below.\\n\\nauthenticator-configuration=Authenticator configuration\\nverification-code=Verification code\\n\\nmanage-two-factor=Manage two-factor\\ngo-back-to-send=Go back to send\\n\\n#\\n# Multi-factor configuration descriptions\\n#\\n{description}two-factor-authentication=Two-factor authentication adds an additional layer of security to your account by requiring more than just a password to login. Configure one or more methods to utilize during login.\\n{description}two-factor-methods-selection=A second step is required to complete sign in. Select one of the following methods to complete login.\\n{description}two-factor-recovery-code-note=If you no longer have access to the device or application to obtain a verification code, you may use a recovery code to disable this two-factor method. Warning, when you use a recovery code to disable any two-factor method, all two-factor methods will be removed and all of your recovery codes will be cleared.\\n{description}recovery-codes-1=Because this is the first time you have enable two-factor, we have generated you %d recovery codes. These codes will not be shown again, so record them right now and store them in safe place. These codes can be used to complete a two-factor login if you lose your device, and they can be used to disable two-factor authentication as well.\\n{description}recovery-codes-2=Once you have recorded the codes, click Done to return to two-factor management.\\n\\n{description}email-verification-required-change-email=Confirm your email address is correct and update it if you mis-typed it during registration. Updating your address will also send you a new email to the new address.\\n{description}email-verification-required=You must verify your email address before you continue.\\n{description}email-verification-required-non-interactive=Email verification is configured to be completed outside of this request. Once you have verified your email, retry this request.\\n\\n{description}registration-verification-required=You must verify your registration before you continue.\\n{description}registration-verification-required-non-interactive=Registration verification is configured to be completed outside of this request. Once you have verified your registration, retry this request.\\n\\n#\\n# Custom Self Service User form sections.\\n#\\n# - Names are optional, and if not provided they will be labeled 'Section 1', 'Section 2', etc.\\n# - The first section label will be omitted unless you specify a named label below. For your convenience, these\\n#   sections are configured below and commented out as 'Optionally name me!'.\\n#\\n# - By default, all section labels will be used for all tenants and all applications that are using this theme.\\n#\\n# - If you want a section title that is specific to a tenant in a user form, you may optionally prefix the key with the Tenant Id.\\n#\\n#   For example, if the tenant Id is equal to: cbeaf8fe-f4a7-4a27-9f77-c609f1b01856\\n#\\n#   [cbeaf8fe-f4a7-4a27-9f77-c609f1b01856]{self-service-form}2=Tenant specific label for section 2\\n#\\n\\n# {self-service-form}1=Optionally name me!\\n# {self-service-form}2=\\n\\n#\\n# Custom Admin User and Registration form sections.\\n#\\n# - Names are optional, and if not provided they will be labeled 'Section 1', 'Section 2', etc.\\n# - The first section label on the User and and Registration form in the admin UI will be omitted unless\\n#   you specify a named label below. For your convenience, these sections are configured below and commented out as 'Optionally name me!'.\\n#\\n# - By default, all section labels will be used for all tenants, and all applications respectively.\\n#\\n# - If you want a section title that is specific to a tenant in a user form, you may optionally prefix the key with the Tenant Id.\\n#\\n#   For example, if the tenant Id is equal to: cbeaf8fe-f4a7-4a27-9f77-c609f1b01856\\n#\\n#   [cbeaf8fe-f4a7-4a27-9f77-c609f1b01856]{user-form-section}2=Tenant specific label for section 2\\n#\\n# - If you want a section title that is specific to an Application in a registration form, you may optionally prefix the key with the Application Id.\\n#\\n#   For example, if the application Id is equal to: de2f91c7-c27a-4ad6-8be2-cfb36996cc89\\n#\\n#   [de2f91c7-c27a-4ad6-8be2-cfb36996cc89]{registration-form-section}2=Application specific label for section 2\\n\\n# {user-form-section}1=Optionally name me!\\n{user-form-section}2=Options\\n\\n# {registration-form-section}1=Optionally name me!\\n{registration-form-section}2=Options\\n\\n#\\n# Custom Admin User and Registration tooltips\\n#\\n{tooltip}registration.preferredLanguages=Select one or more preferred languages\\n{tooltip}user.preferredLanguages=Select one or more preferred languages\\n\\n#\\n# Custom Registration form validation errors.\\n#\\n[confirm]user.password=Confirm password\\n\\n#\\n# Default validation errors. Add custom messages by adding field messages.\\n# For example, to provide a custom message for a string field named user.data.companyName, add the\\n# following message key: [blank]user.data.companyName=Company name is required\\n#\\n[blank]=Required\\n[blocked]=Not allowed\\n[confirm]=Confirm\\n[configured]=Already configured\\n[couldNotConvert]=Invalid\\n[doNotMatch]=Values do not match\\n[duplicate]=Already exists\\n[empty]=Required\\n[inUse]=In use\\n[invalid]=Invalid\\n[missing]=Required\\n[mismatch]=Unexpected value\\n[notEmail]=Invalid email\\n[tooLong]=Too long\\n[tooShort]=Too short\\n\\n#\\n# Tooltips. You can change the key names and values to anything you like.\\n#\\n{tooltip}remember-device=Check this to stay signed into FusionAuth for the configured duration, do not select this on a public computer or when this device is shared with multiple users\\n{tooltip}trustComputer=Check this to bypass two-factor authentication for the configured duration, do not select this on a public computer or when this device is shared with multiple users\\n\\n\\n#\\n# Validation errors when forms are invalid. The format is [<error-code>]<field-name>. These are hard-coded in the FusionAuth code and the\\n# keys cannot be changed. You can still change the values though.\\n#\\n[invalid]applicationId=The provided application id is invalid.\\n[blank]code=Required\\n[invalid]code=Invalid code\\n[blank]email=Required\\n[blank]loginId=Required\\n[blank]methodId=Select a two-factor method\\n[blank]parentEmail=Required\\n[blank]password=Required\\n[blank]user_code=Required\\n[blank]captcha_token=Required\\n[invalid]captcha_token=Invalid challenge, try again\\n[cannotSend]method=A message cannot be sent to an authenticator\\n[disabled]method=Not enabled\\n[invalid]user_code=Invalid user code\\n[notEqual]password=Passwords don't match\\n[onlyAlpha]password=Password requires a non-alphanumeric character\\n[previouslyUsed]password=Password has been recently used\\n[requireNumber]password=Password requires at least one number\\n[singleCase]password=Password requires upper and lower case characters\\n[tooYoung]password=Password was changed too recently, try again later\\n[tooShort]password=Password does not meet the minimum length requirement\\n[tooLong]password=Password exceeds the maximum length requirement\\n[blank]passwordConfirm=Required\\n[missing]user.birthDate=Required\\n[couldNotConvert]user.birthDate=Invalid\\n[blank]user.email=Required\\n[blocked]user.email=Email address not allowed\\n[notEmail]user.email=Invalid email\\n[duplicate]user.email=An account already exists for that email\\n[inactive]user.email=An account already exists for that email but is locked. Contact the administrator for assistance\\n[blank]user.firstName=Required\\n[blank]user.fullName=Required\\n[blank]user.lastName=Required\\n[blank]user.middleName=Required\\n[blank]user.mobilePhone=Required\\n[invalid]user.mobilePhone=Invalid\\n[blank]user.parentEmail=Required\\n[blank]user.password=Required\\n[doNotMatch]user.password=Passwords don't match\\n[singleCase]user.password=Password must use upper and lowercase characters\\n[onlyAlpha]user.password=Password must contain a punctuation character\\n[requireNumber]user.password=Password must contain a number character\\n[tooShort]user.password=Password does not meet the minimum length requirement\\n[tooLong]user.password=Password exceeds the maximum length requirement\\n[blank]user.username=Required\\n[duplicate]user.username=An account already exists for that username\\n[inactive]user.username=An account already exists for that username but is locked. Contact the administrator for assistance\\n[mismatch]email=The requested email does not match where the code was sent\\n[mismatch]mobilePhone=The requested phone number does not match where the code was sent\\n[moderationRejected]registration.username=That username is not allowed. Please select a new one\\n[moderationRejected]user.username=That username is not allowed. Please select a new one\\n\\n#\\n# Breached password messages\\n#\\n# - ExactMatch        The password and email or username combination was found in a breached data set.\\n# - SubAddressMatch   The password and email or username, or email sub-address was found in a breached data set.\\n# - PasswordOnly      The password was found in a breached data set.\\n# - CommonPassword    The password is one of the most commonly known breached passwords.\\n#\\n[breachedExactMatch]password=This password was found in the list of vulnerable passwords, and is no longer secure. Select a different password.\\n[breachedExactMatch]user.password=This password was found in the list of vulnerable passwords, and is no longer secure. Select a different password.\\n[breachedSubAddressMatch]password=This password was found in the list of vulnerable passwords, and is no longer secure. Select a different password.\\n[breachedSubAddressMatch]user.password=This password was found in the list of vulnerable passwords, and is no longer secure. Select a different password.\\n[breachedPasswordOnly]password=This password was found in the list of vulnerable passwords, and is no longer secure. Select a different password.\\n[breachedPasswordOnly]user.password=This password was found in the list of vulnerable passwords, and is no longer secure. Select a different password.\\n[breachedCommonPassword]password=This password is a commonly known vulnerable password. Select a more secure password.\\n[breachedCommonPassword]user.password=This password is a commonly known vulnerable password. Select a more secure password.\\n\\n#\\n# Error messages displayed at the top of the page. These are always inside square brackets. These are hard-coded in the FusionAuth code and\\n# the keys cannot be changed. You can still change the values though.\\n#\\n[APIError]=An unexpected error occurred.\\n[AdditionalFieldsRequired]=Additional fields are required to complete your registration.\\n[EmailVerificationEmailUpdated]=Your email address has been updated and another email is on the way.\\n[EmailVerificationSent]=A verification email is on the way.\\n[EmailVerificationDisabled]=Email verification functionality is currently disabled. Contact your FusionAuth administrator for assistance.\\n[ErrorException]=An unexpected error occurred.\\n[ForgotPasswordDisabled]=Forgot password handling is not enabled. Please contact your system administrator for assistance.\\n[IdentityProviderDoesNotSupportRedirect]=This identity provider does not support this redirect workflow.\\n[InvalidChangePasswordId]=Your password reset code has expired or is invalid. Please retry your request.\\n[InvalidEmail]=FusionAuth was unable to find a user with that email address.\\n[InvalidIdentityProviderId]=Invalid request. Unable to handle the identity provider login. Please contact your system administrator or support for assistance.\\n[InvalidLogin]=Invalid login credentials.\\n[InvalidPasswordlessLoginId]=Your link has expired or is invalid. Please retry your request.\\n[InvalidVerificationId]=Sorry. The request contains an invalid or expired verification Id. You may need to request another verification to be sent.\\n[InvalidPendingIdPLinkId]=Your link has expired or is invalid. Please retry your login request.\\n[LoginPreventedException]=Your account has been locked.\\n[LoginPreventedExceptionTooManyTwoFactorAttempts]=You have exceeded the number of allowed attempts. Your account has been locked.\\n[MissingApplicationId]=An applicationId is required and is missing from the request.\\n[MissingChangePasswordId]=A changePasswordId is required and is missing from the request.\\n[MissingEmail]=Your email address is required and is missing from the request.\\n[MissingEmailAddressException]=You must have an email address to utilize passwordless login.\\n[MissingPendingIdPLinkId]=You must first log into a 3rd party identity provider to complete an account link.\\n[MissingPKCECodeVerifier]=The code_verifier could not be determined, this request likely did not originate from FusionAuth. Unable to complete this login request.\\n[MissingVerificationId]=A verification Id was not sent in the request.\\n[NotFoundException]=The requested OAuth configuration is invalid.\\n[OAuthv1TokenMismatch]=Invalid request. The token provided on the OAuth v1 callback did not match the one sent during authorization. Unable to handle the identity provider login. Please contact your system administrator or support for assistance.\\n[Oauthv2Error]=An invalid request was made to the Authorize endpoint. %s\\n[PasswordlessRequestSent]=An email is on the way.\\n[PasswordChangeRequired]=You must change your password in order to continue.\\n[PasswordChangeReasonExpired]=Your password has expired and must be changed.\\n[PasswordChangeReasonBreached]=Your password was found in the list of vulnerable passwords and must be changed.\\n[PasswordChangeReasonValidation]=Your password does not meet password validation rules and must be changed.\\n[PasswordlessDisabled]=Passwordless login is not currently configured.\\n[PushTwoFactorFailed]=Failed to send a verification code using the configured push service.\\n[RegistrationVerificationSent]=A verification email is on the way.\\n[SSOSessionDeletedOrExpired]=You have been logged out of FusionAuth.\\n[TenantIdRequired]=FusionAuth is unable to determine which tenant to use for this request. Please add the tenantId to the URL as a request parameter.\\n[TwoFactorTimeout]=You did not complete the two factor challenge in time. Please complete login again.\\n[UserAuthorizedNotRegisteredException]=Your account has not been registered for this application.\\n[UserExpiredException]=Your account has expired. Please contact your system administrator.\\n[UserLockedException]=Your account has been locked. Please contact your system administrator.\\n[UserUnauthenticated]=Oops. It looks like you've gotten here by accident. Please return to your application and log in to begin the authorization sequence.\\n[ExternalAuthenticationExpired]=Your external authentication request has expired, please re-attempt authentication.\\n\\n# External authentication errors\\n# - Some of these errors are development time issues. But it is possible they could be shown to an end user depending upon your configuration.\\n[ExternalAuthenticationException]AppleIdToken=The id_token returned from Apple is invalid or cannot be verified. Unable to complete this login request.\\n[ExternalAuthenticationException]AppleTokenEndpoint=A request to the Apple Token API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]AppleUserObject=Failed to read the user details provided by Apple. Unable to complete this login request.\\n[ExternalAuthenticationException]EpicGamesAccount=A request to the Epic Games Account API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]EpicGamesToken=A request to the Epic Games Token API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]FacebookAccessToken=A request to the Facebook Access Token Info API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]FacebookMe=A request to the Facebook Me API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]FacebookMePicture=A request to the Facebook Picture API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]GoogleToken=A request to the Google Token API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]GoogleTokenInfo=A request to the Google Token Info API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]InvalidApplication=The requested application does not exist or is currently disabled. Unable to complete this login request.\\n[ExternalAuthenticationException]InvalidIdentityProviderId=The requested identityProviderId is invalid. Unable to complete this login request.\\n[ExternalAuthenticationException]LinkedInEmail=A request to the LinkedIn Email API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]LinkedInMe=A request to the LinkedIn Me API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]LinkedInToken=A request to the LinkedIn Token API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]MissingEmail=An email address was not provided for the user. This account cannot be used to login, unable to complete this login request.\\n[ExternalAuthenticationException]MissingUniqueId=A unique identifier was not provided for the user. This account cannot be used to login, unable to complete this login request.\\n[ExternalAuthenticationException]MissingUser=An authentication request cannot be completed because the user that started the request no longer exists. This account cannot be used to login, unable to complete this login request.\\n[ExternalAuthenticationException]MissingUsername=A username was not returned by the identity provider. This account cannot be used login, unable to complete this login request.\\n[ExternalAuthenticationException]NintendoToken=A request to the Nintendo Token API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]OpenIDConnectToken=A request to the OpenID Connect Token API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]OpenIDConnectUserinfo=A request to the OpenID Connect Userinfo API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]SAMLIdPInitiatedIssuerVerificationFailed=The SAML issuer failed validation. Unable to complete this login request.\\n[ExternalAuthenticationException]SAMLIdPInitiatedResponseSolicited=The SAML AuthNResponse contained an InResponseTo attribute. In an IdP Initiated Login this is un-expected.\\n[ExternalAuthenticationException]SAMLResponse=The SAML AuthnResponse object could not be parsed or verified. Unable to complete this login request.\\n[ExternalAuthenticationException]SAMLResponseAudienceNotBeforeVerificationFailed=The SAML audience is not yet available to be confirmed. Unable to complete this request.\\n[ExternalAuthenticationException]SAMLResponseAudienceNotOnOrAfterVerificationFailed=The SAML audience is no longer eligible to be confirmed. Unable to complete this request.\\n[ExternalAuthenticationException]SAMLResponseAudienceVerificationFailed=The SAML audience failed validation. Unable to complete this login request.\\n[ExternalAuthenticationException]SAMLResponseDestinationVerificationFailed=The SAML destination failed validation. Unable to complete this login request.\\n[ExternalAuthenticationException]SAMLResponseStatus=The SAML AuthnResponse status indicated the request has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]SAMLResponseSubjectNoOnOrAfterVerificationFailed=The SAML subject is no longer eligible to be confirmed. Unable to complete this login request.\\n[ExternalAuthenticationException]SAMLResponseSubjectNotBeforeVerificationFailed=The SAML subject is not yet available to be confirmed. Unable to complete this login request.\\n[ExternalAuthenticationException]SAMLResponseUnexpectedOrReplayed=The SAML response not requested or has already been processed. Unable to complete this login request.\\n[ExternalAuthenticationException]SAMLResponseUnsolicited=The SAML response was un-solicited. Unable to complete this login request.\\n[ExternalAuthenticationException]SonyPSNToken=A request to the Sony PlayStation Network Token API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]SonyPSNUserInfo=A request to the Sony PlayStation Network User Info API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]SteamPlayerSummary=A request to the Steam Player summary API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]SteamToken=A request to the Steam Token API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]TwitchToken=A request to the Twitch Token API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]TwitchUserInfo=A request to the Twitch User Info API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]TwitterAccessToken=A request to the Twitter Access Token API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]TwitterCallbackUnconfirmed=The Twitter callback URL has not been confirmed. Unable to complete this login request.\\n[ExternalAuthenticationException]TwitterRequestToken=A request to the Twitter Request Token API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]TwitterVerifyCredentials=A request to Twitter Verify Credentials API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]UserDoesNotExistByEmail=You must first create a user with the same email address in order to complete this login request.\\n[ExternalAuthenticationException]UserDoesNotExistByUsername=You must first create a user with the same username in order to complete this login request.\\n[ExternalAuthenticationException]XboxSecurityTokenService=A request to the Xbox Security Token Service API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]XboxToken=A request to the Xbox Token API has failed. Unable to complete this login request.\\n[ExternalAuthenticationException]XboxUserInfo=A request to the Xbox User Info API has failed. Unable to complete this login request.\\n\\n# OAuth token endpoint and callback errors\\n[TokenExchangeFailed]=An unexpected error occurred while completing your login attempt. Please attempt the request again.\\n[TokenExchangeException]=We were unable to complete your login attempt. Please attempt the request again.\\n\\n# Webhook transaction failure\\n[WebhookTransactionException]=One or more webhooks returned an invalid response or were unreachable. Based on your transaction configuration, your action cannot be completed.\\n\\n# Self Service\\n[SelfServiceFormNotConfigured]=Configuration is incomplete. The FusionAuth administrator must configure a form for this application.\\n[SelfServiceUserNotRegisteredException]=You are not registered for this application. Not all features will be available.\\n[TwoFactorAuthenticationMethodDisabled]=Two-factor authentication has been disabled\\n[TwoFactorAuthenticationMethodEnabled]=Two-factor authentication has been enabled\\n[TwoFactorSendFailed]=A request to send a one-time code for two-factor configuration code has failed.\\n[TwoFactorMessageSent]=A one-time use code was sent\\n\\n# General messages\\n[UserWillBeLoggedIn]=You will be logged in after you complete this request.\\n\\n# updates\\nlink-count-exceeded-pending-logout=You have already linked to %s and no additional links are allowed.\\nlogged-in-as=You are logged in as %s.\\nclick-here-to-logout=Click here to logout\\nlogout-and-continue=Logout and continue…\\ndevice-link-count-exceeded-pending-logout=You are logged in as %s. No additional links may be made to %s.\\nlink-count-exceeded-next-step=To continue, click the button below. You will be logged out and then redirected here to link to an existing user or create a new user.\\nlink-count-exceeded-next-step-no-registration=To continue, click the button below. You will be logged out and then redirect here to link to an existing user.\\n[LinkCountExceeded]=You have reached the configured link limit of %d for this identity provider.\\ndevice-link-count-exceeded-next-step=To continue, click the button below. You will be logged out and then redirected here to continue the device login.\\ndevice-logged-in-as-not-you=You are logged in as %s. If you continue, the device login will be completed without an additional prompt. If this is not you, click logout before continuing.","localizedMessages":{"fr":"#\\n# Copyright (c) 2019-2021, FusionAuth, All Rights Reserved\\n#\\n# Licensed under the Apache License, Version 2.0 (the \\"License\\");\\n# you may not use this file except in compliance with the License.\\n# You may obtain a copy of the License at\\n#\\n#   http://www.apache.org/licenses/LICENSE-2.0\\n#\\n# Unless required by applicable law or agreed to in writing,\\n# software distributed under the License is distributed on an\\n# \\"AS IS\\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,\\n# either express or implied. See the License for the specific\\n# language governing permissions and limitations under the License.\\n#\\n\\n#\\n# Text used on the page (inside the HTML). You can create new key-value pairs here and use them in the templates.\\n#\\nreturn-to-emaris=Retour à e-Maris\\nreturn-to-login=Retour à la connexion\\nchild-registration-not-allowed=Nous ne pouvons pas vous créer de compte, seul vos responsables légaux le peuvent. Entrez leurs adresses email, et nous leur demanderons de créer votre compte.\\ncomplete-registration=Achever votre inscription\\ncreate-an-account=S'inscrire\\ndont-have-an-account=Pas encore inscrit ?\\nemail-verification-complete=Merci. Votre adresse email a été vérifiée.\\nemail-verification-complete-title=Vérification de l'adresse email achevée\\nemail-verification-form=Complétez le formulaire afin de demander un nouveau courriel de vérification.\\nemail-verification-form-title=Vérification de l'adresse email\\nemail-verification-sent=Nous avons envoyé un courriel à l'adresse %s avec votre code de vérification. Veuillez suivre les instructions du courriel afin de confirmer votre adresse email.\\nemail-verification-sent-title=Vérification envoyée\\nforgot-password=Mot de passe oublié ? Veuillez entrer votre adresse email dans le formulaire ci-dessous afin de réinitialiser votre mot de passe.\\nforgot-password-email-sent=Nous vous avons envoyé un courriel contenant un lien vous permettant de réinitialiser votre mot de passe. Une fois reçu, suivez les instructions afin de changer votre mot de passe.\\nforgot-password-email-sent-title=Courriel envoyé\\nforgot-password-title=Mot de passe oublié\\nforgot-your-password=Mot de passe oublié ?\\nhelp=Aide\\nlogin=Connexion\\nlogin-button=Connexion\\nlogging-out=Déconnexion\\\\u2026\\nlogout-title=Déconnexion\\nnext=Suivant\\nor=Ou\\nparent-notified=Nous avons envoyé un courriel à vos responsables légaux. Ces derniers peuvent créer un compte pour vous une fois le courriel reçu.\\nparent-notified-title=Responsables légaux contactés\\npassword-alpha-constraint=Doit contenir au moins un caractère spécial\\npassword-case-constraint=Doit contenir des minuscules et des majuscules\\npassword-change-title=Modifier votre mot de passe\\npassword-changed=Votre mot de passe a été modifié avec succès.\\npassword-changed-title=Mot de passe modifié\\npassword-constraints-intro=Votre mot de passe doit répondre aux contraintes suivantes:\\npassword-length-constraint=Doit être d'une longueur entre %s et %s caractères\\npassword-number-constraint=Doit contenir au moins un chiffre\\npassword-previous-constraint=Doit être différent des %s mots de passe précédents\\npasswordless-login=Connexion sans mot de passe\\npasswordless-button-text=Connexion avec un lien magique\\nprovide-parent-email=Fournir l'adresse email des responsables légaux\\nregistration-verification-complete=Merci. Votre inscription a été vérifiée.\\nregistration-verification-complete-title=Vérification de l'inscription finalisée\\nregistration-verification-form=Complétez le formulaire afin de demander un nouveau courriel de vérification.\\nregistration-verification-form-title=Vérification de l'inscription\\nregistration-verification-sent=Nous avons envoyé un courriel à l'adresse %s contenant votre code de vérification. Suivez les instructions contenues dans le courriel afin de vérifier votre adresse email.\\nregistration-verification-sent-title=Vérification envoyée\\nsend-another-code=Envoyer un autre code\\nsend-code-to-phone=Envoyer un code à votre téléphone portable\\nsign-in-as-different-user=Connexion avec un autre utilisateur\\ntwo-factor-challenge=Challenge de l'authentification à deux étapes\\ntrust-computer=Faire confiance à cet ordinateur pour %s jours\\n\\n# Locale Specific separators, etc\\n#  - list separator - comma and a space\\n\\n#\\n# Success messages displayed at the top of the page. These are hard-coded in the FusionAuth code and the keys cannot be changed. You can\\n# still change the values though.\\n#\\nsent-code=Code envoyé avec succès\\n\\n\\n#\\n# Labels for form fields. You can change the key names to anything you like but ensure that you don't change the name of the form fields.\\n#\\nbirthDate=Date de naissance\\ncode=Entrez votre code de vérification\\nemail=Adresse email\\nfirstName=Prénom\\nfullName=Nom complet\\nlastName=Nom de famille\\nloginId=Nom d'utilisateur ou Adresse email\\nmiddleName=Deuxième prénom\\nmobilePhone=Téléphone mobile\\npassword=Mot de passe\\npasswordConfirm=Confirmer le mot de passe\\nparentEmail=Adresses email des responsables légaux\\nregister=S'inscrire\\nsend=Envoyer\\nsubmit=Envoyer\\nusername=Nom d'utilisateur\\nverify=Vérifier\\n\\n#\\n# Custom Registration forms. These must match the domain names.\\n#\\n\\n#\\n# Self service account management\\n#\\n\\n#\\n# Self service two-factor configuration\\n#\\n\\n# Form input place holders\\n\\n#\\n# Multi-factor configuration text\\n#\\n\\n# Authenticator Enable / Disable\\n\\n# Email Enable / Disable\\n\\n# SMS Enable / Disable\\n\\n\\n\\n#\\n# Multi-factor configuration descriptions\\n#\\n\\n\\n\\n#\\n# Custom Self Service User form sections.\\n#\\n# - Names are optional, and if not provided they will be labeled 'Section 1', 'Section 2', etc.\\n# - The first section label will be omitted unless you specify a named label below. For your convenience, these\\n#   sections are configured below and commented out as 'Optionally name me!'.\\n#\\n# - By default, all section labels will be used for all tenants and all applications that are using this theme.\\n#\\n# - If you want a section title that is specific to a tenant in a user form, you may optionally prefix the key with the Tenant Id.\\n#\\n#   For example, if the tenant Id is equal to: cbeaf8fe-f4a7-4a27-9f77-c609f1b01856\\n#\\n#   [cbeaf8fe-f4a7-4a27-9f77-c609f1b01856]{self-service-form}2=Tenant specific label for section 2\\n#\\n\\n# {self-service-form}1=Optionally name me!\\n# {self-service-form}2=\\n\\n#\\n# Custom Admin User and Registration form sections.\\n#\\n# - Names are optional, and if not provided they will be labeled 'Section 1', 'Section 2', etc.\\n# - The first section label on the User and and Registration form in the admin UI will be omitted unless\\n#   you specify a named label below. For your convenience, these sections are configured below and commented out as 'Optionally name me!'.\\n#\\n# - By default, all section labels will be used for all tenants, and all applications respectively.\\n#\\n# - If you want a section title that is specific to a tenant in a user form, you may optionally prefix the key with the Tenant Id.\\n#\\n#   For example, if the tenant Id is equal to: cbeaf8fe-f4a7-4a27-9f77-c609f1b01856\\n#\\n#   [cbeaf8fe-f4a7-4a27-9f77-c609f1b01856]{user-form-section}2=Tenant specific label for section 2\\n#\\n# - If you want a section title that is specific to an Application in a registration form, you may optionally prefix the key with the Application Id.\\n#\\n#   For example, if the application Id is equal to: de2f91c7-c27a-4ad6-8be2-cfb36996cc89\\n#\\n#   [de2f91c7-c27a-4ad6-8be2-cfb36996cc89]{registration-form-section}2=Application specific label for section 2\\n\\n# {user-form-section}1=Optionally name me!\\n\\n# {registration-form-section}1=Optionally name me!\\n\\n#\\n# Custom Admin User and Registration tooltips\\n#\\n\\n#\\n# Custom Registration form validation errors.\\n#\\n\\n#\\n# Default validation errors. Add custom messages by adding field messages.\\n# For example, to provide a custom message for a string field named user.data.companyName, add the\\n# following message key: [blank]user.data.companyName=Company name is required\\n#\\n\\n#\\n# Tooltips. You can change the key names and values to anything you like.\\n#\\n{tooltip}trustComputer=Ne pas sélectionner si vous utilisez un ordinateur public. Faire confiance à cet ordinateur vous permettra de contourner l'authentification à deux étapes pour la durée configurée\\n\\n\\n#\\n# Validation errors when forms are invalid. The format is [<error-code>]<field-name>. These are hard-coded in the FusionAuth code and the\\n# keys cannot be changed. You can still change the values though.\\n#\\n[invalid]applicationId=L'id d'application fournie n'est pas valide\\n[blank]code=Obligatoire\\n[invalid]code=Code invalide\\n[blank]email=Obligatoire\\n[blank]loginId=Obligatoire\\n[blank]parentEmail=Obligatoire\\n[blank]password=Obligatoire\\n[notEqual]password=Les mots de passe ne concordent pas\\n[onlyAlpha]password=Le mot de passe doit contenir un caractère spécial\\n[previouslyUsed]password=Le mot de passe a été utilisé récemment\\n[requireNumber]password=Le mot de passe doit contenir au moins un chiffre\\n[singleCase]password=Le mot de passe doit contenir des majuscules et des minuscules\\n[tooYoung]password=Le mot de passe a été changé trop récemment, veuillez réessayer ultérieurement\\n[tooShort]password=Le mot de passe est trop court\\n[tooLong]password=Le mot de passe est trop long\\n[blank]passwordConfirm=Obligatoire\\n[missing]user.birthDate=Obligatoire\\n[couldNotConvert]user.birthDate=Invalide\\n[blank]user.email=Obligatoire\\n[notEmail]user.email=Adresse email invalide\\n[duplicate]user.email=Il y a déjà un compte associé à cette adresse email\\n[inactive]user.email=Il y a déja un compte associé à cette adresse email, mais ce dernier est verrouillé. Veuillez contacter l'administrateur si vous avez besoin d'aide\\n[blank]user.firstName=Obligatoire\\n[blank]user.fullName=Obligatoire\\n[blank]user.lastName=Obligatoire\\n[blank]user.middleName=Obligatoire\\n[blank]user.mobilePhone=Obligatoire\\n[invalid]user.mobilePhone=Obligatoire\\n[blank]user.parentEmail=Obligatoire\\n[blank]user.password=Obligatoire\\n[doNotMatch]user.password=Les mots de passe ne concordent pas\\n[singleCase]user.password=Le mot de passe doit contenir des majuscules et des minuscules\\n[onlyAlpha]user.password=Le mot de passe doit contenir un caractère de ponctuation\\n[requireNumber]user.password=Le mot de passe doit contenir au moins un chiffre\\n[tooShort]user.password=Le mot de passe est trop court\\n[tooLong]user.password=Le mot de passe est trop long\\n[blank]user.username=Obligatoire\\n[duplicate]user.username=Il y a déjà un compte associé à ce nom d'utilisateur\\n[inactive]user.username=Il y a déjà un compte associé à ce nom d'utilisateur, mais ce dernier est verouillé. Veuillez contacter l'administrateur si vous avez besoin d'aide\\n[moderationRejected]user.username=Ce nom d'utilisateur n'est pas autorisé. Veuillez en choisir un autre\\n\\n#\\n# Breached password messages\\n#\\n# - ExactMatch        The password and email or username combination was found in a breached data set.\\n# - SubAddressMatch   The password and email or username, or email sub-address was found in a breached data set.\\n# - PasswordOnly      The password was found in a breached data set.\\n# - CommonPassword    The password is one of the most commonly known breached passwords.\\n#\\n\\n#\\n# Error messages displayed at the top of the page. These are always inside square brackets. These are hard-coded in the FusionAuth code and\\n# the keys cannot be changed. You can still change the values though.\\n#\\n[APIError]=Une erreur inattendue est survenue.\\n[AdditionalFieldsRequired]=Certains champs doivent encore être renseignés afin de compléter votre inscription.\\n[EmailVerificationDisabled]=La fonctionalité de vérification d'adresse email est actuellement désactivée. Veuillez contacter votre administrateur FusionAuth si vous avez besoin d'aide.\\n[ErrorException]=Une erreur inattendue est survenue.\\n[ForgotPasswordDisabled]=Le traitement des requêtes de mot de passe oublié est actuellent désactivé. Veuillez contacter votre administrateur système si vous avez besoin d'aide\\n[InvalidChangePasswordId]=Votre code de réinitialisation du mot de passe a expiré ou n'est pas valide.\\n[InvalidEmail]=FusionAuth n'a pas pu trouver un utilisateur associé à cette adresse email.\\n[InvalidIdentityProviderId]=Requête invalide. Impossible de traiter la connexion au fournisseur d'identité. Veuillez contacter votre administrateur système ou l'assistance si vous avez besoin d'aide.\\n[InvalidLogin]=Identifiants de connexion invalides.\\n[InvalidVerificationId]=Excusez-nous. La requête contient un identifant de vérification invalide. Veuillez demander l'envoi d'une nouvelle vérification.\\n[LoginPreventedException]=Votre compte a été verrouillé. Veuillez contacter votre administrateur système.\\n[MissingApplicationId]=Il manque un champ applicationId à la requête.\\n[MissingChangePasswordId]=Le champ changePasswordId est requis et n'a pas été fourni par la requête.\\n[MissingEmail]=Votre adresse email est requise et n'a pas été fournie par la requête.\\n[MissingEmailAddressException]=Vous devez avoir renseigné une adresse email afin de vous connecter sans mot de passe.\\n[MissingVerificationId]=Aucun identifiant de vérification n'a été fourni par la requête.\\n[NotFoundException]=La configuration OAuth demandée est invalide.\\n[OAuthv1TokenMismatch]=Invalid request. The token provided on the OAuth v1 callback did not match the one sent during authorization. Unable to handle the identity provider login. Please contact your system administrator or support for assistance.\\n[Oauthv2Error]=L'endpoint d'autorisation a reçu une requête invalide. %s\\n[PasswordlessRequestSent]=Un courriel vous a été envoyé\\n[PasswordChangeRequired]=Vous devez changer votre mot de passe afin de continuer.\\n[PasswordlessDisabled]=La connexion sans mot de passe n'est actuellement pas configurée.\\n[PushTwoFactorFailed]=L'envoi d'un code de vérification via le service push configuré n'a pas réussi.\\n[TenantIdRequired]=FusionAuth est incapable de déterminer l'entité à utiliser pour cette requête. Veuillez ajouter l'identifiant de l'entité (tenantId) à l'URL en tant que paramêtre.\\n[TwoFactorTimeout]=Vous n'avez pas complété la connexion à deux étapes dans les temps. Veuillez vous reconnecter.\\n[UserExpiredException]=Votre compte a expiré. Veuillez contacter votre administrateur système.\\n[UserLockedException]=Votre compte a été verrouillé. Veuillez contacter l'administrateur système.\\n[UserUnauthenticated]=Oups. Il semblerait que vous ayez atterri ici par accident. Veuillez retourner à votre application et vous connecter afin de commencer la séquence d'autorisation.\\n\\n# External authentication errors\\n# - Some of these errors are development time issues. But it is possible they could be shown to an end user depending upon your configuration.\\n\\n# OAuth token endpoint and callback errors\\n\\n# Webhook transaction failure\\n\\n# Self Service\\n\\n# General messages"},"stylesheet":"@import \\"https://res.cloudinary.com/finnhvman/raw/upload/matter/matter-0.2.2.min.css\\";\\n\\nhtml {\\n  background: #001529;\\n}\\nbody {\\n  --bg: #162f45;\\n  background: var(--bg);\\n  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'PingFang SC', 'Ubuntu', 'Hiragino Sans GB', 'Microsoft YaHei', 'Helvetica Neue', Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol';\\n  font-size: 14px;\\n  color: #595959;\\n  font-weight: 300;\\n  margin-left: 80px;\\n  width: calc(100vw - 80px);\\n  min-width: 750px;\\n}\\n\\nbody main {\\n  padding:0px ;\\n}\\n\\n@media (max-height: 850px) {\\n\\n  #logo { display: none; }\\n  .panel { top: 50px !important; } \\n}\\n\\n\\n.page-body {\\n    display: flex;\\n    flex-direction: column;\\n    height: 100vh;\\n    justify-content: center;\\n}\\n\\n.app-header {\\n  display: none !important;\\n}\\n\\n.panel {\\n    margin:0px;\\n    border-radius: 7px; \\n    width: 60%;\\n    height: auto;\\n}\\nbody>main main.page-body {\\n  padding: 0 0 0 0;\\n}\\n\\n.panel h2 {\\n  font-weight: 300;\\n}\\n\\ninput[type=\\"text\\"] {\\n  font-weight: 400;\\n  letter-spacing: 1.3px;\\n}\\n\\ninput[type=\\"password\\"] {\\n  font-weight: 400;\\n  letter-spacing: 1.3px;\\n}\\n\\n.container {\\n  width: 65%;\\n  display: flex;\\n  flex-direction: column;\\n  align-items: center;\\n}\\n\\n.central-modal {\\n  width: 80%;\\n  border: 1px solid yellow;\\n}\\n\\n.button {\\n  font-weight: 400 !important;\\n}\\n\\n.auth-bottom {\\n  display: flex; \\n  \\n}\\n\\n.auth-bottom-left {\\n  flex-grow: 1;      \\n}\\n\\n.auth-bottom-right {\\n  line-height: 1.8;\\n}\\n\\n.locale-selector {\\n  width: 60%;\\n}\\n\\n.actions-row {\\n  width: 100%;\\n  height: 30px;\\n}\\n\\n.outside-panel-link {\\n    float: right;\\n  padding-left: 10px;\\n  padding-right: 10px;\\n    padding-top: 5px;\\n}\\n\\n.outside-panel-link a {\\n    color: white !important;\\n}\\n\\n.left { float: left; }\\n.right {float: right; }\\n\\n.matter-textfield-outlined {\\n    --matter-helper-theme: var(--ems-theme-primary);\\n    --matter-helper-safari1: rgba(var(--matter-onsurface-rgb, 0, 0, 0), 0.38);\\n    --matter-helper-safari2: rgba(var(--matter-onsurface-rgb, 0, 0, 0), 0.6);\\n    --matter-helper-safari3: rgba(var(--matter-onsurface-rgb, 0, 0, 0), 0.87);\\n    position: relative;\\n    display: inline-block;\\n    padding-top: 6px;\\n    font-family: var(--matter-font-family, \\"Roboto\\", \\"Segoe UI\\", BlinkMacSystemFont, system-ui, -apple-system);\\n    font-size: 16px;\\n    line-height: 1.5;\\n}\\n\\n/* Input, Textarea */\\n.matter-textfield-outlined > input,\\n.matter-textfield-outlined > textarea {\\n    box-sizing: border-box;\\n    margin: 0;\\n    border-style: solid;\\n    border-width: 1px;\\n    border-color: transparent var(--matter-helper-safari2) var(--matter-helper-safari2);\\n    border-radius: 4px;\\n    padding: 15px 13px 15px;\\n    width: 100%;\\n    height: inherit;\\n    color: rgba(var(--matter-onsurface-rgb, 0, 0, 0), 0.87);\\n    -webkit-text-fill-color: currentColor; /* Safari */\\n    background-color: transparent;\\n    box-shadow: inset 1px 0 transparent, inset -1px 0 transparent, inset 0 -1px transparent;\\n    font-family: inherit;\\n    font-size: inherit;\\n    line-height: inherit;\\n    caret-color: var(--matter-helper-theme);\\n    transition: border 0.2s, box-shadow 0.2s;\\n}\\n\\n.matter-textfield-outlined > input:not(:focus):placeholder-shown,\\n.matter-textfield-outlined > textarea:not(:focus):placeholder-shown {\\n    border-top-color: var(--matter-helper-safari2);\\n}\\n\\n/* Span */\\n.matter-textfield-outlined > input + span,\\n.matter-textfield-outlined > textarea + span {\\n    position: absolute;\\n    top: 0;\\n    left: 0;\\n    display: flex;\\n    width: 100%;\\n    max-height: 100%;\\n    color: rgba(var(--matter-onsurface-rgb, 0, 0, 0), 0.5);\\n    font-size: 75%;\\n    line-height: 15px;\\n    cursor: text;\\n    transition: color 0.2s, font-size 0.2s, line-height 0.2s;\\n}\\n\\n.matter-textfield-outlined > input:not(:focus):placeholder-shown + span,\\n.matter-textfield-outlined > textarea:not(:focus):placeholder-shown + span {\\n    font-size: inherit;\\n    line-height: 68px;\\n}\\n\\n/* Corners */\\n.matter-textfield-outlined > input + span::before,\\n.matter-textfield-outlined > input + span::after,\\n.matter-textfield-outlined > textarea + span::before,\\n.matter-textfield-outlined > textarea + span::after {\\n    content: \\"\\";\\n    display: block;\\n    box-sizing: border-box;\\n    margin-top: 6px;\\n    border-top: solid 1px var(--matter-helper-safari2);\\n    min-width: 10px;\\n    height: 8px;\\n    pointer-events: none;\\n    box-shadow: inset 0 1px transparent;\\n    transition: border 0.2s, box-shadow 0.2s;\\n}\\n\\n.matter-textfield-outlined > input + span::before,\\n.matter-textfield-outlined > textarea + span::before {\\n    margin-right: 4px;\\n    border-left: solid 1px transparent;\\n    border-radius: 4px 0;\\n}\\n\\n.matter-textfield-outlined > input + span::after,\\n.matter-textfield-outlined > textarea + span::after {\\n    flex-grow: 1;\\n    margin-left: 4px;\\n    border-right: solid 1px transparent;\\n    border-radius: 0 4px;\\n}\\n\\n.matter-textfield-outlined > input:not(:focus):placeholder-shown + span::before,\\n.matter-textfield-outlined > textarea:not(:focus):placeholder-shown + span::before,\\n.matter-textfield-outlined > input:not(:focus):placeholder-shown + span::after,\\n.matter-textfield-outlined > textarea:not(:focus):placeholder-shown + span::after {\\n    border-top-color: transparent;\\n}\\n\\n/* Hover */\\n.matter-textfield-outlined:hover > input,\\n.matter-textfield-outlined:hover > textarea {\\n    border-color: transparent var(--matter-helper-safari3) var(--matter-helper-safari3);\\n}\\n\\n.matter-textfield-outlined:hover > input + span::before,\\n.matter-textfield-outlined:hover > textarea + span::before,\\n.matter-textfield-outlined:hover > input + span::after,\\n.matter-textfield-outlined:hover > textarea + span::after {\\n    border-top-color: var(--matter-helper-safari3);\\n}\\n\\n.matter-textfield-outlined:hover > input:not(:focus):placeholder-shown,\\n.matter-textfield-outlined:hover > textarea:not(:focus):placeholder-shown {\\n    border-color: var(--matter-helper-safari3);\\n}\\n\\n/* Focus */\\n.matter-textfield-outlined > input:focus,\\n.matter-textfield-outlined > textarea:focus {\\n    border-color: transparent var(--matter-helper-theme) var(--matter-helper-theme);\\n    box-shadow: inset 1px 0 var(--matter-helper-theme), inset -1px 0 var(--matter-helper-theme), inset 0 -1px var(--matter-helper-theme);\\n    outline: none;\\n}\\n\\n.matter-textfield-outlined > input:focus + span,\\n.matter-textfield-outlined > textarea:focus + span {\\n    color: var(--matter-helper-theme);\\n}\\n\\n.matter-textfield-outlined > input:focus + span::before,\\n.matter-textfield-outlined > input:focus + span::after,\\n.matter-textfield-outlined > textarea:focus + span::before,\\n.matter-textfield-outlined > textarea:focus + span::after {\\n    border-top-color: var(--matter-helper-theme) !important;\\n    box-shadow: inset 0 1px var(--matter-helper-theme);\\n}\\n\\n/* Disabled */\\n.matter-textfield-outlined > input:disabled,\\n.matter-textfield-outlined > input:disabled + span,\\n.matter-textfield-outlined > textarea:disabled,\\n.matter-textfield-outlined > textarea:disabled + span {\\n    border-color: transparent var(--matter-helper-safari1) var(--matter-helper-safari1) !important;\\n    color: rgba(var(--matter-onsurface-rgb, 0, 0, 0), 0.38);\\n    pointer-events: none;\\n}\\n\\n.matter-textfield-outlined > input:disabled + span::before,\\n.matter-textfield-outlined > input:disabled + span::after,\\n.matter-textfield-outlined > textarea:disabled + span::before,\\n.matter-textfield-outlined > textarea:disabled + span::after {\\n    border-top-color: var(--matter-helper-safari1) !important;\\n}\\n\\n.matter-textfield-outlined > input:disabled:placeholder-shown,\\n.matter-textfield-outlined > input:disabled:placeholder-shown + span,\\n.matter-textfield-outlined > textarea:disabled:placeholder-shown,\\n.matter-textfield-outlined > textarea:disabled:placeholder-shown + span {\\n    border-top-color: var(--matter-helper-safari1) !important;\\n}\\n\\n.matter-textfield-outlined > input:disabled:placeholder-shown + span::before,\\n.matter-textfield-outlined > input:disabled:placeholder-shown + span::after,\\n.matter-textfield-outlined > textarea:disabled:placeholder-shown + span::before,\\n.matter-textfield-outlined > textarea:disabled:placeholder-shown + span::after {\\n    border-top-color: transparent !important;\\n}\\n\\n/* Faster transition in Safari for less noticable fractional font-size issue */\\n@media not all and (min-resolution:.001dpcm) {\\n    @supports (-webkit-appearance:none) {\\n        .matter-textfield-outlined > input,\\n        .matter-textfield-outlined > input + span,\\n        .matter-textfield-outlined > textarea,\\n        .matter-textfield-outlined > textarea + span,\\n        .matter-textfield-outlined > input + span::before,\\n        .matter-textfield-outlined > input + span::after,\\n        .matter-textfield-outlined > textarea + span::before,\\n        .matter-textfield-outlined > textarea + span::after {\\n            transition-duration: 0.1s;\\n        }\\n    }\\n}\\n\\n.matter-button-contained {\\n    --matter-helper-theme: var(--ems-theme-primary);\\n    --matter-helper-ontheme: var(--matter-ontheme-rgb, var(--matter-onprimary-rgb, 255, 255, 255));\\n    position: relative;\\n    display: inline-block;\\n    box-sizing: border-box;\\n    border: none;\\n    border-radius: 4px;\\n    padding: 0 16px;\\n    min-width: 64px;\\n    height: 36px;\\n    vertical-align: middle;\\n    text-align: center;\\n    text-overflow: ellipsis;\\n    color: rgb(var(--matter-helper-ontheme));\\n    background-color: rgb(var(--matter-helper-theme));\\n    box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\\n    font-family: var(--matter-font-family, \\"Roboto\\", \\"Segoe UI\\", BlinkMacSystemFont, system-ui, -apple-system);\\n    font-size: 14px;\\n    font-weight: 500;\\n    line-height: 36px;\\n    outline: none;\\n    cursor: pointer;\\n    transition: box-shadow 0.2s;\\n}\\n\\n.matter-button-contained::-moz-focus-inner {\\n    border: none;\\n}\\n\\n/* Highlight, Ripple */\\n.matter-button-contained::before,\\n.matter-button-contained::after {\\n    content: \\"\\";\\n    position: absolute;\\n    top: 0;\\n    left: 0;\\n    right: 0;\\n    bottom: 0;\\n    border-radius: inherit;\\n    opacity: 0;\\n}\\n\\n.matter-button-contained::before {\\n    background-color: rgb(var(--matter-helper-ontheme));\\n    transition: opacity 0.2s;\\n}\\n\\n.matter-button-contained::after {\\n    background: radial-gradient(circle at center, currentColor 1%, transparent 1%) center/10000% 10000% no-repeat;\\n    transition: opacity 1s, background-size 0.5s;\\n}\\n\\n/* Hover, Focus */\\n.matter-button-contained:hover,\\n.matter-button-contained:focus {\\n    box-shadow: 0 2px 4px -1px rgba(0, 0, 0, 0.2), 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12);\\n}\\n\\n.matter-button-contained:hover::before {\\n    opacity: 0.08;\\n}\\n\\n.matter-button-contained:focus::before {\\n    opacity: 0.24;\\n}\\n\\n.matter-button-contained:hover:focus::before {\\n    opacity: 0.32;\\n}\\n\\n/* Active */\\n.matter-button-contained:active {\\n    box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2), 0 8px 10px 1px rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12);\\n}\\n\\n.matter-button-contained:active::after {\\n    opacity: 0.32;\\n    background-size: 100% 100%;\\n    transition: background-size 0s;\\n}\\n\\n/* Disabled */\\n.matter-button-contained:disabled {\\n    color: rgba(var(--matter-onsurface-rgb, 0, 0, 0), 0.38);\\n    background-color: rgba(var(--matter-onsurface-rgb, 0, 0, 0), 0.12);\\n    box-shadow: none;\\n    cursor: initial;\\n}\\n\\n.matter-button-contained:disabled::before,\\n.matter-button-contained:disabled::after {\\n    opacity: 0;\\n}\\n.matter-textfield-standard {\\n    --matter-helper-theme: var(--ems-theme-primary);\\n    position: relative;\\n    display: inline-block;\\n    font-family: var(--matter-font-family, \\"Roboto\\", \\"Segoe UI\\", BlinkMacSystemFont, system-ui, -apple-system);\\n    font-size: 16px;\\n    line-height: 1.5;\\n}\\n\\n/* Input, Textarea */\\n.matter-textfield-standard > input,\\n.matter-textfield-standard > textarea {\\n    display: block;\\n    box-sizing: border-box;\\n    margin: 0;\\n    border: none;\\n    border-top: solid 24px transparent;\\n    border-bottom: solid 1px rgba(var(--matter-onsurface-rgb, 0, 0, 0), 0.6);\\n    padding: 0 0 7px;\\n    width: 100%;\\n    height: inherit;\\n    color: rgba(var(--matter-onsurface-rgb, 0, 0, 0), 0.87);\\n    -webkit-text-fill-color: currentColor; /* Safari */\\n    background-color: transparent;\\n    box-shadow: none; /* Firefox */\\n    font-family: inherit;\\n    font-size: inherit;\\n    line-height: inherit;\\n    caret-color: rgb(var(--matter-helper-theme));\\n    transition: border-bottom 0.2s, background-color 0.2s;\\n}\\n\\n/* Span */\\n.matter-textfield-standard > input + span,\\n.matter-textfield-standard > textarea + span {\\n    position: absolute;\\n    top: 0;\\n    left: 0;\\n    right: 0;\\n    bottom: 0;\\n    display: block;\\n    box-sizing: border-box;\\n    padding: 7px 0 0;\\n    color: rgba(var(--matter-onsurface-rgb, 0, 0, 0), 0.6);\\n    font-size: 75%;\\n    line-height: 18px;\\n    pointer-events: none;\\n    transition: color 0.2s, font-size 0.2s, line-height 0.2s;\\n}\\n\\n/* Underline */\\n.matter-textfield-standard > input + span::after,\\n.matter-textfield-standard > textarea + span::after {\\n    content: \\"\\";\\n    position: absolute;\\n    left: 0;\\n    bottom: 0;\\n    display: block;\\n    width: 100%;\\n    height: 2px;\\n    background-color: rgb(var(--matter-helper-theme));\\n    transform-origin: bottom center;\\n    transform: scaleX(0);\\n    transition: transform 0.2s;\\n}\\n\\n/* Hover */\\n.matter-textfield-standard:hover > input,\\n.matter-textfield-standard:hover > textarea {\\n    border-bottom-color: rgba(var(--matter-onsurface-rgb, 0, 0, 0), 0.87);\\n}\\n\\n/* Placeholder-shown */\\n.matter-textfield-standard > input:not(:focus):placeholder-shown + span,\\n.matter-textfield-standard > textarea:not(:focus):placeholder-shown + span {\\n    font-size: inherit;\\n    line-height: 56px;\\n}\\n\\n/* Focus */\\n.matter-textfield-standard > input:focus,\\n.matter-textfield-standard > textarea:focus {\\n    outline: none;\\n}\\n\\n.matter-textfield-standard > input:focus + span,\\n.matter-textfield-standard > textarea:focus + span {\\n    color: rgb(var(--matter-helper-theme));\\n}\\n\\n.matter-textfield-standard > input:focus + span::after,\\n.matter-textfield-standard > textarea:focus + span::after {\\n    transform: scale(1);\\n}\\n\\n/* Disabled */\\n.matter-textfield-standard > input:disabled,\\n.matter-textfield-standard > textarea:disabled {\\n    border-bottom-color: rgba(var(--matter-onsurface-rgb, 0, 0, 0), 0.38);\\n    color: rgba(var(--matter-onsurface-rgb, 0, 0, 0), 0.38);\\n}\\n\\n.matter-textfield-standard > input:disabled + span,\\n.matter-textfield-standard > textarea:disabled + span {\\n    color: rgba(var(--matter-onsurface-rgb, 0, 0, 0), 0.38);\\n}\\n\\n/* Faster transition in Safari for less noticable fractional font-size issue */\\n@media not all and (min-resolution:.001dpcm) {\\n    @supports (-webkit-appearance:none) {\\n        .matter-textfield-standard > input,\\n        .matter-textfield-standard > input + span,\\n        .matter-textfield-standard > input + span::after,\\n        .matter-textfield-standard > textarea,\\n        .matter-textfield-standard > textarea + span,\\n        .matter-textfield-standard > textarea + span::after {\\n            transition-duration: 0.1s;\\n        }\\n    }\\n}\\n\\n\\n\\n\\n:root {\\n  --ems-theme-primary: #0895c3; /*#0e9785;*/\\n  --placeholder-color: rgba(91, 106, 119, 1);\\n  --placeholder-color-up: rgba(151, 175, 197, 1);\\n  --matter-helper-theme: var(--ems-theme-primary) !important;\\n}\\n.panel main {\\n  padding-bottom: 50px;\\n}\\n.right {\\n  right: 45px;\\n  float: right;\\n  position: absolute;\\n  bottom: 45px;\\n}\\n.right p {\\n  margin: 0;\\n}\\n.locale-selector {\\n  left: 45px;\\n  float: left;\\n  position: absolute;\\n  bottom: 45px;\\n}\\n.panel {\\n  border-radius: 12px !important;\\n  border-top: unset !important;\\n  padding: 24px !important;\\n  padding-bottom: 30px !important;\\n  will-change: opacity;\\n  min-height: 365px;\\n  width: 365px;\\n  top: 200px;\\n  zoom: .8;\\n}\\nmain p {\\n  color: #fff !important;\\n  font-size: 15px !important;\\n  font-weight: 500;\\n}\\nmain form {\\n  padding-bottom: 20px;\\n}\\nmain > .error, main > .info {\\n  display: block;\\n}\\n.panel *:not(img) {\\n  transition: opacity 500ms ease-in-out;\\n  opacity: 0;\\n}\\n.panel.show *:not(img) {\\n  opacity: 1;\\n}\\n.panel h2 {\\n  text-align: center;\\n  display: none;\\n}\\n.panel img.logo {\\n  transition: opacity 0.9s ease 0s;\\n  position: fixed;\\n  top: 250px;\\n  left: max(50% + 38px);\\n  transform: translate(-50%, -50%);\\n  color: rgb(204, 204, 204);\\n  opacity: 1 !important;\\n  zoom: 1.25;\\n}\\n\\n.panel img.logo-3c219e58-ed0e-4b18-ad48-f4f92793ae32 {\\n   filter: blur(2px);  \\n}\\n\\n.panel button {\\n  width: 100%;\\n  margin-top: 25px;\\n  background: var(--ems-theme-primary);\\n}\\n.panel input {\\n  color: #fff;\\n  box-shadow: none !important;\\n  border-bottom: solid 1px var(--ems-theme-primary) !important;\\n}\\n.panel .matter-textfield-standard:hover > input {\\n  border-bottom-color: var(--ems-theme-primary);\\n}\\n.panel .matter-textfield-standard > input + span {\\n  color: var(--placeholder-color);\\n  font-weight: 500;\\n}\\n.panel .matter-textfield-standard > input:focus + span {\\n  color: var(--placeholder-color-up);\\n}\\n.panel .select {\\n  margin-left: -1px;\\n  font-size: 12px;\\n  font-weight: normal;\\n  transition: text-shadow 320ms;\\n}\\n.panel .select * {\\n  color: var(--matter-helper-theme) !important;\\n}\\n.panel .select:after {\\n  opacity: 0;\\n}\\n.panel .select .lang {\\n  cursor: pointer;\\n  color: var(--ems-theme-primary) !important;\\n}\\n.panel .select .lang:first-letter {\\n  text-transform: uppercase;\\n}\\n.panel .select .lang:hover {\\n  text-shadow: 0 0 1px rgba(0,0,0,.5);\\n  text-decoration: underline;\\n  text-underline-offset: 5px;\\n}\\n.panel .select .lang[selected] {\\n  text-shadow: 0 0 1px rgba(0,0,0,.3);\\n  text-decoration: underline;\\n  text-underline-offset: 5px;\\n}\\n.panel a[href] {\\n  color: var(--ems-theme-primary);\\n  text-align: right;\\n  padding-top: 8px;\\n  font-size: 12px;\\n  display: block;\\n}\\na[href]:hover {\\n  text-decoration: underline;\\n  text-underline-offset: 5px;\\n}\\n.error {\\n  width: 100%;\\n  text-align: left;\\n  font-weight: 500;\\n  color: #EE3E54;\\n}\\n.info {\\n  width: 100%;\\n  text-align: left;\\n  font-weight: 500;\\n  color: white;\\n}\\n.progress-bar {\\n  height: 8px;\\n  overflow: hidden;\\n  border-radius: 4px;\\n}\\n.progress-bar div {\\n  height: 8px;\\n  background: var(--ems-theme-primary);\\n}\\nbody div.panel {\\n  background: var(--bg);\\n  border: none !important;\\n  box-shadow: none !important;\\n}\\ninput:-webkit-autofill,\\ninput:-webkit-autofill:hover, \\ninput:-webkit-autofill:focus, \\ninput:-webkit-autofill:active{\\n  transition: background-color 5000s;\\n  -webkit-text-fill-color: #fff !important;\\n  -webkit-box-shadow: 0 0 0 30px var(--bg) inset !important;\\n}\\n","templates":{"accountEdit":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"fields\\" type=\\"java.util.Map<java.lang.Integer, java.util.List<io.fusionauth.domain.form.FormField>>\\" --]\\n[#-- @ftlvariable name=\\"user\\" type=\\"io.fusionauth.domain.User\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[#function generateSectionLabel sectionNumber tenantId]\\n  [#--  Tenant specific, not tenant specific, then default --]\\n  [#local sectionLabel = theme.optionalMessage(\\"[${tenantId}]{self-service-form}${sectionNumber}\\")/]\\n  [#local resolvedLabel = sectionLabel != \\"[${tenantId}]{self-service-form}${sectionNumber}\\"/]\\n  [#if !resolvedLabel]\\n    [#local sectionLabel = theme.optionalMessage(\\"{self-service-form}${sectionNumber}\\")/]\\n    [#local resolvedLabel = sectionLabel != \\"{self-service-form}${sectionNumber}\\"/]\\n  [/#if]\\n  [#if !resolvedLabel]\\n    [#return \\"\\"/]\\n  [#else]\\n    [#return sectionLabel /]\\n  [/#if]\\n[/#function]\\n\\n[@helpers.html]\\n  [@helpers.head title=theme.message(\\"account\\")/]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n      <script src=\\"/js/ui/Main.js?version=${version}\\"></script>\\n    [/@helpers.header]\\n\\n    [@helpers.accountMain rowClass=\\"row center\\" colClass=\\"col-xs-12 col-sm-12 col-md-10 col-lg-8\\" actionURL=\\"/account/\\" actionText=theme.message(\\"cancel-go-back\\")]\\n      [@helpers.accountPanel title=\\"\\" tenant=tenant user=user action=\\"edit\\" showEdit=true]\\n       <div class=\\"row\\" style=\\"border-bottom: 0;\\">\\n        <div class=\\"col-xs-12 col-md-12\\">\\n          <form action=\\"edit\\" method=\\"POST\\" class=\\"full\\" id=\\"user-form\\">\\n            [@helpers.hidden name=\\"client_id\\" /]\\n            [#if fields?has_content]\\n              <fieldset>\\n                [#list fields as fieldKey, fieldValues]\\n\\n                  [#-- Section labels  --]\\n                  [#assign sectionNumber = fieldKey + 1/]\\n                  [#assign sectionLabel = generateSectionLabel(sectionNumber, tenantId) /]\\n                  [#if sectionLabel?has_content]\\n                    <legend> ${sectionLabel} </legend>\\n                  [/#if]\\n\\n                  [#list fieldValues as field]\\n                    [#if field.key == \\"user.password\\"]\\n                       [@helpers.passwordField field/]\\n                    [#else]\\n                       [@helpers.customField field=field key=field.key autofocus=false placeholder=field.key label=theme.optionalMessage(field.key) leftAddon=\\"false\\"/]\\n                       [#if field.confirm]\\n                         [@helpers.customField field \\"confirm.${field.key}\\" false \\"[confirm]${field.key}\\" /]\\n                       [/#if]\\n                    [/#if]\\n                  [/#list]\\n                [/#list]\\n              </fieldset>\\n            [/#if]\\n            <div class=\\"form-row\\">\\n              [@helpers.button icon=\\"save\\" text=theme.message(\\"submit\\")/]\\n            </div>\\n          </form>\\n        </div>\\n       </div>\\n      [/@helpers.accountPanel]\\n    [/@helpers.accountMain]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","accountIndex":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"formConfigured\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"multiFactorAvailable\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#-- @ftlvariable name=\\"user\\" type=\\"io.fusionauth.domain.User\\" --]\\n\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head title=theme.message(\\"account\\")]\\n    [#-- Custom header code goes here --]\\n    <script src=\\"/js/ui/Main.js?version=${version}\\"></script>\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [#assign actionURL = multiFactorAvailable?then(\\"/account/two-factor/\\", \\"\\")/]\\n    [@helpers.accountMain rowClass=\\"row center\\" colClass=\\"col-xs-12 col-sm-12 col-md-10 col-lg-8\\" actionURL=actionURL actionText=theme.message('manage-two-factor') actionDirection=\\"forward\\"]\\n      [@helpers.accountPanel title=\\"\\" tenant=tenant user=user action=\\"view\\" showEdit=formConfigured]\\n         <div class=\\"row\\" style=\\"border-bottom: 0;\\">\\n            <div class=\\"col-xs-12 col-md-12\\">\\n              [#-- Example landing page. This can be customized and different values can be displayed from the user. --]\\n              <dl class=\\"horizontal\\">\\n                <dt>${theme.message(\\"user.email\\")}</dt>\\n                 <dd>\\n                   [#if user.verified ] <i data-tooltip=\\"Email has been verified\\" class=\\"green-text md-text fa fa-check\\"></i> [/#if]\\n                   ${helpers.display(user, \\"email\\")}\\n                 </dd>\\n              </dl>\\n              <dl class=\\"horizontal\\">\\n                 <dt>${theme.message(\\"user.mobilePhone\\")}</dt>\\n                <dd>${helpers.display(user, \\"mobilePhone\\")}</dd>\\n              </dl>\\n            </div>\\n          </div>\\n      [/@helpers.accountPanel]\\n    [/@helpers.accountMain]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","accountTwoFactorDisable":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"method\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"methodId\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"email\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"mobilePhone\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#-- @ftlvariable name=\\"user\\" type=\\"io.fusionauth.domain.User\\" --]\\n\\n[#import \\"../../_helpers.ftl\\" as helpers/]\\n\\n[#macro instructions method]\\n<div class=\\"d-flex\\">\\n  <div style=\\"flex-grow: 1;\\">\\n    [#if method == \\"authenticator\\"]\\n      <p class=\\"mt-0 mb-3\\">${theme.message(\\"authenticator-disable-step-1\\")}</p>\\n    [#elseif method == \\"email\\" || method == \\"sms\\"]\\n      <p class=\\"mt-0 mb-3\\">${theme.message(\\"${method}-disable-step-1\\", (method == \\"email\\")?then(email, mobilePhone))}</p>\\n      <form id=\\"send-two-factor-form\\" action=\\"disable\\" method=\\"POST\\" class=\\"full\\">\\n        [@helpers.hidden name=\\"action\\" value=\\"send\\" /]\\n        [@helpers.hidden name=\\"client_id\\" /]\\n        [@helpers.hidden name=\\"methodId\\" /]\\n        [#-- Send a code --]\\n        [@helpers.button icon=\\"arrow-circle-right\\" color=\\"gray\\" text=\\"${theme.message('send-one-time-code')}\\"/]\\n      </form>\\n    [/#if]\\n  </div>\\n</div>\\n[/#macro]\\n\\n[@helpers.html]\\n  [@helpers.head title=theme.message(\\"authenticator-configuration\\")/]\\n  [@helpers.body]\\n\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.accountMain rowClass=\\"row center\\" colClass=\\"col-xs-12 col-sm-12 col-md-10 col-lg-8\\" actionURL=\\"/account/two-factor/\\" actionText=theme.message(\\"cancel-go-back\\")]\\n        [@helpers.accountPanelFull]\\n\\n          <fieldset class=\\"pb-3\\">\\n            [#-- Heading --]\\n            <legend>${theme.message(\\"disable-instructions\\")}</legend>\\n\\n            [#-- Instructions --]\\n            [@instructions method/]\\n\\n            <p class=\\"mt-4\\">\\n              <strong>${theme.message('note')}</strong> ${theme.message('{description}two-factor-recovery-code-note')}\\n            </p>\\n          </fieldset>\\n\\n          <form id=\\"two-factor-form\\" action=\\"disable\\" method=\\"POST\\" class=\\"full\\">\\n            [@helpers.hidden name=\\"client_id\\" /]\\n            [@helpers.hidden name=\\"methodId\\" /]\\n\\n            <fieldset>\\n              [@helpers.input type=\\"text\\" name=\\"code\\" id=\\"verification-code\\" label=theme.message(\\"verification-code\\") placeholder=\\"${theme.message('{placeholder}two-factor-code')}\\" autofocus=true autocapitalize=\\"none\\"  autocomplete=\\"one-time-code\\" autocorrect=\\"off\\" required=true/]\\n            </fieldset>\\n\\n            <div class=\\"form-row\\">\\n              [@helpers.button icon=\\"save\\" text=theme.message(\\"disable\\")/]\\n            </div>\\n          </form>\\n       [/@helpers.accountPanelFull]\\n    [/@helpers.accountMain]\\n\\n    [@helpers.footer]\\n       [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n\\n  [/@helpers.body]\\n[/@helpers.html]","accountTwoFactorEnable":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"availableMethods\\" type=\\"java.util.List<java.lang.String>\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"email\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"method\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"mobilePhone\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"recoveryCodes\\" type=\\"java.util.List<java.lang.String>\\" --]\\n[#-- @ftlvariable name=\\"secret\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"secretBase32Encoded\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#-- @ftlvariable name=\\"user\\" type=\\"io.fusionauth.domain.User\\" --]\\n\\n[#import \\"../../_helpers.ftl\\" as helpers/]\\n\\n[#macro instructions method]\\n<div class=\\"d-flex\\">\\n  <div style=\\"flex-grow: 1;\\">\\n    [#if method == \\"authenticator\\"]\\n\\n      [#-- Authenticator Instructions --]\\n      <p class=\\"mt-0 mb-3\\">${theme.message(\\"authenticator-enable-step-1\\", secretBase32Encoded)?no_esc}</p>\\n      <p>${theme.message(\\"authenticator-enable-step-2\\")}</p>\\n\\n    [#elseif method == \\"email\\" || method == \\"sms\\"]\\n\\n      [#-- Email or SMS Instructions --]\\n      <p class=\\"mt-0 mb-3\\">${theme.message(\\"${method}-enable-step-1\\", helpers.display(user, (method == \\"email\\")?then(\\"email\\", \\"mobilePhone\\")))}</p>\\n\\n      <form id=\\"two-factor-send-form\\" action=\\"enable\\" method=\\"POST\\" class=\\"full\\">\\n        [@helpers.hidden name=\\"action\\" value=\\"send\\" /]\\n        [@helpers.hidden name=\\"client_id\\" /]\\n        [@helpers.hidden name=\\"method\\" /]\\n        [#-- 'secret' and 'twoFactorSecretBase32' are required for authenticator. --]\\n        [@helpers.hidden name=\\"secret\\" /]\\n        [@helpers.hidden name=\\"secretBase32Encoded\\" /]\\n\\n        [#-- Send a code --]\\n        [#if method == \\"email\\"]\\n          [@helpers.input type=\\"text\\" id=\\"email\\" name=\\"email\\" label=\\"Email\\" required=true/]\\n        [#elseif method == \\"sms\\"]\\n          [@helpers.input type=\\"text\\" id=\\"mobilePhone\\" name=\\"mobilePhone\\" label=\\"Mobile phone\\" required=true/]\\n        [/#if]\\n\\n        [@helpers.button icon=\\"arrow-circle-right\\" color=\\"gray\\" text=\\"${theme.message('send-one-time-code')}\\"/]\\n      </form>\\n    [/#if]\\n  </div>\\n\\n  [#-- QR Code for Authenticator app --]\\n  [#if method == \\"authenticator\\"]\\n  <div id=\\"qrcode\\" class=\\"qrcode pl-2\\"></div>\\n  [/#if]\\n\\n</div>\\n[/#macro]\\n\\n[@helpers.html]\\n  [@helpers.head title=theme.message(\\"authenticator-configuration\\")]\\n    [#-- JavaScript is used for rendering authenticator QR code --]\\n    <script src=\\"/js/qrcode-min-1.0.js\\"></script>\\n    <script src=\\"/js/account/EnableTwoFactor.js?version=${version}\\"></script>\\n    <script>\\n      Prime.Document.onReady(function() {\\n        [#-- These variables will get set by the FreeMarker template. --]\\n        var params = {\\n          accountName: '${user.getLogin()}',\\n          issuer: '${tenant.issuer}',\\n          secretBase32Encoded: '${secretBase32Encoded}'\\n        }\\n\\n        [#-- This object the Enable Two-Factor form --]\\n        new FusionAuth.Account.EnableTwoFactor(params);\\n      });\\n    </script>\\n  [/@helpers.head]\\n  [@helpers.body]\\n\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [#assign textKey = recoveryCodes?has_content?then(\\"done\\", \\"go-back\\")/]\\n    [#assign actionDirection = recoveryCodes?has_content?then(\\"forward\\", \\"back\\")/]\\n    [@helpers.accountMain rowClass=\\"row center\\" colClass=\\"col-xs-12 col-sm-12 col-md-10 col-lg-8\\" actionURL=\\"/account/two-factor/\\" actionText=theme.message(textKey) actionDirection=actionDirection]\\n      [@helpers.accountPanelFull]\\n\\n        [#-- The first time a user enables a two-factor code, they will be presented with recovery codes. --]\\n        [#if recoveryCodes?has_content]\\n          [#-- Display Recovery codes to the user. --]\\n\\n          <div class=\\"row\\">\\n            <div class=\\"col-xs\\">\\n              <fieldset>\\n                <p> ${theme.message(\\"{description}recovery-codes-1\\", recoveryCodes?size)} </p>\\n              </fieldset>\\n              <fieldset>\\n                <div class=\\"d-flex center\\" style=\\"flex-wrap: wrap;\\">\\n                  [#list recoveryCodes as code]\\n                  <div class=\\"p-2 mr-2 mb-2 code\\">${code}</div>\\n                  [/#list]\\n                </div>\\n              </fieldset>\\n              <fieldset>\\n                <p> ${theme.message(\\"{description}recovery-codes-2\\")}  </p>\\n              </fieldset>\\n            </div>\\n          </div>\\n\\n        [#else]\\n        [#-- Show the Enable Two-Factor form --]\\n\\n         <fieldset>\\n           [#-- Heading --]\\n           <legend>${theme.message(\\"enable-instructions\\")}</legend>\\n           <form class=\\"full\\">\\n             [@helpers.select name=\\"method\\" id=\\"select-method\\" options=availableMethods label=\\"${theme.message('select-two-factor-method')}\\" required=true/]\\n           </form>\\n         </fieldset>\\n\\n          [#list availableMethods as method]\\n            <fieldset class=\\"pb-3\\" data-method-instructions=\\"${method}\\">\\n              [#-- Instructions --]\\n              [@instructions method/]\\n            </fieldset>\\n          [/#list]\\n\\n          [#-- Enable Two Factor Form --]\\n          <form id=\\"two-factor-form\\" action=\\"enable\\" method=\\"POST\\" class=\\"full\\">\\n             [@helpers.hidden name=\\"client_id\\" /]\\n             [@helpers.hidden name=\\"email\\" /]\\n             [@helpers.hidden name=\\"method\\" /]\\n             [@helpers.hidden name=\\"mobilePhone\\" /]\\n             [#-- 'secret' and 'twoFactorSecretBase32' are required for authenticator. --]\\n             [@helpers.hidden name=\\"secret\\" /]\\n             [@helpers.hidden name=\\"secretBase32Encoded\\" /]\\n             <fieldset>\\n               [@helpers.input type=\\"text\\" name=\\"code\\" id=\\"verification-code\\" label=theme.message(\\"verification-code\\") placeholder=\\"${theme.message('{placeholder}two-factor-code')}\\" autocapitalize=\\"none\\"  autocomplete=\\"one-time-code\\" autocorrect=\\"off\\" required=true/]\\n             </fieldset>\\n             <div class=\\"form-row\\">\\n               [@helpers.button icon=\\"save\\" text=theme.message(\\"enable\\")/]\\n             </div>\\n          </form>\\n        [/#if]\\n\\n     [/@helpers.accountPanelFull]\\n    [/@helpers.accountMain]\\n\\n    [@helpers.footer]\\n       [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n\\n  [/@helpers.body]\\n[/@helpers.html]","accountTwoFactorIndex":"[#ftl/]\\n[#setting url_escaping_charset=\\"UTF-8\\"]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"user\\" type=\\"io.fusionauth.domain.User\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n\\n[#import \\"../../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head title=theme.message(\\"multi-factor-configuration\\")/]\\n  [@helpers.body]\\n\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.accountMain rowClass=\\"row center\\" colClass=\\"col-xs-12 col-sm-12 col-md-10 col-lg-8\\" actionURL=\\"/account/\\" actionText=theme.message(\\"go-back\\")]\\n        [@helpers.accountPanelFull]\\n\\n           <fieldset>\\n             <legend>${theme.message(\\"two-factor-authentication\\")}</legend>\\n             <p><em>${theme.message(\\"{description}two-factor-authentication\\")}</em></p>\\n\\n             <table class=\\"hover\\">\\n               <thead>\\n                <tr>\\n                <th>${theme.message(\\"method\\")}</th>\\n                <th>${theme.message(\\"value\\")}</th>\\n                <th class=\\"action\\">${theme.message(\\"action\\")}</th>\\n                </tr>\\n              </thead>\\n               <tbody>\\n                 [#list user.twoFactor.methods as method]\\n                   <tr>\\n                     <td> ${theme.message(method.method)} </td>\\n                     <td>\\n                       [#if method.method == \\"email\\"]${helpers.display(method, \\"email\\")}\\n                       [#elseif method.method == \\"sms\\"]${helpers.display(method, \\"mobilePhone\\")}\\n                       [#else]&ndash;\\n                       [/#if]\\n                      </td>\\n                     <td class=\\"action\\">\\n                      <a class=\\"small-square gray button ml-2 pr-0\\" href=\\"/account/two-factor/disable?client_id=${client_id}&methodId=${method.id?url}\\" data-tooltip=\\"${theme.message('disable')}\\"> <i class=\\"fa fa-minus\\"></i> </a>\\n                     </td>\\n                   </tr>\\n                 [#else]\\n                 <tr>\\n                    <td colspan=\\"4\\">${theme.message(\\"no-two-factor-methods-configured\\")}</td>\\n                 </tr>\\n                 [/#list]\\n               </tbody>\\n             </table>\\n\\n             <div class=\\"form-row mt-3\\">\\n               <a class=\\"blue button\\" href=\\"/account/two-factor/enable?client_id=${client_id}\\"><i class=\\"fa fa-plus\\"></i> ${theme.message(\\"add-two-factor\\")}</a>\\n             </div>\\n\\n           </fieldset>\\n\\n      [/@helpers.accountPanelFull]\\n    [/@helpers.accountMain]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","emailComplete":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('email-verification-complete-title')]\\n      <p>\\n        ${theme.message('email-verification-complete')}\\n      </p>\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","emailSend":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"email\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"emailSent\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('email-verification-sent-title')]\\n      <p>\\n        ${theme.message('email-verification-sent', email)}\\n      </p>\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","emailSent":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"email\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"emailSent\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('email-verification-sent-title')]\\n      <p>\\n        ${theme.message('email-verification-sent', email)}\\n      </p>\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","emailVerificationRequired":"[#ftl/]\\n[#-- @ftlvariable name=\\"allowEmailChange\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"currentUser\\" type=\\"io.fusionauth.domain.User\\" --]\\n[#-- @ftlvariable name=\\"collectVerificationCode\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"email\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"showCaptcha\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#-- @ftlvariable name=\\"verificationId\\" type=\\"java.lang.String\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [@helpers.captchaScripts showCaptcha=showCaptcha captchaMethod=tenant.captchaConfiguration.captchaMethod siteKey=tenant.captchaConfiguration.siteKey/]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('email-verification-required-title')]\\n      [#-- The user does not have a verified email. Add optional messaging here with instruction to the user. --]\\n\\n       [#-- Let the user know why they ended up here --]\\n       <p class=\\"mt-0 mb-3\\">\\n         ${theme.message(\\"{description}email-verification-required\\")}\\n       </p>\\n\\n       [#-- If configured, collect the verification code on this form, this means the user sits here until they verify their email. --]\\n       [#if collectVerificationCode]\\n          <form id=\\"verification-required-enter-code\\" action=\\"${request.contextPath}/email/verification-required\\" method=\\"POST\\" class=\\"full\\">\\n            [@helpers.oauthHiddenFields/]\\n            [@helpers.hidden name=\\"action\\" value=\\"verify\\"/]\\n            [@helpers.hidden name=\\"allowEmailChange\\"/]\\n            [@helpers.hidden name=\\"collectVerificationCode\\"/]\\n            [@helpers.hidden name=\\"email\\"/]\\n            [@helpers.hidden name=\\"verificationId\\"/]\\n            <fieldset>\\n              [@helpers.input type=\\"text\\" name=\\"oneTimeCode\\" id=\\"otp\\" autocapitalize=\\"none\\" autofocus=true autocomplete=\\"one-time-code\\" autocorrect=\\"off\\" placeholder=\\"${theme.message('code')}\\" leftAddon=\\"lock\\"/]\\n              [@helpers.captchaBadge showCaptcha=showCaptcha captchaMethod=tenant.captchaConfiguration.captchaMethod siteKey=tenant.captchaConfiguration.siteKey/]\\n            </fieldset>\\n            <div class=\\"form-row\\">\\n              [@helpers.button text=theme.message('submit')/]\\n            </div>\\n         </form>\\n       [#else]\\n         <p> ${theme.message(\\"{description}email-verification-required-non-interactive\\")} </p>\\n       [/#if]\\n\\n       [#-- Resend a verification email --]\\n       <form id=\\"verification-required-resend-code\\" action=\\"${request.contextPath}/email/verification-required\\" method=\\"POST\\" class=\\"full\\">\\n         [@helpers.oauthHiddenFields/]\\n         [@helpers.hidden name=\\"action\\" value=\\"resend\\"/]\\n         [@helpers.hidden name=\\"allowEmailChange\\"/]\\n         [@helpers.hidden name=\\"collectVerificationCode\\"/]\\n         [@helpers.hidden name=\\"email\\"/]\\n         [@helpers.hidden name=\\"verificationId\\"/]\\n         <div class=\\"form-row\\">\\n           <button style=\\"background: #fff; border: none; cursor: pointer;\\" class=\\"link blue-text\\"><i class=\\"fa fa-arrow-right\\"></i> ${theme.message('email-verification-required-send-another')} </button>\\n         </div>\\n       </form>\\n\\n       [#-- If configured to allow an email change, present the user with a form. This is intended to assist the user if they mis-typed their email address previously. --]\\n       [#if allowEmailChange]\\n         <div class=\\"hr-container\\">\\n           <hr>\\n           <div>${theme.message('or')}</div>\\n         </div>\\n         <form id=\\"verification-required-change-email\\"action=\\"${request.contextPath}/email/verification-required\\" method=\\"POST\\" class=\\"full\\">\\n           [@helpers.oauthHiddenFields/]\\n           [@helpers.hidden name=\\"action\\" value=\\"changeEmail\\"/]\\n           [@helpers.hidden name=\\"allowEmailChange\\"/]\\n           [@helpers.hidden name=\\"collectVerificationCode\\"/]\\n           [@helpers.hidden name=\\"verificationId\\"/]\\n           <p class=\\"mb-3\\">\\n             ${theme.message('{description}email-verification-required-change-email')}\\n           </p>\\n           <fieldset>\\n             [@helpers.input type=\\"text\\" name=\\"email\\" id=\\"email\\" autocapitalize=\\"none\\" autocomplete=\\"on\\" autocorrect=\\"off\\" placeholder=\\"${theme.message('email')}\\" leftAddon=\\"user\\"/]\\n           </fieldset>\\n           <div class=\\"form-row\\">\\n              [@helpers.button text=theme.message('submit')/]\\n           </div>\\n         </form>\\n       [/#if]\\n\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","emailVerify":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"postMethod\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"showCaptcha\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#-- @ftlvariable name=\\"verificationId\\" type=\\"java.lang.String\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [@helpers.captchaScripts showCaptcha=showCaptcha captchaMethod=tenant.captchaConfiguration.captchaMethod siteKey=tenant.captchaConfiguration.siteKey/]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [#if verificationId?? && postMethod]\\n      <form action=\\"${request.contextPath}/email/verify\\" method=\\"POST\\">\\n        <input type=\\"hidden\\" name=\\"client_id\\" value=\\"${client_id!''}\\">\\n        <input type=\\"hidden\\" name=\\"postMethod\\" value=\\"true\\">\\n        <input type=\\"hidden\\" name=\\"tenantId\\" value=\\"${tenantId!''}\\">\\n        <input type=\\"hidden\\" name=\\"verificationId\\" value=\\"${verificationId}\\">\\n      </form>\\n      <script type=\\"text/javascript\\">\\n        document.forms[0].submit();\\n      </script>\\n    [#else]\\n      [@helpers.header]\\n        [#-- Custom header code goes here --]\\n      [/@helpers.header]\\n\\n      [@helpers.main title=theme.message('email-verification-form-title')]\\n        [#-- FusionAuth automatically handles errors that occur during email verification and outputs them in the HTML --]\\n        <form action=\\"${request.contextPath}/email/verify\\" method=\\"POST\\" class=\\"full\\">\\n          [@helpers.hidden name=\\"captcha_token\\"/]\\n          [@helpers.hidden name=\\"client_id\\"/]\\n          [@helpers.hidden name=\\"tenantId\\"/]\\n          <p>\\n            ${theme.message('email-verification-form')}\\n          </p>\\n          <fieldset class=\\"push-less-top\\">\\n            [@helpers.input type=\\"text\\" name=\\"email\\" id=\\"email\\" autocapitalize=\\"none\\" autofocus=true autocomplete=\\"on\\" autocorrect=\\"off\\" placeholder=\\"${theme.message('email')}\\" leftAddon=\\"user\\"/]\\n            [@helpers.captchaBadge showCaptcha=showCaptcha captchaMethod=tenant.captchaConfiguration.captchaMethod siteKey=tenant.captchaConfiguration.siteKey/]\\n          </fieldset>\\n          <div class=\\"form-row\\">\\n            [@helpers.button text=theme.message('submit')/]\\n          </div>\\n        </form>\\n      [/@helpers.main]\\n\\n      [@helpers.footer]\\n        [#-- Custom footer code goes here --]\\n      [/@helpers.footer]\\n    [/#if]\\n  [/@helpers.body]\\n[/@helpers.html]","helpers":"[#ftl/]\\n[#setting url_escaping_charset=\\"UTF-8\\"]\\n[#-- Below are the main blocks for all of the themeable pages --]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"bypassTheme\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"code_challenge\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"code_challenge_method\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"consents\\" type=\\"java.util.Map<java.util.UUID, java.util.List<java.lang.String>>\\" --]\\n[#-- @ftlvariable name=\\"editPasswordOption\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"locale\\" type=\\"java.util.Locale\\" --]\\n[#-- @ftlvariable name=\\"loginTheme\\" type=\\"io.fusionauth.domain.Theme.Templates\\" --]\\n[#-- @ftlvariable name=\\"metaData\\" type=\\"io.fusionauth.domain.jwt.RefreshToken.MetaData\\" --]\\n[#-- @ftlvariable name=\\"nonce\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"passwordValidationRules\\" type=\\"io.fusionauth.domain.PasswordValidationRules\\" --]\\n[#-- @ftlvariable name=\\"redirect_uri\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"response_mode\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"response_type\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"scope\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"state\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"theme\\" type=\\"io.fusionauth.domain.Theme\\" --]\\n[#-- @ftlvariable name=\\"timezone\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"user_code\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"version\\" type=\\"java.lang.String\\" --]\\n\\n[#macro main logo=\\"\\" locale=\\"\\" title=\\"e-MARIS Login\\" rowClass=\\"row center-xs\\" colClass=\\"col-xs col-sm-8 col-md-6 col-lg-5 col-xl-4\\"]\\n<main class=\\"page-body container\\">\\n  <div class=\\"panel\\" id=\\"panel\\">\\n      [#if logo?has_content]\\n          <img height=\\"200\\"\\n    src=\\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJgAAADICAMAAAAjktjkAAADAFBMVEUAAAAEnc0AqtkAr9gAn9clVHQAn+YAo9EAnOIAmt8An+YAn+UAo9UAoOUAlN4ApOQAn+YAoeUArLgAn+YAo9UeW3UZYHcAuKgmVXQAuKgAoOUAi8gAqecAp+cArrQCfIIgWHUXYXYAssYlVHQAoeAlVXMlVnUBrbkAr7MAiuEA3+wAoNYApbUEpegArroAp+ccX3cAqvgBssUDodYA4O0AiuAAieEAWIgAbpcAsLsDs8YCrbkPmtgA4u4A4OwAprUA3ewAdqYAssUlVHQAoOYAsLMAo+MArOg3VWkB3OgAiuEAtqkA3+wkVHMAX44AitkAueQAq/kAfYQAprQAieEAmOMlVHQAq7IAitcfW3YArLUA3ewAueUAiuEFpMwBqbsAuKgAieEFi8gApbgAn+YAieEAiuEA3+wA4OwAqMAAoKYAp7cArLUlVHMlVHMAitkAitMApvECo+0AuOIAuOMBisYAn+YAq/kAeYMA4OwAo9gBe4IAr8MAo9cAXIoBY44AgJkGntkBscQAp7YAq/gAieEA3+wA3+xateYAl9gAj88BgrgAW4oCf7ECqr0Ah5EBk8QAd6kCsscBe4QAuOUA3+sBi9UClswDpsgBaY8A4e0A3+wAtqgAqroAi8kArbQAitwCe4YkVHMAsKUBoLUCjZ8AueYGsMUAitEAi8kAisolVHQDeYEAq/kApswAueUAssUlVHMA4OwBt+MAscUA3+sGeoIApqQAcaAAl50BtK8FYYoAlZ0At6kAo9cAt6kAueUAq/gA3usAscUAb5wIeYEAisoAq/glVHQBeYAAqMwCuNItY3oEr7UAZ5QAq/ghdZoAm98Ihp0A3+sApu4At+U0WG1KhKFVteMetc4AueUit9IDf5QTkaoKzd0bs84ArqU6nLYobY4NutQGeokde44AnOBHtN8Gkaoefak5tNc8tNsFirMWv9NBtN0AieEApbQAssUA4u4AuKgAqboAiskAq/oApdcAe4EA3ekAn+YAueUAWIgAsboAapUBh9i8uKOPAAAA73RSTlMABQ4JE/75GSQf5vL4nCo3q1Ui1PEYDvzX9sPtbC4t9C0I+PB548aCQvvs0/ZiTkoj+/Ln39W0+PXz4lj+9c+1Fe7qtI5hRD7++O3ZRTzqM/Tw6eTeh3BtX0g4HPv07e3s5t7CtZZ/Miby3NmkpGFTPPj16uHNy7u1s6eflpL79/bvz87Mxn5W/Pn29PLo4+Df29jVo3BG7uTavqeSjnp3bFdV+u7hw7yomI2Lf9zX06iZkn91Xzr6+fTx4tPHuLORkIlm3MW8oX5q+/Px7e3o5N7Em39T9vLn07H67Mzz7+ro5OKN/Pr38+nlzaCMpjhOSnEAABifSURBVHja7JUxjt0gFEUfk1xlBaBXpMBCpmYP1LRIXgSsJitwkcZF5F38NWQ1meHh769JlEQj8WdGmlPYCGRzue8C9MEH7wqtJ3p76By9jznQ6zLZfMnJ0JXFA84BPlFDJfsa9qXoALiYrh0MX62tHhzEwDjL4LTeUeAyg8tlZ/BCDRMRm6AQd9167DxbafhE98IyilVEoYCtGOa6DNKma4cXhRmboqGcFdmOSbXHpc9e2vCkRMTZY+JR3GmMPpNLEyNyKgkV3jSpzRZVYyl7bQJKl5wckEVqGVHTWzV2dpaE4KSWl+aP2pwD9idhxruVZMQ1f+UHA5C5G+FWmGRrlbDZpTLymX0ptihaWkkHIPl+Zt4Kr3tXMbJf3dLejnV/h4xoiHbsg0IWkQ/zruGP2HrYHEoKoTL26Tb7G3YV+FHs42OlMWR4cx4XQZE6jwuaKsMxox9napfsa8b6pK6oKosZgZ1dImFl8JY3xny6kHY/c8zmsHft2dMtcKlgo0FMO7bnV9JCN2gbzNHkOfVPVPMvyqqGIMs3Qc7NpV/if2ZKi2l74tgKQDQ0Cs0u18gL/TfKrqZXFpnGscEBvL7I7NnSOJKbY7XqRfdGnGgcZk0T/Y769PD5gf5OrXR31Ndv33/8/PKPNRm6P7+Yr4PQtKEwgOMvUZjdDsJAT0rWMhgIQ7t5iQcpXkTwkKan0tx2UHpcHIvk1kM9djTxsp48LCdLYFntZYdQKEWkMOaCzG0dYo87S0oI7JmsL0kVrLUO/yePP77v+ZIohmEwEbB4xSkokwNgUULnKiAv1sjweNxv/5AMmPQALEr+iKgEoQ2zYJQGFiZMk1hJjWiyMUwFC1RAZeGsGAsmZyd8xPzXsKAqU4ZdSY2CMSVqITDvwo1mvb4ZtknLYRgcx5amiN2uqEgWDRu117kGmFO+QnENs2RPmhzNJ0Co0bzkORhfrzVCGIbjPhCUrallfSMPSk7YBHOp+DGZTL9v71YKRR9o6jrHC7SOogW+lhiys7IBY0Xt5sVRI+Yiwyrpq3/F0uVnAkHwzTrPCTTh4AR+uOB4yRhGSUrUM7Ywpwv3/9azthu7QuX6nU5np1r0YcuhRu3y1BkcwTUTmAZlViXxudtWI3Q+fP0q4o9ml+5jjQc5x3XR63R6P65yyYPKlnV6GnXBocGVfoIyO6bUVbTgEm4fzVOdrgWiWS2iiJJcYqXszK582mHFvg9ZtjOXPilYI0nUad2xnX7dpygG4VhZ6oqiqqqvjo935BJlXFfSsNmO11HSxepZLFSynbdoDU53RW+nVlp9lmKGIWLLNEnWcMUq+Cy3RDWGWP1e7/eFzXKsBxbtMU94ZKRJkuaLTAqWabVaLMsYFGmaLcMdpeJ3d+3mnEN/gVQeWrsIAP70jVtG7G2YThBJrmRSGdPMMB4ZI971dQQ/gZSJldcAWDp7+dO7znNztBQ8Y56gbF4uWBKODATePvziHRrc50jkfp9lZpKhPU4ud+IDsOzho1+EPpmWSbGUW+af2oVVbzevKg6sIquvkQzRxi2UdG+Umf4fcBS7zbjKebT4s8H6Oyjz0j5skGNpaKGU4pvyXk1OZsXKFRyggoeDVSRD0XvbY2yZfQrJIlO5CumJw0q38941fBsM1v8I+s0Ieu/zxjl5c2gsumnj0zy3/7JmdqFJhWEc9xxFNyXMQRE5pazlaHNO60ImZI5Sg4iZRWBRkEHQLsITZdGgWlKpfdD6GBUjBitvtMZaQ9iVWyIjSWtr1fq4WGsftWDnxgsvRs97xGme13UW+10Mhoo/nv/zPu+jnltcavO2nqsHieIt25tOezpjLDMm01tPTjTDQCucHTmz5wruYgfvDZ/btnkXTmnrw0cDL49v5wGwHgpEIgHJJ4hcyXSaenQLYOVQ6SBXttnYko4mefD47qv3rvQMn9vMtNOj4Z6Bey93M5ssv1wqq4zGPR5vFk88OiKzllnDUDJy/RDEWQpGrisb7EKah14Q/7VeILErCy8VKDRRj9euSxdjD3uS8NcCl3pR0dgHInsiGpowbcY91YfZawdBIqkkOC1CXMrnrWd1GjtXaLqb33NhPuW0OJYVRk4Og9e240hKxpLCoUt6KhW8VWh//JfcF9PdXJ9xCVN6Zu0OWU6OHEBev/vuRz1hkOJIOK4V1A/FINDFia1SVD5tYsLksNFaxJmMUHxsh0ZaLhD8/vb65vena8fXzEjSS8Hu0QhWdZ7du3ivDaGefT/WxO1kEpa1GQDkxDWbEolESpxOS1KjiSWq6UBtdV+opNves6E+IpuK5e4pbl9ile8XM2rtCdvo+FF3GqBSNlviqO5fMpQwM+n3ywG/LyNZaSHgA3LnWdbIuBOb6Fy3umCRsd5tes6p/6UrxJnJGwnQotJZ3GA2OkPlJdxuSiKh3LoFKWF7IGA2zOcxmG9PGOsq+mLgEhoKTZxHTISGOvvq17N2LO0YtzuTL13xbnzmkjsvQo3bbLYUlSvN29PmiMEQMQcuITWdxG+OzGN49e5r6sePvSECfdWBIEoOdBgznCDrLXGvPZ1HnMib6dyUUOiTowIZApTQfwOssFzorxr/MDPtUtcpSd7yIbKiKR9OJu12GPXto2A24y5wdQfmgQiywnMdfgirSnXNAc2mDmPFMsoRfLJcYdXKNJUjIxOxTWuqCk8AZc72ktl8wwwYDEXlAq0NVf2vntTOMdQ6TB07nSI+wVtmRMEjRxyukYVZq5MzXnI3A0VRmYyvXR4IRBjBPVXAmgdv4BlPUM1yclC6A8ZqpxLWEoLI7SgVSqWzuvq/C2pU0bSql7mdUPttMTBeOtbQmPTDyejv7wcroMCs0O+yy2XK4nJdvtzscHTVGpciwycFFc46o3EQ6G2gabpVibIQaKM1ZrZXfnj44VEgZzbHBRO/VEP99Q8JQoPq7mBbQ6Ner1chaEDV2t2iHqxTGpunLxoiPvDC4y5Uu1jLRczhxHvVBbtbOtS9gLqjpTvY2tagp0uh//lx6mdwdj+KtLRaZEFsjhNqfL266WIaD9N4VFPXrl37+AmKtyMetpdWk0e4ewEu7FVe0cZ+/2dbsF6fp0DrVxs6Cw3BFxujHnupRCcDoDU9x5GuapzYoAqTWM0lnBfS+qTP2beqrVJN1Ivff7yzX7H9xT1LooXG0Fjjw5QLtBrpAtp6RWQcacA9gS4KRDIc9sYrLWWE4LGDs5iJxIzPVhrHs32+Ii+WFsI1m4QhMULyBWVSuChkWq1VIWLmyskyntPEtWgOJVusWo9v8z+kl0/I0mAcx59NYUm1wwjMiweh5qh2ELaDHgphl3nw0EQRBo2Y0B/QcwwKZaCXDh6kIEuosFN4ECLKOlgEUVAdgg4vbwRRvFFPsejcb7M337VH35kfEIdM/fj9/Z7n+XlE7/jigt4K3vnz49tXe389I/VuXa5QdOjQFBTg2GaKaJaXhH3b15tbX95l4bagF5zvH/afIC72mmoyoUPrB9//rb2VIRfTEJ7PrrLvICwCR12vQ28s8txO38Eyi+hyqNBO08ETGpbaJrmYQmdmliJn+vkFTLeHNzKpHCJS1LDNIn9o4ZuMEr+125+IZlc03PmxkO8f34LWm8twOUVkBgKWY7AOyud2FTsqEiL/utVub5IjgzYjc+b2i0MHn25cdrMccohMVMbYdGuUPL9raGUUJFnIbhGreUXF0GZEbm1stPJe43V5Bi2iomN9gIBIrrVb91MoyI144iVsUQEyEsYQ2TKaPIcWE7UxlljkwvEnV95iETNxnHjBamZ2lPHKcxB7jbEOz4vIDKc+LWKX4RLyoKpL69kiLm0l4TjONS5ndVPzM+l1p6NijA8sGjS6lkj7BkuGLTaAIhuL0vSsMnsgc43dviV3erHaSY4Y+QTETomIArft3DJ3j0iqoEpqh5AZTI0KR/2JIlqsD2p3ZEkzDBUwDE2yZbNWb8QoZGKMB/PSlFurDRioHAezCeO9OT0eNfOeWvYucEQVOvt2umW6Q16cWUVixUpNtjUdExAMSa6ZUEt5R7BcuXU09H4BcAUQS2wvWYpJK/yoCaN1yp3NoKCuWyqTz3eH1ngmRTONSkmWVLwcAR4G62tohVjQRf9IrjmzyOZEolw6Nx3zlvXIwICgv75wvQf9E2Mb9YEJTjoOhyDXo77GyZ0/GW4jA9IJNzKidS5/VxJm36BqkiRpmqHj1dDtgS+1SJUPVPQG8lG9plQ52mt/4HEEBah24RR/pOE1kUpFyldR0Tp3dMkQqzjxROExNLNXy0Iy2H1Dt9+VoqniNdFKvtTcLaAPuS0U80gUCu66jCuBvXfkeo0pFOmZxtpqtRjyAauMP/4nuH5A7H3C+cs16p+8XK/81HuVKtbsdWOzK4GTJ8Kly+dPn7vFB8XOzs1O8cmdbZZ069gU/9pGezUZOn8NdJNFQSiwSyIfOajg7Ydzs/ip87no36XahDJanH9IYuvuLm/owv+ugkoEhUAEp5tPHsadOYlJmfMUptkf+ZFI+Bgquoct9mAvsyVD+I/QmBBi6VOgcv+BzwwmjRscVR3lm/308l9H72nAkaStWly5EVIsfunBWcfxq/H9schQKASRWL1kr+Ym1dFuJAsOcO/i1feOn/hEoVFomJ4prVJUo0Kh5XAgBvxm3dxCkwrjAH4857BpmmNDErN8ErZmF+jYtijNPWxRG/oSKI10bbRLDdZDQkm6JNqo1DZiTedqixWtPfRgxLKLg4g9FA2q14ieoocgsB2kEOr/qfOox3Ox9ntxOpk//rfvO+47l5pvQuiK0SxDfQkhyb9F5miqYJgcnSME8oCWIhSyAJRZKd0zOiExvLYGy1HZDD7Rx28mWc46XEpHIHYsOtqEglaT2Ma8RWI7dWyjzGZ+5kLW/EFTxkwzJBS0XZSXxBg1ZZ/YYjvhwFiwFktozDR0Zjk6Dkj4k+lJITMG6ZyoHmV2tpwbMbUmk8v1kLErrZo/mVTKSxT36PCpYzv+Vwzmhfr06S+auK98lQHq5eO8ZepNKbaVvqZ0NJVz+/PnDzPMbvFnAqp//n7k02o6HfgAjVk+nbsl/CGjatj9rjxVxoym6Yzajn19UgwTrv54i9Xns0ZedGjKm3XP8rQQ4UmlPOzfK6+gedV0pXi9p69fp/+cbJoDLQHsGRf1/NLS0qXA8w+nu8uaaWZIjJNaRUrFOkJhQ147+nBSemuuD633cN2JLjnd2jfntuGYMDrGZB464HnkBewcWai7Z+qUcE5F793sSSQSHo9Xv2tPjTRrKzOwQ3Y243Vq3YCAiyulzQZX6dWvkknjE6Y0+Iqs0Cydbo7cADeNWo18NJr5+S+nX9z/FIl8MCgUqUIUKkPCq2+vJjBvKkVtLyp+xzGUxz687MmmZLLxCSZMfc7qmUYNZr40ELBGbt6/H4/H79+IRKwjgUAzEp5QpcqgMnh2eeFxW9G//Y+ituNYqfuNELNOEXvF7pxYOPxO8zM+ks7RDKQZOM0A9Lonb4EPozQeg000B5OmZHK6S1CMfLkeMqffGdIsQdDKA2aKFCeG9myVkcNNO34cvdJn4y4jy3QymRzFAKGBkTc7eND5bj7ewqUWGE9xo/LsATVbXxN803OLf7EYg5CZB4R3sZpCM3/4GbcaV8gUU4jxh2e3kDIS5z2eNjDQ399phpBNWnAxuWTMDoafqdVLn0bKub1WsZ1U2mDr01VrhufnL8LtEl0WouyBuclYdNpsNhobG00g1miejsYm4d3cs3tWXWxGvVPDxI1fWvX5Sso/yMrf9VZr0ZtG7n02oWODk2NwSq/LYrF0DfR3PpmMXTAjGzamRnO0k2chz/POnzMD5pfiny61jATyJTahyiUOTvFMwQ9TwVWQKqH1be4TG42IbID4MHGKSWYKRrzrIEA9Y15Aei0jzWnf6rgiozUebF21BlaDU8HsaCmZKhC0I8lKuIDzXcSpu3MtoHEiszCYFTL/4vx3w3pRKRRTQV+62Wp9/XoieB0RnHhtDTB+rZ8r8DLxrALE0M/lhraZ5ZfdMP2f+TNmasYKvqia1eH4dn0iX/tBSGxQgUgh4FE1PhHJJ73lrXixaQvfxFjWIb9qXYO9fuiBn6IoQ0c30NHxcnnG3lBH5Pr3sDcbtqmRMtMW3F5b19O5dyMChpQwhrpeVygUGmpogAMvdWTJAFfqDaDQwrE+KcYnrM1Zs8EjogMmGvsacIbr8kimN7TmvDjU0ggftIC4gImn7jIyq+dc6qL3VoMoj7xqYPZLTEsKBgwOzthnb18cQoQyITvAziSiK2qa3u2FgAmp+QaFzRoFtz+S2f1rpewMXe4dqm9rqMML9PovGEe7MKLdw6cGbRBAZoJiMUJM/jhwhdDBNDzr3zkd7cxo4nuE1UaEpgZsMYRp27nGQ6j39u46DJ/8OIYzJy29BgWvmvXbRlQ+cXGNn52Xbz9+Yinu0FoPr1qvkVcsimNi0EEy+Qm5DrVVF5emHolxyW2t6R81c6/gxn6xR/5ca4Ls7G0jsTy4PrPdr/UmDCr2VYpXipq4M8bh1jiGiURSvyaGQw2S9UxuViAvGdSBtH0X2FGUSgXLpooyePTtsrlFJQaA2+i0kS33SoKJhRwSZebKHhyQtCdQGr3VBUdUa9pr4eRRu5Ik0J9b6Fn/Up8YGBuNwl7RxDdahctMOGg6WDW9FHgZdvEUsNR9bZGpScIygG5HeRVDHWEeHcAqoeGMOLPLdzOLuUEvxfi4paUXZOybsvjqS7gBXGEn1Isz92xwMAzPAacTXvr26XmQSnhrq4WqdpGmF8jSu1GY+qoEuys/8B89ojZVfX0DVk7KX4WA+xAQfoPb3TsrZghJe2h6sUgC/4jqqwurGMmsq2A4PKr6XeX3y8GoBLl8q0NJYILcoWmto+guSfT9ANTX/5m5KHBAG1q//GtVqd61ngWHUihsMjdNu2XFXhfA6//MBilw8Q/mnoSdCr+c8bpKA9qVO2dxoZDRdwq9TDHI4z+a2V15ryoVeDEMOlW5wMmv0QjktjhsIzEuzmohZLkmscRMSfMTAvtnJG0hJOGHj892ZbGbHHyvghdDz8rDO8M2afZIfDHECvx+OLeVM5knu7D/YveZwbD8t5xC4WKrGd6DFgttj3tl4WGexcU7Djjfg6NcPgRdYiwaG7Ng/4vOI5dDFstxpl5nW6BFoQVZNzy6SRBDNxxuAKQ+XH53MVuHppGjh66AawtzZ0lsg5AcYC9PoaHd5HpRr9AVAT1i2yg1XfFmw3XIXnj4oXpRW5naihLbKHD7X8ECzTLbPjyKCS1I80mJzqR54LCmXqDFf4nPtgevZMME/iuIjsgVBQzUBcx6rhrszDhzCJHRWQpcOkNrQEZ0Js3jZKA/4FtB0FkDdEIZyzxpfJFIf2chonNaKc6yK4KTYSABJ7Y8IF26ivLSnvJAO4bhqvzBcZIb+zx49gS2bOdNAw6sDRbAtyrpqzSwYbZgGicTw6ACjP7TCjgGmZtGwSgYBaNgFIwCegPEWB0zH1L7AdhdGVjAyOuGOE0E4RgmXn34/JK7O78sJyPEF+6yzJCJHRUVfdq2GVkA7ZtBzqQgEIVRO4VCUAg7oju3xkuQ7hN4A87gPTv5T/NWAyjtYlaT+K+m30KhsfFL1asyMdGq6kT0lrOip9Tnz+2bSIrd9+kSjb2Lp8USIHXPflGDENOZ07cYPllVKJNWAATAdRnMtoytApAEBM5+T4bww7JmkLlwAV8VMDduFtgKWDySGtaQWH9PM2Dz/p0FZlayCuDdF7CY7MHCNScY3/Nf+uVerqrJkbhyBhrTaCSguH8SoIR5gdURiZ9gtYM07GZ1o9LeNJf3dwAu1hd3xyBrA3mLvbvA2jiti8c2QPhXc6vVHaKkSgBXbKTJXiOI9ghSgopwywmmBxMA/zF/FwAIPdzYGwJoDyIl75SR0AK6qQNEgHwV383J/+5xgJEkYG8/YIzPlgCxVHeBrQQ3dCbfsopKDNJoiGGVUMsZo25Pxq91XMpgR5AnVsAOS+wEMdxYgsJaS3i3rdJaL0xBTCZCRYrXIOHPMpBaKwvoJoNZAflkF9iBplNcb5IHiEhKKaYnAVAPh/0RD5IiRG+hq1wGReJ5mH8muO4DxtcxTZ4S6r6IkTfGjN7UrVdKmV5A5xCltCVKnstAuCRLUFUG4xrwTQGbSZoDzN/Y5nXNaiUUPzy25r7f2qNTaIiJ1SFCPnjUZNM8t4s4lEsBM4TQp1TSchcYd6DgHcHxEsK8+fY5rWyVCHVZxXb2sUXCDiWVDrBhx52PpKdF0l7KSYOG3M1yM12AsdEgczU52x5gjQLCowrZ/K9zlye7T5MPLlxfuI7K8/xgmvOiUutjU1tpv9WoVP/wylSMRU/6Pl7/kxanLe3SsltV85r9pZb/8y7fl0BfffXVf6M/kiRLnaZ61+EAAAAASUVORK5CYII=\\"\\n class=\\"logo logo-${client_id!'none'}\\" id=\\"logo\\">\\n      [/#if]\\n\\n      [#if title?has_content]\\n          <h2>${title}</h2>\\n      [/#if]\\n      <main>\\n          [#nested/]\\n          [@printErrorAlerts rowClass colClass/]\\n          [@printInfoAlerts rowClass colClass/]\\n          [#if !locale?contains(\\"false\\")]\\n             <div class=\\"locale-selector\\">\\n              [@appriseLocalSelector/]\\n            </div>\\n          [/#if]\\n      </main>\\n      <script>\\n        setTimeout(() => panel.classList.add('show'));\\n      </script>\\n  </div>\\n</main>\\n[/#macro]\\n\\n[#macro alert message type icon includeDismissButton=true rowClass=\\"row center-xs\\" colClass=\\"col-xs col-sm-8 col-md-6 col-lg-5 col-xl-4\\"]\\n    <div class=\\"alert ${type}\\" style=\\"width: 60%;\\">\\n      <i class=\\"fa fa-${icon}\\"></i>\\n      <p>\\n        ${message}\\n      </p>\\n      [#if includeDismissButton]\\n        <a href=\\"#\\" class=\\"dismiss-button\\"><i class=\\"fa fa-times-circle\\"></i></a>\\n      [/#if]\\n    </div>\\n[/#macro]\\n\\n[#macro accountMain rowClass=\\"row center-xs\\" colClass=\\"col-xs col-sm-8 col-md-6 col-lg-5 col-xl-4\\" actionURL=\\"\\" actionText=\\"Go back\\" actionDirection=\\"back\\"]\\n<main class=\\"page-body container\\">\\n      [#nested/]\\n  [@accountFooter rowClass \\"col-xs-6 col-sm-6 col-md-5 col-lg-4\\" actionURL actionText actionDirection/]\\n</main>\\n[/#macro]\\n\\n[#macro accountFooter rowClass colClass actionURL actionText actionDirection]\\n<div class=\\"locale-selector\\">\\n    [@localSelector/]\\n  \\n  [#if actionURL?has_content]\\n    [#if !actionURL?contains(\\"client_id\\")]\\n      [#if actionURL?contains(\\"?\\")]\\n       [#local actionURL = actionURL + \\"&client_id=${client_id}\\"/]\\n      [#else]\\n       [#local actionURL = actionURL + \\"?client_id=${client_id}\\"/]\\n      [/#if]\\n    [/#if]\\n    [#if actionDirection == \\"back\\"]\\n      <div class=\\"outside-panel-link\\"><a href=\\"${actionURL}\\"> <i class=\\"fa fa-arrow-left\\"></i> ${actionText}</a></div>\\n    [#else]\\n      <div class=\\"outside-panel-link\\"><a href=\\"${actionURL}\\">${actionText} <i class=\\"fa fa-arrow-right\\"></i></a></div>\\n    [/#if]\\n  [/#if]\\n  </div>\\n[/#macro]\\n\\n[#macro accountPanel title tenant user action showEdit]\\n<div class=\\"panel\\">\\n  [#if title?has_content]\\n    <h2>${title}</h2>\\n  [/#if]\\n  <main>\\n   <div class=\\"row mb-5 user-details\\">\\n      [#-- Column 1 --]\\n      <div class=\\"col-xs-12 col-md-4 col-lg-4 tight-left\\" style=\\"padding-bottom: 0;\\">\\n        <div class=\\"avatar pr-2\\">\\n          <div>\\n            [#if user.imageUrl??]\\n              <img src=\\"${user.imageUrl}\\" class=\\"profile w-100\\" alt=\\"profile image\\"/>\\n            [#elseif user.lookupEmail()??]\\n              <img src=\\"${function.gravatar(user.lookupEmail(), 200)}\\" class=\\"profile w-100\\" alt=\\"profile image\\"/>\\n            [#else]\\n              <img src=\\"${request.contextPath}/images/missing-user-image.jpg\\" class=\\"profile w-100\\" alt=\\"profile image\\"/>\\n            [/#if]\\n          </div>\\n          <div>${display(user, \\"name\\")}</div>\\n       </div>\\n      </div>\\n      [#-- Column 2 --]\\n      <div class=\\"col-xs-12 col-md-8 col-lg-8 tight-left\\">\\n        [#nested/]\\n      </div>\\n      [#if action == \\"view\\"]\\n        <div class=\\"panel-actions\\">\\n         <div class=\\"status\\">\\n           [#if showEdit]\\n            <a id=\\"edit-profile\\" class=\\"blue icon\\" href=\\"/account/edit?client_id=${client_id}\\">\\n              <span style=\\"font-size: 0.9rem;\\">\\n              <i class=\\"fa fa-pencil blue-text\\" data-tooltip=\\"${theme.message(\\"edit-profile\\")}\\"></i>\\n              </span>\\n            </a>\\n           [/#if]\\n         </div>\\n       </div>\\n      [/#if]\\n  </div>\\n  </main>\\n</div>\\n[/#macro]\\n\\n[#macro footer]\\n  [#nested/]\\n\\n  [#-- Powered by FusionAuth branding. This backlink helps FusionAuth web ranking so more\\n       people can find us! However, we always want to give the developer choice, remove this if you like. --]\\n  <div style=\\"position: fixed; bottom: 5px; right: 0; padding-bottom: 5px; padding-right: 10px;\\">\\n    \\n  </div>\\n[/#macro]\\n\\n\\n[#macro button text icon=\\"arrow-right\\" color=\\"blue\\" disabled=false name=\\"\\" value=\\"\\"]\\n<button class=\\"${color} button${disabled?then(' disabled', '')}\\"[#if disabled] disabled=\\"disabled\\"[/#if][#if name !=\\"\\"]name=\\"${name}\\"[/#if][#if value !=\\"\\"]value=\\"${value}\\"[/#if]><i class=\\"fa fa-${icon}\\"></i> ${text}</button>\\n[/#macro]\\n\\n[#macro html]\\n<!DOCTYPE html>\\n<html lang=\\"en\\">\\n  [#nested/]\\n</html>\\n[/#macro]\\n\\n[#macro head title=\\"e-MARIS Login\\" author=\\"emaris\\" description=\\"User Management Redefined. A Single Sign-On solution for your entire enterprise.\\"]\\n<head>\\n  <title>${title}</title>\\n  <meta name=\\"viewport\\" content=\\"width=device-width, initial-scale=1, shrink-to-fit=no\\">\\n  <meta http-equiv=\\"Content-Type\\" content=\\"text/html; charset=UTF-8\\">\\n  <meta name=\\"application-name\\" content=\\"FusionAuth\\">\\n  <meta name=\\"author\\" content=\\"FusionAuth\\">\\n  <meta name=\\"description\\" content=\\"${description}\\">\\n  <meta name=\\"robots\\" content=\\"index, follow\\">\\n\\n  [#-- https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy --]\\n  <meta name=\\"referrer\\" content=\\"strict-origin\\">\\n\\n  [#--  Browser Address bar color --]\\n  <meta name=\\"theme-color\\" content=\\"#ffffff\\">\\n\\n  [#-- Begin Favicon Madness\\n       You can check if this is working using this site https://realfavicongenerator.net/\\n       Questions about icon names and sizes? https://realfavicongenerator.net/faq#.XrBnPJNKg3g --]\\n\\n  [#-- Apple & iOS --]\\n  <link rel=\\"apple-touch-icon\\" sizes=\\"57x57\\" href=\\"/images/apple-icon-57x57.png\\">\\n  <link rel=\\"apple-touch-icon\\" sizes=\\"60x60\\" href=\\"/images/apple-icon-60x60.png\\">\\n  <link rel=\\"apple-touch-icon\\" sizes=\\"72x72\\" href=\\"/images/apple-icon-72x72.png\\">\\n  <link rel=\\"apple-touch-icon\\" sizes=\\"76x76\\" href=\\"/images/apple-icon-76x76.png\\">\\n  <link rel=\\"apple-touch-icon\\" sizes=\\"114x114\\" href=\\"/images/apple-icon-114x114.png\\">\\n  <link rel=\\"apple-touch-icon\\" sizes=\\"120x120\\" href=\\"/images/apple-icon-120x120.png\\">\\n  <link rel=\\"apple-touch-icon\\" sizes=\\"144x144\\" href=\\"/images/apple-icon-144x144.png\\">\\n  <link rel=\\"apple-touch-icon\\" sizes=\\"152x152\\" href=\\"/images/apple-icon-152x152.png\\">\\n  <link rel=\\"apple-touch-icon\\" sizes=\\"180x180\\" href=\\"/images/apple-icon-180x180.png\\">\\n\\n  [#--  Android Icons --]\\n  <link rel=\\"manifest\\" href=\\"/images/manifest.json\\">\\n\\n  [#-- IE 11+ configuration --]\\n  <meta name=\\"msapplication-config\\" content=\\"/images/browserconfig.xml\\" />\\n\\n  [#-- Windows 8 Compatible --]\\n  <meta name=\\"msapplication-TileColor\\" content=\\"#ffffff\\">\\n  <meta name=\\"msapplication-TileImage\\" content=\\"/images/ms-icon-144x144.png\\">\\n\\n  [#--  Standard Favicon Fare --]\\n  <link rel=\\"icon\\" type=\\"image/png\\" sizes=\\"16x16\\" href=\\"/images/favicon-16x16.png\\">\\n  <link rel=\\"icon\\" type=\\"image/png\\" sizes=\\"32x32\\" href=\\"/images/favicon-32x32.png\\">\\n  <link rel=\\"icon\\" type=\\"image/png\\" sizes=\\"96x96\\" href=\\"/images/favicon-96x96.png\\">\\n  <link rel=\\"icon\\" type=\\"image/png\\" sizes=\\"128\\" href=\\"/images/favicon-128.png\\">\\n\\n  [#-- End Favicon Madness --]\\n\\n  <link rel=\\"stylesheet\\" href=\\"/css/font-awesome-4.7.0.min.css\\"/>\\n  <link rel=\\"stylesheet\\" href=\\"/css/fusionauth-style.css?version=${version}\\"/>\\n\\n  [#-- Theme Stylesheet, only Authorize defines this boolean.\\n       Using the ?no_esc on the stylesheet to allow selectors that contain a > symbols.\\n       Once insde of a style tag we are safe and the stylesheet is validated not to contain an end style tag --]\\n  [#if !(bypassTheme!false)]\\n    <style>\\n    ${theme.stylesheet()?no_esc}\\n    </style>\\n  [/#if]\\n\\n  <script src=\\"${request.contextPath}/js/prime-min-1.5.3.js?version=${version}\\"></script>\\n  <script src=\\"/js/oauth2/LocaleSelect.js?version=${version}\\"></script>\\n  <script>\\n    \\"use strict\\";\\n    Prime.Document.onReady(function() {\\n      Prime.Document.query('.alert').each(function(e) {\\n        var dismissButton = e.queryFirst('a.dismiss-button');\\n        if (dismissButton !== null) {\\n          new Prime.Widgets.Dismissable(e, dismissButton).initialize();\\n        }\\n      });\\n      Prime.Document.query('[data-tooltip]').each(function(e) {\\n        new Prime.Widgets.Tooltip(e).withClassName('tooltip').initialize();\\n      });\\n      Prime.Document.query('.date-picker').each(function(e) {\\n        new Prime.Widgets.DateTimePicker(e).withDateOnly().initialize();\\n      });\\n      [#-- You may optionally remove the Locale Selector, or it may not be on every page. --]\\n      var localeSelect = Prime.Document.queryById('locale-select');\\n      if (localeSelect !== null) {\\n        new FusionAuth.OAuth2.LocaleSelect(localeSelect);\\n      }\\n    });\\n    FusionAuth.Version = \\"${version}\\";\\n  </script>\\n\\n  [#-- The nested, page-specific head HTML goes here --]\\n  [#nested/]\\n\\n</head>\\n[/#macro]\\n\\n[#macro body]\\n<body class=\\"app-sidebar-closed\\">\\n<main>\\n  [#nested/]\\n</main>\\n</body>\\n[/#macro]\\n\\n[#macro header]\\n  <header class=\\"app-header\\">\\n    <div class=\\"right-menu\\" [#if request.requestURI == \\"/\\"]style=\\"display: block !important;\\" [/#if]>\\n      <nav>\\n        <ul>\\n          [#if request.requestURI == \\"/\\"]\\n            <li><a href=\\"${request.contextPath}/admin/\\" title=\\"Administrative login\\"><i class=\\"fa fa-lock\\" style=\\"font-size: 18px;\\"></i></a></li>\\n          [#elseif request.requestURI?starts_with(\\"/account\\")]\\n            <li><a href=\\"${request.contextPath}/account/logout?client_id=${client_id!''}\\" title=\\"Logout\\"><i class=\\"fa fa-sign-out\\"></i></a></li>\\n          [#else]\\n            <li class=\\"help\\"><a target=\\"_blank\\" href=\\"https://fusionauth.io/docs\\"><i class=\\"fa fa-question-circle-o\\"></i> ${theme.message(\\"help\\")}</a></li>\\n          [/#if]\\n        </ul>\\n      </nav>\\n    </div>\\n  </header>\\n\\n  [#nested/]\\n[/#macro]\\n\\n[#macro alternativeLoginsScript clientId identityProviders]\\n  [#if identityProviders[\\"Apple\\"]?has_content]\\n    <script src=\\"https://appleid.cdn-apple.com/appleauth/static/jsapi/appleid/1/en_US/appleid.auth.js\\"></script>\\n    <script src=\\"/js/identityProvider/Apple.js?version=${version}\\"></script>\\n  [/#if]\\n  [#if identityProviders[\\"Facebook\\"]?has_content]\\n    <script src=\\"https://connect.facebook.net/en_US/sdk.js\\"></script>\\n    <script src=\\"/js/identityProvider/Facebook.js?version=${version}\\" data-app-id=\\"${identityProviders[\\"Facebook\\"][0].lookupAppId(clientId)}\\"></script>\\n  [/#if]\\n  [#if identityProviders[\\"Google\\"]?has_content]\\n    <script src=\\"https://apis.google.com/js/api:client.js\\"></script>\\n    <script src=\\"/js/identityProvider/Google.js?version=${version}\\" data-client-id=\\"${identityProviders[\\"Google\\"][0].lookupClientId(clientId)}\\"></script>\\n  [/#if]\\n  [#if identityProviders[\\"Twitter\\"]?has_content]\\n    [#-- This is the FusionAuth clientId --]\\n    <script src=\\"/js/identityProvider/Twitter.js?version=${version}\\" data-client-id=\\"${clientId}\\"></script>\\n  [/#if]\\n  [#if identityProviders[\\"OpenIDConnect\\"]?has_content || identityProviders[\\"SAMLv2\\"]?has_content || identityProviders[\\"LinkedIn\\"]?has_content]\\n    <script src=\\"/js/identityProvider/Redirect.js?version=${version}\\"></script>\\n  [/#if]\\n[/#macro]\\n\\n[#macro localSelector]\\n<label class=\\"select\\">\\n  <select id=\\"locale-select\\" name=\\"locale\\" class=\\"select\\">\\n    <option value=\\"en\\" [#if locale == 'en']selected[/#if]>English</option>\\n      [#list theme.additionalLocales() as l]\\n        <option value=\\"${l}\\" [#if locale == l]selected[/#if]>${l.getDisplayLanguage(locale)}</option>\\n      [/#list]\\n  </select>\\n</label>\\n[/#macro]\\n\\n[#macro accountPanelFull title=\\"\\"]\\n<div class=\\"panel\\">\\n  [#if title?has_content]\\n    <h2>${title}</h2>\\n  [/#if]\\n  <main>\\n    [#nested/]\\n  </main>\\n</div>\\n[/#macro]\\n\\n[#-- Below are the social login buttons and helpers --]\\n[#macro appleButton identityProvider clientId]\\n [#-- https://developer.apple.com/design/human-interface-guidelines/sign-in-with-apple/overview/buttons/ --]\\n <button id=\\"apple-login-button\\" class=\\"apple login-button\\" data-scope=\\"${identityProvider.lookupScope(clientId)!''}\\" data-services-id=\\"${identityProvider.lookupServicesId(clientId)}\\">\\n   <div>\\n     <div class=\\"icon\\">\\n      <svg version=\\"1.1\\" viewBox=\\"4 6 30 30\\" xmlns=\\"http://www.w3.org/2000/svg\\">\\n        <g id=\\"Left-Black-Logo-Large\\" stroke=\\"none\\" stroke-width=\\"1\\" fill=\\"none\\" fill-rule=\\"evenodd\\">\\n          <path class=\\"cls-1\\" d=\\"M19.8196726,13.1384615 C20.902953,13.1384615 22.2608678,12.406103 23.0695137,11.4296249 C23.8018722,10.5446917 24.3358837,9.30883662 24.3358837,8.07298156 C24.3358837,7.9051494 24.3206262,7.73731723 24.2901113,7.6 C23.0847711,7.64577241 21.6353115,8.4086459 20.7656357,9.43089638 C20.0790496,10.2090273 19.4534933,11.4296249 19.4534933,12.6807374 C19.4534933,12.8638271 19.4840083,13.0469167 19.4992657,13.1079466 C19.5755531,13.1232041 19.6976128,13.1384615 19.8196726,13.1384615 Z M16.0053051,31.6 C17.4852797,31.6 18.1413509,30.6082645 19.9875048,30.6082645 C21.8641736,30.6082645 22.2761252,31.5694851 23.923932,31.5694851 C25.5412238,31.5694851 26.6245041,30.074253 27.6467546,28.6095359 C28.7910648,26.9312142 29.2640464,25.2834075 29.2945613,25.2071202 C29.1877591,25.1766052 26.0904927,23.9102352 26.0904927,20.3552448 C26.0904927,17.2732359 28.5316879,15.8848061 28.6690051,15.7780038 C27.0517133,13.4588684 24.5952606,13.3978385 23.923932,13.3978385 C22.1082931,13.3978385 20.6283185,14.4963764 19.6976128,14.4963764 C18.6906198,14.4963764 17.36322,13.4588684 15.7917006,13.4588684 C12.8012365,13.4588684 9.765,15.9305785 9.765,20.5993643 C9.765,23.4982835 10.8940528,26.565035 12.2824825,28.548506 C13.4725652,30.2268277 14.5100731,31.6 16.0053051,31.6 Z\\" id=\\"���\\"  fill-rule=\\"nonzero\\"></path>\\n        </g>\\n      </svg>\\n     </div>\\n     <div class=\\"text\\">${identityProvider.lookupButtonText(clientId)?trim}</div>\\n   </div>\\n </button>\\n[/#macro]\\n\\n[#macro facebookButton identityProvider clientId]\\n <button id=\\"facebook-login-button\\" class=\\"facebook login-button\\" data-permissions=\\"${identityProvider.lookupPermissions(clientId)!''}\\">\\n   <div>\\n     <div class=\\"icon\\">\\n       <svg version=\\"1.1\\" xmlns=\\"http://www.w3.org/2000/svg\\" viewBox=\\"0 0 216 216\\">\\n         <path class=\\"cls-1\\" d=\\"M204.1 0H11.9C5.3 0 0 5.3 0 11.9v192.2c0 6.6 5.3 11.9 11.9 11.9h103.5v-83.6H87.2V99.8h28.1v-24c0-27.9 17-43.1 41.9-43.1 11.9 0 22.2.9 25.2 1.3v29.2h-17.3c-13.5 0-16.2 6.4-16.2 15.9v20.8h32.3l-4.2 32.6h-28V216h55c6.6 0 11.9-5.3 11.9-11.9V11.9C216 5.3 210.7 0 204.1 0z\\"></path>\\n       </svg>\\n     </div>\\n     <div class=\\"text\\">${identityProvider.lookupButtonText(clientId)?trim}</div>\\n   </div>\\n </button>\\n[/#macro]\\n\\n[#macro googleButton identityProvider clientId]\\n <button id=\\"google-login-button\\" class=\\"google login-button\\" data-scope=\\"${identityProvider.lookupScope(clientId)!''}\\">\\n   <div>\\n     <div class=\\"icon\\">\\n       <svg version=\\"1.1\\" viewBox=\\"0 0 48 48\\" xmlns=\\"http://www.w3.org/2000/svg\\">\\n         <g>\\n           <path class=\\"cls-1\\" d=\\"M24 9.5c3.54 0 6.71 1.22 9.21 3.6l6.85-6.85C35.9 2.38 30.47 0 24 0 14.62 0 6.51 5.38 2.56 13.22l7.98 6.19C12.43 13.72 17.74 9.5 24 9.5z\\"></path>\\n           <path class=\\"cls-2\\" d=\\"M46.98 24.55c0-1.57-.15-3.09-.38-4.55H24v9.02h12.94c-.58 2.96-2.26 5.48-4.78 7.18l7.73 6c4.51-4.18 7.09-10.36 7.09-17.65z\\"></path>\\n           <path class=\\"cls-3\\" d=\\"M10.53 28.59c-.48-1.45-.76-2.99-.76-4.59s.27-3.14.76-4.59l-7.98-6.19C.92 16.46 0 20.12 0 24c0 3.88.92 7.54 2.56 10.78l7.97-6.19z\\"></path>\\n           <path class=\\"cls-4\\" d=\\"M24 48c6.48 0 11.93-2.13 15.89-5.81l-7.73-6c-2.15 1.45-4.92 2.3-8.16 2.3-6.26 0-11.57-4.22-13.47-9.91l-7.98 6.19C6.51 42.62 14.62 48 24 48z\\"></path>\\n           <path class=\\"cls-5\\" d=\\"M0 0h48v48H0z\\"></path>\\n         </g>\\n       </svg>\\n     </div>\\n     <div class=\\"text\\">${identityProvider.lookupButtonText(clientId)?trim}</div>\\n   </div>\\n </button>\\n[/#macro]\\n\\n[#macro linkedInBottom identityProvider clientId]\\n <button id=\\"linkedin-login-button\\" class=\\"linkedin login-button\\" data-identity-provider-id=\\"${identityProvider.id}\\">\\n   <div>\\n     <div class=\\"icon\\">\\n       <svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n            viewBox=\\"0 0 382 382\\" style=\\"enable-background:new 0 0 382 382;\\" xml:space=\\"preserve\\">\\n       <path style=\\"fill:#0077B7;\\" d=\\"M347.445,0H34.555C15.471,0,0,15.471,0,34.555v312.889C0,366.529,15.471,382,34.555,382h312.889\\n        C366.529,382,382,366.529,382,347.444V34.555C382,15.471,366.529,0,347.445,0z M118.207,329.844c0,5.554-4.502,10.056-10.056,10.056\\n        H65.345c-5.554,0-10.056-4.502-10.056-10.056V150.403c0-5.554,4.502-10.056,10.056-10.056h42.806\\n        c5.554,0,10.056,4.502,10.056,10.056V329.844z M86.748,123.432c-22.459,0-40.666-18.207-40.666-40.666S64.289,42.1,86.748,42.1\\n        s40.666,18.207,40.666,40.666S109.208,123.432,86.748,123.432z M341.91,330.654c0,5.106-4.14,9.246-9.246,9.246H286.73\\n        c-5.106,0-9.246-4.14-9.246-9.246v-84.168c0-12.556,3.683-55.021-32.813-55.021c-28.309,0-34.051,29.066-35.204,42.11v97.079\\n        c0,5.106-4.139,9.246-9.246,9.246h-44.426c-5.106,0-9.246-4.14-9.246-9.246V149.593c0-5.106,4.14-9.246,9.246-9.246h44.426\\n        c5.106,0,9.246,4.14,9.246,9.246v15.655c10.497-15.753,26.097-27.912,59.312-27.912c73.552,0,73.131,68.716,73.131,106.472\\n        L341.91,330.654L341.91,330.654z\\"/>\\n       </svg>\\n     </div>\\n     <div class=\\"text\\">${identityProvider.lookupButtonText(clientId)?trim}</div>\\n   </div>\\n </button>\\n[/#macro]\\n\\n[#macro twitterButton identityProvider clientId]\\n <button id=\\"twitter-login-button\\" class=\\"twitter login-button\\">\\n   <div>\\n     <div class=\\"icon\\">\\n       <svg version=\\"1.1\\" viewBox=\\"0 0 400 400\\" xmlns=\\"http://www.w3.org/2000/svg\\">\\n         <g>\\n           <rect class=\\"cls-1\\" width=\\"400\\" height=\\"400\\"></rect>\\n         </g>\\n         <g>\\n           <path class=\\"cls-2\\" d=\\"M153.62,301.59c94.34,0,145.94-78.16,145.94-145.94,0-2.22,0-4.43-.15-6.63A104.36,104.36,0,0,0,325,122.47a102.38,102.38,0,0,1-29.46,8.07,51.47,51.47,0,0,0,22.55-28.37,102.79,102.79,0,0,1-32.57,12.45,51.34,51.34,0,0,0-87.41,46.78A145.62,145.62,0,0,1,92.4,107.81a51.33,51.33,0,0,0,15.88,68.47A50.91,50.91,0,0,1,85,169.86c0,.21,0,.43,0,.65a51.31,51.31,0,0,0,41.15,50.28,51.21,51.21,0,0,1-23.16.88,51.35,51.35,0,0,0,47.92,35.62,102.92,102.92,0,0,1-63.7,22A104.41,104.41,0,0,1,75,278.55a145.21,145.21,0,0,0,78.62,23\\"></path>\\n           <rect class=\\"cls-3\\" width=\\"400\\" height=\\"400\\"></rect>\\n         </g>\\n       </svg>\\n     </div>\\n     <div class=\\"text\\">${identityProvider.lookupButtonText(clientId)?trim}</div>\\n   </div>\\n </button>\\n[/#macro]\\n\\n[#macro openIDConnectButton identityProvider clientId]\\n <button class=\\"openid login-button\\" data-identity-provider-id=\\"${identityProvider.id}\\">\\n   <div>\\n     <div class=\\"icon\\">\\n       [#if identityProvider.lookupButtonImageURL(clientId)?has_content]\\n         <img src=\\"${identityProvider.lookupButtonImageURL(clientId)}\\" title=\\"OpenID Connect Logo\\" alt=\\"OpenID Connect Logo\\"/>\\n       [#else]\\n         <svg version=\\"1.1\\" viewBox=\\"0 0 100 100\\" xmlns=\\"http://www.w3.org/2000/svg\\">\\n           <g id=\\"g2189\\">\\n             <g id=\\"g2202\\">\\n               <path class=\\"cls-1\\" d=\\"M87.57,39.57c-8.9-5.55-21.38-9-34.95-9C25.18,30.59,3,44.31,3,61.17,3,76.64,21.46,89.34,45.46,91.52v-8.9c-16.12-2-28.24-10.87-28.24-21.45,0-12,15.84-21.9,35.4-21.9,9.78,0,18.6,2.41,24.95,6.43l-9,5.62H96.84V33.8Z\\"></path>\\n               <path class=\\"cls-2\\" d=\\"M45.46,15.41v76l14.23-8.9V6.22Z\\"></path>\\n             </g>\\n           </g>\\n         </svg>\\n       [/#if]\\n     </div>\\n     <div class=\\"text\\">${identityProvider.lookupButtonText(clientId)?trim}</div>\\n   </div>\\n </button>\\n[/#macro]\\n\\n[#macro samlv2Button identityProvider clientId]\\n <button class=\\"samlv2 login-button\\" data-identity-provider-id=\\"${identityProvider.id}\\">\\n   <div>\\n     <div class=\\"icon\\">\\n       [#if identityProvider.lookupButtonImageURL(clientId)?has_content]\\n         <img src=\\"${identityProvider.lookupButtonImageURL(clientId)}\\" title=\\"SAML Login\\" alt=\\"SAML Login\\"/>\\n       [#else]\\n         <img src=\\"/images/identityProviders/samlv2.svg\\" title=\\"SAML 2 Logo\\" alt=\\"SAML 2 Logo\\"/>\\n       [/#if]\\n     </div>\\n     <div class=\\"text\\">${identityProvider.lookupButtonText(clientId)?trim}</div>\\n   </div>\\n </button>\\n[/#macro]\\n\\n[#macro alternativeLogins clientId identityProviders passwordlessEnabled]\\n  [#if identityProviders?has_content || passwordlessEnabled]\\n    <div class=\\"login-button-container\\">\\n      <div class=\\"hr-container\\">\\n        <hr>\\n        <div>${theme.message('or')}</div>\\n      </div>\\n\\n      [#if passwordlessEnabled]\\n      <div class=\\"form-row push-less-top\\">\\n        [@link url = \\"/oauth2/passwordless\\"]\\n          <div class=\\"magic login-button\\">\\n            <div>\\n              <div class=\\"icon\\">\\n                <i class=\\"fa fa-link\\"></i>\\n              </div>\\n              <div class=\\"text\\">${theme.message('passwordless-button-text')}</div>\\n            </div>\\n          </div>\\n        [/@link]\\n      </div>\\n      [/#if]\\n\\n      [#if identityProviders[\\"Apple\\"]?has_content]\\n      <div class=\\"form-row push-less-top\\">\\n        [@appleButton identityProvider=identityProviders[\\"Apple\\"][0] clientId=clientId /]\\n      </div>\\n      [/#if]\\n\\n      [#if identityProviders[\\"Facebook\\"]?has_content]\\n      <div class=\\"form-row push-less-top\\">\\n        [@facebookButton identityProvider=identityProviders[\\"Facebook\\"][0] clientId=clientId /]\\n      </div>\\n      [/#if]\\n\\n      [#if identityProviders[\\"Google\\"]?has_content]\\n      <div class=\\"form-row push-less-top\\">\\n        [@googleButton identityProvider=identityProviders[\\"Google\\"][0] clientId=clientId/]\\n      </div>\\n      [/#if]\\n\\n      [#if identityProviders[\\"LinkedIn\\"]?has_content]\\n      <div class=\\"form-row push-less-top\\">\\n        [@linkedInBottom identityProvider=identityProviders[\\"LinkedIn\\"][0] clientId=clientId/]\\n      </div>\\n      [/#if]\\n\\n      [#if identityProviders[\\"Twitter\\"]?has_content]\\n      <div class=\\"form-row push-less-top\\">\\n        [@twitterButton identityProvider=identityProviders[\\"Twitter\\"][0] clientId=clientId/]\\n      </div>\\n      [/#if]\\n\\n      [#if identityProviders[\\"OpenIDConnect\\"]?has_content]\\n        [#list identityProviders[\\"OpenIDConnect\\"] as identityProvider]\\n          <div class=\\"form-row push-less-top\\">\\n            [@openIDConnectButton identityProvider=identityProvider clientId=clientId/]\\n          </div>\\n        [/#list]\\n      [/#if]\\n\\n      [#if identityProviders[\\"SAMLv2\\"]?has_content]\\n        [#list identityProviders[\\"SAMLv2\\"] as identityProvider]\\n          <div class=\\"form-row push-less-top\\">\\n            [@samlv2Button identityProvider=identityProvider clientId=clientId/]\\n          </div>\\n        [/#list]\\n      [/#if]\\n    </div>\\n  [/#if]\\n[/#macro]\\n\\n[#-- Below are the helpers for errors and alerts --]\\n\\n[#macro printErrorAlerts rowClass colClass]\\n  [#if errorMessages?size > 0]\\n    [#list errorMessages as m]\\n      <span class=\\"error\\">${ m }</span>\\n    [/#list]\\n  [/#if]\\n[/#macro]\\n\\n[#macro printInfoAlerts rowClass colClass]\\n  [#if infoMessages?size > 0]\\n    [#list infoMessages as m]\\n      <span class=\\"info\\">${ m }</span>\\n    [/#list]\\n  [/#if]\\n[/#macro]\\n\\n[#-- Below are the input helpers for hidden, text, buttons, labels and form errors.\\n     These fields are general purpose and can be used on any form you like. --]\\n\\n[#-- Hidden Input --]\\n[#macro hidden name value=\\"\\" dateTimeFormat=\\"\\"]\\n  [#if !value?has_content]\\n    [#local value=(\\"((\\" + name + \\")!'')\\")?eval?string/]\\n  [/#if]\\n  <input type=\\"hidden\\" name=\\"${name}\\" [#if value == \\"\\"]value=\\"${value}\\" [#else]value=\\"${value?string}\\"[/#if]/>\\n  [#if dateTimeFormat != \\"\\"]\\n  <input type=\\"hidden\\" name=\\"${name}@dateTimeFormat\\" value=\\"${dateTimeFormat}\\"/>\\n  [/#if]\\n[/#macro]\\n\\n[#-- Input field of optional type: [number | password | text --]\\n[#macro input type name id autocapitalize=\\"none\\" autocomplete=\\"on\\" autocorrect=\\"off\\" autofocus=false spellcheck=\\"false\\" label=\\"\\" placeholder=\\"\\" leftAddon=\\"\\" required=false tooltip=\\"\\" disabled=false class=\\"\\" dateTimeFormat=\\"\\"]\\n<div class=\\"form-row\\">\\n  [#if label?has_content]\\n  [#compress]\\n    <label for=\\"${id}\\"[#if (fieldMessages[name]![])?size > 0] class=\\"error\\"[/#if]>${label}[#if required] <span class=\\"required\\">*</span>[/#if]\\n    [#if tooltip?has_content]\\n      <i class=\\"fa fa-info-circle\\" data-tooltip=\\"${tooltip}\\"></i>\\n    [/#if]\\n    </label>\\n  [/#compress]\\n  [/#if]\\n  [#if leftAddon?has_content]\\n  <div class=\\"input-addon-group\\">\\n    <span class=\\"icon\\"><i class=\\"fa fa-${leftAddon}\\"></i></span>\\n  [/#if]\\n  [#local value=(\\"((\\" + name + \\")!'')\\")?eval/]\\n      <input id=\\"${id}\\" type=\\"${type}\\" name=\\"${name}\\" [#if type != \\"password\\"]value=\\"${value}\\"[/#if] class=\\"${class}\\" autocapitalize=\\"${autocapitalize}\\" autocomplete=\\"${autocomplete}\\" autocorrect=\\"${autocorrect}\\" spellcheck=\\"${spellcheck}\\" [#if autofocus]autofocus=\\"autofocus\\"[/#if] placeholder=\\"${placeholder}\\" [#if disabled]disabled=\\"disabled\\"[/#if] style=\\"height: 35px;\\"/>\\n  [#if dateTimeFormat != \\"\\"]\\n      <input type=\\"hidden\\" name=\\"${name}@dateTimeFormat\\" value=\\"${dateTimeFormat}\\"/>\\n  [/#if]\\n  [#if leftAddon?has_content]\\n  </div>\\n  [/#if]\\n  [@errors field=name/]\\n</div>\\n[/#macro]\\n\\n[#-- Select --]\\n[#macro select name id autocapitalize=\\"none\\" autofocus=false label=\\"\\" required=false tooltip=\\"\\" disabled=false class=\\"select\\" options=[]]\\n<div class=\\"form-row\\">\\n  [#if label?has_content]\\n  [#compress]\\n    <label for=\\"${id}\\"[#if (fieldMessages[name]![])?size > 0] class=\\"error\\"[/#if]>${label}[#if required] <span class=\\"required\\">*</span>[/#if]\\n    [#if tooltip?has_content]\\n      <i class=\\"fa fa-info-circle\\" data-tooltip=\\"${tooltip}\\"></i>\\n    [/#if]\\n    </label>\\n  [/#compress]\\n  [/#if]\\n  <label class=\\"select\\">\\n    [#local value=(\\"((\\" + name + \\")!'')\\")?eval/]\\n    [#if name == \\"user.timezone\\" || name == \\"registration.timezone\\"]\\n      <select id=\\"${id}\\" class=\\"${class}\\" name=\\"${name}\\">\\n        [#list timezones as option]\\n          [#local selected = value == option/]\\n          <option value=\\"${option}\\" [#if selected]selected=\\"selected\\"[/#if] >${option}</option>\\n        [/#list]]\\n      </select>\\n    [#else]\\n    <select id=\\"${id}\\" class=\\"${class}\\" name=\\"${name}\\">\\n      [#list options as option]\\n        [#local selected = value == option/]\\n        <option value=\\"${option}\\" [#if selected]selected=\\"selected\\"[/#if] >${theme.optionalMessage(option)}</option>\\n      [/#list]\\n    </select>\\n    [/#if]\\n  </label>\\n  [@errors field=name/]\\n</div>\\n[/#macro]\\n\\n[#-- Text Area --]\\n[#macro textarea name id autocapitalize=\\"none\\" autofocus=false label=\\"\\" required=false tooltip=\\"\\" disabled=false class=\\"textarea\\" placeholder=\\"\\"]\\n<div class=\\"form-row\\">\\n  <textarea id=\\"${id}\\" name=\\"${name}\\" class=\\"${class}\\">${(name?eval!'')}</textarea>\\n  [@errors field=name/]\\n</div>\\n[/#macro]\\n\\n[#-- Begin : Used for Advanced Registration.\\n     The following form controls require a 'field' argument which is only available during registration. --]\\n\\n[#-- Radio List --]\\n[#macro radio_list field name id autocapitalize=\\"none\\" autofocus=false label=\\"\\" required=false tooltip=\\"\\" disabled=false class=\\"radio-list\\" options=[]]\\n<div class=\\"form-row\\">\\n  [#if label?has_content]\\n  [#compress]\\n  <label for=\\"${id}\\"[#if (fieldMessages[name]![])?size > 0] class=\\"error\\"[/#if]>${label}[#if required] <span class=\\"required\\">*</span>[/#if]\\n    [#if tooltip?has_content]\\n      <i class=\\"fa fa-info-circle\\" data-tooltip=\\"${tooltip}\\"></i>\\n    [/#if]\\n  </label>\\n  [/#compress]\\n  [/#if]\\n  [#local value=(\\"((\\" + name + \\")!'')\\")?eval/]\\n  <div id=\\"${id}\\" class=\\"${class}\\">\\n    [#list options as option]\\n      [#local checked = value == option/]\\n      [#if field.type == \\"consent\\"]\\n        [#local checked = consents(field.consentId)?? && consents(field.consentId)?contains(option)]\\n      [/#if]\\n      <label class=\\"radio\\"><input type=\\"radio\\" name=\\"${name}\\" value=\\"${option}\\" [#if checked]checked=\\"checked\\"[/#if]><span class=\\"box\\"></span><span class=\\"label\\">${theme.optionalMessage(option)}</span></label>\\n    [/#list]\\n  </div>\\n  [@errors field=name/]\\n</div>\\n[/#macro]\\n\\n[#macro checkbox field name id autocapitalize=\\"none\\" autofocus=false label=\\"\\" required=false tooltip=\\"\\" disabled=false class=\\"checkbox\\"]\\n<div class=\\"form-row\\">\\n   <label class=\\"${class}\\">\\n     [#local value=(\\"((\\" + name + \\")!'')\\")?eval/]\\n     [#local checked = value?has_content]\\n     [#if field.type == \\"consent\\"]\\n       [#local checked = consents(field.consentId)??]\\n     [/#if]\\n     <input id=\\"${id}\\" type=\\"checkbox\\" name=\\"${name}\\" value=\\"${value}\\" [#if checked]checked=\\"checked\\"[/#if]>\\n       <span class=\\"box\\"></span>\\n       <span class=\\"label\\">${theme.optionalMessage(name)}</span>\\n   </label>\\n  [@errors field=name/]\\n</div>\\n[/#macro]\\n\\n[#macro checkbox_list field name id autocapitalize=\\"none\\" autofocus=false label=\\"\\" required=false tooltip=\\"\\" disabled=false class=\\"checkbox-list\\" options=[]]\\n<div class=\\"form-row\\">\\n  [#if label?has_content][#t/]\\n  <label for=\\"${id}\\"[#if (fieldMessages[name]![])?size > 0] class=\\"error\\"[/#if]>${label}[#if required] <span class=\\"required\\">*</span>[/#if][#t/]\\n    [#if tooltip?has_content][#t/]\\n      <i class=\\"fa fa-info-circle\\" data-tooltip=\\"${tooltip}\\"></i>[#t/]\\n    [/#if][#t/]\\n  </label>[#t/]\\n  [/#if]\\n  <div id=\\"${id}\\" class=\\"${class}\\">\\n    [#list options as option]\\n      [#local value=(\\"((\\" + name + \\")!'')\\")?eval/]\\n      [#local checked = value?is_sequence && value?seq_contains(option)/]\\n      [#if field.type == \\"consent\\"]\\n        [#local checked = consents(field.consentId)?? && consents(field.consentId)?contains(option)]\\n      [/#if]\\n      <label class=\\"checkbox\\"><input type=\\"checkbox\\" name=\\"${name}\\" value=\\"${option}\\" [#if checked]checked=\\"checked\\"[/#if]><span class=\\"box\\"></span><span class=\\"label\\">${theme.optionalMessage(option)}</span></label>\\n    [/#list]\\n  </div>\\n  [@errors field=name/]\\n</div>\\n[/#macro]\\n\\n[#macro locale_select field name id autocapitalize=\\"none\\" autofocus=false label=\\"\\" required=false tooltip=\\"\\" disabled=false class=\\"checkbox-list\\" options=[]]\\n  [#local value=(\\"((\\" + name + \\")!'')\\")?eval/]\\n  <div class=\\"form-row\\">\\n    <div id=\\"${id}\\" class=\\"${class}\\">\\n      [#list fusionAuth.locales() as l, n]\\n        [#local checked = value?is_sequence && value?seq_contains(l)/]\\n         <label class=\\"checkbox\\">\\n           <input type=\\"checkbox\\" name=\\"${name}\\" value=\\"${l}\\" [#if checked]checked=\\"checked\\"[/#if]>\\n           <span class=\\"box\\"></span>\\n           <span class=\\"label\\">${l.getDisplayName()}</span>\\n         </label>\\n      [/#list]\\n    </div>\\n  </div>\\n[/#macro]\\n\\n[#-- End : Used for Advanced Registration. --]\\n\\n[#macro oauthHiddenFields]\\n  [@hidden name=\\"client_id\\"/]\\n  [@hidden name=\\"code_challenge\\"/]\\n  [@hidden name=\\"code_challenge_method\\"/]\\n  [@hidden name=\\"metaData.device.name\\"/]\\n  [@hidden name=\\"metaData.device.type\\"/]\\n  [@hidden name=\\"nonce\\"/]\\n  [@hidden name=\\"redirect_uri\\"/]\\n  [@hidden name=\\"response_mode\\"/]\\n  [@hidden name=\\"response_type\\"/]\\n  [@hidden name=\\"scope\\"/]\\n  [@hidden name=\\"state\\"/]\\n  [@hidden name=\\"tenantId\\"/]\\n  [@hidden name=\\"timezone\\"/]\\n  [@hidden name=\\"user_code\\"/]\\n[/#macro]\\n\\n[#macro errors field]\\n[#if fieldMessages[field]?has_content]\\n<span class=\\"error\\">[#list fieldMessages[field] as message]${message?no_esc}[#if message_has_next], [/#if][/#list]</span>\\n[/#if]\\n[/#macro]\\n\\n[#macro link url style=\\"\\" extraParameters=\\"\\"]\\n<a [#if style !=\\"\\"]style=\\"${style}\\"[/#if] href=\\"${url}?tenantId=${(tenantId)!''}&client_id=${(client_id?url)!''}&nonce=${(nonce?url)!''}&redirect_uri=${(redirect_uri?url)!''}&response_mode=${(response_mode?url)!''}&response_type=${(response_type?url)!''}&scope=${(scope?url)!''}&state=${(state?url)!''}&timezone=${(timezone?url)!''}&metaData.device.name=${(metaData.device.name?url)!''}&metaData.device.type=${(metaData.device.type?url)!''}${extraParameters!''}&code_challenge=${(code_challenge?url)!''}&code_challenge_method=${(code_challenge_method?url)!''}&user_code=${(user_code?url)!''}\\">\\n[#nested/]\\n</a>\\n[/#macro]\\n\\n[#macro defaultIfNull text default]\\n  ${text!default}\\n[/#macro]\\n\\n[#macro passwordRules passwordValidationRules]\\n<div class=\\"font-italic\\">\\n  <span>\\n    ${theme.message('password-constraints-intro')}\\n  </span>\\n  <ul>\\n    <li>${theme.message('password-length-constraint', passwordValidationRules.minLength, passwordValidationRules.maxLength)}</li>\\n    [#if passwordValidationRules.requireMixedCase]\\n      <li>${theme.message('password-case-constraint')}</li>\\n    [/#if]\\n    [#if passwordValidationRules.requireNonAlpha]\\n      <li>${theme.message('password-alpha-constraint')}</li>\\n    [/#if]\\n    [#if passwordValidationRules.requireNumber]\\n      <li>${theme.message('password-number-constraint')}</li>\\n    [/#if]\\n    [#if passwordValidationRules.rememberPreviousPasswords.enabled]\\n      <li>${theme.message('password-previous-constraint', passwordValidationRules.rememberPreviousPasswords.count)}</li>\\n    [/#if]\\n  </ul>\\n</div>\\n[/#macro]\\n\\n[#macro customField field key autofocus=false placeholder=\\"\\" label=\\"\\" leftAddon=\\"true\\"]\\n  [#assign fieldId = field.key?replace(\\".\\", \\"_\\") /]\\n  [#local leftAddon = (leftAddon == \\"true\\")?then(field.data.leftAddon!'info', \\"\\") /]\\n\\n  [#if field.key == \\"user.preferredLanguages\\" || field.key == \\"registration.preferredLanguages\\"]\\n    [@locale_select field=field id=fieldId name=field.key required=field.required autofocus=autofocus label=label /]\\n  [#elseif field.control == \\"checkbox\\"]\\n    [#if field.options?has_content]\\n      [@checkbox_list field=field id=\\"${fieldId}\\" name=\\"${key}\\" required=field.required autofocus=autofocus label=label options=field.options /]\\n    [#else]\\n      [@checkbox field=field id=\\"${fieldId}\\" name=\\"${key}\\" required=field.required autofocus=autofocus label=label /]\\n    [/#if]\\n  [#elseif field.control == \\"number\\"]\\n    [@input id=\\"${fieldId}\\" type=\\"number\\" name=\\"${key}\\" leftAddon=\\"${leftAddon}\\" required=field.required autofocus=autofocus label=label placeholder=theme.optionalMessage(placeholder) /]\\n  [#elseif field.control == \\"password\\"]\\n    [@input id=\\"${fieldId}\\" type=\\"password\\" name=\\"${key}\\" leftAddon=\\"lock\\" autocomplete=\\"new-password\\" autofocus=autofocus label=label placeholder=theme.optionalMessage(placeholder)/]\\n  [#elseif field.control == \\"radio\\"]\\n    [@radio_list field=field id=\\"${fieldId}\\" name=\\"${key}\\" required=field.required autofocus=autofocus label=label options=field.options /]\\n  [#elseif field.control == \\"select\\"]\\n    [@select id=\\"${fieldId}\\" name=\\"${key}\\" required=field.required autofocus=autofocus label=label options=field.options /]\\n  [#elseif field.control == \\"textarea\\"]\\n    [@textarea id=\\"${fieldId}\\" name=\\"${key}\\" required=field.required autofocus=autofocus label=label placeholder=theme.optionalMessage(placeholder) /]\\n  [#elseif field.control == \\"text\\"]\\n    [#if field.type == \\"date\\"]\\n      [@input id=\\"${fieldId}\\" type=\\"text\\" name=\\"${key}\\" leftAddon=\\"${leftAddon}\\" required=field.required autofocus=autofocus label=label placeholder=theme.optionalMessage(placeholder) class=\\"date-picker\\" dateTimeFormat=\\"yyyy-MM-dd\\" /]\\n    [#else]\\n      [@input id=\\"${fieldId}\\" type=\\"text\\" name=\\"${key}\\" leftAddon=\\"${leftAddon}\\" required=field.required autofocus=autofocus label=label placeholder=theme.optionalMessage(placeholder)/]\\n    [/#if]\\n  [/#if]\\n[/#macro]\\n\\n[#function display object propertyName default=\\"\\\\x2013\\" ]\\n  [#assign value=(\\"((object.\\" + propertyName + \\")!'')\\")?eval/]\\n  [#-- ?has_content is false for boolean types, check it first --]\\n  [#if value?has_content]\\n    [#if value?is_number]\\n      [#return value?string('#,###')]\\n    [#else]\\n      [#return (value == default?is_markup_output?then(default?markup_string, default))?then(value, value?string)]\\n    [/#if]\\n  [#else]\\n    [#return default]\\n  [/#if]\\n[/#function]\\n\\n[#macro passwordField field]\\n  [#-- Render checkbox used to determine whether the form submit should update password--]\\n  <div class=\\"form-row\\">\\n    <label for=\\"editPasswordOption\\"> ${theme.optionalMessage(\\"change-password\\")} </label>\\n    <input type=\\"hidden\\" name=\\"__cb_editPasswordOption\\" value=\\"useExisting\\">\\n    <label class=\\"toggle\\">\\n      <input id=\\"editPasswordOption\\" type=\\"checkbox\\" name=\\"editPasswordOption\\" value=\\"update\\" data-slide-open=\\"password-fields\\" [#if editPasswordOption == \\"update\\"]checked[/#if]>\\n      <span class=\\"rail\\"></span>\\n      <span class=\\"pin\\"></span>\\n    </label>\\n  </div>\\n  <div id=\\"password-fields\\" class=\\"slide-open ${(editPasswordOption == \\"update\\")?then('open', '')}\\">\\n    [#-- Show the Password Validation Rules if there is a field error for 'user.password' --]\\n    [#if (fieldMessages?keys?seq_contains(\\"user.password\\")!false) && passwordValidationRules??]\\n      [@passwordRules passwordValidationRules/]\\n    [/#if]\\n\\n    [#-- Render password field--]\\n    [@customField field=field key=field.key autofocus=false placeholder=field.key label=theme.optionalMessage(field.key) leftAddon=\\"false\\"/]\\n\\n    [#-- Render confirm if set to true on the field     --]\\n    [#if field.confirm]\\n      [@customField field \\"confirm.${field.key}\\" false \\"[confirm]${field.key}\\" /]\\n    [/#if]\\n  </div>\\n[/#macro]\\n\\n\\n\\n\\n\\n[#-- inputMaterialOutlined field of optional type: [number | password | text --]\\n[#macro inputMaterialOutlined type name id autocapitalize=\\"none\\" autocomplete=\\"on\\" autocorrect=\\"off\\" autofocus=false spellcheck=\\"false\\" label=\\"\\" placeholder=\\"\\" leftAddon=\\"\\" required=false tooltip=\\"\\" disabled=false class=\\"\\" dateTimeFormat=\\"\\"]\\n  <label for=\\"${id}\\" class=\\"matter-textfield-outlined\\" style=\\"width: 100%; margin-bottom: 30px; color: #888;\\">\\n    [#local value=(\\"((\\" + name + \\")!'')\\")?eval/]\\n      <input id=\\"${id}\\" type=\\"${type}\\" name=\\"${name}\\" [#if type != \\"password\\"]value=\\"${value}\\"[/#if] class=\\"${class}\\" autocapitalize=\\"${autocapitalize}\\" autocomplete=\\"${autocomplete}\\" autocorrect=\\"${autocorrect}\\" spellcheck=\\"${spellcheck}\\" [#if autofocus]autofocus=\\"autofocus\\"[/#if] placeholder=\\" \\" [#if disabled]disabled=\\"disabled\\"[/#if]/>\\n    <span>${placeholder}</span>\\n  </label>\\n  [@errors field=name/]\\n[/#macro]\\n\\n[#-- InputMaterial field of optional type: [number | password | text --]\\n[#macro inputMaterial type name id autocapitalize=\\"none\\" autocomplete=\\"off\\" autocorrect=\\"off\\" autofocus=false spellcheck=\\"false\\" label=\\"\\" placeholder=\\"\\" leftAddon=\\"\\" required=false tooltip=\\"\\" disabled=false class=\\"\\" dateTimeFormat=\\"\\"]\\n  <label for=\\"${id}\\" class=\\"matter-textfield-standard\\" style=\\"width: 100%; margin-bottom: 10px; margin-top: 20px; color: #888;\\">\\n    [#local value=(\\"((\\" + name + \\")!'')\\")?eval/]\\n      <input id=\\"${id}\\" type=\\"${type}\\" name=\\"${name}\\" [#if type != \\"password\\"]value=\\"${value}\\"[/#if] class=\\"${class}\\" autocapitalize=\\"${autocapitalize}\\" autocomplete=\\"${autocomplete}\\" autocorrect=\\"${autocorrect}\\" spellcheck=\\"${spellcheck}\\" [#if autofocus]autofocus=\\"autofocus\\"[/#if] placeholder=\\" \\" [#if disabled]disabled=\\"disabled\\"[/#if]/>\\n    <span>${placeholder}</span>\\n  </label>\\n  [@errors field=name/]\\n[/#macro]\\n\\n[#-- ButtonMaterial --]\\n[#macro buttonMaterial text onClick=\\"\\" icon=\\"arrow-right\\" color=\\"blue\\" disabled=false name=\\"\\" value=\\"\\" style=\\"\\"]\\n  <button\\n    class=\\"matter-button-contained ${color} ${disabled?then(' disabled', '')}\\"\\n    [#if disabled] disabled=\\"disabled\\"[/#if]\\n    [#if style !=\\"\\"]style=\\"${style}\\"[/#if]\\n    [#if value !=\\"\\"]value=\\"${value}\\"[/#if]\\n    [#if name !=\\"\\"]name=\\"${name}\\"[/#if]\\n    [#if onClick !=\\"\\"]onClick=\\"${onClick}\\"[/#if]\\n  >\\n    [#if icon !=\\"\\"]<i class=\\"fa fa-${icon}\\" style=\\"padding-right: 4px;\\"></i>[/#if]\\n    ${text}\\n  </button>\\n[/#macro]\\n\\n[#-- Apprise Main --]\\n[#macro appriseMain locale=\\"\\" title=\\"Login\\" rowClass=\\"row center-xs\\" colClass=\\"col-xs col-sm-8 col-md-6 col-lg-5 col-xl-4\\"]\\n  [@helpers.main locale=locale logo=\\"true\\" title=title rowClass=rowClass colClass=colClass]\\n    [#nested/]\\n  [/@helpers.main]\\n[/#macro]\\n\\n[#-- apprise Lang Selector --]\\n[#macro appriseLocalSelector]\\n  <label class=\\"select\\">\\n    <select id=\\"locale-select\\" name=\\"locale\\" class=\\"select\\" style=\\"opacity: 0; position: absolute; z-index: -1;\\">\\n      <option value=\\"en\\" [#if locale?contains(\\"en\\")]selected[/#if]>English</option>\\n        [#list theme.additionalLocales() as l]\\n          <option value=\\"${l}\\" [#if locale == l]selected[/#if]>${l.getDisplayLanguage(locale)}</option>\\n        [/#list]\\n    </select>\\n    <script>\\n      var selectLang = function(lang) {\\n        const langs = window['locale-select'];\\n\\n        if (lang !== langs.value) {\\n          langs.value = lang;\\n          langs.click();\\n          langs.dispatchEvent(new Event('change'));\\n        }\\n      };\\n    </script>\\n    <a class=\\"lang\\" style=\\"display: inline-block;\\" [#if locale?contains(\\"en\\")]selected[/#if] onclick=\\"selectLang('en');\\">English</a>\\n    &nbsp;/&nbsp;\\n    [#list theme.additionalLocales() as ll]\\n      <a class=\\"lang\\" style=\\"display: inline-block;\\" [#if locale == ll]selected[/#if] onclick=\\"selectLang('${ll}');\\">${ll.getDisplayLanguage(locale)}</a>\\n    [/#list]\\n  </label>\\n[/#macro]\\n\\n[#macro applink]\\n<a href=\\"https://localhost:3000\\">${theme.message(\\"return-to-login\\")}</a>\\n[/#macro]","index":"[#ftl/]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#import \\"_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head title=\\"FusionAuth\\"/]\\n <meta http-equiv=\\"refresh\\" content=\\"0;URL='/admin'\\" />  \\n [@helpers.body]\\n    [@helpers.appriseMain]\\n    [/@helpers.appriseMain]\\n\\n  [/@helpers.body]\\n[/@helpers.html]","oauth2Authorize":"[#ftl/]\\n[#setting url_escaping_charset=\\"UTF-8\\"]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"code_challenge\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"code_challenge_method\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"devicePendingIdPLink\\" type=\\"io.fusionauth.domain.provider.PendingIdPLink\\" --]\\n[#-- @ftlvariable name=\\"hasDomainBasedIdentityProviders\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"identityProviders\\" type=\\"java.util.Map<java.lang.String, java.util.List<io.fusionauth.domain.provider.BaseIdentityProvider<?>>>\\" --]\\n[#-- @ftlvariable name=\\"loginId\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"metaData\\" type=\\"io.fusionauth.domain.jwt.RefreshToken.MetaData\\" --]\\n[#-- @ftlvariable name=\\"nonce\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"passwordlessEnabled\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"pendingIdPLink\\" type=\\"io.fusionauth.domain.provider.PendingIdPLink\\" --]\\n[#-- @ftlvariable name=\\"redirect_uri\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"rememberDevice\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"response_type\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"scope\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"showCaptcha\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"showPasswordField\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"state\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#-- @ftlvariable name=\\"timezone\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"user_code\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"version\\" type=\\"java.lang.String\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n[@helpers.html]\\n  [@helpers.head]\\n    <script src=\\"/js/jstz-min-1.0.6.js\\"></script>\\n    <script src=\\"/js/oauth2/Authorize.js?version=${version}\\"></script>\\n    <script src=\\"/js/identityProvider/InProgress.js?version=${version}\\"></script>\\n    [@helpers.alternativeLoginsScript clientId=client_id identityProviders=identityProviders/]\\n    <script>\\n      Prime.Document.onReady(function() {\\n        [#-- This object handles guessing the timezone and filling in the device id of the user --]\\n        new FusionAuth.OAuth2.Authorize();\\n      });\\n    </script>\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n    [@helpers.appriseMain title=theme.message('login')]\\n      [#-- During a linking work flow, optionally indicate to the user which IdP is being linked. --]\\n      [#if devicePendingIdPLink?? || pendingIdPLink??]\\n        <p class=\\"mt-0\\">\\n        [#if devicePendingIdPLink?? && pendingIdPLink??]\\n          ${theme.message('pending-links-login-to-complete', devicePendingIdPLink.identityProviderName, pendingIdPLink.identityProviderName)}\\n        [#elseif devicePendingIdPLink??]\\n          ${theme.message('pending-link-login-to-complete', devicePendingIdPLink.identityProviderName)}\\n        [#else]\\n          ${theme.message('pending-link-login-to-complete', pendingIdPLink.identityProviderName)}\\n        [/#if]\\n        [#-- A pending link can be cancled. If we also have a device link in progress, this cannot be canceled. --]\\n        [#if pendingIdPLink??]\\n          [@helpers.link url=\\"\\" extraParameters=\\"&cancelPendingIdpLink=true\\"]${theme.message('login-cancel-link')}[/@helpers.link]\\n        [/#if]\\n        </p>\\n      [/#if]\\n      <form action=\\"${request.contextPath}/oauth2/authorize\\" method=\\"POST\\" class=\\"full\\">\\n        [@helpers.oauthHiddenFields/]\\n        [@helpers.hidden name=\\"showPasswordField\\"/]\\n        [#if showPasswordField && hasDomainBasedIdentityProviders]\\n          [@helpers.hidden name=\\"loginId\\"/]\\n        [/#if]\\n        <fieldset>\\n          [@helpers.inputMaterial type=\\"text\\" name=\\"loginId\\" id=\\"loginId\\" autocomplete=\\"username\\" autocapitalize=\\"none\\" autocomplete=\\"on\\" autocorrect=\\"off\\" spellcheck=\\"false\\" autofocus=(!loginId?has_content) placeholder=theme.message('loginId') leftAddon=\\"user\\" disabled=(showPasswordField && hasDomainBasedIdentityProviders)/]\\n          [#if showPasswordField]\\n            [@helpers.inputMaterial type=\\"password\\" name=\\"password\\" id=\\"password\\" autocomplete=\\"current-password\\" autofocus=loginId?has_content placeholder=theme.message('password') leftAddon=\\"lock\\"/]\\n          [/#if]\\n        </fieldset>\\n          <div class=\\"form-row\\">\\n            [#if showPasswordField]\\n              [@helpers.buttonMaterial icon=\\"key\\" text=theme.message('submit') style=\\"width: 100%\\"/]\\n              <div class=\\"right\\" style=\\"width: 50%;\\">\\n                [@helpers.link url=\\"${request.contextPath}/password/forgot\\"]${theme.message('forgot-your-password')}[/@helpers.link]\\n              </div>\\n            [#else]\\n              [@helpers.buttonMaterial icon=\\"arrow-right\\" text=theme.message('next')/]\\n            [/#if]\\n          </div>\\n      </form>\\n      <div>\\n        [#if showPasswordField && hasDomainBasedIdentityProviders]\\n          [@helpers.link url=\\"\\" extraParameters=\\"&showPasswordField=false\\"]${theme.message('sign-in-as-different-user')}[/@helpers.link]\\n        [/#if]\\n      </div>\\n      [#if application.registrationConfiguration.enabled]\\n        <div class=\\"form-row push-top\\">\\n          ${theme.message('dont-have-an-account')}\\n          [@helpers.link url=\\"${request.contextPath}/oauth2/register\\"]${theme.message('create-an-account')}[/@helpers.link]\\n        </div>\\n      [/#if]\\n      [@helpers.alternativeLogins clientId=client_id identityProviders=identityProviders passwordlessEnabled=passwordlessEnabled/]\\n    [/@helpers.appriseMain]\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","oauth2AuthorizedNotRegistered":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"currentUser\\" type=\\"io.fusionauth.domain.User\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('authorized-not-registered-title')]\\n      <span>${theme.message('authorized-not-registered')}</span>\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","oauth2ChildRegistrationNotAllowed":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('provide-parent-email')]\\n      <form action=\\"child-registration-not-allowed\\" method=\\"POST\\" class=\\"full\\">\\n        [@helpers.hidden name=\\"client_id\\"/]\\n        [@helpers.hidden name=\\"tenantId\\"/]\\n        <p>\\n          ${theme.message('child-registration-not-allowed')}\\n        </p>\\n        <fieldset>\\n          [@helpers.input type=\\"text\\" name=\\"parentEmail\\" id=\\"parentEmail\\" placeholder=theme.message('parentEmail') leftAddon=\\"user\\" required=true/]\\n        </fieldset>\\n\\n        <div class=\\"form-row\\">\\n          [@helpers.button icon=\\"left-arrow\\" text=theme.message('submit')/]\\n        </div>\\n      </form>\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","oauth2ChildRegistrationNotAllowedComplete":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('parent-notified-title')]\\n      <p>\\n        ${theme.message('parent-notified')}\\n      </p>\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","oauth2CompleteRegistration":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"currentUser\\" type=\\"io.fusionauth.domain.User\\" --]\\n[#-- @ftlvariable name=\\"fields\\" type=\\"java.util.List<io.fusionauth.domain.form.FormField>\\" --]\\n[#-- @ftlvariable name=\\"step\\" type=\\"int\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#-- @ftlvariable name=\\"totalSteps\\" type=\\"int\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n    <script>\\n    Prime.Document.onReady(function() {\\n      var firstInput = Prime.Document.queryFirst('form[action=/oauth2/complete-registration]').queryFirst('input:not([type=hidden])');\\n      if (firstInput !== null) {\\n          firstInput.focus();\\n      }\\n    });\\n    </script>\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('complete-registration')]\\n      <form action=\\"${request.contextPath}/oauth2/complete-registration\\" method=\\"POST\\" class=\\"full\\">\\n        [@helpers.oauthHiddenFields/]\\n        [@helpers.hidden name=\\"step\\"/]\\n        [@helpers.hidden name=\\"registrationState\\"/]\\n\\n        [#-- Begin Self Service Custom Registration Form Steps --]\\n        [#if fields?has_content]\\n          <fieldset>\\n            [#list fields as field]\\n              [@helpers.customField field field.key field?is_first?then(true, false) field.key /]\\n              [#if field.confirm]\\n                [@helpers.customField field \\"confirm.${field.key}\\" false \\"[confirm]${field.key}\\" /]\\n              [/#if]\\n            [/#list]\\n          </fieldset>\\n\\n          <div class=\\"form-row\\">\\n          [#if step == totalSteps]\\n            [@helpers.button icon=\\"key\\" text=theme.message('register')/]\\n          [#else]\\n            [@helpers.button icon=\\"arrow-right\\" text=\\"Next\\"/]\\n          [/#if]\\n          </div>\\n        [#-- End Custom Self Service Registration Form Steps --]\\n        [#else]\\n        [#-- Begin Basic Self Service Registration Form --]\\n        <fieldset>\\n          [#if application.registrationConfiguration.firstName.enabled]\\n            [@helpers.input type=\\"text\\" name=\\"user.firstName\\" id=\\"firstName\\" placeholder=theme.message('firstName') leftAddon=\\"user\\" required=application.registrationConfiguration.firstName.required/]\\n          [/#if]\\n          [#if application.registrationConfiguration.fullName.enabled]\\n            [@helpers.input type=\\"text\\" name=\\"user.fullName\\" id=\\"fullName\\" placeholder=theme.message('fullName') leftAddon=\\"user\\" required=application.registrationConfiguration.fullName.required/]\\n          [/#if]\\n          [#if application.registrationConfiguration.middleName.enabled]\\n            [@helpers.input type=\\"text\\" name=\\"user.middleName\\" id=\\"middleName\\" placeholder=theme.message('middleName') leftAddon=\\"user\\" required=application.registrationConfiguration.middleName.required/]\\n          [/#if]\\n          [#if application.registrationConfiguration.lastName.enabled]\\n            [@helpers.input type=\\"text\\" name=\\"user.lastName\\" id=\\"lastName\\" placeholder=theme.message('lastName') leftAddon=\\"user\\" required=application.registrationConfiguration.lastName.required/]\\n          [/#if]\\n          [#if application.registrationConfiguration.birthDate.enabled]\\n            [@helpers.input type=\\"text\\" name=\\"user.birthDate\\" id=\\"birthDate\\" placeholder=theme.message('birthDate') leftAddon=\\"calendar\\" class=\\"date-picker\\" dateTimeFormat=\\"yyyy-MM-dd\\" required=application.registrationConfiguration.birthDate.required/]\\n          [/#if]\\n          [#if application.registrationConfiguration.mobilePhone.enabled]\\n            [@helpers.input type=\\"text\\" name=\\"user.mobilePhone\\" id=\\"mobilePhone\\" placeholder=theme.message('mobilePhone') leftAddon=\\"phone\\" required=application.registrationConfiguration.mobilePhone.required/]\\n          [/#if]\\n        </fieldset>\\n\\n        <div class=\\"form-row\\">\\n          [@helpers.button icon=\\"key\\" text=theme.message('submit')/]\\n        </div>\\n        [/#if]\\n        [#-- End Basic Self Service Registration Form --]\\n\\n        [#-- Begin Self Service Custom Registration Form Step Counter --]\\n        [#if step > 0]\\n          <div class=\\"w-100 mt-3\\" style=\\"display: inline-flex; flex-direction: row; justify-content: space-evenly;\\">\\n            <div class=\\"text-right\\" style=\\"flex-grow: 1;\\"> ${theme.message('register-step', step, totalSteps)} </div>\\n          </div>\\n        [/#if]\\n        [#-- End Self Service Custom Registration Form Step Counter --]\\n\\n      </form>\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","oauth2Device":"[#ftl/]\\n[#-- @ftlvariable name=\\"activationComplete\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"currentUser\\" type=\\"io.fusionauth.domain.User\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"devicePendingIdPLink\\" type=\\"io.fusionauth.domain.provider.PendingIdPLink\\" --]\\n[#-- @ftlvariable name=\\"theme\\" type=\\"io.fusionauth.domain.Theme\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#-- @ftlvariable name=\\"userCodeLength\\" type=\\"int\\" --]\\n[#-- @ftlvariable name=\\"version\\" type=\\"java.lang.String\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head title=theme.message('device-title')]\\n  <script src=\\"/js/oauth2/Device.js?version=${version}\\"></script>\\n  <script>\\n    Prime.Document.onReady(function() {\\n      var form = Prime.Document.queryById('device-form');\\n      new FusionAuth.OAuth2.Device(form, ${userCodeLength});\\n    });\\n  </script>\\n  <style>\\n    #user_code_container {\\n      display: flex;\\n      flex-wrap: wrap;\\n      justify-content: center;\\n    }\\n\\n    #user_code_container > div {\\n      margin-left: 2px;\\n      margin-right: 2px;\\n    }\\n\\n    #user_code_container input[type=\\"text\\"] {\\n      font-size: 30px;\\n      padding: 5px 0;\\n      margin-bottom: 5px;\\n      text-align: center;\\n      width: 32px;\\n    }\\n\\n    #user_code_container input[type=\\"text\\"] + span {\\n      font-size: 32px;\\n    }\\n  </style>\\n  [/@helpers.head]\\n  [@helpers.body]\\n\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [#-- If a pending link will cause us to exceed our linking limit, the next step will be to lgoout. --]\\n    [#assign logoutToContinue = devicePendingIdPLink?? && devicePendingIdPLink.linkLimitExceeded /]\\n\\n    [@helpers.main title=theme.message('device-form-title')]\\n      [#setting url_escaping_charset='UTF-8']\\n\\n      [#-- During a linking work flow, optionally indicate to the user which IdP is being linked. --]\\n      [#if devicePendingIdPLink?? && !logoutToContinue]\\n        <p class=\\"mt-0\\">\\n          ${theme.message('pending-device-link', devicePendingIdPLink.identityProviderName)}\\n        </p>\\n      [/#if]\\n\\n      [#-- If there is an active SSO session, give the user the option to logout of the SSO session\\n           that does not belong to them. If a link count has been exceeded provide a logout button.\\n      --]\\n      [#if currentUser??]\\n        <div class=\\"form-row mb-3\\">\\n          [#-- You may wish to obfuscate a portion of this value for anonymity.. --]\\n          [#assign currentLoginId = currentUser.login /]\\n          [#if logoutToContinue]\\n            [#-- The user must logout before continuing because the currently logged in user has exceeded the number of allowed links to this IdP. --]\\n            <p>${theme.message('device-link-count-exceeded-pending-logout', currentLoginId, devicePendingIdPLink.identityProviderName)}</p>\\n            <p class=\\"pb-3\\">${theme.message('device-link-count-exceeded-next-step')}</p>\\n            <div class=\\"row\\">\\n              <div class=\\"col-xs\\">\\n                [@helpers.logoutLink redirectURI=\\"/oauth2/device\\"]\\n                  <button class=\\"blue button w-100\\" style=\\"height: 35px;\\"><i class=\\"fa fa-arrow-right\\"></i> ${theme.message('logout-and-continue')}</button>\\n                [/@helpers.logoutLink]\\n              </div>\\n            </div>\\n          [#else]\\n            <p>${theme.message('device-logged-in-as-not-you', currentLoginId)}</p>\\n            <div class=\\"row\\">\\n              <div class=\\"col-xs\\">\\n                [@helpers.logoutLink redirectURI=\\"/oauth2/device\\"]\\n                  <button class=\\"blue button\\"><i class=\\"fa fa-arrow-right\\"></i> ${theme.message('logout-and-continue')}</button>\\n                [/@helpers.logoutLink]\\n              </div>\\n            </div>\\n          [/#if]\\n        </div>\\n      [/#if]\\n\\n      [#-- Not showing the form if the user must logout first. --]\\n      [#if !logoutToContinue]\\n      <form action=\\"/oauth2/device\\" method=\\"POST\\" id=\\"device-form\\">\\n        [@helpers.oauthHiddenFields/]\\n        <p>${theme.message('userCode')}</p>\\n        <fieldset>\\n          <div id=\\"user_code_container\\">\\n            [#list 0..<userCodeLength as i]\\n            <div>\\n              <label for=\\"user_code_${i}\\"></label>\\n              <input type=\\"text\\" id=\\"user_code_${i}\\" maxlength=\\"1\\" [#if i?index == 0]autofocus[/#if] autocomplete=\\"off\\"/>\\n              [#if i == (userCodeLength/2)?floor - 1]<span>-</span>[/#if]\\n            </div>\\n            [/#list]\\n            <input type=\\"hidden\\" name=\\"interactive_user_code\\" id=\\"interactive_user_code\\" />\\n          </div>\\n        </fieldset>\\n\\n        <div class=\\"form-row\\">\\n          [@helpers.errors field=\\"user_code\\" /]\\n        </div>\\n\\n        <div class=\\"form-row push-top\\">\\n          [@helpers.button text=theme.message('submit')/]\\n        </div>\\n      </form>\\n      [/#if]\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","oauth2DeviceComplete":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"completedLinks\\" type=\\"java.util.List<io.fusionauth.domain.provider.PendingIdPLink>\\" --]\\n[#-- @ftlvariable name=\\"currentUser\\" type=\\"io.fusionauth.domain.User\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head title=theme.message('device-title')/]\\n  [@helpers.body]\\n\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('device-form-title')]\\n      [#if completedLinks?has_content]\\n        [#if completedLinks?size == 1]\\n        ${theme.message('completed-link', completedLinks.get(0).identityProviderType)}\\n        [#elseif completedLinks?size == 2]\\n        ${theme.message('completed-links', completedLinks.get(0).identityProviderType.name(), completedLinks.get(1).identityProviderType.name())}\\n        [/#if]\\n      [/#if]\\n      <p>\\n        ${theme.message('device-login-complete')}\\n      </p>\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","oauth2Error":"[#ftl/]\\n[#-- @ftlvariable name=\\"oauthJSONError\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n  [@helpers.body]\\n\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=\\"Error\\"]\\n      <p>\\n        We're sorry, your request was malformed or was unable to be completed for some reason. Try hitting the back button and restarting the process to see if it fixes the problem.\\n      </p>\\n\\n      [#if oauthJSONError?has_content]\\n      If you want the nerdy explanation, review the following JSON body.\\n      <pre class=\\"code scrollable horizontal\\">${oauthJSONError}</pre>\\n      [/#if]\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n\\n  [/@helpers.body]\\n[/@helpers.html]","oauth2Logout":"[#ftl/]\\n[#-- @ftlvariable name=\\"allLogoutURLs\\" type=\\"java.util.Set<java.lang.String>\\" --]\\n[#-- @ftlvariable name=\\"registeredLogoutURLs\\" type=\\"java.util.Set<java.lang.String>\\" --]\\n[#-- @ftlvariable name=\\"redirectURL\\" type=\\"java.lang.String\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[#-- You may adjust the duration before we redirect the user --]\\n[#assign logoutDurationInSeconds = 2 /]\\n\\n[@helpers.html]\\n  [@helpers.head title=theme.message('logout-title')]\\n  [#if redirectURL?has_content]\\n    <meta http-equiv=\\"Refresh\\" content=\\"${logoutDurationInSeconds}; url=${redirectURL}\\">\\n  [/#if]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.appriseMain locale=\\"false\\" title=theme.message('logging-out')]\\n      <div class=\\"progress-bar\\" >\\n        <div style=\\"animation-duration: ${logoutDurationInSeconds + 1}s; animation-timing-function: ease-out;\\">\\n        </div>\\n      </div>\\n    [/@helpers.appriseMain]\\n\\n    [#-- Use allLogoutURLs to call the logout URL of all applications in the tenant, or use registeredLogoutURLs to log out of just the applications the user is currently registered.\\n        Note, that just because a user does not currently have a registration, does not necessarily mean the user does not hold a session with an application. It is possible the user has been un-registered\\n        recently, or an application may have created a session for a user regardless of their registration. In most cases it is safest to simply call all applications and ensure that the Single Logout\\n        URL can handle logout requests for users that may or may not have a session with that application.\\n     --]\\n    [#list allLogoutURLs![] as logoutURL]\\n      <iframe src=\\"${logoutURL}\\" style=\\"width:0; height:0; border:0; border:none;\\"></iframe>\\n    [/#list]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","oauth2Passwordless":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"code\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"postMethod\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"showCaptcha\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [@helpers.captchaScripts showCaptcha=showCaptcha captchaMethod=tenant.captchaConfiguration.captchaMethod siteKey=tenant.captchaConfiguration.siteKey/]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [#if code?? && postMethod]\\n      <form action=\\"${request.contextPath}/oauth2/passwordless/${code}\\" method=\\"POST\\">\\n        [@helpers.oauthHiddenFields/]\\n        <input type=\\"hidden\\" name=\\"code\\" value=\\"${code}\\">\\n        <input type=\\"hidden\\" name=\\"postMethod\\" value=\\"true\\">\\n      </form>\\n      <script type=\\"text/javascript\\">\\n        document.forms[0].submit();\\n      </script>\\n    [#else]\\n      [@helpers.header]\\n        [#-- Custom header code goes here --]\\n      [/@helpers.header]\\n\\n      [@helpers.main title=theme.message('passwordless-login')]\\n        [#setting url_escaping_charset='UTF-8']\\n        <form action=\\"${request.contextPath}/oauth2/passwordless\\" method=\\"POST\\" class=\\"full\\">\\n          [@helpers.oauthHiddenFields/]\\n\\n          <fieldset>\\n            [@helpers.input type=\\"text\\" name=\\"loginId\\" id=\\"loginId\\" autocomplete=\\"username\\" autocapitalize=\\"none\\" autocomplete=\\"on\\" autocorrect=\\"off\\" spellcheck=\\"false\\" autofocus=true placeholder=theme.message('loginId') leftAddon=\\"user\\" required=true/]\\n            [@helpers.captchaBadge showCaptcha=showCaptcha captchaMethod=tenant.captchaConfiguration.captchaMethod siteKey=tenant.captchaConfiguration.siteKey/]\\n          </fieldset>\\n\\n          [@helpers.input id=\\"rememberDevice\\" type=\\"checkbox\\" name=\\"rememberDevice\\" label=theme.message('remember-device') value=\\"true\\" uncheckedValue=\\"false\\"]\\n            <i class=\\"fa fa-info-circle\\" data-tooltip=\\"${theme.message('{tooltip}remember-device')}\\"></i>[#t/]\\n          [/@helpers.input]\\n\\n          <div class=\\"form-row\\">\\n            [@helpers.button icon=\\"send\\" text=theme.message('send')/]\\n            <p class=\\"mt-2\\">[@helpers.link url=\\"/oauth2/authorize\\"]${theme.message('return-to-login')}[/@helpers.link]</p>\\n          </div>\\n        </form>\\n      [/@helpers.main]\\n\\n      [@helpers.footer]\\n        [#-- Custom footer code goes here --]\\n      [/@helpers.footer]\\n    [/#if]\\n  [/@helpers.body]\\n[/@helpers.html]","oauth2Register":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"collectBirthDate\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"devicePendingIdPLink\\" type=\\"io.fusionauth.domain.provider.PendingIdPLink\\" --]\\n[#-- @ftlvariable name=\\"fields\\" type=\\"java.util.List<io.fusionauth.domain.form.FormField>\\" --]\\n[#-- @ftlvariable name=\\"hideBirthDate\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"identityProviders\\" type=\\"java.util.Map<java.lang.String, java.util.List<io.fusionauth.domain.provider.BaseIdentityProvider<?>>>\\" --]\\n[#-- @ftlvariable name=\\"passwordValidationRules\\" type=\\"io.fusionauth.domain.PasswordValidationRules\\" --]\\n[#-- @ftlvariable name=\\"parentEmailRequired\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"pendingIdPLink\\" type=\\"io.fusionauth.domain.provider.PendingIdPLink\\" --]\\n[#-- @ftlvariable name=\\"showCaptcha\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"step\\" type=\\"int\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#-- @ftlvariable name=\\"totalSteps\\" type=\\"int\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    <script src=\\"/js/identityProvider/InProgress.js?version=${version}\\"></script>\\n    [@helpers.alternativeLoginsScript clientId=client_id identityProviders=identityProviders/]\\n    [#if step == totalSteps]\\n      [@helpers.captchaScripts showCaptcha=showCaptcha captchaMethod=tenant.captchaConfiguration.captchaMethod siteKey=tenant.captchaConfiguration.siteKey/]\\n    [/#if]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('register')]\\n      [#-- During a linking work flow, optionally indicate to the user which IdP is being linked. --]\\n      [#if devicePendingIdPLink?? || pendingIdPLink??]\\n        <p class=\\"mt-0\\">\\n        [#if devicePendingIdPLink?? && pendingIdPLink??]\\n          ${theme.message('pending-links-register-to-complete', devicePendingIdPLink.identityProviderName, pendingIdPLink.identityProviderName)}\\n        [#elseif devicePendingIdPLink??]\\n          ${theme.message('pending-link-register-to-complete', devicePendingIdPLink.identityProviderName)}\\n        [#else]\\n          ${theme.message('pending-link-register-to-complete', pendingIdPLink.identityProviderName)}\\n        [/#if]\\n        [#-- A pending link can be cancled. If we also have a device link in progress, this cannot be canceled. --]\\n        [#if pendingIdPLink??]\\n          [@helpers.link url=\\"\\" extraParameters=\\"&cancelPendingIdpLink=true\\"]${theme.message('register-cancel-link')}[/@helpers.link]\\n        [/#if]\\n        </p>\\n      [/#if]\\n      <form action=\\"register\\" method=\\"POST\\" class=\\"full\\">\\n        [@helpers.oauthHiddenFields/]\\n        [@helpers.hidden name=\\"step\\"/]\\n        [@helpers.hidden name=\\"registrationState\\"/]\\n        [@helpers.hidden name=\\"parentEmailRequired\\"/]\\n\\n        [#-- Show the Password Validation Rules if there is a field error for 'user.password' --]\\n        [#if (fieldMessages?keys?seq_contains(\\"user.password\\")!false) && passwordValidationRules??]\\n          [@helpers.passwordRules passwordValidationRules/]\\n        [/#if]\\n\\n        [#-- Begin Self Service Custom Registration Form Steps --]\\n        [#if fields?has_content]\\n          <fieldset>\\n            [#list fields as field]\\n              [@helpers.customField field field.key field?is_first?then(true, false) field.key /]\\n              [#if field.confirm]\\n                [@helpers.customField field \\"confirm.${field.key}\\" false \\"[confirm]${field.key}\\" /]\\n              [/#if]\\n            [/#list]\\n            [#-- If this is the last step of the form, optionally show a captcha. --]\\n            [#if step == totalSteps]\\n              [@helpers.captchaBadge showCaptcha=showCaptcha captchaMethod=tenant.captchaConfiguration.captchaMethod siteKey=tenant.captchaConfiguration.siteKey/]\\n            [/#if]\\n          </fieldset>\\n\\n          [#if step == totalSteps]\\n            [@helpers.input id=\\"rememberDevice\\" type=\\"checkbox\\" name=\\"rememberDevice\\" label=theme.message('remember-device') value=\\"true\\" uncheckedValue=\\"false\\"]\\n              <i class=\\"fa fa-info-circle\\" data-tooltip=\\"${theme.message('{tooltip}remember-device')}\\"></i>[#t/]\\n            [/@helpers.input]\\n            <div class=\\"form-row\\">\\n              [@helpers.button icon=\\"key\\" text=theme.message('register')/]\\n            [#else]\\n              [@helpers.button icon=\\"arrow-right\\" text=theme.message('next')/]\\n              </div>\\n          [/#if]\\n        [#-- End Custom Self Service Registration Form Steps --]\\n        [#else]\\n        [#-- Begin Basic Self Service Registration Form --]\\n        <fieldset>\\n          [@helpers.hidden name=\\"collectBirthDate\\"/]\\n          [#if !collectBirthDate && (!application.registrationConfiguration.birthDate.enabled || hideBirthDate)]\\n            [@helpers.hidden name=\\"user.birthDate\\" dateTimeFormat=\\"yyyy-MM-dd\\"/]\\n          [/#if]\\n          [#if collectBirthDate]\\n            [@helpers.input type=\\"text\\" name=\\"user.birthDate\\" id=\\"birthDate\\" placeholder=theme.message('birthDate') leftAddon=\\"calendar\\" class=\\"date-picker\\" dateTimeFormat=\\"yyyy-MM-dd\\" required=true/]\\n          [#else]\\n            [#if application.registrationConfiguration.loginIdType == 'email']\\n              [@helpers.input type=\\"text\\" name=\\"user.email\\" id=\\"email\\" autocomplete=\\"username\\" autocapitalize=\\"none\\" autocorrect=\\"off\\" spellcheck=\\"false\\" autofocus=true placeholder=theme.message('email') leftAddon=\\"user\\" required=true/]\\n            [#else]\\n              [@helpers.input type=\\"text\\" name=\\"user.username\\" id=\\"username\\" autocomplete=\\"username\\" autocapitalize=\\"none\\" autocorrect=\\"off\\" spellcheck=\\"false\\" autofocus=true placeholder=theme.message('username') leftAddon=\\"user\\" required=true/]\\n            [/#if]\\n            [@helpers.input type=\\"password\\" name=\\"user.password\\" id=\\"password\\" autocomplete=\\"new-password\\" placeholder=theme.message('password') leftAddon=\\"lock\\" required=true/]\\n            [#if application.registrationConfiguration.confirmPassword]\\n              [@helpers.input type=\\"password\\" name=\\"passwordConfirm\\" id=\\"passwordConfirm\\" autocomplete=\\"new-password\\" placeholder=theme.message('passwordConfirm') leftAddon=\\"lock\\" required=true/]\\n            [/#if]\\n            [#if parentEmailRequired]\\n              [@helpers.input type=\\"text\\" name=\\"user.parentEmail\\" id=\\"parentEmail\\" placeholder=theme.message('parentEmail') leftAddon=\\"user\\" required=true/]\\n            [/#if]\\n            [#if application.registrationConfiguration.birthDate.enabled ||\\n            application.registrationConfiguration.firstName.enabled    ||\\n            application.registrationConfiguration.fullName.enabled     ||\\n            application.registrationConfiguration.middleName.enabled   ||\\n            application.registrationConfiguration.lastName.enabled     ||\\n            application.registrationConfiguration.mobilePhone.enabled   ]\\n              <div class=\\"mt-5 mb-5\\"></div>\\n              [#if application.registrationConfiguration.firstName.enabled]\\n                  [@helpers.input type=\\"text\\" name=\\"user.firstName\\" id=\\"firstName\\" placeholder=theme.message('firstName') leftAddon=\\"user\\" required=application.registrationConfiguration.firstName.required/]\\n              [/#if]\\n              [#if application.registrationConfiguration.fullName.enabled]\\n                  [@helpers.input type=\\"text\\" name=\\"user.fullName\\" id=\\"fullName\\" placeholder=theme.message('fullName') leftAddon=\\"user\\" required=application.registrationConfiguration.fullName.required/]\\n              [/#if]\\n              [#if application.registrationConfiguration.middleName.enabled]\\n                  [@helpers.input type=\\"text\\" name=\\"user.middleName\\" id=\\"middleName\\" placeholder=theme.message('middleName') leftAddon=\\"user\\" required=application.registrationConfiguration.middleName.required/]\\n              [/#if]\\n              [#if application.registrationConfiguration.lastName.enabled]\\n                  [@helpers.input type=\\"text\\" name=\\"user.lastName\\" id=\\"lastName\\" placeholder=theme.message('lastName') leftAddon=\\"user\\" required=application.registrationConfiguration.lastName.required/]\\n              [/#if]\\n              [#if application.registrationConfiguration.birthDate.enabled && !hideBirthDate]\\n                  [@helpers.input type=\\"text\\" name=\\"user.birthDate\\" id=\\"birthDate\\" placeholder=theme.message('birthDate') leftAddon=\\"calendar\\" class=\\"date-picker\\" dateTimeFormat=\\"yyyy-MM-dd\\" required=application.registrationConfiguration.birthDate.required/]\\n              [/#if]\\n              [#if application.registrationConfiguration.mobilePhone.enabled]\\n                  [@helpers.input type=\\"text\\" name=\\"user.mobilePhone\\" id=\\"mobilePhone\\" placeholder=theme.message('mobilePhone') leftAddon=\\"phone\\" required=application.registrationConfiguration.mobilePhone.required/]\\n              [/#if]\\n            [/#if]\\n          [/#if]\\n          [@helpers.captchaBadge showCaptcha=showCaptcha captchaMethod=tenant.captchaConfiguration.captchaMethod siteKey=tenant.captchaConfiguration.siteKey/]\\n        </fieldset>\\n\\n        [@helpers.input id=\\"rememberDevice\\" type=\\"checkbox\\" name=\\"rememberDevice\\" label=theme.message('remember-device') value=\\"true\\" uncheckedValue=\\"false\\"]\\n          <i class=\\"fa fa-info-circle\\" data-tooltip=\\"${theme.message('{tooltip}remember-device')}\\"></i>[#t/]\\n        [/@helpers.input]\\n\\n        <div class=\\"form-row\\">\\n          [@helpers.button icon=\\"key\\" text=theme.message('register')/]\\n          <p class=\\"mt-2\\">[@helpers.link url=\\"/oauth2/authorize\\"]${theme.message('return-to-login')}[/@helpers.link]</p>\\n        </div>\\n        [/#if]\\n        [#-- End Basic Self Service Registration Form --]\\n\\n        [#-- Begin Self Service Custom Registration Form Step Counter --]\\n        [#if step > 0]\\n          <div class=\\"w-100 mt-3\\" style=\\"display: inline-flex; flex-direction: row; justify-content: space-evenly;\\">\\n            <div class=\\"text-right\\" style=\\"flex-grow: 1;\\"> ${theme.message('register-step', step, totalSteps)} </div>\\n          </div>\\n        [/#if]\\n        [#-- End Self Service Custom Registration Form Step Counter --]\\n\\n        [#-- Identity Provider Buttons (if you want to include these, remove the if-statement) --]\\n        [#if true]\\n          [@helpers.alternativeLogins clientId=client_id identityProviders=identityProviders![] passwordlessEnabled=false/]\\n        [/#if]\\n        [#-- End Identity Provider Buttons --]\\n\\n      </form>\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","oauth2StartIdPLink":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"currentUser\\" type=\\"io.fusionauth.domain.User\\" --]\\n[#-- @ftlvariable name=\\"devicePendingIdPLink\\" type=\\"io.fusionauth.domain.provider.PendingIdPLink\\" --]\\n[#-- @ftlvariable name=\\"pendingIdPLink\\" type=\\"io.fusionauth.domain.provider.PendingIdPLink\\" --]\\n[#-- @ftlvariable name=\\"registrationEnabled\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[#-- Note in most cases, the currentUser reference will not be available. In the case where the user exceeds the number of\\n     links allowed for an IdP, the user will be returned here while having an SSO session. In this case, currentUser will\\n     be available.\\n--]\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [#-- If a pending link will cause us to exceed our linking limit, the next step will be to lgoout. --]\\n    [#assign logoutToContinue = (devicePendingIdPLink?? && devicePendingIdPLink.linkLimitExceeded) || (pendingIdPLink?? && pendingIdPLink.linkLimitExceeded) /]\\n\\n    [@helpers.main title=theme.message('start-idp-link-title')]\\n      [#if pendingIdPLink??]\\n        ${theme.message('pending-link-info', pendingIdPLink.identityProviderName)}\\n      [/#if]\\n\\n      [#if logoutToContinue]\\n        [#-- You may wish to obfuscate a portion of this value for anonymity.. --]\\n        [#assign currentLoginId = currentUser.login /]\\n        ${theme.message('logged-in-as', currentLoginId)}\\n        [#if devicePendingIdPLink?? && devicePendingIdPLink.linkLimitExceeded]\\n          <p>${theme.message('link-count-exceeded-pending-logout', devicePendingIdPLink.identityProviderName)}</p>\\n        [/#if]\\n        [#if pendingIdPLink?? && pendingIdPLink.linkLimitExceeded]\\n          <p>${theme.message('link-count-exceeded-pending-logout', pendingIdPLink.identityProviderName)}</p>\\n        [/#if]\\n        <p class=\\"mb-4\\">\\n          ${theme.message('link-count-exceeded-next-step${registrationEnabled?then(\\"\\", \\"-no-registration\\")}')}\\n        </p>\\n      [#else]\\n        <p class=\\"mb-5\\">\\n        [#if devicePendingIdPLink?? && pendingIdPLink??]\\n          ${theme.message('pending-device-links', devicePendingIdPLink.identityProviderName, pendingIdPLink.identityProviderName)}\\n        [#elseif devicePendingIdPLink??]\\n          ${theme.message('pending-device-link', devicePendingIdPLink.identityProviderName)}\\n        [/#if]\\n        ${theme.message('pending-link-next-step${registrationEnabled?then(\\"\\", \\"-no-registration\\")}')}\\n        </p>\\n      [/#if]\\n\\n      <div class=\\"row mt-3 mb-3\\">\\n        <div class=\\"col-xs\\">\\n          [#if logoutToContinue]\\n            [@helpers.logoutLink redirectURI=\\"/oauth2/start-idp-link\\"]\\n              <button class=\\"blue button w-100\\" style=\\"height: 35px;\\"><i class=\\"fa fa-arrow-right\\"></i> ${theme.message('logout')} </button>\\n            [/@helpers.logoutLink]\\n          [#else]\\n            [@helpers.link url=\\"/oauth2/authorize\\"]\\n              <button class=\\"blue button w-100\\" style=\\"height: 35px;\\"><i class=\\"fa fa-arrow-right\\"></i> ${theme.message('link-to-existing-user')} </button>\\n            [/@helpers.link]\\n          [/#if]\\n        </div>\\n      </div>\\n\\n      [#-- If self service registration is enabled, we can also provide an option to register. --]\\n      [#if registrationEnabled && !logoutToContinue]\\n       <div class=\\"row mb-3\\">\\n         <div class=\\"col-xs\\">\\n           [@helpers.link url=\\"/oauth2/register\\"]\\n             <button class=\\"blue button w-100\\" style=\\"height: 35px;\\"> <i class=\\"fa fa-arrow-right\\"></i> ${theme.message('link-to-new-user')} </button>\\n           [/@helpers.link]\\n         </div>\\n       </div>\\n      [/#if]\\n\\n      [#if pendingIdPLink??]\\n      <div class=\\"hr-container\\">\\n        <hr>\\n        <div>${theme.message('or')}</div>\\n      </div>\\n\\n      <div class=\\"row mt-3 mb-3\\">\\n        <div class=\\"col-xs\\">\\n        [@helpers.link url=\\"/oauth2/authorize\\" extraParameters=\\"&cancelPendingIdpLink=true\\"]\\n          <button class=\\"blue button w-100\\" style=\\"height: 35px;\\"><i class=\\"fa fa-arrow-right\\"></i> ${theme.message('cancel-link')} </button>\\n        [/@helpers.link]\\n        </div>\\n      </div>\\n      [/#if]\\n\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","oauth2TwoFactor":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"method\\" type=\\"io.fusionauth.domain.TwoFactorMethod\\" --]\\n[#-- @ftlvariable name=\\"methodId\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"showResendOrSelectMethod\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#-- @ftlvariable name=\\"trustComputer\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"twoFactorId\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"version\\" type=\\"java.lang.String\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n    <script src=\\"${request.contextPath}/js/oauth2/TwoFactor.js?version=${version}\\"></script>\\n    <script>\\n      Prime.Document.onReady(function() {\\n        new FusionAuth.OAuth2.TwoFactor();\\n      });\\n    </script>\\n  [/@helpers.head]\\n  [@helpers.body]\\n\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('two-factor-challenge')]\\n      [#setting url_escaping_charset='UTF-8']\\n      <form id=\\"2fa-form\\" action=\\"two-factor\\" method=\\"POST\\" class=\\"full\\">\\n        [@helpers.input type=\\"text\\" name=\\"code\\" id=\\"code\\" autocapitalize=\\"none\\" autocomplete=\\"one-time-code\\" autocorrect=\\"off\\" autofocus=true leftAddon=\\"lock\\" placeholder=theme.message('code')/]\\n\\n        [@helpers.oauthHiddenFields/]\\n        [@helpers.hidden name=\\"methodId\\"/]\\n        [@helpers.hidden name=\\"twoFactorId\\"/]\\n        <fieldset>\\n          <div class=\\"form-row\\">\\n            <label>\\n              <input type=\\"checkbox\\" name=\\"trustComputer\\" [#if trustComputer]checked[/#if]/>\\n              [#assign trustInDays = (tenant.externalIdentifierConfiguration.twoFactorTrustIdTimeToLiveInSeconds / (24 * 60 * 60))?string(\\"##0\\")/]\\n              ${theme.message('trust-computer', trustInDays)}\\n              <i class=\\"fa fa-info-circle\\" data-tooltip=\\"${theme.message('{tooltip}trustComputer')}\\"></i>[#t/]\\n            </label>\\n          </div>\\n        </fieldset>\\n        <div class=\\"form-row\\">\\n          [@helpers.button text=theme.message('verify')/]\\n        </div>\\n\\n        [#-- If more than one option was available, allow the user to change their mind, or go back and request another code. --]\\n        [#if showResendOrSelectMethod]\\n           <div class=\\"form-row mt-4 mb-0\\">\\n            [@helpers.link url=\\"/oauth2/two-factor-methods\\" extraParameters=\\"&twoFactorId=${twoFactorId?url}&methodId=${methodId!''}&selectMethod=true\\"]\\n              <i class=\\"fa fa-arrow-left\\"></i>\\n              ${theme.message('two-factor-select-method')}\\n            [/@helpers.link]\\n           </div>\\n        [/#if]\\n\\n      </form>\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","oauth2TwoFactorMethods":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"availableMethodsMap\\" type=\\"java.util.Map<java.lang.String, io.fusionauth.domain.TwoFactorMethod>\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"methodId\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"recoverCodesAvailable\\" type=\\"int\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#-- @ftlvariable name=\\"twoFactorId\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"version\\" type=\\"java.lang.String\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[#macro methodOption id method]\\n <div class=\\"form-row\\" >\\n   <label>\\n     <input type=\\"radio\\" name=\\"methodId\\" value=\\"${id}\\" [#if id = methodId!'']checked[/#if]>\\n     [#if method.method == \\"email\\"]\\n       <span>Email message</span>\\n       <span>\\n        [#assign index = method.email?index_of(\\"@\\")/]\\n        Get a code at ${method.email?substring(0, index + 2)}&hellip;\\n       </span>\\n     [#elseif method.method == \\"authenticator\\"]\\n       &nbsp;<span>Authenticator</span>\\n        <span>\\n         Get a code from your authenticator app\\n        </span>\\n     [#elseif method.method == \\"sms\\"]\\n        <span>Text message</span>\\n        <span>\\n         Get a code at (***) ***-**${method.mobilePhone?substring(method.mobilePhone?length - 2)}\\n        </span>\\n     [#else]\\n        ${theme.optionalMessage(method.method)}\\n     [/#if]\\n     </label>\\n </div>\\n[/#macro]\\n\\n[#macro recoveryCodeOption]\\n<div class=\\"form-row\\">\\n  <label>\\n    <input type=\\"radio\\" name=\\"methodId\\" value=\\"recoveryCode\\" [#if \\"recoveryCode\\" == methodId!'']checked[/#if]>\\n       <span>${theme.message('two-factor-recovery-code')}</span>\\n       <span>\\n         ${theme.message('two-factor-use-one-of-n-recover-codes', recoverCodesAvailable)}\\n       </span>\\n  </label>\\n</div>\\n[/#macro]\\n\\n<style>\\n  .radio-items .form-row label {\\n    border-left: 2px solid transparent;\\n    font-weight: 500;\\n    padding-bottom: 15px;\\n    padding-left: 15px;\\n    padding-top: 5px;\\n  }\\n\\n  .radio-items .form-row label span:first-of-type {\\n    margin-left: 5px;\\n  }\\n\\n  .radio-items .form-row label span:last-of-type {\\n    border-top: 1px solid #d2d2d2;\\n    display: block;\\n    font-size: 12px;\\n    font-weight: 400;\\n    margin-left: 22px;\\n    margin-top: 2px;\\n    padding-top:2px;\\n  }\\n</style>\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n  [@helpers.body]\\n\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('two-factor-challenge-options')]\\n      [#setting url_escaping_charset='UTF-8']\\n      <form id=\\"2fa-form\\" action=\\"two-factor-methods\\" method=\\"POST\\" class=\\"full\\">\\n\\n        [@helpers.oauthHiddenFields/]\\n        [@helpers.hidden name=\\"twoFactorId\\"/]\\n\\n        [#-- Panel description --]\\n        ${theme.message('{description}two-factor-methods-selection')}\\n\\n        [#-- Available methods --]\\n        <fieldset class=\\"mt-3 hover radio-items\\">\\n\\n          [#list availableMethodsMap as id, method]\\n             [@methodOption id method/]\\n          [/#list]\\n\\n          [#-- Optionally show an option for recovery codes. A recovery code can always be used to login, so selecting this is not\\n               required to allow the user to enter a recovery code. But it is a cue to the user that they have this option.\\n               Feel free to remove it if you do not want to show it, it will not affect the user's ability to use a recovery code.\\n           --]\\n          [#if recoverCodesAvailable gt 0]\\n            [@recoveryCodeOption/]\\n          [/#if]\\n\\n          [#-- Show the methodId error here sinc we will have more than one radio button. This can be moved to suit you. --]\\n          [@helpers.errors \\"methodId\\"/]\\n        </fieldset>\\n\\n        [#-- Continue to the next step to enter your code. --]\\n        [@helpers.button text=theme.message('continue')/]\\n      </form>\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","oauth2Wait":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"code\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"identityProviderId\\" type=\\"java.util.UUID\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#-- @ftlvariable name=\\"waitURL\\" type=\\"java.lang.String\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head title=theme.message('wait-title')]\\n  <meta http-equiv=\\"Refresh\\" content=\\"2; url=${waitURL}\\">\\n  [/@helpers.head ]\\n\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('waiting')]\\n      <span>${theme.message('complete-external-login')}</span>\\n      <p class=\\"mt-2\\">[@helpers.link url=\\"/oauth2/authorize\\"]${theme.message('return-to-login')}[/@helpers.link]</p>\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","passwordChange":"[#ftl/]\\n[#-- @ftlvariable name=\\"passwordValidationRules\\" type=\\"io.fusionauth.domain.PasswordValidationRules\\" --]\\n\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.appriseMain title=theme.message('password-change-title')]\\n      <form action=\\"change\\" method=\\"POST\\" class=\\"full\\">\\n        [@helpers.oauthHiddenFields/]\\n        [@helpers.hidden name=\\"changePasswordId\\"/]\\n\\n        [#-- Show the Password Validation Rules if there is a field error for 'password' --]\\n        [#if (fieldMessages?keys?seq_contains(\\"password\\")!false) && passwordValidationRules??]\\n          [@helpers.passwordRules passwordValidationRules/]\\n        [/#if]\\n        <fieldset>\\n          [@helpers.inputMaterial type=\\"password\\" name=\\"password\\" \\n          id=\\"password\\" placeholder=theme.message('password') leftAddon=\\"lock\\" autofocus=true required=true/]\\n          [@helpers.inputMaterial type=\\"password\\" name=\\"passwordConfirm\\" autocomplete=\\"new-password\\" id=\\"passwordConfirm\\" placeholder=theme.message('passwordConfirm') leftAddon=\\"lock\\" required=true/]\\n        </fieldset>\\n        <div class=\\"form-row\\">\\n          [@helpers.button text=theme.message('submit')/]\\n        </div>\\n      </form>\\n    [/@helpers.appriseMain]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","passwordComplete":"[#ftl/]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.appriseMain title=theme.message('password-changed-title')]\\n      <p>\\n        ${theme.message('password-changed')}\\n      </p>\\n      <div class=\\"right return\\">\\n        [@helpers.applink /]\\n      </div>\\n    [/@helpers.appriseMain]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","passwordForgot":"[#ftl/]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.appriseMain title=theme.message('forgot-password-title')]\\n      <form action=\\"forgot\\" method=\\"POST\\" class=\\"full\\">\\n        [@helpers.oauthHiddenFields/]\\n\\n        <p>\\n          ${theme.message('forgot-password')}\\n        </p>\\n        <fieldset class=\\"push-less-top\\">\\n          [@helpers.inputMaterial type=\\"text\\" name=\\"email\\" id=\\"email\\" autocapitalize=\\"none\\" autofocus=true autocomplete=\\"on\\" autocorrect=\\"off\\" placeholder=theme.message('email') leftAddon=\\"user\\" required=true/]\\n        </fieldset>\\n        <div class=\\"form-row\\">\\n          <div class=\\"actions-row\\">\\n          \\t\\t[@helpers.buttonMaterial text=theme.message('submit')/]\\n          \\t<div class=\\"right return\\">\\n          \\t\\t<p class=\\"mt-2\\">[@helpers.link url=\\"/oauth2/authorize\\"]${theme.message('return-to-login')}[/@helpers.link]</p>\\n          \\t</div>\\n          </div>\\n        </div>\\n      </form>\\n    [/@helpers.appriseMain]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","passwordSent":"[#ftl/]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.appriseMain title=theme.message('forgot-password-email-sent-title')]\\n      <p>\\n        ${theme.message('forgot-password-email-sent')}\\n      </p>\\n      <div class=\\"right return\\">\\n        [@helpers.applink /]\\n      </div>\\n    [/@helpers.appriseMain]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","registrationComplete":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('registration-verification-complete-title')]\\n      <p>\\n        ${theme.message('registration-verification-complete')}\\n      </p>\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","registrationSend":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"email\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"emailSent\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('registration-verification-sent-title')]\\n      <p>\\n        ${theme.message('registration-verification-sent', email)}\\n      </p>\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","registrationSent":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"email\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"emailSent\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('registration-verification-sent-title')]\\n      <p>\\n        ${theme.message('registration-verification-sent', email)}\\n      </p>\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","registrationVerificationRequired":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"collectVerificationCode\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"currentUser\\" type=\\"io.fusionauth.domain.User\\" --]\\n[#-- @ftlvariable name=\\"showCaptcha\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#-- @ftlvariable name=\\"verificationId\\" type=\\"java.lang.String\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [@helpers.captchaScripts showCaptcha=showCaptcha captchaMethod=tenant.captchaConfiguration.captchaMethod siteKey=tenant.captchaConfiguration.siteKey/]\\n    [#-- Custom <head> code goes here --]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('registration-verification-required-title')]\\n      [#-- The user does not have a verified registration. Add optionall messaging here with instruction to the user. --]\\n\\n       [#-- Let the user know why they ended up here --]\\n       <p class=\\"mt-0 mb-3\\">\\n         ${theme.message(\\"{description}registration-verification-required\\")}\\n       </p>\\n\\n       [#-- If configured, collect the verification code on this form, this means the user sits here until they verify their registration. --]\\n       [#if collectVerificationCode]\\n          <form id=\\"verification-required-enter-code\\" action=\\"${request.contextPath}/registration/verification-required\\" method=\\"POST\\" class=\\"full\\">\\n            [@helpers.oauthHiddenFields/]\\n            [@helpers.hidden name=\\"action\\" value=\\"verify\\"/]\\n            [@helpers.hidden name=\\"collectVerificationCode\\"/]\\n            [@helpers.hidden name=\\"verificationId\\"/]\\n            <fieldset>\\n              [@helpers.input type=\\"text\\" name=\\"oneTimeCode\\" id=\\"otp\\" autocapitalize=\\"none\\" autofocus=true autocomplete=\\"one-time-code\\" autocorrect=\\"off\\" placeholder=\\"${theme.message('code')}\\" leftAddon=\\"lock\\"/]\\n              [@helpers.captchaBadge showCaptcha=showCaptcha captchaMethod=tenant.captchaConfiguration.captchaMethod siteKey=tenant.captchaConfiguration.siteKey/]\\n            </fieldset>\\n            <div class=\\"form-row\\">\\n              [@helpers.button text=theme.message('submit')/]\\n            </div>\\n         </form>\\n       [#else]\\n         <p> ${theme.message(\\"{description}registration-verification-required-non-interactive\\")} </p>\\n       [/#if]\\n\\n       [#-- Resend a verification email --]\\n       <form id=\\"verification-required-resend-code\\" action=\\"${request.contextPath}/registration/verification-required\\" method=\\"POST\\" class=\\"full\\">\\n         [@helpers.oauthHiddenFields/]\\n         [@helpers.hidden name=\\"action\\" value=\\"resend\\"/]\\n         [@helpers.hidden name=\\"collectVerificationCode\\"/]\\n         [@helpers.hidden name=\\"verificationId\\"/]\\n         <div class=\\"form-row\\">\\n           <button style=\\"background: #fff; border: none; cursor: pointer;\\" class=\\"link blue-text\\"><i class=\\"fa fa-arrow-right\\"></i> ${theme.message('registration-verification-required-send-another')} </button>\\n         </div>\\n       </form>\\n\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","registrationVerify":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"postMethod\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"showCaptcha\\" type=\\"boolean\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#-- @ftlvariable name=\\"verificationId\\" type=\\"java.lang.String\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head]\\n    [@helpers.captchaScripts showCaptcha=showCaptcha captchaMethod=tenant.captchaConfiguration.captchaMethod siteKey=tenant.captchaConfiguration.siteKey/]\\n  [/@helpers.head]\\n  [@helpers.body]\\n    [#if verificationId?? && postMethod]\\n      <form action=\\"${request.contextPath}/registration/verify\\" method=\\"POST\\">\\n        <input type=\\"hidden\\" name=\\"client_id\\" value=\\"${client_id!''}\\">\\n        <input type=\\"hidden\\" name=\\"postMethod\\" value=\\"true\\">\\n        <input type=\\"hidden\\" name=\\"tenantId\\" value=\\"${tenantId!''}\\">\\n        <input type=\\"hidden\\" name=\\"verificationId\\" value=\\"${verificationId}\\">\\n      </form>\\n      <script type=\\"text/javascript\\">\\n        document.forms[0].submit();\\n      </script>\\n    [#else]\\n      [@helpers.header]\\n        [#-- Custom header code goes here --]\\n      [/@helpers.header]\\n\\n      [@helpers.main title=theme.message('registration-verification-form-title')]\\n        [#-- FusionAuth automatically handles errors that occur during registration verification and outputs them in the HTML --]\\n        <form action=\\"${request.contextPath}/registration/verify\\" method=\\"POST\\" class=\\"full\\">\\n          [@helpers.hidden name=\\"captcha_token\\"/]\\n          [@helpers.hidden name=\\"client_id\\"/]\\n          [@helpers.hidden name=\\"tenantId\\"/]\\n          <p>\\n            ${theme.message('registration-verification-form')}\\n          </p>\\n          <fieldset class=\\"push-less-top\\">\\n            [@helpers.input type=\\"text\\" name=\\"email\\" id=\\"email\\" autocapitalize=\\"none\\" autofocus=true autocomplete=\\"on\\" autocorrect=\\"off\\" placeholder=\\"${theme.message('email')}\\" leftAddon=\\"user\\"/]\\n            [@helpers.captchaBadge showCaptcha=showCaptcha captchaMethod=tenant.captchaConfiguration.captchaMethod siteKey=tenant.captchaConfiguration.siteKey/]\\n          </fieldset>\\n          <div class=\\"form-row\\">\\n            [@helpers.button text=theme.message('submit')/]\\n          </div>\\n        </form>\\n      [/@helpers.main]\\n\\n      [@helpers.footer]\\n        [#-- Custom footer code goes here --]\\n      [/@helpers.footer]\\n    [/#if]\\n  [/@helpers.body]\\n[/@helpers.html]","samlv2Logout":"[#ftl/]\\n[#-- @ftlvariable name=\\"allLogoutURLs\\" type=\\"java.util.Set<java.lang.String>\\" --]\\n[#-- @ftlvariable name=\\"registeredLogoutURLs\\" type=\\"java.util.Set<java.lang.String>\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#import \\"../_helpers.ftl\\" as helpers/]\\n\\n[#-- You may adjust the duration that we wait before completing the logout. --]\\n[#assign logoutDurationInSeconds = 2 /]\\n\\n[@helpers.html]\\n  [@helpers.head title=theme.message('logout-title')/]\\n  [@helpers.body]\\n    [@helpers.header]\\n      [#-- Custom header code goes here --]\\n    [/@helpers.header]\\n\\n    [@helpers.main title=theme.message('logging-out')]\\n      <div class=\\"progress-bar\\" >\\n        <div style=\\"animation-duration: ${logoutDurationInSeconds + 1}s; animation-timing-function: ease-out;\\">\\n        </div>\\n      </div>\\n    [/@helpers.main]\\n\\n    [#-- Use allLogoutURLs to call the logout URL of all applications in the tenant, or use registeredLogoutURLs to log out of just the applications the user is currently registered.\\n        Note, that just because a user does not currently have a registration, does not neccessarily mean the user does not hold a session with an application. It is possible the user has been un-registered\\n        recently, or an application may have created a session for a user regardless of their registration. In most cases it is safest to simply call all applications and ensure that the Single Logout\\n        URL can handle logout requests for users that may or may not have a session with that application.\\n     --]\\n    [#list allLogoutURLs![] as logoutURL]\\n      <iframe src=\\"${logoutURL}\\" style=\\"width:0; height:0; border:0; border:none;\\"></iframe>\\n    [/#list]\\n    <form action=\\"/samlv2/logout/complete\\" method=\\"POST\\">\\n      [@helpers.hidden name=\\"binding\\"/]\\n      [@helpers.hidden name=\\"RelayState\\"/]\\n      [@helpers.hidden name=\\"SAMLRequest\\"/]\\n      [@helpers.hidden name=\\"Signature\\"/]\\n      [@helpers.hidden name=\\"SigAlg\\"/]\\n      [@helpers.hidden name=\\"tenantId\\"/]\\n    </form>\\n    <script type=\\"text/javascript\\">\\n      [#-- Allow enough time for each iframe to load and make a request to the SAML logout endpoints. --]\\n      setTimeout(function() {\\n        document.forms[0].submit();\\n      }, ${logoutDurationInSeconds * 1000});\\n    </script>\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]","unauthorized":"[#ftl/]\\n[#-- @ftlvariable name=\\"application\\" type=\\"io.fusionauth.domain.Application\\" --]\\n[#-- @ftlvariable name=\\"cause\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"client_id\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"currentUser\\" type=\\"io.fusionauth.domain.User\\" --]\\n[#-- @ftlvariable name=\\"incidentId\\" type=\\"java.lang.String\\" --]\\n[#-- @ftlvariable name=\\"tenant\\" type=\\"io.fusionauth.domain.Tenant\\" --]\\n[#-- @ftlvariable name=\\"tenantId\\" type=\\"java.util.UUID\\" --]\\n[#import \\"_helpers.ftl\\" as helpers/]\\n\\n[@helpers.html]\\n  [@helpers.head title=theme.message('unauthorized')/]\\n  [@helpers.body]\\n\\n    [@helpers.main title=theme.message('access-denied') colClass=\\"col-xs col-sm-10 col-md-8 col-lg-7 col-xl-5\\"]\\n      [#assign incidentId = (request.getAttribute(\\"incidentId\\")!'')/]\\n      [#assign incidentCause = (request.getAttribute(\\"incidentCause\\")!'')/]\\n\\n      [#if incidentCause?has_content && incidentCause == \\"BlockedIPAddressException\\"]\\n      ${theme.message('unauthorized-message-blocked-ip', currentBaseURL)}\\n      [#else]\\n      ${theme.message('unauthorized-message')}\\n      [/#if]\\n      <p>\\n      ${theme.message('ip-address')}${theme.message('propertySeparator')} ${currentIPAddress}\\n      [#assign currentLocation = fusionAuth.currentLocation()!{}/]\\n      [#if currentLocation?has_content]\\n      <br>${theme.message('location')}${theme.message('propertySeparator')} ${currentLocation.displayString}\\n      [/#if]\\n      </p>\\n\\n      <hr>\\n       [#if incidentId??]${theme.message('incident-id')}${theme.message('propertySeparator')} <span style=\\"font-weight: 600\\">${incidentId}</span> &middot; [/#if]${theme.message('security-by')} <a href=\\"https://fusionauth.io\\">FusionAuth</a>\\n    [/@helpers.main]\\n\\n    [@helpers.footer]\\n      [#-- Custom footer code goes here --]\\n    [/@helpers.footer]\\n  [/@helpers.body]\\n[/@helpers.html]"}}	1666539991301	1693909827657	emaris
\.


--
-- Data for Name: user_action_logs; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.user_action_logs (id, actioner_users_id, actionee_users_id, comment, email_user_on_end, end_event_sent, expiry, history, insert_instant, localized_name, localized_option, localized_reason, name, notify_user_on_end, option_name, reason, reason_code, user_actions_id) FROM stdin;
\.


--
-- Data for Name: user_action_logs_applications; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.user_action_logs_applications (applications_id, user_action_logs_id) FROM stdin;
\.


--
-- Data for Name: user_action_reasons; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.user_action_reasons (id, insert_instant, last_update_instant, localized_texts, text, code) FROM stdin;
\.


--
-- Data for Name: user_actions; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.user_actions (id, active, cancel_email_templates_id, end_email_templates_id, include_email_in_event_json, insert_instant, last_update_instant, localized_names, modify_email_templates_id, name, options, prevent_login, send_end_event, start_email_templates_id, temporal, transaction_type, user_notifications_enabled, user_emailing_enabled) FROM stdin;
\.


--
-- Data for Name: user_comments; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.user_comments (id, comment, commenter_id, insert_instant, users_id) FROM stdin;
\.


--
-- Data for Name: user_consents; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.user_consents (id, consents_id, data, giver_users_id, insert_instant, last_update_instant, users_id) FROM stdin;
\.


--
-- Data for Name: user_consents_email_plus; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.user_consents_email_plus (id, next_email_instant, user_consents_id) FROM stdin;
\.


--
-- Data for Name: user_registrations; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.user_registrations (id, applications_id, authentication_token, clean_speak_id, data, insert_instant, last_login_instant, last_update_instant, timezone, username, username_status, users_id, verified) FROM stdin;
4ce698f0-c437-4b14-aa90-9625c4dc712e	3c219e58-ed0e-4b18-ad48-f4f92793ae32	\N	\N	{"data":{},"preferredLanguages":[],"tokens":{}}	1666539967237	1693920176111	1666539967237	\N	\N	0	60b1ad57-9b8f-48c5-be33-0cbb01c35bdb	t
\.


--
-- Data for Name: user_registrations_application_roles; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.user_registrations_application_roles (application_roles_id, user_registrations_id) FROM stdin;
631ecd9d-8d40-4c13-8277-80cedb8236e2	4ce698f0-c437-4b14-aa90-9625c4dc712e
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.users (id, active, birth_date, clean_speak_id, data, expiry, first_name, full_name, image_url, insert_instant, last_name, last_update_instant, middle_name, mobile_phone, parent_email, tenants_id, timezone) FROM stdin;
60b1ad57-9b8f-48c5-be33-0cbb01c35bdb	t	\N	\N	{"data":{},"preferredLanguages":[],"twoFactor":{}}	\N	Apprise	\N	\N	1666539967180	Support	1668079764259	\N	\N	\N	e4ed14d6-3706-3908-2179-9e3b74617677	\N
\.


--
-- Data for Name: version; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.version (version) FROM stdin;
1.32.1
\.


--
-- Data for Name: webhooks; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.webhooks (id, connect_timeout, description, data, global, headers, http_authentication_username, http_authentication_password, insert_instant, last_update_instant, read_timeout, ssl_certificate, url) FROM stdin;
\.


--
-- Data for Name: webhooks_applications; Type: TABLE DATA; Schema: public; Owner: apprise
--

COPY public.webhooks_applications (webhooks_id, applications_id) FROM stdin;
\.


--
-- Name: audit_logs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: apprise
--

SELECT pg_catalog.setval('public.audit_logs_id_seq', 1, false);


--
-- Name: event_logs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: apprise
--

SELECT pg_catalog.setval('public.event_logs_id_seq', 1, false);


--
-- Name: identities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: apprise
--

SELECT pg_catalog.setval('public.identities_id_seq', 2, true);


--
-- Name: raw_application_registration_counts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: apprise
--

SELECT pg_catalog.setval('public.raw_application_registration_counts_id_seq', 1, false);


--
-- Name: raw_global_registration_counts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: apprise
--

SELECT pg_catalog.setval('public.raw_global_registration_counts_id_seq', 1, false);


--
-- Name: user_consents_email_plus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: apprise
--

SELECT pg_catalog.setval('public.user_consents_email_plus_id_seq', 1, false);


--
-- Name: application_daily_active_users application_daily_active_users_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.application_daily_active_users
    ADD CONSTRAINT application_daily_active_users_uk_1 UNIQUE (applications_id, day);


--
-- Name: application_monthly_active_users application_monthly_active_users_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.application_monthly_active_users
    ADD CONSTRAINT application_monthly_active_users_uk_1 UNIQUE (applications_id, month);


--
-- Name: application_registration_counts application_registration_counts_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.application_registration_counts
    ADD CONSTRAINT application_registration_counts_uk_1 UNIQUE (applications_id, hour);


--
-- Name: application_roles application_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.application_roles
    ADD CONSTRAINT application_roles_pkey PRIMARY KEY (id);


--
-- Name: application_roles application_roles_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.application_roles
    ADD CONSTRAINT application_roles_uk_1 UNIQUE (name, applications_id);


--
-- Name: applications applications_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_pkey PRIMARY KEY (id);


--
-- Name: applications applications_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_uk_1 UNIQUE (name, tenants_id);


--
-- Name: applications applications_uk_2; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_uk_2 UNIQUE (samlv2_issuer, tenants_id);


--
-- Name: asynchronous_tasks asynchronous_tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.asynchronous_tasks
    ADD CONSTRAINT asynchronous_tasks_pkey PRIMARY KEY (id);


--
-- Name: asynchronous_tasks asynchronous_tasks_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.asynchronous_tasks
    ADD CONSTRAINT asynchronous_tasks_uk_1 UNIQUE (entity_id);


--
-- Name: audit_logs audit_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.audit_logs
    ADD CONSTRAINT audit_logs_pkey PRIMARY KEY (id);


--
-- Name: authentication_keys authentication_keys_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.authentication_keys
    ADD CONSTRAINT authentication_keys_pkey PRIMARY KEY (id);


--
-- Name: authentication_keys authentication_keys_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.authentication_keys
    ADD CONSTRAINT authentication_keys_uk_1 UNIQUE (key_value);


--
-- Name: breached_password_metrics breached_password_metrics_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.breached_password_metrics
    ADD CONSTRAINT breached_password_metrics_pkey PRIMARY KEY (tenants_id);


--
-- Name: clean_speak_applications clean_speak_applications_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.clean_speak_applications
    ADD CONSTRAINT clean_speak_applications_uk_1 UNIQUE (applications_id, clean_speak_application_id);


--
-- Name: common_breached_passwords common_breached_passwords_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.common_breached_passwords
    ADD CONSTRAINT common_breached_passwords_pkey PRIMARY KEY (password);


--
-- Name: connectors connectors_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.connectors
    ADD CONSTRAINT connectors_pkey PRIMARY KEY (id);


--
-- Name: connectors_tenants connectors_tenants_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.connectors_tenants
    ADD CONSTRAINT connectors_tenants_pkey PRIMARY KEY (connectors_id, tenants_id);


--
-- Name: connectors_tenants connectors_tenants_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.connectors_tenants
    ADD CONSTRAINT connectors_tenants_uk_1 UNIQUE (connectors_id, tenants_id, sequence);


--
-- Name: connectors connectors_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.connectors
    ADD CONSTRAINT connectors_uk_1 UNIQUE (name);


--
-- Name: consents consents_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.consents
    ADD CONSTRAINT consents_pkey PRIMARY KEY (id);


--
-- Name: consents consents_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.consents
    ADD CONSTRAINT consents_uk_1 UNIQUE (name);


--
-- Name: data_sets data_sets_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.data_sets
    ADD CONSTRAINT data_sets_pkey PRIMARY KEY (name);


--
-- Name: email_templates email_templates_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.email_templates
    ADD CONSTRAINT email_templates_pkey PRIMARY KEY (id);


--
-- Name: email_templates email_templates_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.email_templates
    ADD CONSTRAINT email_templates_uk_1 UNIQUE (name);


--
-- Name: entities entities_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entities
    ADD CONSTRAINT entities_pkey PRIMARY KEY (id);


--
-- Name: entities entities_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entities
    ADD CONSTRAINT entities_uk_1 UNIQUE (client_id);


--
-- Name: entity_entity_grant_permissions entity_entity_grant_permissions_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_entity_grant_permissions
    ADD CONSTRAINT entity_entity_grant_permissions_uk_1 UNIQUE (entity_entity_grants_id, entity_type_permissions_id);


--
-- Name: entity_entity_grants entity_entity_grants_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_entity_grants
    ADD CONSTRAINT entity_entity_grants_pkey PRIMARY KEY (id);


--
-- Name: entity_entity_grants entity_entity_grants_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_entity_grants
    ADD CONSTRAINT entity_entity_grants_uk_1 UNIQUE (recipient_id, target_id);


--
-- Name: entity_type_permissions entity_type_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_type_permissions
    ADD CONSTRAINT entity_type_permissions_pkey PRIMARY KEY (id);


--
-- Name: entity_type_permissions entity_type_permissions_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_type_permissions
    ADD CONSTRAINT entity_type_permissions_uk_1 UNIQUE (entity_types_id, name);


--
-- Name: entity_types entity_types_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_types
    ADD CONSTRAINT entity_types_pkey PRIMARY KEY (id);


--
-- Name: entity_types entity_types_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_types
    ADD CONSTRAINT entity_types_uk_1 UNIQUE (name);


--
-- Name: entity_user_grant_permissions entity_user_grant_permissions_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_user_grant_permissions
    ADD CONSTRAINT entity_user_grant_permissions_uk_1 UNIQUE (entity_user_grants_id, entity_type_permissions_id);


--
-- Name: entity_user_grants entity_user_grants_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_user_grants
    ADD CONSTRAINT entity_user_grants_pkey PRIMARY KEY (id);


--
-- Name: entity_user_grants entity_user_grants_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_user_grants
    ADD CONSTRAINT entity_user_grants_uk_1 UNIQUE (entities_id, users_id);


--
-- Name: event_logs event_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.event_logs
    ADD CONSTRAINT event_logs_pkey PRIMARY KEY (id);


--
-- Name: external_identifiers external_identifiers_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.external_identifiers
    ADD CONSTRAINT external_identifiers_pkey PRIMARY KEY (id);


--
-- Name: families families_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.families
    ADD CONSTRAINT families_pkey PRIMARY KEY (family_id, users_id);


--
-- Name: federated_domains federated_domains_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.federated_domains
    ADD CONSTRAINT federated_domains_uk_1 UNIQUE (domain);


--
-- Name: form_fields form_fields_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.form_fields
    ADD CONSTRAINT form_fields_pkey PRIMARY KEY (id);


--
-- Name: form_fields form_fields_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.form_fields
    ADD CONSTRAINT form_fields_uk_1 UNIQUE (name);


--
-- Name: form_steps form_steps_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.form_steps
    ADD CONSTRAINT form_steps_pkey PRIMARY KEY (forms_id, form_fields_id);


--
-- Name: forms forms_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.forms
    ADD CONSTRAINT forms_pkey PRIMARY KEY (id);


--
-- Name: forms forms_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.forms
    ADD CONSTRAINT forms_uk_1 UNIQUE (name);


--
-- Name: global_daily_active_users global_daily_active_users_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.global_daily_active_users
    ADD CONSTRAINT global_daily_active_users_uk_1 UNIQUE (day);


--
-- Name: global_monthly_active_users global_monthly_active_users_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.global_monthly_active_users
    ADD CONSTRAINT global_monthly_active_users_uk_1 UNIQUE (month);


--
-- Name: global_registration_counts global_registration_counts_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.global_registration_counts
    ADD CONSTRAINT global_registration_counts_uk_1 UNIQUE (hour);


--
-- Name: group_application_roles group_application_roles_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.group_application_roles
    ADD CONSTRAINT group_application_roles_uk_1 UNIQUE (groups_id, application_roles_id);


--
-- Name: group_members group_members_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.group_members
    ADD CONSTRAINT group_members_pkey PRIMARY KEY (id);


--
-- Name: group_members group_members_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.group_members
    ADD CONSTRAINT group_members_uk_1 UNIQUE (groups_id, users_id);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: groups groups_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_uk_1 UNIQUE (name, tenants_id);


--
-- Name: hourly_logins hourly_logins_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.hourly_logins
    ADD CONSTRAINT hourly_logins_uk_1 UNIQUE (applications_id, hour);


--
-- Name: identities identities_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identities
    ADD CONSTRAINT identities_pkey PRIMARY KEY (id);


--
-- Name: identities identities_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identities
    ADD CONSTRAINT identities_uk_1 UNIQUE (email, tenants_id);


--
-- Name: identities identities_uk_2; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identities
    ADD CONSTRAINT identities_uk_2 UNIQUE (username_index, tenants_id);


--
-- Name: identity_provider_links identity_provider_links_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identity_provider_links
    ADD CONSTRAINT identity_provider_links_uk_1 UNIQUE (identity_providers_id, identity_providers_user_id, tenants_id);


--
-- Name: identity_providers identity_providers_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identity_providers
    ADD CONSTRAINT identity_providers_pkey PRIMARY KEY (id);


--
-- Name: identity_providers identity_providers_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identity_providers
    ADD CONSTRAINT identity_providers_uk_1 UNIQUE (name);


--
-- Name: ip_access_control_lists ip_access_control_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.ip_access_control_lists
    ADD CONSTRAINT ip_access_control_lists_pkey PRIMARY KEY (id);


--
-- Name: ip_access_control_lists ip_access_control_lists_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.ip_access_control_lists
    ADD CONSTRAINT ip_access_control_lists_uk_1 UNIQUE (name);


--
-- Name: keys keys_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.keys
    ADD CONSTRAINT keys_pkey PRIMARY KEY (id);


--
-- Name: keys keys_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.keys
    ADD CONSTRAINT keys_uk_1 UNIQUE (kid);


--
-- Name: keys keys_uk_2; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.keys
    ADD CONSTRAINT keys_uk_2 UNIQUE (name);


--
-- Name: kickstart_files kickstart_files_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.kickstart_files
    ADD CONSTRAINT kickstart_files_pkey PRIMARY KEY (id);


--
-- Name: lambdas lambdas_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.lambdas
    ADD CONSTRAINT lambdas_pkey PRIMARY KEY (id);


--
-- Name: locks locks_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.locks
    ADD CONSTRAINT locks_pkey PRIMARY KEY (type);


--
-- Name: message_templates message_templates_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.message_templates
    ADD CONSTRAINT message_templates_pkey PRIMARY KEY (id);


--
-- Name: message_templates message_templates_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.message_templates
    ADD CONSTRAINT message_templates_uk_1 UNIQUE (name);


--
-- Name: messengers messengers_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.messengers
    ADD CONSTRAINT messengers_pkey PRIMARY KEY (id);


--
-- Name: messengers messengers_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.messengers
    ADD CONSTRAINT messengers_uk_1 UNIQUE (name);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (name);


--
-- Name: nodes nodes_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.nodes
    ADD CONSTRAINT nodes_pkey PRIMARY KEY (id);


--
-- Name: previous_passwords previous_passwords_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.previous_passwords
    ADD CONSTRAINT previous_passwords_uk_1 UNIQUE (users_id, insert_instant);


--
-- Name: raw_application_daily_active_users raw_application_daily_active_users_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.raw_application_daily_active_users
    ADD CONSTRAINT raw_application_daily_active_users_uk_1 UNIQUE (applications_id, day, users_id);


--
-- Name: raw_application_monthly_active_users raw_application_monthly_active_users_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.raw_application_monthly_active_users
    ADD CONSTRAINT raw_application_monthly_active_users_uk_1 UNIQUE (applications_id, month, users_id);


--
-- Name: raw_application_registration_counts raw_application_registration_counts_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.raw_application_registration_counts
    ADD CONSTRAINT raw_application_registration_counts_pkey PRIMARY KEY (id);


--
-- Name: raw_global_daily_active_users raw_global_daily_active_users_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.raw_global_daily_active_users
    ADD CONSTRAINT raw_global_daily_active_users_uk_1 UNIQUE (day, users_id);


--
-- Name: raw_global_monthly_active_users raw_global_monthly_active_users_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.raw_global_monthly_active_users
    ADD CONSTRAINT raw_global_monthly_active_users_uk_1 UNIQUE (month, users_id);


--
-- Name: raw_global_registration_counts raw_global_registration_counts_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.raw_global_registration_counts
    ADD CONSTRAINT raw_global_registration_counts_pkey PRIMARY KEY (id);


--
-- Name: refresh_tokens refresh_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.refresh_tokens
    ADD CONSTRAINT refresh_tokens_pkey PRIMARY KEY (id);


--
-- Name: refresh_tokens refresh_tokens_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.refresh_tokens
    ADD CONSTRAINT refresh_tokens_uk_1 UNIQUE (token);


--
-- Name: request_frequencies request_frequencies_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.request_frequencies
    ADD CONSTRAINT request_frequencies_uk_1 UNIQUE (tenants_id, type, request_id);


--
-- Name: tenants tenants_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_pkey PRIMARY KEY (id);


--
-- Name: tenants tenants_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_uk_1 UNIQUE (name);


--
-- Name: themes themes_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.themes
    ADD CONSTRAINT themes_pkey PRIMARY KEY (id);


--
-- Name: themes themes_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.themes
    ADD CONSTRAINT themes_uk_1 UNIQUE (name);


--
-- Name: user_action_logs user_action_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_action_logs
    ADD CONSTRAINT user_action_logs_pkey PRIMARY KEY (id);


--
-- Name: user_action_reasons user_action_reasons_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_action_reasons
    ADD CONSTRAINT user_action_reasons_pkey PRIMARY KEY (id);


--
-- Name: user_action_reasons user_action_reasons_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_action_reasons
    ADD CONSTRAINT user_action_reasons_uk_1 UNIQUE (text);


--
-- Name: user_action_reasons user_action_reasons_uk_2; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_action_reasons
    ADD CONSTRAINT user_action_reasons_uk_2 UNIQUE (code);


--
-- Name: user_actions user_actions_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_actions
    ADD CONSTRAINT user_actions_pkey PRIMARY KEY (id);


--
-- Name: user_actions user_actions_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_actions
    ADD CONSTRAINT user_actions_uk_1 UNIQUE (name);


--
-- Name: user_comments user_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_comments
    ADD CONSTRAINT user_comments_pkey PRIMARY KEY (id);


--
-- Name: user_consents_email_plus user_consents_email_plus_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_consents_email_plus
    ADD CONSTRAINT user_consents_email_plus_pkey PRIMARY KEY (id);


--
-- Name: user_consents user_consents_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_consents
    ADD CONSTRAINT user_consents_pkey PRIMARY KEY (id);


--
-- Name: user_registrations_application_roles user_registrations_application_roles_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_registrations_application_roles
    ADD CONSTRAINT user_registrations_application_roles_uk_1 UNIQUE (user_registrations_id, application_roles_id);


--
-- Name: user_registrations user_registrations_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_registrations
    ADD CONSTRAINT user_registrations_pkey PRIMARY KEY (id);


--
-- Name: user_registrations user_registrations_uk_1; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_registrations
    ADD CONSTRAINT user_registrations_uk_1 UNIQUE (applications_id, users_id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: webhooks_applications webhooks_applications_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.webhooks_applications
    ADD CONSTRAINT webhooks_applications_pkey PRIMARY KEY (webhooks_id, applications_id);


--
-- Name: webhooks webhooks_pkey; Type: CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.webhooks
    ADD CONSTRAINT webhooks_pkey PRIMARY KEY (id);


--
-- Name: applications_i_1; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX applications_i_1 ON public.applications USING btree (tenants_id);


--
-- Name: audit_logs_i1; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX audit_logs_i1 ON public.audit_logs USING btree (insert_instant);


--
-- Name: event_logs_i1; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX event_logs_i1 ON public.event_logs USING btree (insert_instant);


--
-- Name: external_identifiers_i_1; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX external_identifiers_i_1 ON public.external_identifiers USING btree (tenants_id, type, insert_instant);


--
-- Name: external_identifiers_i_2; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX external_identifiers_i_2 ON public.external_identifiers USING btree (type, users_id, applications_id);


--
-- Name: external_identifiers_i_3; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX external_identifiers_i_3 ON public.external_identifiers USING btree (expiration_instant);


--
-- Name: families_i_1; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX families_i_1 ON public.families USING btree (users_id);


--
-- Name: group_members_i_1; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX group_members_i_1 ON public.group_members USING btree (users_id);


--
-- Name: identities_i_1; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX identities_i_1 ON public.identities USING btree (users_id);


--
-- Name: raw_logins_i_1; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX raw_logins_i_1 ON public.raw_logins USING btree (instant);


--
-- Name: raw_logins_i_2; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX raw_logins_i_2 ON public.raw_logins USING btree (users_id, instant);


--
-- Name: refresh_tokens_i_1; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX refresh_tokens_i_1 ON public.refresh_tokens USING btree (start_instant);


--
-- Name: refresh_tokens_i_2; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX refresh_tokens_i_2 ON public.refresh_tokens USING btree (applications_id);


--
-- Name: refresh_tokens_i_3; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX refresh_tokens_i_3 ON public.refresh_tokens USING btree (users_id);


--
-- Name: refresh_tokens_i_4; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX refresh_tokens_i_4 ON public.refresh_tokens USING btree (tenants_id);


--
-- Name: request_frequencies_i_1; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX request_frequencies_i_1 ON public.request_frequencies USING btree (tenants_id, type, last_update_instant);


--
-- Name: user_action_logs_i_1; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX user_action_logs_i_1 ON public.user_action_logs USING btree (insert_instant);


--
-- Name: user_action_logs_i_2; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX user_action_logs_i_2 ON public.user_action_logs USING btree (expiry, end_event_sent);


--
-- Name: user_comments_i_1; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX user_comments_i_1 ON public.user_comments USING btree (insert_instant);


--
-- Name: user_comments_i_2; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX user_comments_i_2 ON public.user_comments USING btree (users_id);


--
-- Name: user_comments_i_3; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX user_comments_i_3 ON public.user_comments USING btree (commenter_id);


--
-- Name: user_consents_email_plus_i_1; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX user_consents_email_plus_i_1 ON public.user_consents_email_plus USING btree (next_email_instant);


--
-- Name: user_registrations_i_1; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX user_registrations_i_1 ON public.user_registrations USING btree (clean_speak_id);


--
-- Name: user_registrations_i_2; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX user_registrations_i_2 ON public.user_registrations USING btree (users_id);


--
-- Name: users_i_1; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX users_i_1 ON public.users USING btree (clean_speak_id);


--
-- Name: users_i_2; Type: INDEX; Schema: public; Owner: apprise
--

CREATE INDEX users_i_2 ON public.users USING btree (parent_email);


--
-- Name: application_daily_active_users application_daily_active_users_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.application_daily_active_users
    ADD CONSTRAINT application_daily_active_users_fk_1 FOREIGN KEY (applications_id) REFERENCES public.applications(id);


--
-- Name: application_monthly_active_users application_monthly_active_users_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.application_monthly_active_users
    ADD CONSTRAINT application_monthly_active_users_fk_1 FOREIGN KEY (applications_id) REFERENCES public.applications(id);


--
-- Name: application_registration_counts application_registration_counts_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.application_registration_counts
    ADD CONSTRAINT application_registration_counts_fk_1 FOREIGN KEY (applications_id) REFERENCES public.applications(id);


--
-- Name: application_roles application_roles_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.application_roles
    ADD CONSTRAINT application_roles_fk_1 FOREIGN KEY (applications_id) REFERENCES public.applications(id);


--
-- Name: applications applications_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_1 FOREIGN KEY (verification_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: applications applications_fk_10; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_10 FOREIGN KEY (email_verification_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: applications applications_fk_11; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_11 FOREIGN KEY (forgot_password_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: applications applications_fk_12; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_12 FOREIGN KEY (passwordless_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: applications applications_fk_13; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_13 FOREIGN KEY (set_password_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: applications applications_fk_14; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_14 FOREIGN KEY (samlv2_default_verification_keys_id) REFERENCES public.keys(id);


--
-- Name: applications applications_fk_15; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_15 FOREIGN KEY (admin_registration_forms_id) REFERENCES public.forms(id);


--
-- Name: applications applications_fk_16; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_16 FOREIGN KEY (samlv2_logout_keys_id) REFERENCES public.keys(id);


--
-- Name: applications applications_fk_17; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_17 FOREIGN KEY (samlv2_logout_default_verification_keys_id) REFERENCES public.keys(id);


--
-- Name: applications applications_fk_18; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_18 FOREIGN KEY (samlv2_single_logout_keys_id) REFERENCES public.keys(id);


--
-- Name: applications applications_fk_19; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_19 FOREIGN KEY (multi_factor_email_message_templates_id) REFERENCES public.email_templates(id);


--
-- Name: applications applications_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_2 FOREIGN KEY (tenants_id) REFERENCES public.tenants(id);


--
-- Name: applications applications_fk_20; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_20 FOREIGN KEY (multi_factor_sms_message_templates_id) REFERENCES public.message_templates(id);


--
-- Name: applications applications_fk_21; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_21 FOREIGN KEY (self_service_user_forms_id) REFERENCES public.forms(id);


--
-- Name: applications applications_fk_22; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_22 FOREIGN KEY (themes_id) REFERENCES public.themes(id);


--
-- Name: applications applications_fk_23; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_23 FOREIGN KEY (email_verified_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: applications applications_fk_24; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_24 FOREIGN KEY (login_new_device_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: applications applications_fk_25; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_25 FOREIGN KEY (login_suspicious_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: applications applications_fk_26; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_26 FOREIGN KEY (password_reset_success_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: applications applications_fk_27; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_27 FOREIGN KEY (ui_ip_access_control_lists_id) REFERENCES public.ip_access_control_lists(id);


--
-- Name: applications applications_fk_28; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_28 FOREIGN KEY (email_update_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: applications applications_fk_29; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_29 FOREIGN KEY (login_id_in_use_on_create_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: applications applications_fk_3; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_3 FOREIGN KEY (access_token_populate_lambdas_id) REFERENCES public.lambdas(id);


--
-- Name: applications applications_fk_30; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_30 FOREIGN KEY (login_id_in_use_on_update_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: applications applications_fk_31; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_31 FOREIGN KEY (password_update_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: applications applications_fk_32; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_32 FOREIGN KEY (two_factor_method_add_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: applications applications_fk_33; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_33 FOREIGN KEY (two_factor_method_remove_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: applications applications_fk_4; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_4 FOREIGN KEY (id_token_populate_lambdas_id) REFERENCES public.lambdas(id);


--
-- Name: applications applications_fk_5; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_5 FOREIGN KEY (samlv2_keys_id) REFERENCES public.keys(id);


--
-- Name: applications applications_fk_6; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_6 FOREIGN KEY (samlv2_populate_lambdas_id) REFERENCES public.lambdas(id);


--
-- Name: applications applications_fk_7; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_7 FOREIGN KEY (access_token_signing_keys_id) REFERENCES public.keys(id);


--
-- Name: applications applications_fk_8; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_8 FOREIGN KEY (id_token_signing_keys_id) REFERENCES public.keys(id);


--
-- Name: applications applications_fk_9; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_fk_9 FOREIGN KEY (forms_id) REFERENCES public.forms(id);


--
-- Name: asynchronous_tasks asynchronous_tasks_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.asynchronous_tasks
    ADD CONSTRAINT asynchronous_tasks_fk_1 FOREIGN KEY (nodes_id) REFERENCES public.nodes(id);


--
-- Name: authentication_keys authentication_keys_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.authentication_keys
    ADD CONSTRAINT authentication_keys_fk_1 FOREIGN KEY (tenants_id) REFERENCES public.tenants(id);


--
-- Name: authentication_keys authentication_keys_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.authentication_keys
    ADD CONSTRAINT authentication_keys_fk_2 FOREIGN KEY (ip_access_control_lists_id) REFERENCES public.ip_access_control_lists(id);


--
-- Name: breached_password_metrics breached_password_metrics_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.breached_password_metrics
    ADD CONSTRAINT breached_password_metrics_fk_1 FOREIGN KEY (tenants_id) REFERENCES public.tenants(id);


--
-- Name: clean_speak_applications clean_speak_applications_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.clean_speak_applications
    ADD CONSTRAINT clean_speak_applications_fk_1 FOREIGN KEY (applications_id) REFERENCES public.applications(id);


--
-- Name: connectors connectors_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.connectors
    ADD CONSTRAINT connectors_fk_1 FOREIGN KEY (ssl_certificate_keys_id) REFERENCES public.keys(id);


--
-- Name: connectors connectors_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.connectors
    ADD CONSTRAINT connectors_fk_2 FOREIGN KEY (reconcile_lambdas_id) REFERENCES public.lambdas(id);


--
-- Name: connectors_tenants connectors_tenants_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.connectors_tenants
    ADD CONSTRAINT connectors_tenants_fk_1 FOREIGN KEY (connectors_id) REFERENCES public.connectors(id);


--
-- Name: connectors_tenants connectors_tenants_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.connectors_tenants
    ADD CONSTRAINT connectors_tenants_fk_2 FOREIGN KEY (tenants_id) REFERENCES public.tenants(id);


--
-- Name: consents consents_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.consents
    ADD CONSTRAINT consents_fk_1 FOREIGN KEY (consent_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: consents consents_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.consents
    ADD CONSTRAINT consents_fk_2 FOREIGN KEY (email_plus_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: entities entities_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entities
    ADD CONSTRAINT entities_fk_1 FOREIGN KEY (entity_types_id) REFERENCES public.entity_types(id) ON DELETE CASCADE;


--
-- Name: entities entities_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entities
    ADD CONSTRAINT entities_fk_2 FOREIGN KEY (parent_id) REFERENCES public.entities(id) ON DELETE CASCADE;


--
-- Name: entities entities_fk_3; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entities
    ADD CONSTRAINT entities_fk_3 FOREIGN KEY (tenants_id) REFERENCES public.tenants(id) ON DELETE CASCADE;


--
-- Name: entity_entity_grant_permissions entity_entity_grant_permissions_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_entity_grant_permissions
    ADD CONSTRAINT entity_entity_grant_permissions_fk_1 FOREIGN KEY (entity_entity_grants_id) REFERENCES public.entity_entity_grants(id) ON DELETE CASCADE;


--
-- Name: entity_entity_grant_permissions entity_entity_grant_permissions_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_entity_grant_permissions
    ADD CONSTRAINT entity_entity_grant_permissions_fk_2 FOREIGN KEY (entity_type_permissions_id) REFERENCES public.entity_type_permissions(id) ON DELETE CASCADE;


--
-- Name: entity_entity_grants entity_entity_grants_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_entity_grants
    ADD CONSTRAINT entity_entity_grants_fk_1 FOREIGN KEY (recipient_id) REFERENCES public.entities(id) ON DELETE CASCADE;


--
-- Name: entity_entity_grants entity_entity_grants_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_entity_grants
    ADD CONSTRAINT entity_entity_grants_fk_2 FOREIGN KEY (target_id) REFERENCES public.entities(id) ON DELETE CASCADE;


--
-- Name: entity_type_permissions entity_type_permissions_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_type_permissions
    ADD CONSTRAINT entity_type_permissions_fk_1 FOREIGN KEY (entity_types_id) REFERENCES public.entity_types(id) ON DELETE CASCADE;


--
-- Name: entity_types entity_types_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_types
    ADD CONSTRAINT entity_types_fk_1 FOREIGN KEY (access_token_signing_keys_id) REFERENCES public.keys(id);


--
-- Name: entity_user_grant_permissions entity_user_grant_permissions_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_user_grant_permissions
    ADD CONSTRAINT entity_user_grant_permissions_fk_1 FOREIGN KEY (entity_user_grants_id) REFERENCES public.entity_user_grants(id) ON DELETE CASCADE;


--
-- Name: entity_user_grant_permissions entity_user_grant_permissions_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_user_grant_permissions
    ADD CONSTRAINT entity_user_grant_permissions_fk_2 FOREIGN KEY (entity_type_permissions_id) REFERENCES public.entity_type_permissions(id) ON DELETE CASCADE;


--
-- Name: entity_user_grants entity_user_grants_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_user_grants
    ADD CONSTRAINT entity_user_grants_fk_1 FOREIGN KEY (entities_id) REFERENCES public.entities(id) ON DELETE CASCADE;


--
-- Name: entity_user_grants entity_user_grants_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.entity_user_grants
    ADD CONSTRAINT entity_user_grants_fk_2 FOREIGN KEY (users_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: external_identifiers external_identifiers_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.external_identifiers
    ADD CONSTRAINT external_identifiers_fk_1 FOREIGN KEY (users_id) REFERENCES public.users(id);


--
-- Name: external_identifiers external_identifiers_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.external_identifiers
    ADD CONSTRAINT external_identifiers_fk_2 FOREIGN KEY (applications_id) REFERENCES public.applications(id);


--
-- Name: external_identifiers external_identifiers_fk_3; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.external_identifiers
    ADD CONSTRAINT external_identifiers_fk_3 FOREIGN KEY (tenants_id) REFERENCES public.tenants(id);


--
-- Name: families families_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.families
    ADD CONSTRAINT families_fk_1 FOREIGN KEY (users_id) REFERENCES public.users(id);


--
-- Name: federated_domains federated_domains_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.federated_domains
    ADD CONSTRAINT federated_domains_fk_1 FOREIGN KEY (identity_providers_id) REFERENCES public.identity_providers(id);


--
-- Name: form_fields form_fields_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.form_fields
    ADD CONSTRAINT form_fields_fk_1 FOREIGN KEY (consents_id) REFERENCES public.consents(id);


--
-- Name: form_steps form_steps_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.form_steps
    ADD CONSTRAINT form_steps_fk_1 FOREIGN KEY (forms_id) REFERENCES public.forms(id);


--
-- Name: form_steps form_steps_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.form_steps
    ADD CONSTRAINT form_steps_fk_2 FOREIGN KEY (form_fields_id) REFERENCES public.form_fields(id);


--
-- Name: group_application_roles group_application_roles_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.group_application_roles
    ADD CONSTRAINT group_application_roles_fk_1 FOREIGN KEY (groups_id) REFERENCES public.groups(id);


--
-- Name: group_application_roles group_application_roles_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.group_application_roles
    ADD CONSTRAINT group_application_roles_fk_2 FOREIGN KEY (application_roles_id) REFERENCES public.application_roles(id);


--
-- Name: group_members group_members_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.group_members
    ADD CONSTRAINT group_members_fk_1 FOREIGN KEY (users_id) REFERENCES public.users(id);


--
-- Name: group_members group_members_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.group_members
    ADD CONSTRAINT group_members_fk_2 FOREIGN KEY (groups_id) REFERENCES public.groups(id);


--
-- Name: groups groups_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_fk_1 FOREIGN KEY (tenants_id) REFERENCES public.tenants(id);


--
-- Name: hourly_logins hourly_logins_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.hourly_logins
    ADD CONSTRAINT hourly_logins_fk_1 FOREIGN KEY (applications_id) REFERENCES public.applications(id);


--
-- Name: identities identities_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identities
    ADD CONSTRAINT identities_fk_1 FOREIGN KEY (tenants_id) REFERENCES public.tenants(id);


--
-- Name: identities identities_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identities
    ADD CONSTRAINT identities_fk_2 FOREIGN KEY (users_id) REFERENCES public.users(id);


--
-- Name: identity_provider_links identity_provider_links_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identity_provider_links
    ADD CONSTRAINT identity_provider_links_fk_1 FOREIGN KEY (identity_providers_id) REFERENCES public.identity_providers(id);


--
-- Name: identity_provider_links identity_provider_links_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identity_provider_links
    ADD CONSTRAINT identity_provider_links_fk_2 FOREIGN KEY (tenants_id) REFERENCES public.tenants(id);


--
-- Name: identity_provider_links identity_provider_links_fk_3; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identity_provider_links
    ADD CONSTRAINT identity_provider_links_fk_3 FOREIGN KEY (users_id) REFERENCES public.users(id);


--
-- Name: identity_providers_applications identity_providers_applications_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identity_providers_applications
    ADD CONSTRAINT identity_providers_applications_fk_1 FOREIGN KEY (applications_id) REFERENCES public.applications(id);


--
-- Name: identity_providers_applications identity_providers_applications_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identity_providers_applications
    ADD CONSTRAINT identity_providers_applications_fk_2 FOREIGN KEY (identity_providers_id) REFERENCES public.identity_providers(id);


--
-- Name: identity_providers_applications identity_providers_applications_fk_3; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identity_providers_applications
    ADD CONSTRAINT identity_providers_applications_fk_3 FOREIGN KEY (keys_id) REFERENCES public.keys(id);


--
-- Name: identity_providers identity_providers_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identity_providers
    ADD CONSTRAINT identity_providers_fk_1 FOREIGN KEY (keys_id) REFERENCES public.keys(id);


--
-- Name: identity_providers identity_providers_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identity_providers
    ADD CONSTRAINT identity_providers_fk_2 FOREIGN KEY (reconcile_lambdas_id) REFERENCES public.lambdas(id);


--
-- Name: identity_providers identity_providers_fk_3; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identity_providers
    ADD CONSTRAINT identity_providers_fk_3 FOREIGN KEY (request_signing_keys_id) REFERENCES public.keys(id);


--
-- Name: identity_providers_tenants identity_providers_tenants_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identity_providers_tenants
    ADD CONSTRAINT identity_providers_tenants_fk_1 FOREIGN KEY (tenants_id) REFERENCES public.tenants(id);


--
-- Name: identity_providers_tenants identity_providers_tenants_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.identity_providers_tenants
    ADD CONSTRAINT identity_providers_tenants_fk_2 FOREIGN KEY (identity_providers_id) REFERENCES public.identity_providers(id);


--
-- Name: previous_passwords previous_passwords_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.previous_passwords
    ADD CONSTRAINT previous_passwords_fk_1 FOREIGN KEY (users_id) REFERENCES public.users(id);


--
-- Name: raw_logins raw_logins_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.raw_logins
    ADD CONSTRAINT raw_logins_fk_1 FOREIGN KEY (applications_id) REFERENCES public.applications(id);


--
-- Name: raw_logins raw_logins_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.raw_logins
    ADD CONSTRAINT raw_logins_fk_2 FOREIGN KEY (users_id) REFERENCES public.users(id);


--
-- Name: refresh_tokens refresh_tokens_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.refresh_tokens
    ADD CONSTRAINT refresh_tokens_fk_1 FOREIGN KEY (users_id) REFERENCES public.users(id);


--
-- Name: refresh_tokens refresh_tokens_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.refresh_tokens
    ADD CONSTRAINT refresh_tokens_fk_2 FOREIGN KEY (applications_id) REFERENCES public.applications(id);


--
-- Name: refresh_tokens refresh_tokens_fk_3; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.refresh_tokens
    ADD CONSTRAINT refresh_tokens_fk_3 FOREIGN KEY (tenants_id) REFERENCES public.tenants(id);


--
-- Name: request_frequencies request_frequencies_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.request_frequencies
    ADD CONSTRAINT request_frequencies_fk_1 FOREIGN KEY (tenants_id) REFERENCES public.tenants(id);


--
-- Name: tenants tenants_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_1 FOREIGN KEY (forgot_password_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_10; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_10 FOREIGN KEY (access_token_signing_keys_id) REFERENCES public.keys(id);


--
-- Name: tenants tenants_fk_11; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_11 FOREIGN KEY (id_token_signing_keys_id) REFERENCES public.keys(id);


--
-- Name: tenants tenants_fk_12; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_12 FOREIGN KEY (admin_user_forms_id) REFERENCES public.forms(id);


--
-- Name: tenants tenants_fk_13; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_13 FOREIGN KEY (multi_factor_email_message_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_14; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_14 FOREIGN KEY (multi_factor_sms_message_templates_id) REFERENCES public.message_templates(id);


--
-- Name: tenants tenants_fk_15; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_15 FOREIGN KEY (multi_factor_sms_messengers_id) REFERENCES public.messengers(id);


--
-- Name: tenants tenants_fk_16; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_16 FOREIGN KEY (client_credentials_access_token_populate_lambdas_id) REFERENCES public.lambdas(id);


--
-- Name: tenants tenants_fk_17; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_17 FOREIGN KEY (email_update_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_18; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_18 FOREIGN KEY (email_verified_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_19; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_19 FOREIGN KEY (login_id_in_use_on_create_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_2 FOREIGN KEY (set_password_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_20; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_20 FOREIGN KEY (login_id_in_use_on_update_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_21; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_21 FOREIGN KEY (login_new_device_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_22; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_22 FOREIGN KEY (login_suspicious_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_23; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_23 FOREIGN KEY (password_reset_success_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_24; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_24 FOREIGN KEY (password_update_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_25; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_25 FOREIGN KEY (two_factor_method_add_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_26; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_26 FOREIGN KEY (two_factor_method_remove_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_27; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_27 FOREIGN KEY (ui_ip_access_control_lists_id) REFERENCES public.ip_access_control_lists(id);


--
-- Name: tenants tenants_fk_3; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_3 FOREIGN KEY (verification_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_4; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_4 FOREIGN KEY (passwordless_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_5; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_5 FOREIGN KEY (confirm_child_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_6; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_6 FOREIGN KEY (family_request_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_7; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_7 FOREIGN KEY (parent_registration_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: tenants tenants_fk_8; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_8 FOREIGN KEY (failed_authentication_user_actions_id) REFERENCES public.user_actions(id);


--
-- Name: tenants tenants_fk_9; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.tenants
    ADD CONSTRAINT tenants_fk_9 FOREIGN KEY (themes_id) REFERENCES public.themes(id);


--
-- Name: user_action_logs_applications user_action_logs_applications_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_action_logs_applications
    ADD CONSTRAINT user_action_logs_applications_fk_1 FOREIGN KEY (applications_id) REFERENCES public.applications(id) ON DELETE CASCADE;


--
-- Name: user_action_logs_applications user_action_logs_applications_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_action_logs_applications
    ADD CONSTRAINT user_action_logs_applications_fk_2 FOREIGN KEY (user_action_logs_id) REFERENCES public.user_action_logs(id) ON DELETE CASCADE;


--
-- Name: user_action_logs user_action_logs_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_action_logs
    ADD CONSTRAINT user_action_logs_fk_1 FOREIGN KEY (actioner_users_id) REFERENCES public.users(id);


--
-- Name: user_action_logs user_action_logs_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_action_logs
    ADD CONSTRAINT user_action_logs_fk_2 FOREIGN KEY (actionee_users_id) REFERENCES public.users(id);


--
-- Name: user_action_logs user_action_logs_fk_3; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_action_logs
    ADD CONSTRAINT user_action_logs_fk_3 FOREIGN KEY (user_actions_id) REFERENCES public.user_actions(id);


--
-- Name: user_actions user_actions_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_actions
    ADD CONSTRAINT user_actions_fk_1 FOREIGN KEY (cancel_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: user_actions user_actions_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_actions
    ADD CONSTRAINT user_actions_fk_2 FOREIGN KEY (end_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: user_actions user_actions_fk_3; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_actions
    ADD CONSTRAINT user_actions_fk_3 FOREIGN KEY (modify_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: user_actions user_actions_fk_4; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_actions
    ADD CONSTRAINT user_actions_fk_4 FOREIGN KEY (start_email_templates_id) REFERENCES public.email_templates(id);


--
-- Name: user_comments user_comments_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_comments
    ADD CONSTRAINT user_comments_fk_1 FOREIGN KEY (users_id) REFERENCES public.users(id);


--
-- Name: user_comments user_comments_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_comments
    ADD CONSTRAINT user_comments_fk_2 FOREIGN KEY (commenter_id) REFERENCES public.users(id);


--
-- Name: user_consents_email_plus user_consents_email_plus_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_consents_email_plus
    ADD CONSTRAINT user_consents_email_plus_fk_1 FOREIGN KEY (user_consents_id) REFERENCES public.user_consents(id);


--
-- Name: user_consents user_consents_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_consents
    ADD CONSTRAINT user_consents_fk_1 FOREIGN KEY (consents_id) REFERENCES public.consents(id);


--
-- Name: user_consents user_consents_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_consents
    ADD CONSTRAINT user_consents_fk_2 FOREIGN KEY (giver_users_id) REFERENCES public.users(id);


--
-- Name: user_consents user_consents_fk_3; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_consents
    ADD CONSTRAINT user_consents_fk_3 FOREIGN KEY (users_id) REFERENCES public.users(id);


--
-- Name: user_registrations_application_roles user_registrations_application_roles_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_registrations_application_roles
    ADD CONSTRAINT user_registrations_application_roles_fk_1 FOREIGN KEY (user_registrations_id) REFERENCES public.user_registrations(id);


--
-- Name: user_registrations_application_roles user_registrations_application_roles_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_registrations_application_roles
    ADD CONSTRAINT user_registrations_application_roles_fk_2 FOREIGN KEY (application_roles_id) REFERENCES public.application_roles(id);


--
-- Name: user_registrations user_registrations_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_registrations
    ADD CONSTRAINT user_registrations_fk_1 FOREIGN KEY (applications_id) REFERENCES public.applications(id);


--
-- Name: user_registrations user_registrations_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.user_registrations
    ADD CONSTRAINT user_registrations_fk_2 FOREIGN KEY (users_id) REFERENCES public.users(id);


--
-- Name: users users_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_fk_1 FOREIGN KEY (tenants_id) REFERENCES public.tenants(id);


--
-- Name: webhooks_applications webhooks_applications_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.webhooks_applications
    ADD CONSTRAINT webhooks_applications_fk_1 FOREIGN KEY (webhooks_id) REFERENCES public.webhooks(id);


--
-- Name: webhooks_applications webhooks_applications_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: apprise
--

ALTER TABLE ONLY public.webhooks_applications
    ADD CONSTRAINT webhooks_applications_fk_2 FOREIGN KEY (applications_id) REFERENCES public.applications(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: apprise
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

